<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $mail;
    public $rememberMe = true;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Email',
            'password' => 'Password',
            'rememberMe' => 'Remember me',
            'password' => 'Password',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    protected function getUser()
    {
        $authUser = explode("@", $this->username)[0];
        if ($this->_user === null) {
            $this->_user = User::findByUsername($authUser);
        }
        return $this->_user;
    }

    public function getIsInLocal()
    {
        $user =  $this->getUser();
        if (is_null($user)) {
            return false;
        } else {
            return $user->id;
        }
    }

    public function getMail()
    {
        $domainValue = Yii::$app->params['ldap']['domain'];
        $authUser = explode("@", $this->username)[0];
        $this->mail = $authUser . "@" . strtolower($domainValue);
        return $this->mail;
    }

    public function getUserName()
    {
        return explode("@", $this->username)[0];
    }

    public function getIsInLdap()
    {
        $authUser = explode("@", $this->username)[0];
        $hostValue = Yii::$app->params['ldap']['host'];
        $portValue = Yii::$app->params['ldap']['port'];
        $domainValue = Yii::$app->params['ldap']['domain'];
        $this->mail = $authUser . "@" . strtolower($domainValue);

        try {
            $ldapConn = ldap_connect($hostValue, $portValue) or die("Could not connect to LDAP server.");
        } catch (\Throwable $th) {
            $ldapConn = false;
        }

        if ($ldapConn) {

            try {
                ldap_set_option($ldapConn, LDAP_OPT_REFERRALS, 0);
                ldap_set_option($ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
                $ldapBind = ldap_bind($ldapConn,  $this->mail, $this->password);
            } catch (\Throwable $th) {
                $ldapBind = false;
            }
            return  $ldapBind;
        }
    }
}
