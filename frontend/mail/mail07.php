<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<span>
  <h3>
    Results ready for verification: <?= $code ?>
  </h3>
</span>
To see this order click on:
<?= Html::a('REQUEST ' . $code,  str_replace("site/home", "business/request/manage-request&requestId=", (string) (Url::home('http'))) . '?r=business/request/manage-request&requestId=' . $id);
?>