<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<span>
  <h3>A new request has been generated, please enter the link to review.</h3>
</span>
<?= Html::a('REQUEST ' . $code,  str_replace("site/home", "business/request/manage-request&requestId=", (string) (Url::home('http'))) . '?r=business/request/manage-request&requestId=' . $id);
?>