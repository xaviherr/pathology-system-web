<?php
/**
 * File for class MELOCSWSStructGetTemplatePropertiesResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplatePropertiesResponse originally named GetTemplatePropertiesResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplatePropertiesResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetTemplatePropertiesResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateProperties
     */
    public $GetTemplatePropertiesResult;
    /**
     * Constructor method for GetTemplatePropertiesResponse
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateProperties $_getTemplatePropertiesResult
     * @return MELOCSWSStructGetTemplatePropertiesResponse
     */
    public function __construct($_getTemplatePropertiesResult = NULL)
    {
        parent::__construct(array('GetTemplatePropertiesResult'=>$_getTemplatePropertiesResult),false);
    }
    /**
     * Get GetTemplatePropertiesResult value
     * @return MELOCSWSStructTemplateProperties|null
     */
    public function getGetTemplatePropertiesResult()
    {
        return $this->GetTemplatePropertiesResult;
    }
    /**
     * Set GetTemplatePropertiesResult value
     * @param MELOCSWSStructTemplateProperties $_getTemplatePropertiesResult the GetTemplatePropertiesResult
     * @return MELOCSWSStructTemplateProperties
     */
    public function setGetTemplatePropertiesResult($_getTemplatePropertiesResult)
    {
        return ($this->GetTemplatePropertiesResult = $_getTemplatePropertiesResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplatePropertiesResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
