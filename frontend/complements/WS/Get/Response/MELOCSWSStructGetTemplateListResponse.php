<?php
/**
 * File for class MELOCSWSStructGetTemplateListResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateListResponse originally named GetTemplateListResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateListResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetTemplateListResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateList
     */
    public $GetTemplateListResult;
    /**
     * Constructor method for GetTemplateListResponse
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateList $_getTemplateListResult
     * @return MELOCSWSStructGetTemplateListResponse
     */
    public function __construct($_getTemplateListResult = NULL)
    {
        parent::__construct(array('GetTemplateListResult'=>$_getTemplateListResult),false);
    }
    /**
     * Get GetTemplateListResult value
     * @return MELOCSWSStructTemplateList|null
     */
    public function getGetTemplateListResult()
    {
        return $this->GetTemplateListResult;
    }
    /**
     * Set GetTemplateListResult value
     * @param MELOCSWSStructTemplateList $_getTemplateListResult the GetTemplateListResult
     * @return MELOCSWSStructTemplateList
     */
    public function setGetTemplateListResult($_getTemplateListResult)
    {
        return ($this->GetTemplateListResult = $_getTemplateListResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateListResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
