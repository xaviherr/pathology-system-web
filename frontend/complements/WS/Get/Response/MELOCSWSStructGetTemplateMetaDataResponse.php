<?php
/**
 * File for class MELOCSWSStructGetTemplateMetaDataResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateMetaDataResponse originally named GetTemplateMetaDataResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateMetaDataResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetTemplateMetaDataResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateMetaData
     */
    public $GetTemplateMetaDataResult;
    /**
     * Constructor method for GetTemplateMetaDataResponse
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateMetaData $_getTemplateMetaDataResult
     * @return MELOCSWSStructGetTemplateMetaDataResponse
     */
    public function __construct($_getTemplateMetaDataResult = NULL)
    {
        parent::__construct(array('GetTemplateMetaDataResult'=>$_getTemplateMetaDataResult),false);
    }
    /**
     * Get GetTemplateMetaDataResult value
     * @return MELOCSWSStructTemplateMetaData|null
     */
    public function getGetTemplateMetaDataResult()
    {
        return $this->GetTemplateMetaDataResult;
    }
    /**
     * Set GetTemplateMetaDataResult value
     * @param MELOCSWSStructTemplateMetaData $_getTemplateMetaDataResult the GetTemplateMetaDataResult
     * @return MELOCSWSStructTemplateMetaData
     */
    public function setGetTemplateMetaDataResult($_getTemplateMetaDataResult)
    {
        return ($this->GetTemplateMetaDataResult = $_getTemplateMetaDataResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateMetaDataResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
