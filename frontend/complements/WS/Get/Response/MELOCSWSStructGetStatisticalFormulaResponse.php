<?php
/**
 * File for class MELOCSWSStructGetStatisticalFormulaResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetStatisticalFormulaResponse originally named GetStatisticalFormulaResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetStatisticalFormulaResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetStatisticalFormulaResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructStatisticalFormula
     */
    public $GetStatisticalFormulaResult;
    /**
     * Constructor method for GetStatisticalFormulaResponse
     * @see parent::__construct()
     * @param MELOCSWSStructStatisticalFormula $_getStatisticalFormulaResult
     * @return MELOCSWSStructGetStatisticalFormulaResponse
     */
    public function __construct($_getStatisticalFormulaResult = NULL)
    {
        parent::__construct(array('GetStatisticalFormulaResult'=>$_getStatisticalFormulaResult),false);
    }
    /**
     * Get GetStatisticalFormulaResult value
     * @return MELOCSWSStructStatisticalFormula|null
     */
    public function getGetStatisticalFormulaResult()
    {
        return $this->GetStatisticalFormulaResult;
    }
    /**
     * Set GetStatisticalFormulaResult value
     * @param MELOCSWSStructStatisticalFormula $_getStatisticalFormulaResult the GetStatisticalFormulaResult
     * @return MELOCSWSStructStatisticalFormula
     */
    public function setGetStatisticalFormulaResult($_getStatisticalFormulaResult)
    {
        return ($this->GetStatisticalFormulaResult = $_getStatisticalFormulaResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetStatisticalFormulaResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
