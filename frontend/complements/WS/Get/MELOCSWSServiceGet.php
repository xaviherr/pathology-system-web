<?php
/**
 * File for class MELOCSWSServiceGet
 * @package MELOCSWS
 * @subpackage Services
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSServiceGet originally named Get
 * @package MELOCSWS
 * @subpackage Services
 * @date 2016-01-19
 */
class MELOCSWSServiceGet extends MELOCSWSWsdlClass
{
    /**
     * Method to call the operation originally named GetTemplateProperties
     * Documentation : Returns the properties associated with BRT
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetTemplateProperties $_mELOCSWSStructGetTemplateProperties
     * @return MELOCSWSStructGetTemplatePropertiesResponse
     */
    public function GetTemplateProperties(MELOCSWSStructGetTemplateProperties $_mELOCSWSStructGetTemplateProperties)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetTemplatePropertiesResponse(self::getSoapClient()->GetTemplateProperties($_mELOCSWSStructGetTemplateProperties)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetTemplateResultAsDataSet
     * Documentation : Returns the template result as a MS DataSet
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetTemplateResultAsDataSet $_mELOCSWSStructGetTemplateResultAsDataSet
     * @return MELOCSWSStructGetTemplateResultAsDataSetResponse
     */
    public function GetTemplateResultAsDataSet(MELOCSWSStructGetTemplateResultAsDataSet $_mELOCSWSStructGetTemplateResultAsDataSet)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetTemplateResultAsDataSetResponse(self::getSoapClient()->GetTemplateResultAsDataSet($_mELOCSWSStructGetTemplateResultAsDataSet)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetTemplateResultOptions
     * Documentation : Returns the options with default values, that are used to specify what output is wanted from GetTemplateResultAsDataSet
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetTemplateResultOptions $_mELOCSWSStructGetTemplateResultOptions
     * @return MELOCSWSStructGetTemplateResultOptionsResponse
     */
    public function GetTemplateResultOptions(MELOCSWSStructGetTemplateResultOptions $_mELOCSWSStructGetTemplateResultOptions)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetTemplateResultOptionsResponse(self::getSoapClient()->GetTemplateResultOptions($_mELOCSWSStructGetTemplateResultOptions)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetTemplateResultAsXML
     * Documentation : Returns the template result as a XML string
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetTemplateResultAsXML $_mELOCSWSStructGetTemplateResultAsXML
     * @return MELOCSWSStructGetTemplateResultAsXMLResponse
     */
    public function GetTemplateResultAsXML(MELOCSWSStructGetTemplateResultAsXML $_mELOCSWSStructGetTemplateResultAsXML)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetTemplateResultAsXMLResponse(self::getSoapClient()->GetTemplateResultAsXML($_mELOCSWSStructGetTemplateResultAsXML)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetSearchCriteria
     * Documentation : Returns all the searchable columns in the BRT
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetSearchCriteria $_mELOCSWSStructGetSearchCriteria
     * @return MELOCSWSStructGetSearchCriteriaResponse
     */
    public function GetSearchCriteria(MELOCSWSStructGetSearchCriteria $_mELOCSWSStructGetSearchCriteria)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetSearchCriteriaResponse(self::getSoapClient()->GetSearchCriteria($_mELOCSWSStructGetSearchCriteria)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetExpression
     * Documentation : Returns all expressions in the BRT
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetExpression $_mELOCSWSStructGetExpression
     * @return MELOCSWSStructGetExpressionResponse
     */
    public function GetExpression(MELOCSWSStructGetExpression $_mELOCSWSStructGetExpression)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetExpressionResponse(self::getSoapClient()->GetExpression($_mELOCSWSStructGetExpression)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetStatisticalFormula
     * Documentation : Returns all statistical formulas in the BRT
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetStatisticalFormula $_mELOCSWSStructGetStatisticalFormula
     * @return MELOCSWSStructGetStatisticalFormulaResponse
     */
    public function GetStatisticalFormula(MELOCSWSStructGetStatisticalFormula $_mELOCSWSStructGetStatisticalFormula)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetStatisticalFormulaResponse(self::getSoapClient()->GetStatisticalFormula($_mELOCSWSStructGetStatisticalFormula)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetFormatInfo
     * Documentation : Returns formatting information for a BRT as defined in the BRT definition
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetFormatInfo $_mELOCSWSStructGetFormatInfo
     * @return MELOCSWSStructGetFormatInfoResponse
     */
    public function GetFormatInfo(MELOCSWSStructGetFormatInfo $_mELOCSWSStructGetFormatInfo)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetFormatInfoResponse(self::getSoapClient()->GetFormatInfo($_mELOCSWSStructGetFormatInfo)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetTemplateList
     * Documentation : Returns all the available BRT's for the current user.
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetTemplateList $_mELOCSWSStructGetTemplateList
     * @return MELOCSWSStructGetTemplateListResponse
     */
    public function GetTemplateList(MELOCSWSStructGetTemplateList $_mELOCSWSStructGetTemplateList)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetTemplateListResponse(self::getSoapClient()->GetTemplateList($_mELOCSWSStructGetTemplateList)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetTemplateMetaData
     * Documentation : GetTemplateMetaData is a helper function that gets SearchCriteria, TemplatePropertiesand FormatInfo in one go, returned as a TemplateMetaData object. GetTemplateMetaData is very performance boosting, as QE WS only needs to be called one time to get all the values.
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructGetTemplateMetaData $_mELOCSWSStructGetTemplateMetaData
     * @return MELOCSWSStructGetTemplateMetaDataResponse
     */
    public function GetTemplateMetaData(MELOCSWSStructGetTemplateMetaData $_mELOCSWSStructGetTemplateMetaData)
    {
        try
        {
            return $this->setResult(new MELOCSWSStructGetTemplateMetaDataResponse(self::getSoapClient()->GetTemplateMetaData($_mELOCSWSStructGetTemplateMetaData)));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see MELOCSWSWsdlClass::getResult()
     * @return MELOCSWSStructGetExpressionResponse|MELOCSWSStructGetFormatInfoResponse|MELOCSWSStructGetSearchCriteriaResponse|MELOCSWSStructGetStatisticalFormulaResponse|MELOCSWSStructGetTemplateListResponse|MELOCSWSStructGetTemplateMetaDataResponse|MELOCSWSStructGetTemplatePropertiesResponse|MELOCSWSStructGetTemplateResultAsDataSetResponse|MELOCSWSStructGetTemplateResultAsXMLResponse|MELOCSWSStructGetTemplateResultOptionsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
