<?php
/**
 * File for class MELOCSWSStructGetTemplateList
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateList originally named GetTemplateList
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateList extends MELOCSWSWsdlClass
{
    /**
     * The formList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $formList;
    /**
     * The descrList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $descrList;
    /**
     * The credentials
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructWSCredentials
     */
    public $credentials;
    /**
     * Constructor method for GetTemplateList
     * @see parent::__construct()
     * @param string $_formList
     * @param string $_descrList
     * @param MELOCSWSStructWSCredentials $_credentials
     * @return MELOCSWSStructGetTemplateList
     */
    public function __construct($_formList = NULL,$_descrList = NULL,$_credentials = NULL)
    {
        parent::__construct(array('formList'=>$_formList,'descrList'=>$_descrList,'credentials'=>$_credentials),false);
    }
    /**
     * Get formList value
     * @return string|null
     */
    public function getFormList()
    {
        return $this->formList;
    }
    /**
     * Set formList value
     * @param string $_formList the formList
     * @return string
     */
    public function setFormList($_formList)
    {
        return ($this->formList = $_formList);
    }
    /**
     * Get descrList value
     * @return string|null
     */
    public function getDescrList()
    {
        return $this->descrList;
    }
    /**
     * Set descrList value
     * @param string $_descrList the descrList
     * @return string
     */
    public function setDescrList($_descrList)
    {
        return ($this->descrList = $_descrList);
    }
    /**
     * Get credentials value
     * @return MELOCSWSStructWSCredentials|null
     */
    public function getCredentials()
    {
        return $this->credentials;
    }
    /**
     * Set credentials value
     * @param MELOCSWSStructWSCredentials $_credentials the credentials
     * @return MELOCSWSStructWSCredentials
     */
    public function setCredentials($_credentials)
    {
        return ($this->credentials = $_credentials);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateList
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
