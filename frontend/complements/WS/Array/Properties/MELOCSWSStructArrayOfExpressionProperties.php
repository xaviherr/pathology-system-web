<?php
/**
 * File for class MELOCSWSStructArrayOfExpressionProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructArrayOfExpressionProperties originally named ArrayOfExpressionProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructArrayOfExpressionProperties extends MELOCSWSWsdlClass
{
    /**
     * The ExpressionProperties
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var MELOCSWSStructExpressionProperties
     */
    public $ExpressionProperties;
    /**
     * Constructor method for ArrayOfExpressionProperties
     * @see parent::__construct()
     * @param MELOCSWSStructExpressionProperties $_expressionProperties
     * @return MELOCSWSStructArrayOfExpressionProperties
     */
    public function __construct($_expressionProperties = NULL)
    {
        parent::__construct(array('ExpressionProperties'=>$_expressionProperties),false);
    }
    /**
     * Get ExpressionProperties value
     * @return MELOCSWSStructExpressionProperties|null
     */
    public function getExpressionProperties()
    {
        return $this->ExpressionProperties;
    }
    /**
     * Set ExpressionProperties value
     * @param MELOCSWSStructExpressionProperties $_expressionProperties the ExpressionProperties
     * @return MELOCSWSStructExpressionProperties
     */
    public function setExpressionProperties($_expressionProperties)
    {
        return ($this->ExpressionProperties = $_expressionProperties);
    }
    /**
     * Returns the current element
     * @see MELOCSWSWsdlClass::current()
     * @return MELOCSWSStructExpressionProperties
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see MELOCSWSWsdlClass::item()
     * @param int $_index
     * @return MELOCSWSStructExpressionProperties
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see MELOCSWSWsdlClass::first()
     * @return MELOCSWSStructExpressionProperties
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see MELOCSWSWsdlClass::last()
     * @return MELOCSWSStructExpressionProperties
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see MELOCSWSWsdlClass::last()
     * @param int $_offset
     * @return MELOCSWSStructExpressionProperties
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see MELOCSWSWsdlClass::getAttributeName()
     * @return string ExpressionProperties
     */
    public function getAttributeName()
    {
        return 'ExpressionProperties';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructArrayOfExpressionProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
