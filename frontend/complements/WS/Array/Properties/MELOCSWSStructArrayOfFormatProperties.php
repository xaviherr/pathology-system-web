<?php
/**
 * File for class MELOCSWSStructArrayOfFormatProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructArrayOfFormatProperties originally named ArrayOfFormatProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructArrayOfFormatProperties extends MELOCSWSWsdlClass
{
    /**
     * The FormatProperties
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var MELOCSWSStructFormatProperties
     */
    public $FormatProperties;
    /**
     * Constructor method for ArrayOfFormatProperties
     * @see parent::__construct()
     * @param MELOCSWSStructFormatProperties $_formatProperties
     * @return MELOCSWSStructArrayOfFormatProperties
     */
    public function __construct($_formatProperties = NULL)
    {
        parent::__construct(array('FormatProperties'=>$_formatProperties),false);
    }
    /**
     * Get FormatProperties value
     * @return MELOCSWSStructFormatProperties|null
     */
    public function getFormatProperties()
    {
        return $this->FormatProperties;
    }
    /**
     * Set FormatProperties value
     * @param MELOCSWSStructFormatProperties $_formatProperties the FormatProperties
     * @return MELOCSWSStructFormatProperties
     */
    public function setFormatProperties($_formatProperties)
    {
        return ($this->FormatProperties = $_formatProperties);
    }
    /**
     * Returns the current element
     * @see MELOCSWSWsdlClass::current()
     * @return MELOCSWSStructFormatProperties
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see MELOCSWSWsdlClass::item()
     * @param int $_index
     * @return MELOCSWSStructFormatProperties
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see MELOCSWSWsdlClass::first()
     * @return MELOCSWSStructFormatProperties
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see MELOCSWSWsdlClass::last()
     * @return MELOCSWSStructFormatProperties
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see MELOCSWSWsdlClass::last()
     * @param int $_offset
     * @return MELOCSWSStructFormatProperties
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see MELOCSWSWsdlClass::getAttributeName()
     * @return string FormatProperties
     */
    public function getAttributeName()
    {
        return 'FormatProperties';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructArrayOfFormatProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
