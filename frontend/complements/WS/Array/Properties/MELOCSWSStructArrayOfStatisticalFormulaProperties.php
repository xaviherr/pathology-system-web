<?php
/**
 * File for class MELOCSWSStructArrayOfStatisticalFormulaProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructArrayOfStatisticalFormulaProperties originally named ArrayOfStatisticalFormulaProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructArrayOfStatisticalFormulaProperties extends MELOCSWSWsdlClass
{
    /**
     * The StatisticalFormulaProperties
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var MELOCSWSStructStatisticalFormulaProperties
     */
    public $StatisticalFormulaProperties;
    /**
     * Constructor method for ArrayOfStatisticalFormulaProperties
     * @see parent::__construct()
     * @param MELOCSWSStructStatisticalFormulaProperties $_statisticalFormulaProperties
     * @return MELOCSWSStructArrayOfStatisticalFormulaProperties
     */
    public function __construct($_statisticalFormulaProperties = NULL)
    {
        parent::__construct(array('StatisticalFormulaProperties'=>$_statisticalFormulaProperties),false);
    }
    /**
     * Get StatisticalFormulaProperties value
     * @return MELOCSWSStructStatisticalFormulaProperties|null
     */
    public function getStatisticalFormulaProperties()
    {
        return $this->StatisticalFormulaProperties;
    }
    /**
     * Set StatisticalFormulaProperties value
     * @param MELOCSWSStructStatisticalFormulaProperties $_statisticalFormulaProperties the StatisticalFormulaProperties
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function setStatisticalFormulaProperties($_statisticalFormulaProperties)
    {
        return ($this->StatisticalFormulaProperties = $_statisticalFormulaProperties);
    }
    /**
     * Returns the current element
     * @see MELOCSWSWsdlClass::current()
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see MELOCSWSWsdlClass::item()
     * @param int $_index
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see MELOCSWSWsdlClass::first()
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see MELOCSWSWsdlClass::last()
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see MELOCSWSWsdlClass::last()
     * @param int $_offset
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see MELOCSWSWsdlClass::getAttributeName()
     * @return string StatisticalFormulaProperties
     */
    public function getAttributeName()
    {
        return 'StatisticalFormulaProperties';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructArrayOfStatisticalFormulaProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
