<?php
/**
 * File for class MELOCSWSStructArrayOfTemplateHeader
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructArrayOfTemplateHeader originally named ArrayOfTemplateHeader
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructArrayOfTemplateHeader extends MELOCSWSWsdlClass
{
    /**
     * The TemplateHeader
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var MELOCSWSStructTemplateHeader
     */
    public $TemplateHeader;
    /**
     * Constructor method for ArrayOfTemplateHeader
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateHeader $_templateHeader
     * @return MELOCSWSStructArrayOfTemplateHeader
     */
    public function __construct($_templateHeader = NULL)
    {
        parent::__construct(array('TemplateHeader'=>$_templateHeader),false);
    }
    /**
     * Get TemplateHeader value
     * @return MELOCSWSStructTemplateHeader|null
     */
    public function getTemplateHeader()
    {
        return $this->TemplateHeader;
    }
    /**
     * Set TemplateHeader value
     * @param MELOCSWSStructTemplateHeader $_templateHeader the TemplateHeader
     * @return MELOCSWSStructTemplateHeader
     */
    public function setTemplateHeader($_templateHeader)
    {
        return ($this->TemplateHeader = $_templateHeader);
    }
    /**
     * Returns the current element
     * @see MELOCSWSWsdlClass::current()
     * @return MELOCSWSStructTemplateHeader
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see MELOCSWSWsdlClass::item()
     * @param int $_index
     * @return MELOCSWSStructTemplateHeader
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see MELOCSWSWsdlClass::first()
     * @return MELOCSWSStructTemplateHeader
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see MELOCSWSWsdlClass::last()
     * @return MELOCSWSStructTemplateHeader
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see MELOCSWSWsdlClass::last()
     * @param int $_offset
     * @return MELOCSWSStructTemplateHeader
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see MELOCSWSWsdlClass::getAttributeName()
     * @return string TemplateHeader
     */
    public function getAttributeName()
    {
        return 'TemplateHeader';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructArrayOfTemplateHeader
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
