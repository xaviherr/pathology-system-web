<?php
/**
 * File for class MELOCSWSStructTemplateProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructTemplateProperties originally named TemplateProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructTemplateProperties extends MELOCSWSWsdlClass
{
    /**
     * The Aggregated
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $Aggregated;
    /**
     * The AutoFind
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $AutoFind;
    /**
     * The IsBrowseTable
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsBrowseTable;
    /**
     * The UseTable1
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $UseTable1;
    /**
     * The UseTable2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $UseTable2;
    /**
     * The UseTable3
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $UseTable3;
    /**
     * The DWPeriod
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DWPeriod;
    /**
     * The StartLevel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $StartLevel;
    /**
     * The MaxRows
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $MaxRows;
    /**
     * The TemplateId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $TemplateId;
    /**
     * The AggregateId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $AggregateId;
    /**
     * The ExtraTables
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ExtraTables;
    /**
     * The ExtraWhere
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ExtraWhere;
    /**
     * The Frame
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Frame;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Name;
    /**
     * The Table1
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Table1;
    /**
     * The Table2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Table2;
    /**
     * The Table3
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Table3;
    /**
     * The Version
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Version;
    /**
     * The VersionMinor
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $VersionMinor;
    /**
     * Constructor method for TemplateProperties
     * @see parent::__construct()
     * @param boolean $_aggregated
     * @param boolean $_autoFind
     * @param boolean $_isBrowseTable
     * @param boolean $_useTable1
     * @param boolean $_useTable2
     * @param boolean $_useTable3
     * @param int $_dWPeriod
     * @param int $_startLevel
     * @param long $_maxRows
     * @param long $_templateId
     * @param string $_aggregateId
     * @param string $_extraTables
     * @param string $_extraWhere
     * @param string $_frame
     * @param string $_name
     * @param string $_table1
     * @param string $_table2
     * @param string $_table3
     * @param string $_version
     * @param string $_versionMinor
     * @return MELOCSWSStructTemplateProperties
     */
    public function __construct($_aggregated,$_autoFind,$_isBrowseTable,$_useTable1,$_useTable2,$_useTable3,$_dWPeriod,$_startLevel,$_maxRows,$_templateId,$_aggregateId = NULL,$_extraTables = NULL,$_extraWhere = NULL,$_frame = NULL,$_name = NULL,$_table1 = NULL,$_table2 = NULL,$_table3 = NULL,$_version = NULL,$_versionMinor = NULL)
    {
        parent::__construct(array('Aggregated'=>$_aggregated,'AutoFind'=>$_autoFind,'IsBrowseTable'=>$_isBrowseTable,'UseTable1'=>$_useTable1,'UseTable2'=>$_useTable2,'UseTable3'=>$_useTable3,'DWPeriod'=>$_dWPeriod,'StartLevel'=>$_startLevel,'MaxRows'=>$_maxRows,'TemplateId'=>$_templateId,'AggregateId'=>$_aggregateId,'ExtraTables'=>$_extraTables,'ExtraWhere'=>$_extraWhere,'Frame'=>$_frame,'Name'=>$_name,'Table1'=>$_table1,'Table2'=>$_table2,'Table3'=>$_table3,'Version'=>$_version,'VersionMinor'=>$_versionMinor),false);
    }
    /**
     * Get Aggregated value
     * @return boolean
     */
    public function getAggregated()
    {
        return $this->Aggregated;
    }
    /**
     * Set Aggregated value
     * @param boolean $_aggregated the Aggregated
     * @return boolean
     */
    public function setAggregated($_aggregated)
    {
        return ($this->Aggregated = $_aggregated);
    }
    /**
     * Get AutoFind value
     * @return boolean
     */
    public function getAutoFind()
    {
        return $this->AutoFind;
    }
    /**
     * Set AutoFind value
     * @param boolean $_autoFind the AutoFind
     * @return boolean
     */
    public function setAutoFind($_autoFind)
    {
        return ($this->AutoFind = $_autoFind);
    }
    /**
     * Get IsBrowseTable value
     * @return boolean
     */
    public function getIsBrowseTable()
    {
        return $this->IsBrowseTable;
    }
    /**
     * Set IsBrowseTable value
     * @param boolean $_isBrowseTable the IsBrowseTable
     * @return boolean
     */
    public function setIsBrowseTable($_isBrowseTable)
    {
        return ($this->IsBrowseTable = $_isBrowseTable);
    }
    /**
     * Get UseTable1 value
     * @return boolean
     */
    public function getUseTable1()
    {
        return $this->UseTable1;
    }
    /**
     * Set UseTable1 value
     * @param boolean $_useTable1 the UseTable1
     * @return boolean
     */
    public function setUseTable1($_useTable1)
    {
        return ($this->UseTable1 = $_useTable1);
    }
    /**
     * Get UseTable2 value
     * @return boolean
     */
    public function getUseTable2()
    {
        return $this->UseTable2;
    }
    /**
     * Set UseTable2 value
     * @param boolean $_useTable2 the UseTable2
     * @return boolean
     */
    public function setUseTable2($_useTable2)
    {
        return ($this->UseTable2 = $_useTable2);
    }
    /**
     * Get UseTable3 value
     * @return boolean
     */
    public function getUseTable3()
    {
        return $this->UseTable3;
    }
    /**
     * Set UseTable3 value
     * @param boolean $_useTable3 the UseTable3
     * @return boolean
     */
    public function setUseTable3($_useTable3)
    {
        return ($this->UseTable3 = $_useTable3);
    }
    /**
     * Get DWPeriod value
     * @return int
     */
    public function getDWPeriod()
    {
        return $this->DWPeriod;
    }
    /**
     * Set DWPeriod value
     * @param int $_dWPeriod the DWPeriod
     * @return int
     */
    public function setDWPeriod($_dWPeriod)
    {
        return ($this->DWPeriod = $_dWPeriod);
    }
    /**
     * Get StartLevel value
     * @return int
     */
    public function getStartLevel()
    {
        return $this->StartLevel;
    }
    /**
     * Set StartLevel value
     * @param int $_startLevel the StartLevel
     * @return int
     */
    public function setStartLevel($_startLevel)
    {
        return ($this->StartLevel = $_startLevel);
    }
    /**
     * Get MaxRows value
     * @return long
     */
    public function getMaxRows()
    {
        return $this->MaxRows;
    }
    /**
     * Set MaxRows value
     * @param long $_maxRows the MaxRows
     * @return long
     */
    public function setMaxRows($_maxRows)
    {
        return ($this->MaxRows = $_maxRows);
    }
    /**
     * Get TemplateId value
     * @return long
     */
    public function getTemplateId()
    {
        return $this->TemplateId;
    }
    /**
     * Set TemplateId value
     * @param long $_templateId the TemplateId
     * @return long
     */
    public function setTemplateId($_templateId)
    {
        return ($this->TemplateId = $_templateId);
    }
    /**
     * Get AggregateId value
     * @return string|null
     */
    public function getAggregateId()
    {
        return $this->AggregateId;
    }
    /**
     * Set AggregateId value
     * @param string $_aggregateId the AggregateId
     * @return string
     */
    public function setAggregateId($_aggregateId)
    {
        return ($this->AggregateId = $_aggregateId);
    }
    /**
     * Get ExtraTables value
     * @return string|null
     */
    public function getExtraTables()
    {
        return $this->ExtraTables;
    }
    /**
     * Set ExtraTables value
     * @param string $_extraTables the ExtraTables
     * @return string
     */
    public function setExtraTables($_extraTables)
    {
        return ($this->ExtraTables = $_extraTables);
    }
    /**
     * Get ExtraWhere value
     * @return string|null
     */
    public function getExtraWhere()
    {
        return $this->ExtraWhere;
    }
    /**
     * Set ExtraWhere value
     * @param string $_extraWhere the ExtraWhere
     * @return string
     */
    public function setExtraWhere($_extraWhere)
    {
        return ($this->ExtraWhere = $_extraWhere);
    }
    /**
     * Get Frame value
     * @return string|null
     */
    public function getFrame()
    {
        return $this->Frame;
    }
    /**
     * Set Frame value
     * @param string $_frame the Frame
     * @return string
     */
    public function setFrame($_frame)
    {
        return ($this->Frame = $_frame);
    }
    /**
     * Get Name value
     * @return string|null
     */
    public function getName()
    {
        return $this->Name;
    }
    /**
     * Set Name value
     * @param string $_name the Name
     * @return string
     */
    public function setName($_name)
    {
        return ($this->Name = $_name);
    }
    /**
     * Get Table1 value
     * @return string|null
     */
    public function getTable1()
    {
        return $this->Table1;
    }
    /**
     * Set Table1 value
     * @param string $_table1 the Table1
     * @return string
     */
    public function setTable1($_table1)
    {
        return ($this->Table1 = $_table1);
    }
    /**
     * Get Table2 value
     * @return string|null
     */
    public function getTable2()
    {
        return $this->Table2;
    }
    /**
     * Set Table2 value
     * @param string $_table2 the Table2
     * @return string
     */
    public function setTable2($_table2)
    {
        return ($this->Table2 = $_table2);
    }
    /**
     * Get Table3 value
     * @return string|null
     */
    public function getTable3()
    {
        return $this->Table3;
    }
    /**
     * Set Table3 value
     * @param string $_table3 the Table3
     * @return string
     */
    public function setTable3($_table3)
    {
        return ($this->Table3 = $_table3);
    }
    /**
     * Get Version value
     * @return string|null
     */
    public function getVersion()
    {
        return $this->Version;
    }
    /**
     * Set Version value
     * @param string $_version the Version
     * @return string
     */
    public function setVersion($_version)
    {
        return ($this->Version = $_version);
    }
    /**
     * Get VersionMinor value
     * @return string|null
     */
    public function getVersionMinor()
    {
        return $this->VersionMinor;
    }
    /**
     * Set VersionMinor value
     * @param string $_versionMinor the VersionMinor
     * @return string
     */
    public function setVersionMinor($_versionMinor)
    {
        return ($this->VersionMinor = $_versionMinor);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructTemplateProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
