<?php
/**
 * File for class MELOCSWSStructTemplateResultAsXML
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructTemplateResultAsXML originally named TemplateResultAsXML
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructTemplateResultAsXML extends MELOCSWSWsdlClass
{
    /**
     * The ReturnCode
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $ReturnCode;
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Status;
    /**
     * The TemplateResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $TemplateResult;
    /**
     * Constructor method for TemplateResultAsXML
     * @see parent::__construct()
     * @param int $_returnCode
     * @param string $_status
     * @param string $_templateResult
     * @return MELOCSWSStructTemplateResultAsXML
     */
    public function __construct($_returnCode,$_status = NULL,$_templateResult = NULL)
    {
        parent::__construct(array('ReturnCode'=>$_returnCode,'Status'=>$_status,'TemplateResult'=>$_templateResult),false);
    }
    /**
     * Get ReturnCode value
     * @return int
     */
    public function getReturnCode()
    {
        return $this->ReturnCode;
    }
    /**
     * Set ReturnCode value
     * @param int $_returnCode the ReturnCode
     * @return int
     */
    public function setReturnCode($_returnCode)
    {
        return ($this->ReturnCode = $_returnCode);
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param string $_status the Status
     * @return string
     */
    public function setStatus($_status)
    {
        return ($this->Status = $_status);
    }
    /**
     * Get TemplateResult value
     * @return string|null
     */
    public function getTemplateResult()
    {
        return $this->TemplateResult;
    }
    /**
     * Set TemplateResult value
     * @param string $_templateResult the TemplateResult
     * @return string
     */
    public function setTemplateResult($_templateResult)
    {
        return ($this->TemplateResult = $_templateResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructTemplateResultAsXML
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
