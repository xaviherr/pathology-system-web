<?php
/**
 * File for class MELOCSWSStructStatisticalFormulaProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructStatisticalFormulaProperties originally named StatisticalFormulaProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructStatisticalFormulaProperties extends MELOCSWSWsdlClass
{
    /**
     * The ExcludeZero
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $ExcludeZero;
    /**
     * The Formula
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Formula;
    /**
     * The ColName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ColName;
    /**
     * The DisplayMethod
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $DisplayMethod;
    /**
     * The DisplayColumn
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $DisplayColumn;
    /**
     * Constructor method for StatisticalFormulaProperties
     * @see parent::__construct()
     * @param boolean $_excludeZero
     * @param string $_formula
     * @param string $_colName
     * @param string $_displayMethod
     * @param string $_displayColumn
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public function __construct($_excludeZero,$_formula = NULL,$_colName = NULL,$_displayMethod = NULL,$_displayColumn = NULL)
    {
        parent::__construct(array('ExcludeZero'=>$_excludeZero,'Formula'=>$_formula,'ColName'=>$_colName,'DisplayMethod'=>$_displayMethod,'DisplayColumn'=>$_displayColumn),false);
    }
    /**
     * Get ExcludeZero value
     * @return boolean
     */
    public function getExcludeZero()
    {
        return $this->ExcludeZero;
    }
    /**
     * Set ExcludeZero value
     * @param boolean $_excludeZero the ExcludeZero
     * @return boolean
     */
    public function setExcludeZero($_excludeZero)
    {
        return ($this->ExcludeZero = $_excludeZero);
    }
    /**
     * Get Formula value
     * @return string|null
     */
    public function getFormula()
    {
        return $this->Formula;
    }
    /**
     * Set Formula value
     * @param string $_formula the Formula
     * @return string
     */
    public function setFormula($_formula)
    {
        return ($this->Formula = $_formula);
    }
    /**
     * Get ColName value
     * @return string|null
     */
    public function getColName()
    {
        return $this->ColName;
    }
    /**
     * Set ColName value
     * @param string $_colName the ColName
     * @return string
     */
    public function setColName($_colName)
    {
        return ($this->ColName = $_colName);
    }
    /**
     * Get DisplayMethod value
     * @return string|null
     */
    public function getDisplayMethod()
    {
        return $this->DisplayMethod;
    }
    /**
     * Set DisplayMethod value
     * @param string $_displayMethod the DisplayMethod
     * @return string
     */
    public function setDisplayMethod($_displayMethod)
    {
        return ($this->DisplayMethod = $_displayMethod);
    }
    /**
     * Get DisplayColumn value
     * @return string|null
     */
    public function getDisplayColumn()
    {
        return $this->DisplayColumn;
    }
    /**
     * Set DisplayColumn value
     * @param string $_displayColumn the DisplayColumn
     * @return string
     */
    public function setDisplayColumn($_displayColumn)
    {
        return ($this->DisplayColumn = $_displayColumn);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructStatisticalFormulaProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
