<?php
/**
 * File for class MELOCSWSStructSearchCriteria
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructSearchCriteria originally named SearchCriteria
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructSearchCriteria extends MELOCSWSWsdlClass
{
    /**
     * The ReturnCode
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $ReturnCode;
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Status;
    /**
     * The SearchCriteriaPropertiesList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructArrayOfSearchCriteriaProperties
     */
    public $SearchCriteriaPropertiesList;
    /**
     * Constructor method for SearchCriteria
     * @see parent::__construct()
     * @param int $_returnCode
     * @param string $_status
     * @param MELOCSWSStructArrayOfSearchCriteriaProperties $_searchCriteriaPropertiesList
     * @return MELOCSWSStructSearchCriteria
     */
    public function __construct($_returnCode,$_status = NULL,$_searchCriteriaPropertiesList = NULL)
    {
        parent::__construct(array('ReturnCode'=>$_returnCode,'Status'=>$_status,'SearchCriteriaPropertiesList'=>($_searchCriteriaPropertiesList instanceof MELOCSWSStructArrayOfSearchCriteriaProperties)?$_searchCriteriaPropertiesList:new MELOCSWSStructArrayOfSearchCriteriaProperties($_searchCriteriaPropertiesList)),false);
    }
    /**
     * Get ReturnCode value
     * @return int
     */
    public function getReturnCode()
    {
        return $this->ReturnCode;
    }
    /**
     * Set ReturnCode value
     * @param int $_returnCode the ReturnCode
     * @return int
     */
    public function setReturnCode($_returnCode)
    {
        return ($this->ReturnCode = $_returnCode);
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param string $_status the Status
     * @return string
     */
    public function setStatus($_status)
    {
        return ($this->Status = $_status);
    }
    /**
     * Get SearchCriteriaPropertiesList value
     * @return MELOCSWSStructArrayOfSearchCriteriaProperties|null
     */
    public function getSearchCriteriaPropertiesList()
    {
        return $this->SearchCriteriaPropertiesList;
    }
    /**
     * Set SearchCriteriaPropertiesList value
     * @param MELOCSWSStructArrayOfSearchCriteriaProperties $_searchCriteriaPropertiesList the SearchCriteriaPropertiesList
     * @return MELOCSWSStructArrayOfSearchCriteriaProperties
     */
    public function setSearchCriteriaPropertiesList($_searchCriteriaPropertiesList)
    {
        return ($this->SearchCriteriaPropertiesList = $_searchCriteriaPropertiesList);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructSearchCriteria
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
