<?php
/**
 * File for class MELOCSWSStructSearchCriteriaProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructSearchCriteriaProperties originally named SearchCriteriaProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructSearchCriteriaProperties extends MELOCSWSWsdlClass
{
    /**
     * The DataType
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DataType;
    /**
     * The DataLength
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DataLength;
    /**
     * The DataCase
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DataCase;
    /**
     * The IsParameter
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsParameter;
    /**
     * The IsVisible
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsVisible;
    /**
     * The IsPrompt
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsPrompt;
    /**
     * The IsMandatory
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsMandatory;
    /**
     * The CanBeOverridden
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $CanBeOverridden;
    /**
     * The ColumnName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ColumnName;
    /**
     * The Description
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Description;
    /**
     * The RestrictionType
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $RestrictionType;
    /**
     * The FromValue
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $FromValue;
    /**
     * The ToValue
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ToValue;
    /**
     * The RelDateCrit
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $RelDateCrit;
    /**
     * Constructor method for SearchCriteriaProperties
     * @see parent::__construct()
     * @param int $_dataType
     * @param int $_dataLength
     * @param int $_dataCase
     * @param boolean $_isParameter
     * @param boolean $_isVisible
     * @param boolean $_isPrompt
     * @param boolean $_isMandatory
     * @param boolean $_canBeOverridden
     * @param string $_columnName
     * @param string $_description
     * @param string $_restrictionType
     * @param string $_fromValue
     * @param string $_toValue
     * @param string $_relDateCrit
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function __construct($_dataType,$_dataLength,$_dataCase,$_isParameter,$_isVisible,$_isPrompt,$_isMandatory,$_canBeOverridden,$_columnName = NULL,$_description = NULL,$_restrictionType = NULL,$_fromValue = NULL,$_toValue = NULL,$_relDateCrit = NULL)
    {
        parent::__construct(array('DataType'=>$_dataType,'DataLength'=>$_dataLength,'DataCase'=>$_dataCase,'IsParameter'=>$_isParameter,'IsVisible'=>$_isVisible,'IsPrompt'=>$_isPrompt,'IsMandatory'=>$_isMandatory,'CanBeOverridden'=>$_canBeOverridden,'ColumnName'=>$_columnName,'Description'=>$_description,'RestrictionType'=>$_restrictionType,'FromValue'=>$_fromValue,'ToValue'=>$_toValue,'RelDateCrit'=>$_relDateCrit),false);
    }
    /**
     * Get DataType value
     * @return int
     */
    public function getDataType()
    {
        return $this->DataType;
    }
    /**
     * Set DataType value
     * @param int $_dataType the DataType
     * @return int
     */
    public function setDataType($_dataType)
    {
        return ($this->DataType = $_dataType);
    }
    /**
     * Get DataLength value
     * @return int
     */
    public function getDataLength()
    {
        return $this->DataLength;
    }
    /**
     * Set DataLength value
     * @param int $_dataLength the DataLength
     * @return int
     */
    public function setDataLength($_dataLength)
    {
        return ($this->DataLength = $_dataLength);
    }
    /**
     * Get DataCase value
     * @return int
     */
    public function getDataCase()
    {
        return $this->DataCase;
    }
    /**
     * Set DataCase value
     * @param int $_dataCase the DataCase
     * @return int
     */
    public function setDataCase($_dataCase)
    {
        return ($this->DataCase = $_dataCase);
    }
    /**
     * Get IsParameter value
     * @return boolean
     */
    public function getIsParameter()
    {
        return $this->IsParameter;
    }
    /**
     * Set IsParameter value
     * @param boolean $_isParameter the IsParameter
     * @return boolean
     */
    public function setIsParameter($_isParameter)
    {
        return ($this->IsParameter = $_isParameter);
    }
    /**
     * Get IsVisible value
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->IsVisible;
    }
    /**
     * Set IsVisible value
     * @param boolean $_isVisible the IsVisible
     * @return boolean
     */
    public function setIsVisible($_isVisible)
    {
        return ($this->IsVisible = $_isVisible);
    }
    /**
     * Get IsPrompt value
     * @return boolean
     */
    public function getIsPrompt()
    {
        return $this->IsPrompt;
    }
    /**
     * Set IsPrompt value
     * @param boolean $_isPrompt the IsPrompt
     * @return boolean
     */
    public function setIsPrompt($_isPrompt)
    {
        return ($this->IsPrompt = $_isPrompt);
    }
    /**
     * Get IsMandatory value
     * @return boolean
     */
    public function getIsMandatory()
    {
        return $this->IsMandatory;
    }
    /**
     * Set IsMandatory value
     * @param boolean $_isMandatory the IsMandatory
     * @return boolean
     */
    public function setIsMandatory($_isMandatory)
    {
        return ($this->IsMandatory = $_isMandatory);
    }
    /**
     * Get CanBeOverridden value
     * @return boolean
     */
    public function getCanBeOverridden()
    {
        return $this->CanBeOverridden;
    }
    /**
     * Set CanBeOverridden value
     * @param boolean $_canBeOverridden the CanBeOverridden
     * @return boolean
     */
    public function setCanBeOverridden($_canBeOverridden)
    {
        return ($this->CanBeOverridden = $_canBeOverridden);
    }
    /**
     * Get ColumnName value
     * @return string|null
     */
    public function getColumnName()
    {
        return $this->ColumnName;
    }
    /**
     * Set ColumnName value
     * @param string $_columnName the ColumnName
     * @return string
     */
    public function setColumnName($_columnName)
    {
        return ($this->ColumnName = $_columnName);
    }
    /**
     * Get Description value
     * @return string|null
     */
    public function getDescription()
    {
        return $this->Description;
    }
    /**
     * Set Description value
     * @param string $_description the Description
     * @return string
     */
    public function setDescription($_description)
    {
        return ($this->Description = $_description);
    }
    /**
     * Get RestrictionType value
     * @return string|null
     */
    public function getRestrictionType()
    {
        return $this->RestrictionType;
    }
    /**
     * Set RestrictionType value
     * @param string $_restrictionType the RestrictionType
     * @return string
     */
    public function setRestrictionType($_restrictionType)
    {
        return ($this->RestrictionType = $_restrictionType);
    }
    /**
     * Get FromValue value
     * @return string|null
     */
    public function getFromValue()
    {
        return $this->FromValue;
    }
    /**
     * Set FromValue value
     * @param string $_fromValue the FromValue
     * @return string
     */
    public function setFromValue($_fromValue)
    {
        return ($this->FromValue = $_fromValue);
    }
    /**
     * Get ToValue value
     * @return string|null
     */
    public function getToValue()
    {
        return $this->ToValue;
    }
    /**
     * Set ToValue value
     * @param string $_toValue the ToValue
     * @return string
     */
    public function setToValue($_toValue)
    {
        return ($this->ToValue = $_toValue);
    }
    /**
     * Get RelDateCrit value
     * @return string|null
     */
    public function getRelDateCrit()
    {
        return $this->RelDateCrit;
    }
    /**
     * Set RelDateCrit value
     * @param string $_relDateCrit the RelDateCrit
     * @return string
     */
    public function setRelDateCrit($_relDateCrit)
    {
        return ($this->RelDateCrit = $_relDateCrit);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
