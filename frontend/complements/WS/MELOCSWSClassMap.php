<?php

/**
 * File for the class which returns the class map definition
 * @package MELOCSWS
 * @date 2016-01-19
 */
/**
 * Class which returns the class map definition by the static method MELOCSWSClassMap::classMap()
 * @package MELOCSWS
 * @date 2016-01-19
 */
class MELOCSWSClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array(
            'About' => 'MELOCSWSStructAbout',
            'AboutResponse' => 'MELOCSWSStructAboutResponse',
            'ArrayOfExpressionProperties' => 'MELOCSWSStructArrayOfExpressionProperties',
            'ArrayOfFormatProperties' => 'MELOCSWSStructArrayOfFormatProperties',
            'ArrayOfSearchCriteriaProperties' => 'MELOCSWSStructArrayOfSearchCriteriaProperties',
            'ArrayOfStatisticalFormulaProperties' => 'MELOCSWSStructArrayOfStatisticalFormulaProperties',
            'ArrayOfTemplateHeader' => 'MELOCSWSStructArrayOfTemplateHeader',
            'Expression' => 'MELOCSWSStructExpression',
            'ExpressionProperties' => 'MELOCSWSStructExpressionProperties',
            'FormatInfo' => 'MELOCSWSStructFormatInfo',
            'FormatProperties' => 'MELOCSWSStructFormatProperties',
            'GetExpression' => 'MELOCSWSStructGetExpression',
            'GetExpressionResponse' => 'MELOCSWSStructGetExpressionResponse',
            'GetFormatInfo' => 'MELOCSWSStructGetFormatInfo',
            'GetFormatInfoResponse' => 'MELOCSWSStructGetFormatInfoResponse',
            'GetSearchCriteria' => 'MELOCSWSStructGetSearchCriteria',
            'GetSearchCriteriaResponse' => 'MELOCSWSStructGetSearchCriteriaResponse',
            'GetStatisticalFormula' => 'MELOCSWSStructGetStatisticalFormula',
            'GetStatisticalFormulaResponse' => 'MELOCSWSStructGetStatisticalFormulaResponse',
            'GetTemplateList' => 'MELOCSWSStructGetTemplateList',
            'GetTemplateListResponse' => 'MELOCSWSStructGetTemplateListResponse',
            'GetTemplateMetaData' => 'MELOCSWSStructGetTemplateMetaData',
            'GetTemplateMetaDataResponse' => 'MELOCSWSStructGetTemplateMetaDataResponse',
            'GetTemplateProperties' => 'MELOCSWSStructGetTemplateProperties',
            'GetTemplatePropertiesResponse' => 'MELOCSWSStructGetTemplatePropertiesResponse',
            'GetTemplateResultAsDataSet' => 'MELOCSWSStructGetTemplateResultAsDataSet',
            'GetTemplateResultAsDataSetResponse' => 'MELOCSWSStructGetTemplateResultAsDataSetResponse',
            'GetTemplateResultAsXML' => 'MELOCSWSStructGetTemplateResultAsXML',
            'GetTemplateResultAsXMLResponse' => 'MELOCSWSStructGetTemplateResultAsXMLResponse',
            'GetTemplateResultOptions' => 'MELOCSWSStructGetTemplateResultOptions',
            'GetTemplateResultOptionsResponse' => 'MELOCSWSStructGetTemplateResultOptionsResponse',
            'InputForTemplateResult' => 'MELOCSWSStructInputForTemplateResult',
            'SearchCriteria' => 'MELOCSWSStructSearchCriteria',
            'SearchCriteriaProperties' => 'MELOCSWSStructSearchCriteriaProperties',
            'StatisticalFormula' => 'MELOCSWSStructStatisticalFormula',
            'StatisticalFormulaProperties' => 'MELOCSWSStructStatisticalFormulaProperties',
            'TemplateHeader' => 'MELOCSWSStructTemplateHeader',
            'TemplateList' => 'MELOCSWSStructTemplateList',
            'TemplateMetaData' => 'MELOCSWSStructTemplateMetaData',
            'TemplateProperties' => 'MELOCSWSStructTemplateProperties',
            'TemplateResult' => 'MELOCSWSStructTemplateResult',
            'TemplateResultAsDataSet' => 'MELOCSWSStructTemplateResultAsDataSet',
            'TemplateResultAsXML' => 'MELOCSWSStructTemplateResultAsXML',
            'TemplateResultOptions' => 'MELOCSWSStructTemplateResultOptions',
            'WSCredentials' => 'MELOCSWSStructWSCredentials',
        );
    }
}
