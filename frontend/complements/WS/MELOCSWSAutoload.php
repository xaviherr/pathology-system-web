<?php

/**
 * File to load generated classes once at once time
 * @package MELOCSWS
 * @date 2016-01-19
 */
/**
 * Includes for all generated classes files
 * @date 2016-01-19
 */
require_once dirname(__FILE__) . '/MELOCSWSWsdlClass.php';
require_once dirname(__FILE__) . '/Get/Info/MELOCSWSStructGetFormatInfo.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetFormatInfoResponse.php';
require_once dirname(__FILE__) . '/Format/Info/MELOCSWSStructFormatInfo.php';
require_once dirname(__FILE__) . '/Array/Properties/MELOCSWSStructArrayOfFormatProperties.php';
require_once dirname(__FILE__) . '/Statistical/Properties/MELOCSWSStructStatisticalFormulaProperties.php';
require_once dirname(__FILE__) . '/Array/Properties/MELOCSWSStructArrayOfStatisticalFormulaProperties.php';
require_once dirname(__FILE__) . '/Expression/Properties/MELOCSWSStructExpressionProperties.php';
require_once dirname(__FILE__) . '/Get/Formula/MELOCSWSStructGetStatisticalFormula.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetStatisticalFormulaResponse.php';
require_once dirname(__FILE__) . '/Statistical/Formula/MELOCSWSStructStatisticalFormula.php';
require_once dirname(__FILE__) . '/Format/Properties/MELOCSWSStructFormatProperties.php';
require_once dirname(__FILE__) . '/Get/List/MELOCSWSStructGetTemplateList.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetTemplateMetaDataResponse.php';
require_once dirname(__FILE__) . '/Template/Data/MELOCSWSStructTemplateMetaData.php';
require_once dirname(__FILE__) . '/About/MELOCSWSStructAbout.php';
require_once dirname(__FILE__) . '/About/Response/MELOCSWSStructAboutResponse.php';
require_once dirname(__FILE__) . '/Get/Data/MELOCSWSStructGetTemplateMetaData.php';
require_once dirname(__FILE__) . '/Template/Header/MELOCSWSStructTemplateHeader.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetTemplateListResponse.php';
require_once dirname(__FILE__) . '/Template/List/MELOCSWSStructTemplateList.php';
require_once dirname(__FILE__) . '/Array/Header/MELOCSWSStructArrayOfTemplateHeader.php';
require_once dirname(__FILE__) . '/Array/Properties/MELOCSWSStructArrayOfExpressionProperties.php';
require_once dirname(__FILE__) . '/Expression/MELOCSWSStructExpression.php';
require_once dirname(__FILE__) . '/Array/Properties/MELOCSWSStructArrayOfSearchCriteriaProperties.php';
require_once dirname(__FILE__) . '/Search/Properties/MELOCSWSStructSearchCriteriaProperties.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetTemplateResultAsDataSetResponse.php';
require_once dirname(__FILE__) . '/Template/Set/MELOCSWSStructTemplateResultAsDataSet.php';
require_once dirname(__FILE__) . '/Template/Options/MELOCSWSStructTemplateResultOptions.php';
require_once dirname(__FILE__) . '/Input/Result/MELOCSWSStructInputForTemplateResult.php';
require_once dirname(__FILE__) . '/WSC/Redentials/MELOCSWSStructWSCredentials.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetTemplatePropertiesResponse.php';
require_once dirname(__FILE__) . '/Template/Properties/MELOCSWSStructTemplateProperties.php';
require_once dirname(__FILE__) . '/Get/Set/MELOCSWSStructGetTemplateResultAsDataSet.php';
require_once dirname(__FILE__) . '/Template/Result/MELOCSWSStructTemplateResult.php';
require_once dirname(__FILE__) . '/Get/Options/MELOCSWSStructGetTemplateResultOptions.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetSearchCriteriaResponse.php';
require_once dirname(__FILE__) . '/Search/Criteria/MELOCSWSStructSearchCriteria.php';
require_once dirname(__FILE__) . '/Get/Expression/MELOCSWSStructGetExpression.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetExpressionResponse.php';
require_once dirname(__FILE__) . '/Get/Criteria/MELOCSWSStructGetSearchCriteria.php';
require_once dirname(__FILE__) . '/Template/XML/MELOCSWSStructTemplateResultAsXML.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetTemplateResultOptionsResponse.php';
require_once dirname(__FILE__) . '/Get/XML/MELOCSWSStructGetTemplateResultAsXML.php';
require_once dirname(__FILE__) . '/Get/Response/MELOCSWSStructGetTemplateResultAsXMLResponse.php';
require_once dirname(__FILE__) . '/Get/Properties/MELOCSWSStructGetTemplateProperties.php';
require_once dirname(__FILE__) . '/Get/MELOCSWSServiceGet.php';
require_once dirname(__FILE__) . '/About/MELOCSWSServiceAbout.php';
require_once dirname(__FILE__) . '/MELOCSWSClassMap.php';
