<?php
/**
 * File for class MELOCSWSStructExpression
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructExpression originally named Expression
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructExpression extends MELOCSWSWsdlClass
{
    /**
     * The ReturnCode
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $ReturnCode;
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Status;
    /**
     * The ExpressionPropertiesList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructArrayOfExpressionProperties
     */
    public $ExpressionPropertiesList;
    /**
     * Constructor method for Expression
     * @see parent::__construct()
     * @param int $_returnCode
     * @param string $_status
     * @param MELOCSWSStructArrayOfExpressionProperties $_expressionPropertiesList
     * @return MELOCSWSStructExpression
     */
    public function __construct($_returnCode,$_status = NULL,$_expressionPropertiesList = NULL)
    {
        parent::__construct(array('ReturnCode'=>$_returnCode,'Status'=>$_status,'ExpressionPropertiesList'=>($_expressionPropertiesList instanceof MELOCSWSStructArrayOfExpressionProperties)?$_expressionPropertiesList:new MELOCSWSStructArrayOfExpressionProperties($_expressionPropertiesList)),false);
    }
    /**
     * Get ReturnCode value
     * @return int
     */
    public function getReturnCode()
    {
        return $this->ReturnCode;
    }
    /**
     * Set ReturnCode value
     * @param int $_returnCode the ReturnCode
     * @return int
     */
    public function setReturnCode($_returnCode)
    {
        return ($this->ReturnCode = $_returnCode);
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param string $_status the Status
     * @return string
     */
    public function setStatus($_status)
    {
        return ($this->Status = $_status);
    }
    /**
     * Get ExpressionPropertiesList value
     * @return MELOCSWSStructArrayOfExpressionProperties|null
     */
    public function getExpressionPropertiesList()
    {
        return $this->ExpressionPropertiesList;
    }
    /**
     * Set ExpressionPropertiesList value
     * @param MELOCSWSStructArrayOfExpressionProperties $_expressionPropertiesList the ExpressionPropertiesList
     * @return MELOCSWSStructArrayOfExpressionProperties
     */
    public function setExpressionPropertiesList($_expressionPropertiesList)
    {
        return ($this->ExpressionPropertiesList = $_expressionPropertiesList);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructExpression
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
