<?php
/**
 * File for class MELOCSWSStructExpressionProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructExpressionProperties originally named ExpressionProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructExpressionProperties extends MELOCSWSWsdlClass
{
    /**
     * The IsRunningTotal
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsRunningTotal;
    /**
     * The HasSum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $HasSum;
    /**
     * The HasVerticalSum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $HasVerticalSum;
    /**
     * The Formula
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Formula;
    /**
     * The ColName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ColName;
    /**
     * Constructor method for ExpressionProperties
     * @see parent::__construct()
     * @param boolean $_isRunningTotal
     * @param boolean $_hasSum
     * @param boolean $_hasVerticalSum
     * @param string $_formula
     * @param string $_colName
     * @return MELOCSWSStructExpressionProperties
     */
    public function __construct($_isRunningTotal,$_hasSum,$_hasVerticalSum,$_formula = NULL,$_colName = NULL)
    {
        parent::__construct(array('IsRunningTotal'=>$_isRunningTotal,'HasSum'=>$_hasSum,'HasVerticalSum'=>$_hasVerticalSum,'Formula'=>$_formula,'ColName'=>$_colName),false);
    }
    /**
     * Get IsRunningTotal value
     * @return boolean
     */
    public function getIsRunningTotal()
    {
        return $this->IsRunningTotal;
    }
    /**
     * Set IsRunningTotal value
     * @param boolean $_isRunningTotal the IsRunningTotal
     * @return boolean
     */
    public function setIsRunningTotal($_isRunningTotal)
    {
        return ($this->IsRunningTotal = $_isRunningTotal);
    }
    /**
     * Get HasSum value
     * @return boolean
     */
    public function getHasSum()
    {
        return $this->HasSum;
    }
    /**
     * Set HasSum value
     * @param boolean $_hasSum the HasSum
     * @return boolean
     */
    public function setHasSum($_hasSum)
    {
        return ($this->HasSum = $_hasSum);
    }
    /**
     * Get HasVerticalSum value
     * @return boolean
     */
    public function getHasVerticalSum()
    {
        return $this->HasVerticalSum;
    }
    /**
     * Set HasVerticalSum value
     * @param boolean $_hasVerticalSum the HasVerticalSum
     * @return boolean
     */
    public function setHasVerticalSum($_hasVerticalSum)
    {
        return ($this->HasVerticalSum = $_hasVerticalSum);
    }
    /**
     * Get Formula value
     * @return string|null
     */
    public function getFormula()
    {
        return $this->Formula;
    }
    /**
     * Set Formula value
     * @param string $_formula the Formula
     * @return string
     */
    public function setFormula($_formula)
    {
        return ($this->Formula = $_formula);
    }
    /**
     * Get ColName value
     * @return string|null
     */
    public function getColName()
    {
        return $this->ColName;
    }
    /**
     * Set ColName value
     * @param string $_colName the ColName
     * @return string
     */
    public function setColName($_colName)
    {
        return ($this->ColName = $_colName);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructExpressionProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
