<?php
/**
 * File for class MELOCSWSStructAboutResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructAboutResponse originally named AboutResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructAboutResponse extends MELOCSWSWsdlClass
{
    /**
     * The AboutResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $AboutResult;
    /**
     * Constructor method for AboutResponse
     * @see parent::__construct()
     * @param string $_aboutResult
     * @return MELOCSWSStructAboutResponse
     */
    public function __construct($_aboutResult = NULL)
    {
        parent::__construct(array('AboutResult'=>$_aboutResult),false);
    }
    /**
     * Get AboutResult value
     * @return string|null
     */
    public function getAboutResult()
    {
        return $this->AboutResult;
    }
    /**
     * Set AboutResult value
     * @param string $_aboutResult the AboutResult
     * @return string
     */
    public function setAboutResult($_aboutResult)
    {
        return ($this->AboutResult = $_aboutResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructAboutResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
