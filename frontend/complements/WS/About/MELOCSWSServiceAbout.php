<?php
/**
 * File for class MELOCSWSServiceAbout
 * @package MELOCSWS
 * @subpackage Services
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSServiceAbout originally named About
 * @package MELOCSWS
 * @subpackage Services
 * @date 2016-01-19
 */
class MELOCSWSServiceAbout extends MELOCSWSWsdlClass
{
    /**
     * Method to call the operation originally named About
     * Documentation : Diagnostics method that checks for presence of nessecary components and database connection
     * @uses MELOCSWSWsdlClass::getSoapClient()
     * @uses MELOCSWSWsdlClass::setResult()
     * @uses MELOCSWSWsdlClass::saveLastError()
     * @param MELOCSWSStructAbout $_mELOCSWSStructAbout
     * @return MELOCSWSStructAboutResponse
     */
    public function About()
    {
        try
        {
            return $this->setResult(new MELOCSWSStructAboutResponse(self::getSoapClient()->About()));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see MELOCSWSWsdlClass::getResult()
     * @return MELOCSWSStructAboutResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
