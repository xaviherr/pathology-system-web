<?php
/**
 * File for class MELOCSWSStructFormatInfo
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructFormatInfo originally named FormatInfo
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructFormatInfo extends MELOCSWSWsdlClass
{
    /**
     * The FormatPropertiesList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructArrayOfFormatProperties
     */
    public $FormatPropertiesList;
    /**
     * Constructor method for FormatInfo
     * @see parent::__construct()
     * @param MELOCSWSStructArrayOfFormatProperties $_formatPropertiesList
     * @return MELOCSWSStructFormatInfo
     */
    public function __construct($_formatPropertiesList = NULL)
    {
        parent::__construct(array('FormatPropertiesList'=>($_formatPropertiesList instanceof MELOCSWSStructArrayOfFormatProperties)?$_formatPropertiesList:new MELOCSWSStructArrayOfFormatProperties($_formatPropertiesList)),false);
    }
    /**
     * Get FormatPropertiesList value
     * @return MELOCSWSStructArrayOfFormatProperties|null
     */
    public function getFormatPropertiesList()
    {
        return $this->FormatPropertiesList;
    }
    /**
     * Set FormatPropertiesList value
     * @param MELOCSWSStructArrayOfFormatProperties $_formatPropertiesList the FormatPropertiesList
     * @return MELOCSWSStructArrayOfFormatProperties
     */
    public function setFormatPropertiesList($_formatPropertiesList)
    {
        return ($this->FormatPropertiesList = $_formatPropertiesList);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructFormatInfo
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
