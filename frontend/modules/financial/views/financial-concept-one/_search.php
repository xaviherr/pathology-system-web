<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialConceptOneSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="financial-concept-one-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'essayId') ?>

    <?= $form->field($model, 'statusConcept') ?>

    <?= $form->field($model, 'statusBus') ?>

    <?= $form->field($model, 'period') ?>

    <?= $form->field($model, 'account') ?>

    <?= $form->field($model, 'resno') ?>

    <?= $form->field($model, 'bus') ?>

    <?= $form->field($model, 'task') ?>

    <?= $form->field($model, 'crop') ?>

    <?= $form->field($model, 'testOfLaboratory') ?>

    <?= $form->field($model, 'owner') ?>

    <?= $form->field($model, 'pathogens') ?>

    <?= $form->field($model, 'numberOfExperiments') ?>

    <?= $form->field($model, 'numberOfPathogens') ?>

    <?= $form->field($model, 'descriptionFinal') ?>

    <?= $form->field($model, 'ACE') ?>

    <?= $form->field($model, 'agreement') ?>

    <?= $form->field($model, 'samples') ?>

    <?= $form->field($model, 'costId') ?>

    <?= $form->field($model, 'fee') ?>

    <?= $form->field($model, 'total') ?>

    <?= $form->field($model, 'registeredBy') ?>

    <?= $form->field($model, 'registeredAt') ?>

    <?= $form->field($model, 'updatedBy') ?>

    <?= $form->field($model, 'updatedAt') ?>

    <?= $form->field($model, 'deletedBy') ?>

    <?= $form->field($model, 'deletedAt') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'resnoLab') ?>

    <?= $form->field($model, 'requestId') ?>

    <?= $form->field($model, 'cropId') ?>

    <?= $form->field($model, 'workFlowId') ?>

    <?= $form->field($model, 'numOrderId') ?>

    <?= $form->field($model, 'percent') ?>

    <?= $form->field($model, 'totalPercentage') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>