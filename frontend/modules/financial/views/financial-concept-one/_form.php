<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialConceptOne */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="financial-concept-one-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'essayId')->textInput() ?>

    <?= $form->field($model, 'statusConcept')->dropDownList(['sent' => 'Sent', 'to_send' => 'To send',], ['prompt' => '']) ?>

    <?= $form->field($model, 'statusBus')->dropDownList(['active' => 'Active', 'inactive' => 'Inactive',], ['prompt' => '']) ?>

    <?= $form->field($model, 'period')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'resno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'task')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'crop')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'testOfLaboratory')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pathogens')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'numberOfExperiments')->textInput() ?>

    <?= $form->field($model, 'numberOfPathogens')->textInput() ?>

    <?= $form->field($model, 'descriptionFinal')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ACE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agreement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'samples')->textInput() ?>

    <?= $form->field($model, 'costId')->textInput() ?>

    <?= $form->field($model, 'fee')->textInput() ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'registeredBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredAt')->textInput() ?>

    <?= $form->field($model, 'updatedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'disabled' => 'Disabled',], ['prompt' => '']) ?>

    <?= $form->field($model, 'resnoLab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'requestId')->textInput() ?>

    <?= $form->field($model, 'cropId')->textInput() ?>

    <?= $form->field($model, 'workFlowId')->textInput() ?>

    <?= $form->field($model, 'numOrderId')->textInput() ?>

    <?= $form->field($model, 'percent')->textInput() ?>

    <?= $form->field($model, 'totalPercentage')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>