<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = 'Financial Concept';
$this->params['breadcrumbs'][] = $this->title;
?>



<?php
$js_code_financial_concept_one = <<<JS
<script language="JavaScript">
$('.gvDefault').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
  });
</script>
JS;
?>

<div class="financial-concept-one-index">

    <?php Pjax::begin(['id' => 'pjax-financial-concept-one']); ?>
    <?php $form_financial_concept_one = ActiveForm::begin(
        [
            'id' => 'form-financial-concept-one',
            'options' =>
            [
                'data-pjax' => true,
                'id' => 'dynamic-form-financial-concept-one',
            ]
        ]
    ); ?>
    <?= $js_code_financial_concept_one; ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <div class="row">
        <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Create <? //= Html::encode($this->title) 
                                                                                    ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-create-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>View <? //= Html::encode($this->title) 
                                                                                ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-view-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Edit <? //=Html::encode($this->title) 
                                                                                ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-update-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-bars "></i> <?= Html::encode($this->title) ?> List</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= GridView::widget(
                        [
                            'dataProvider' => $dataProvider,
                            'layout'       => "{items}",
                            'options'       => ['style' => 'font-size:10px; overflow-x: auto; padding-right: 0px; padding-left: 0px;'],
                            'rowOptions' => function ($model) {
                                if ($model->status == "disabled") {
                                    return ['class' => 'danger'];
                                } else if ($model->status == "active") {
                                    return ['class' => 'success'];
                                }
                            },
                            'columns'       => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',

                                    'checkboxOptions' => function ($model, $key, $index, $column) {

                                        if (!is_null(Yii::$app->request->post('selection'))) {
                                            return [
                                                'value' => $model->id,
                                                'checked' => in_array($model->id, Yii::$app->request->post('selection')) ? true : false
                                            ];
                                        } else {
                                            return [
                                                'value' => $model->id,
                                            ];
                                        }
                                    },
                                ],
                                [
                                    'class' => 'yii\grid\SerialColumn'
                                ],
                                [
                                    'attribute'     => 'requestId',
                                    'header'        => 'Request',
                                    'value' => 'request.code',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'statusConcept',
                                    'value'     =>  function ($model) {
                                        return  Html::tag(
                                            'span',
                                            Html::encode($model->statusConcept),
                                            ['class' => 'label label-' . $model->statusConcept]
                                        );
                                    },
                                    'enableSorting' => false,
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute'     => 'statusBus',
                                    'value'     =>  function ($model) {
                                        return  Html::tag(
                                            'span',
                                            Html::encode($model->statusBus),
                                            ['class' => 'label label-' . $model->statusBus]
                                        );
                                    },
                                    'enableSorting' => false,
                                    'format' => 'raw',
                                ],
                                [
                                    'header' => 'Expiry date',
                                    'value' => function ($model) {
                                        return
                                            date("d-M-Y",  strtotime($model->dateTo));
                                    },
                                ],
                                [
                                    'attribute'     => 'period',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'account',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'resno',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'bus',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'task',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'crop',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'testOfLaboratory',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'owner',
                                    'enableSorting' => false,
                                ],
                                // [
                                //     'attribute'     => 'pathogens',
                                //     'enableSorting' => false,
                                // ],
                                [
                                    'attribute'     => 'numberOfExperiments',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'numberOfPathogens',
                                    'enableSorting' => false,
                                ],
                                // [
                                //     'attribute'     => 'descriptionFinal',
                                //     'enableSorting' => false,
                                // ],
                                [
                                    'attribute'     => 'ACE',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'agreement',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'samples',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'fee',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'percent',
                                    'header' => 'Percent %',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     =>   'totalPercentage',
                                    'header' => 'Sub total Cost USD',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'total',
                                    'enableSorting' => false,
                                ],
                                /*
                                [
                                    'attribute'     => 'costId',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'resnoLab',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'cropId',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'workFlowId',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'numOrderId',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'registeredBy',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'registeredAt',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'updatedBy',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'updatedAt',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'deletedBy',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'deletedAt',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'status',
                                    'enableSorting' => false,
                                ],

                                */


                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered table-striped dataTable gvDefault',
                                'id'    => "gvConcepts",
                            ],
                        ]
                    ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 ">
                    <span class="info">
                        <i class="icon fa fa-info"></i> Legend information about the status of the financial concepts and the buses entered by the request owner.
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p>
                        <span class="label label-to_send" style="font-size: 10px;">to_send</span>
                        <span style="font-size:10px;"> Financial concept of the list that has not yet been sent to finance for processing.</span>
                    </p>
                    <p>
                        <span class="label label-sent" style="font-size: 10px;">sent</span>
                        <span style="font-size:10px;"> Financial concept of the list that was already sent to finance.</span>
                    </p>
                </div>
                <div class="col-sm-5">
                    <p>
                        <span class="label label-active" style="font-size: 10px;">active</span>
                        <span style="font-size:10px;"> Active bus status. Financial concept ready to be sent.</span>
                    </p>
                    <p>
                        <span class="label label-inactive" style="font-size: 10px;">inactive</span>
                        <span style="font-size:10px;">Bus status deactivated or inactive, it is recommended to notify the user who registered the request.</span>
                    </p>
                </div>
            </div>
        </div>

        <div class="box-footer">

            <?= Html::a(
                'Refresh',
                [
                    null,
                    //'refresh',
                    // 'requestId' => 0,
                ],
                [
                    'class' => 'btn btn-primary pull-left',
                    'title' => 'Refresh financial concept list checked to know if the STATUS BUS remains active',
                    'data-method' => 'post',
                    'style' => 'margin:0px 5px;'
                ]
            );
            ?>

            <?= Html::a(
                'Notify',
                [
                    null,
                    //'notify',
                ],
                [
                    'class' => 'btn btn-primary pull-left',
                    'title' => 'Notify inactive STATUS BUS from financial concept list checked',
                    'data-method' => 'post',
                    'style' => 'margin:0px 5px;'
                ]
            );
            ?>

            <?= Html::a(
                'Preview',
                [
                    'preview',
                    // 'requestId' => $requestId,
                ],
                [
                    'class' => 'btn btn-success pull-right',
                    'title' => 'Preview',
                    'data-method' => 'post',
                    'style' => 'margin:0px 5px;'
                ]
            ); ?>

        </div>
    </div>


    <div class="box">


        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">

                    <?= GridView::widget(
                        [
                            'dataProvider' => $dataProviderFinancialReport,
                            'layout'       => "{items}",
                            'options'       => ['style' => 'font-size:12px;'],
                            'columns'       => [
                                [
                                    'class' => 'yii\grid\SerialColumn'
                                ],

                                // [
                                //     'attribute'     => 'Request',
                                //     'enableSorting' => false,
                                // ],
                                [
                                    'attribute'     => 'Period',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'Account',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'Resno',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'Bus',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'Task',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'Description Final',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'Agreement',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     =>   'Sub total cost USD',
                                    'enableSorting' => false,
                                ],
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered table-striped dataTable',
                                'id'    => "gvConcepts",
                            ],
                        ]
                    );
                    ?>


                </div>
            </div>

        </div>

        <div class="box-footer">

            <?= Html::a(
                'Send & Download',
                [
                    'send',
                    'implodeSelection' => is_null($selection) ? $selection : implode(",", $selection)
                ],
                [
                    'class' => 'btn btn-success pull-right',
                    'title' => 'Add',
                    'data-pjax' => 0, //pjax inactive
                    //'data-method' => 'post', //in Form
                    'style' => 'margin:0px 5px;',
                    'target' => '_blank',
                ]
            ); ?>

            <?= Html::a(
                'Refresh',
                [
                    'refresh',
                    // 'requestId' => 0,
                ],
                [
                    'class' => 'btn btn-primary pull-right',
                    'title' => 'Refresh financial concept list checked to know if the STATUS BUS remains active',
                    'data-method' => 'post',
                    'style' => 'margin:0px 5px;'
                ]
            ); ?>

        </div>
    </div>


    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>