<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialConceptOne */

$this->title = 'Update Financial Concept One: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Financial Concept Ones', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => [
        'view', 'id' => $model->id,
        'resnoLab' => $model->resnoLab,
        'requestId' => $model->requestId,
        'cropId' => $model->cropId,
        'workFlowId' => $model->workFlowId,
        'numOrderId' => $model->numOrderId,
    ]
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="financial-concept-one-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>