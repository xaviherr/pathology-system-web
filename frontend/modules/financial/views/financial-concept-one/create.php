<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialConceptOne */

$this->title = 'Create Financial Concept One';
$this->params['breadcrumbs'][] = ['label' => 'Financial Concept Ones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financial-concept-one-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>