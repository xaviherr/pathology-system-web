<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialAccount */

$this->title = 'Create Financial Account';
$this->params['breadcrumbs'][] = ['label' => 'Financial Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financial-account-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>