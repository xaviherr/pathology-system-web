<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialAccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="financial-account-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'resnoLab') ?>

    <?= $form->field($model, 'accountToCharge') ?>

    <?= $form->field($model, 'accountToRecovery') ?>

    <?= $form->field($model, 'accountDescription') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'recoveryAgreement') ?>

    <?php // echo $form->field($model, 'busAgreement') ?>

    <?php // echo $form->field($model, 'taskAgreement') ?>

    <?php // echo $form->field($model, 'registeredBy') ?>

    <?php // echo $form->field($model, 'registeredAt') ?>

    <?php // echo $form->field($model, 'updatedBy') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedBy') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
