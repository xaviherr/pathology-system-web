<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialAccount */

$this->title = 'Update Financial Account: ' . $model->resnoLab;
$this->params['breadcrumbs'][] = ['label' => 'Financial Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->resnoLab, 'url' => ['view', 'id' => $model->resnoLab]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="financial-account-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>