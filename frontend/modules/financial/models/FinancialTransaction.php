<?php

namespace frontend\modules\financial\models;

use Yii;
use frontend\modules\intelligence\models\RequestProcess;

/**
 * This is the model class for table "{{%FinancialTransaction}}".
 *
 * @property string $resnoLab
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $requestId
 * @property int $cropId
 * @property int $workFlowId
 * @property int $numOrderId
 *
 * @property FinancialConceptOne[] $financialConceptOnes
 * @property FinancialAccount $resnoLab0
 * @property RequestProcess $request
 */
class FinancialTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%FinancialTransaction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resnoLab', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['requestId', 'cropId', 'workFlowId', 'numOrderId'], 'integer'],
            [['resnoLab'], 'string', 'max' => 6],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['resnoLab', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'unique', 'targetAttribute' => ['resnoLab', 'requestId', 'cropId', 'workFlowId', 'numOrderId']],
            [['resnoLab'], 'exist', 'skipOnError' => true, 'targetClass' => FinancialAccount::className(), 'targetAttribute' => ['resnoLab' => 'resnoLab']],

            // [
            //     [
            //         'requestId',
            //         'cropId',
            //         'workFlowId',
            //         'numOrderId'
            //     ],
            //     'exist',
            //     'skipOnError' => true,
            //     'targetClass' => RequestProcess::className(),
            //     'targetAttribute' => [
            //         'requestId' => 'requestId',
            //         'cropId' => 'cropId',
            //         'workFlowId' => 'workFlowId',
            //         'numOrderId' => 'numOrderId'
            //     ]
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'resnoLab' => 'Resno Lab',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialConceptOnes()
    {
        return $this->hasMany(FinancialConceptOne::className(), ['resnoLab' => 'resnoLab', 'requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialAccount()
    {
        return $this->hasOne(FinancialAccount::className(), ['resnoLab' => 'resnoLab']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(RequestProcess::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
