<?php

namespace frontend\modules\financial\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\financial\models\FinancialConceptOne;

/**
 * FinancialConceptOneSearch represents the model behind the search form of `frontend\modules\financial\models\FinancialConceptOne`.
 */
class FinancialConceptOneSearch extends FinancialConceptOne
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['essayId', 'numberOfExperiments', 'numberOfPathogens', 'samples', 'costId', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'integer'],
            [['statusConcept', 'statusBus', 'period', 'account', 'resno', 'bus', 'task', 'crop', 'testOfLaboratory', 'owner', 'pathogens', 'descriptionFinal', 'ACE', 'agreement', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status', 'resnoLab', 'dateTo', 'projectDescription', 'busDescription', 'busStatus', 'resourceId', 'taskDescription', 'taskStatus', 'email'], 'safe'],
            [['fee', 'total', 'percent', 'totalPercentage'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancialConceptOne::find();

        $query->andWhere(
            [
                'FinancialConceptOne.deletedBy' => null,
                'FinancialConceptOne.deletedAt' => null,
                'FinancialConceptOne.status' => 'active',
                'FinancialConceptOne.statusConcept' => 'to_send'
            ]
        );

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['requestId' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'essayId' => $this->essayId,
            'numberOfExperiments' => $this->numberOfExperiments,
            'numberOfPathogens' => $this->numberOfPathogens,
            'samples' => $this->samples,
            'costId' => $this->costId,
            'fee' => $this->fee,
            'total' => $this->total,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'dateTo' => $this->dateTo,
            'requestId' => $this->requestId,
            'cropId' => $this->cropId,
            'workFlowId' => $this->workFlowId,
            'numOrderId' => $this->numOrderId,
            'percent' => $this->percent,
            'totalPercentage' => $this->totalPercentage,
        ]);

        $query->andFilterWhere(['like', 'statusConcept', $this->statusConcept])
            ->andFilterWhere(['like', 'statusBus', $this->statusBus])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'account', $this->account])
            ->andFilterWhere(['like', 'resno', $this->resno])
            ->andFilterWhere(['like', 'bus', $this->bus])
            ->andFilterWhere(['like', 'task', $this->task])
            ->andFilterWhere(['like', 'crop', $this->crop])
            ->andFilterWhere(['like', 'testOfLaboratory', $this->testOfLaboratory])
            ->andFilterWhere(['like', 'owner', $this->owner])
            ->andFilterWhere(['like', 'pathogens', $this->pathogens])
            ->andFilterWhere(['like', 'descriptionFinal', $this->descriptionFinal])
            ->andFilterWhere(['like', 'ACE', $this->ACE])
            ->andFilterWhere(['like', 'agreement', $this->agreement])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'resnoLab', $this->resnoLab])
            ->andFilterWhere(['like', 'tab', $this->tab])
            ->andFilterWhere(['like', 'projectDescription', $this->projectDescription])
            ->andFilterWhere(['like', 'busDescription', $this->busDescription])
            ->andFilterWhere(['like', 'busStatus', $this->busStatus])
            ->andFilterWhere(['like', 'resourceId', $this->resourceId])
            ->andFilterWhere(['like', 'taskDescription', $this->taskDescription])
            ->andFilterWhere(['like', 'taskStatus', $this->taskStatus])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
