<?php

namespace frontend\modules\security\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use common\models\User;
use frontend\modules\configuration\models\Parameter;

class AuthenticationAssignmentController extends Controller
{
  //BEHAVIORS
  public function behaviors()
  {
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'only' => [],
        'rules' => [
          [
            'allow' => false,
            'actions' =>  [],
            'roles' => ['?'],
          ],
          [
            'allow' => true,
            'actions' => [],
            'roles' => ['@'],
          ],
        ],
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
          Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
          return $this->goHome();
        }
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function actionIndex()
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $auth = Yii::$app->authManager;
      $postArray = Yii::$app->request->post();
      // -----------Assignments USERS BY ROLE-----------//
      $roleName1 = isset($postArray['UserByRoleCtrlRole']) ? $postArray['UserByRoleCtrlRole'] : null;
      $valueUser =  $auth->getUserIdsByRole($roleName1);
      // -----------Assignments CHID ROLES BY ROLE-----------//
      $roleName2 = isset($postArray['ChildRoleByRoleCtrlRole']) ? $postArray['ChildRoleByRoleCtrlRole'] : null;
      if (is_null($roleName2)) {
        $valueChildRole = null;
      } else {
        $getChildRoles = $auth->getChildRoles($roleName2);
        $valueChildRole = array_map(create_function('$o', 'return $o->name;'), $getChildRoles);
        ArrayHelper::remove($valueChildRole, $roleName2);
      }
      // -----------Assignments PERMISSIONS BY ROLE-----------//
      $roleName3 = isset($postArray['PermissionByRoleCtrlRole']) ? $postArray['PermissionByRoleCtrlRole'] : null;
      $valuePermission = $this->getPermissionsByRole($roleName3);
      // -----------Data-----------//
      $getRoles = $auth->getRoles();
      $dataRole = array_map(create_function('$o', 'return $o->name;'), $getRoles);
      $dataChildRole = array_map(create_function('$o', 'return $o->name;'), $getRoles);
      ArrayHelper::remove($dataChildRole, $roleName2);
      ArrayHelper::remove($dataChildRole, 'system_admin');
      $user = User::find()->all();
      $dataUser = ArrayHelper::map($user, 'id', 'username');

      return $this->render('index', [
        // -----------Data-----------//
        'dataUser' => $dataUser,
        'dataRole' => $dataRole,
        'dataChildRole' => $dataChildRole,
        // -----------Assignments USERS BY ROLE----------- //
        'roleName1' => $roleName1,
        'valueUser' => $valueUser,
        // -----------Assignments ROLES BY ROLE----------- //
        'roleName2' => $roleName2,
        'valueChildRole' => $valueChildRole,
        // -----------Assignments PERMISSIONS BY ROLE-----------//
        'roleName3' => $roleName3,
        'valuePermission' => $valuePermission,
      ]);
    } else {
      Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->goHome();
    }
  }
  //---------------------------------------------------------------Add Remove Role By Role
  public function actionAddRemoveChildRoleByRole()
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {

      $postArray = Yii::$app->request->post();
      $roleName = isset($postArray['ChildRoleByRoleCtrlRole']) ? $postArray['ChildRoleByRoleCtrlRole'] : null;
      $newChildRoleArray = isset($postArray['ChildRoleByRoleCtrlChildRole']) ? $postArray['ChildRoleByRoleCtrlChildRole'] : null;

      if (!is_null($roleName)) {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($roleName);
        $oldChildRolesArray =  $auth->getChildRoles($roleName);
        try {
          if (!empty($oldChildRolesArray)) {
            foreach ($oldChildRolesArray as $oldChildRoleName => $oldChildRole) {
              if ($roleName !== 'system_admin')
                if ($roleName !== $oldChildRoleName)
                  $auth->removeChild($role, $oldChildRole);
            }
          }
        } catch (\Throwable $th) {
          $th->getMessage();
        }

        try {
          if (!empty($newChildRoleArray)) {

            foreach ($newChildRoleArray as $key => $newChildRoleName) {
              $newChildRole = $auth->getRole($newChildRoleName);
              if ($auth->canAddChild($role, $newChildRole) or $newChildRoleName != 'system_admin')
                $auth->addChild($role, $newChildRole);
            }
          }
        } catch (\Throwable $th) {
          $th->getMessage();
        }
      }

      Yii::$app->session->setFlash('info', "<h4><i class='icon fa fa-check'></i>Information!</h4> You have successfully add/remove user(s) in a role.");
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect([
        'index'
      ]);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }
  //---------------------------------------------------------------Add Remove User By Role

  public function actionAddRemoveUserByRole()
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $postArray = Yii::$app->request->post();
      $roleName = isset($postArray['UserByRoleCtrlRole']) ? $postArray['UserByRoleCtrlRole'] : null;
      $userIdArrayNew = isset($postArray['UserByRoleCtrlUsers']) ? $postArray['UserByRoleCtrlUsers'] : null;


      if (!is_null($roleName)) {
        $auth = Yii::$app->authManager;
        $getRole = $auth->getRole($roleName);
        $userIdArrayOld = $auth->getUserIdsByRole($roleName);
        if (!empty($userIdArrayOld))
          foreach ($userIdArrayOld as $key => $value) {
            $auth->revoke($getRole, $value);
          }
        if (!empty($userIdArrayNew))
          foreach ($userIdArrayNew as $key => $value) {
            $auth->assign($getRole, $value);
          }
      }

      Yii::$app->session->setFlash('info', "<h4><i class='icon fa fa-check'></i>Information!</h4> You have successfully add/remove user(s) in a role.");
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect([
        'index'
      ]);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  //---------------------------------------------------------------Add Remove Permission By Role

  public function actionAddRemovePermissionByRole()
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $postArray = Yii::$app->request->post();
      $roleName = isset($postArray['PermissionByRoleCtrlRole']) ? $postArray['PermissionByRoleCtrlRole'] : null;
      $permissionArrayNew =  isset($postArray['cbxActionArray']) ? $postArray['cbxActionArray'] : null;
      if (!is_null($roleName)) {
        $auth = Yii::$app->authManager;
        $permissionArrayOld = $auth->getPermissionsByRole($roleName);
        $getRole = $auth->getRole($roleName);
        if (!empty($permissionArrayOld))
          $auth->removeChildren($getRole);
        if (!empty($permissionArrayNew))
          foreach ($permissionArrayNew as $key => $value) {
            $getPermission = $auth->getPermission($value);
            $auth->addChild($getRole, $getPermission);
          }
      }
      Yii::$app->session->setFlash('info', "<h4><i class='icon fa fa-check'></i>Information!</h4> You have successfully add/remove permission(s) in a role.");
      return $this->redirect([
        'index'
      ]);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  public function getPermissionsByRole($roleName = null)
  {
    $auth = Yii::$app->authManager;
    $allObjectsTree = [];
    $allObjectsTree = '';
    if (!is_null($roleName) and !empty($roleName)) {
      $permissions = $auth->getPermissionsByRole($roleName);
      // CONTROLLER_ACTION
      $allObjectsTree = $allObjectsTree . '  
      <label class="control-label">Permission(s) <small>The order of the permissions is by module, control and action.</small></label>
     
      <table class="table table-hover tableModule"  style="margin: auto">
        <tr style="background-color: #bce6e3;">
          <th style="text-align: center; width: 25%;"> <h4> MODULES FROM APPLICATION </h4></th>
          <th style="text-align: center; width: 75%;"> <h4> CONTROLLERS AND ACTIONS </h4></th>
        </tr>';


      $modules = $this->getAllModules();
      foreach ($modules as $key1 => $value1) {
        $allObjectsTree = $allObjectsTree . '<tr>';
        $allObjectsTree = $allObjectsTree . '<td>';
        // MODULES
        $allObjectsTree = $allObjectsTree .
          '<div class="text-justify custom-control custom-switch">
            <input 
              class="custom-control-input" 
              id="cbx_' . $value1 . '" 
              type="checkbox"  
              value="' . $value1 . '">
            <label class="custom-control-label" for= "cbx_' . $value1 . '"><small>' . $value1 . '</small></label> 
          </div>';
        $allObjectsTree = $allObjectsTree . '</td>';

        $allObjectsTree = $allObjectsTree . '<td>';


        //---------------------
        $allObjectsTree = $allObjectsTree .
          '<table class="table table-hover tableController">
            <tr  style="background-color: #e6e6e6;">  
              <th class="text-center border-right-" style="width: 30%;"><h5>Controllers from module</h5></th>      
              <th  class="text-center border-left-" style="width: 70%;"><h5>Actions from Controllers</h5></th>
            </tr>';

        $controllers = $this->getAllControllers($value1);
        foreach ($controllers as $key2 => $value2) {
          $allObjectsTree = $allObjectsTree . '<tr>';
          $allObjectsTree = $allObjectsTree . '<td>';
          // CONTROLLERS
          $allObjectsTree = $allObjectsTree
            . '<div class="text-justify custom-control custom-switch">
                <input 
                  class="class_' . $value1 . ' pull-left custom-control-input" 
                  type="checkbox" 
                  id = "' . $value2['controller'] . '"
                  value="' . $value2['controller'] . '">
                    <label class="custom-control-label" for="' . $value2['controller'] . '" title="' . $value2['controller'] . '">
                      <small>' . explode('-controller', $value2['controller'])[0] . '</small>
                    </label>
                </input>
              </div>';
          $allObjectsTree = $allObjectsTree . '</td>';
          $allObjectsTreeActions = '';
          $actions = $this->getAllActions($value1, $value2['controllerName']);


          $allObjectsTreeActions = $allObjectsTreeActions . '<td>';
          foreach ($actions as $key3 => $value3) {

            $value = strtolower(explode('-controller', $value2['controller'])[0]) . '_' .  strtolower($value3);
            $checked = '';
            if (array_key_exists($value, $permissions))
              $checked = 'checked="true"';
            // ACTIONS
            $allObjectsTreeActions = $allObjectsTreeActions
              . '<div class="text-justify custom-control custom-switch" >
                  <input 
                    class="class_' . $value1 . ' class_' . $value2['controller'] . ' class_' . $value1 . $value3 . ' custom-control-input" 
                    type="checkbox" 
                    name="cbxActionArray[]" 
                    id = "' . $value . '"  ' . $checked . '"
                    value="' . $value . '"  ' . $checked . ' >
                    <label class="custom-control-label" for="' . $value . '"  ' . $checked . '" title="' . $value3 . '">
                      <small>' . $value3 . '</small>
                    </label>
                </div>';
          }
          $allObjectsTreeActions = $allObjectsTreeActions . '</td>';


          $allObjectsTree = $allObjectsTree .  $allObjectsTreeActions;
          $allObjectsTree = $allObjectsTree . '</tr>';
        }

        $allObjectsTree = $allObjectsTree .
          '</table>';
        //---------------------

        $allObjectsTree = $allObjectsTree . '</td>';

        $allObjectsTree = $allObjectsTree . '</tr>';
      }
      $allObjectsTree = $allObjectsTree .
        '</table>';
    }
    return  $allObjectsTree;
  }

  public function getAllModules()
  {
    $modules = scandir('../modules');
    ArrayHelper::remove($modules, '0');
    ArrayHelper::remove($modules, '1');
    $modules = array_values($modules);
    return $modules;
  }

  public function getAllControllers($module)
  {
    $modules = scandir('../modules/' . $module . '/controllers');
    $controllers = [];
    foreach ($modules as $key => $value) {
      if ($controller = $this->extractController($value)) {
        $controllers[] = [
          'controllerName' => $controller,
          'controller' => substr(strtolower(preg_replace('/(?<!\ )[A-Z]/', '-$0', $controller)), 1, 255),
        ];
      }
    }
    return $controllers;
  }

  public function getAllActions($module, $controller)
  {
    $class = 'frontend\modules\\' . $module . '\\controllers\\' . $controller;
    $functions = [];
    $actions = [];
    $functions = get_class_methods($class);

    array_walk($functions, function (&$element) {
      return $element = strtolower(preg_replace('/(?<!\ )[A-Z]/', '-$0', $element));
    });

    $actions = array_filter($functions, function ($element) {
      return (strpos($element, 'action-') !== false
        and strpos($element, 'bind-action-params') === false);
    });

    array_walk($actions, function (&$element) {
      return $element = explode('action-', $element)[1];
    });

    return $actions;
  }

  protected function extractController($name)
  {
    $filename = explode('.php', $name);
    if (count(explode('Controller.php', $name)) > 1) {
      if (count($filename) > 1) {
        return $filename[0];
      }
    }
    return false;
  }

  //---------------------------------------------------------------
}
