<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

$this->title = 'Authentication and Assignment';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$js_code = <<<JS
<script language="JavaScript" type="text/javascript">

$(document).ready(function() {
	$("input:checkbox").click(function(event) {
		var val01 = $(this).val();
		var class01 = ".class_" + val01;
		if (this.checked) {
			$(class01).each(function() {
				this.checked = true;
			});
		} else {
			$(class01).each(function() {
				this.checked = false;
			});
		}
  });
});

</script>
JS;
?>


<div class="authentication-index">

  <h1><?= Html::encode($this->title) ?> </h1>








  <?php Pjax::begin(['enablePushState' => true]); ?>


  <!--♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦FIRST BOX♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦-->
  <?php $form = ActiveForm::begin(
    [
      'options' => [
        'data-pjax' => true,
        'id' => 'dynamic-form111',
      ]
    ]
  ); ?>
  <div class='box'>
    <div class='box-header'>
      <div class="row">
        <div class="col-md-12">
          <h3>Assignments <small>USERS BY ROLE</small></h3>
          It is allowed to assign one or more roles to a user.<i class="icon fa fa-info-circle"></i>
        </div>
      </div>
    </div>
    <div class='box-body'>
      <div class="row">
        <div class="col-md-12">
          <label class="control-label">Role</label>
          <?php
          echo
            Select2::widget([
              'id' => 'select1_01',
              'name' => 'UserByRoleCtrlRole',
              'data' => $dataRole,
              'value' => $roleName1,
              'options' => [
                'placeholder' => 'Select a type ...',
              ],
              'pluginEvents' => [
                'select2:select' => 'function() {  
                  document.getElementById("dynamic-form111").submit();
                  }',
              ]
            ]);
          ?>
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-md-12">
          <label class="control-label">User(s)</label>
          <?php
          echo Select2::widget([
            'id' => 'select1_02',
            'name' => 'UserByRoleCtrlUsers',
            'data' => $dataUser,
            'value' => $valueUser,
            'options' => [
              'placeholder' => 'Select users ...',
              'multiple' => true
            ],
          ]);
          ?>
        </div>
      </div>
    </div>
    <div class='box-footer'>
      <div class="row">
        <div class="col-md-12">
          <?= Html::a(
            'Add/Remove users by role',
            [
              'add-remove-user-by-role',
            ],
            [
              'class' => 'btn btn-primary pull-right',
              'title'        => 'Add/Remove User(s) By Role',
              'data-method'  => 'post',
            ]
          ); ?>

        </div>
      </div>
    </div>

  </div>
  <?php ActiveForm::end(); ?>
  <!--♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦-->


  <!--♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦SECOND BOX♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦-->
  <?php $form = ActiveForm::begin(
    [
      'options' => [
        'data-pjax' => true,
        'id' => 'dynamic-form112',
      ]
    ]
  ); ?>
  <div class='box'>
    <div class='box-header'>
      <div class="row">
        <div class="col-md-12">
          <h3>Assignments <small>ROLE CHILD BY ROLE</small></h3>
          It is allowed to assign one or more roles children to a role.<i class="icon fa fa-info-circle"></i>
        </div>
      </div>
    </div>
    <div class='box-body'>
      <div class="row">
        <div class="col-md-12">
          <label class="control-label">Role</label>
          <?php
          echo
            Select2::widget([
              'id' => 'select2_01',
              'name' => 'ChildRoleByRoleCtrlRole',
              'data' => $dataRole,
              'value' => $roleName2,
              'options' => [
                'placeholder' => 'Select a type ...',
              ],
              'pluginEvents' => [
                'select2:select' => 'function() {  
                  document.getElementById("dynamic-form112").submit();
                  }',
              ]
            ]);
          ?>
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-md-12">
          <label class="control-label">Child(ren) Role(s)</label>
          <?php
          echo Select2::widget([
            'id' => 'select2_02',
            'name' => 'ChildRoleByRoleCtrlChildRole',
            'data' => $dataChildRole,
            'value' => $valueChildRole,
            'options' => [
              'placeholder' => 'Select roles ...',
              'multiple' => true
            ],
          ]);
          ?>
        </div>
      </div>
    </div>
    <div class='box-footer'>
      <div class="row">
        <div class="col-md-12">
          <?= Html::a(
            'Add/Remove roles by role',
            [
              'add-remove-child-role-by-role',
            ],
            [
              'class' => 'btn btn-primary pull-right',
              'title'        => 'Add/Remove Child(ren) By Role',
              'data-method'  => 'post',
            ]
          ); ?>

        </div>
      </div>
    </div>

  </div>
  <?php ActiveForm::end(); ?>
  <!--♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦-->


  <!--♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦THIRD BOX♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦-->
  <?php $form = ActiveForm::begin(
    [
      'options' => [
        'data-pjax' => true,
        'id' => 'dynamic-form113',
      ]
    ]
  ); ?>
  <?php print $js_code; ?>
  <div class='box'>
    <div class='box-header'>
      <div class="row">
        <div class="col-md-12">
          <h3>Assignments <small>PERMISSIONS BY ROLE</small></h3>
          It is allowed to assign one or more permissions in a role.<i class="icon fa fa-info-circle"></i>
        </div>
      </div>
    </div>
    <div class='box-body'>
      <div class="row">
        <div class="col-md-12">


          <label class="control-label">Role</label>


          <?php
          echo
            Select2::widget([
              'name' => 'PermissionByRoleCtrlRole',
              'data' => $dataRole,
              'value' => $roleName3,
              'options' => [
                'placeholder' => 'Select a type ...',
              ],
              'pluginEvents' => [
                'select2:select' => 'function() {  
                  document.getElementById("dynamic-form113").submit();
                  }',
              ]
            ]);
          ?>


        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-md-12">
          <?php
          echo $valuePermission
          ?>
        </div>
      </div>
    </div>
    <div class='box-footer'>
      <div class="row">
        <div class="col-md-12">
          <?= Html::a(
            'Add/Remove permissions by role',
            [
              'add-remove-permission-by-role',
            ],
            [
              'class' => 'btn btn-primary pull-right',
              'title'        => 'save',
              'data-method'  => 'post',
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
  <!--♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦-->


  <?php Pjax::end(); ?>






</div>