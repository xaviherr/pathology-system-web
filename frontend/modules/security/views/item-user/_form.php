<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="item-user-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

  <?php
  // echo $form->field($model, 'status')->dropDownList(
  //   [
  //     'active' => 'Active',
  //     'disabled' => 'Disabled',
  //   ],
  //   [
  //     'prompt' => 'Select...',
  //     'options' => [
  //       'active'  => [
  //         'selected' => true
  //       ]
  //     ]
  //   ]
  // ) 
  ?>

  <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>