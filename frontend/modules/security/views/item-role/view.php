<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Crop */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = ['label' => 'Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="crop-view">

  <?= DetailView::widget([
    'model' => $model,
    'options' => ['class' => 'table table-condensed'],

    'attributes' => [
      'name',
      'type',
      'description',
      'data',
      [
        'attribute' => 'createdAt',
        'value' => date('d/M/Y', $model->createdAt),
      ],
      [
        'attribute' => 'updatedAt',
        'value' => date('d/M/Y', $model->updatedAt),
      ],
      'ruleName',
    ],
  ]) ?>
</div>