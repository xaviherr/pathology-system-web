<?php

$this->title = 'Create Role';
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-role-create">
  <h1>
    <?php
    // echo Html::encode($this->title) 
    ?>
  </h1>

  <?= $this->render('_form', [
    'objectRoleOld' => $objectRoleOld,
  ]) ?>

</div>