<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="item-role-form">

  <?php
  $form = ActiveForm::begin([
    'id' => 'item-form',
  ]);
  ?>

  <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

  <?php echo Html::submitButton(
    'Save',
    [
      'class' => 'btn btn-success',
      'style' => 'margin-bottom: 15px'
    ]
  ) ?>

  <?php
  ActiveForm::end();
  ?>

</div>

<!-- 
=======================================================================================
 $form->field(/*...*/)->textInput(
   [
     'disabled' => !\Yii::$app->user->can('editFormField')
   ]
 );

=======================================================================================


  IN function rules
  
  [['role'], \Yii::$app->user->can('editFormField') ? 'safe' : 'unsafe'], 
=======================================================================================
-->