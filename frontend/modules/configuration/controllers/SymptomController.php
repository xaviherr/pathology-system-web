<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\Symptom;
use frontend\modules\configuration\models\SymptomSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SymptomController implements the CRUD actions for Symptom model.
 */
class SymptomController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Symptom models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $searchModel = new SymptomSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination = false;
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->goHome();
        }
    }

    /**
     * Displays a single Symptom model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Creates a new Symptom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = new Symptom();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Updates an existing Symptom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Deletes an existing Symptom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            // $this->findModel($id)->delete();
            $model = $this->findModel($id);
            $model->deletedAt = date("Y-m-d H:i:s", time());
            $model->deletedBy = Yii::$app->user->identity->username;
            $model->save();
            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Finds the Symptom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Symptom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Symptom::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
