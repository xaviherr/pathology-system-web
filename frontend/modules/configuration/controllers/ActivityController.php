<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\ActivitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ActivityController implements the CRUD actions for Activity model.
 */
class ActivityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Activity models.
     * @return mixed
     */
    public function actionIndex($id = null, $from = null)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $searchModel = new ActivitySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination = false;
            /***********************************************/
            if (!is_null($id)) {
                $action = Yii::$app->controller->action;
                Yii::$app->session->setFlash('success',  "<h4><i class='icon fa fa-check'></i>Success!</h4> You have successfully " . $from . " a new " . $action->controller->id . " (Id: " . (string) $id . ")");
            }
            /***********************************************/
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->goHome();
        }
    }

    /**
     * Displays a single Activity model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Creates a new Activity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = new Activity();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /***********************************************/
                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect([
                    'index',
                    'id' => $model->id,
                    'from' => Yii::$app->controller->action->id,
                ]);
                /***********************************************/
            }
            return $this->renderAjax('create', ['model' => $model,]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Updates an existing Activity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect(['index']);
            }
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Deletes an existing Activity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = $this->findModel($id);
            $model->deletedAt = date("Y-m-d H:i:s", time());
            $model->deletedBy = Yii::$app->user->identity->username;
            $model->save();
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Finds the Activity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Activity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Activity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
