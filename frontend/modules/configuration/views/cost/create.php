<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Cost */

$this->title = 'Create Cost';
$this->params['breadcrumbs'][] = ['label' => 'Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-create">
    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>