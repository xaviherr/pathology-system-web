<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Essay */

$this->title = 'Create Essay';
$this->params['breadcrumbs'][] = ['label' => 'Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="essay-create">
    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>