<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\EssaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Essays';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="essay-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box-body">
        <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Create <?php echo Html::encode($this->title) ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-create-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>View <?php echo Html::encode($this->title) ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-view-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Edit <?php echo Html::encode($this->title) ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-update-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box collapsed-box">
        <div class="box-header">
            <button type="button" class="btn btn-primary" data-widget="collapse">
                <i class="fa fa-search"></i> Advanced Search
            </button>
        </div>
        <div class="box-body">
            <?php
            echo $this->render('_search', ['model' => $searchModel]);
            ?>
        </div>

    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-bars "></i> <?= Html::encode($this->title) ?> List</h3>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout'       => "{items}",
                'options'       => ['style' => 'font-size:12px;'],
                'rowOptions' => function ($model) {
                    if ($model->status == "disabled") {
                        return ['class' => 'danger'];
                    } else if ($model->status == "active") {
                        return ['class' => 'success'];
                    }
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute'     => 'shortName',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'longName',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'description',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'status',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'costId',
                        'value'         => 'cost.code',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'costId',
                        'header'        => 'Cost',
                        'value'         => 'cost.cost',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'costId',
                        'header'        => 'Currency',
                        'value'         => 'cost.parameter.shortName',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'essayTypeId',
                        'header'        => 'Type',
                        'value'         => 'essayType.shortName',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'locationId',
                        'header'        => 'Location',
                        'value'         => 'location.shortName',
                        'enableSorting' => false,
                    ],
                    [
                        'class'         => 'yii\grid\ActionColumn',
                        'header'        => 'Action',
                        'template'      => '{view} {update} ',
                        'headerOptions' => ['width' => '50'],
                        'buttons'       => [
                            'view' => function ($url, $model, $key) {
                                return Html::button(
                                    '<span class="glyphicon glyphicon-eye-open"></span>',
                                    [
                                        'title' => Yii::t('yii', 'View'),
                                        'class' => 'btn btn-default btn-xs modal-view-action',
                                        'value' => Url::to(
                                            [
                                                'essay/view',
                                                'id' => $key
                                            ]
                                        ),
                                    ]
                                );
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::button(
                                    "<span class='glyphicon glyphicon-pencil'></span>",
                                    [
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-default btn-xs modal-update-action',
                                        'value' => Url::to(
                                            [
                                                'essay/update',
                                                'id' => $key
                                            ]
                                        ),

                                    ]
                                );
                            },
                        ],
                    ],
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered table-striped dataTable',
                    'id'    => "gvEssay",
                ],
            ]); ?>
        </div>

        <div class="box-footer">
            <div class="col-md-12">

                <?php
                echo Html::button(
                    'Create ' . Html::encode($this->title),
                    [
                        'value' => Url::to(['essay/create']),
                        'class' => 'btn btn-success modal-create-action',
                    ]
                )
                ?>

            </div>
        </div>
    </div>
</div>