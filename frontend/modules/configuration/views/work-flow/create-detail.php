<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = $model->shortName;

$this->params['breadcrumbs'][] = ['label' => 'Work-Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="essay-create">
    <?php $form = ActiveForm::begin(
		[
			'action' =>
			[
				'work-flow/create-detail',
				'workFlowId' => $workFlowId,
				'cropId' => $cropId,
			],
			'method' => 'post',
		]
	); ?>


    <span>
        <h2> Essays By Work-Flow [ <small> <?= Html::encode($this->title) ?> </small>] </h2>
    </span>

    <div class="row">

        <div class="col-md-12">
            <div class="box">

                <div class="box-header with-border">

                    <h3 class="box-title"><i class="fa fa-eyedropper"></i> Details Work-Flow</h3>


                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>

                </div>

                <div class="box-body">

                    <div class="col-md-12">
                        <h4> <?= Html::encode($this->title) ?> <h4>
                    </div>

                    <div class="col-md-3">
                        <?= DetailView::widget([
							'model' => $model,
							'options' => ['class' => 'table table-condensed'],
							'attributes' => [

								'id',
								'longName',

							],
						]) ?>
                    </div>

                    <div class="col-md-3">
                        <?= DetailView::widget([
							'model' => $model,
							'options' => ['class' => 'table table-condensed'],
							'attributes' => [

								[
									'attribute' => 'cropId',
									'value' => $model->crop == null ? '(not-set)' : $model->crop->shortName . " - " . $model->crop->longName,
									'contentOptions' => $model->crop == null ? ['class' => 'not-set'] : ['class' => '']
								], // 'crop.shortName', //It works well too!!!
								'description',

							],
						]) ?>
                    </div>

                    <div class="col-md-3">
                        <?= DetailView::widget([
							'model' => $model,
							'options' => ['class' => 'table table-condensed'],
							'attributes' => [

								'registeredBy',
								'registeredAt',

							],
						]) ?>
                    </div>

                    <div class="col-md-3">
                        <?= DetailView::widget([
							'model' => $model,
							'options' => ['class' => 'table table-condensed'],
							'attributes' => [

								'updatedBy',
								'updatedAt',

							],
						]) ?>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box">

                <div class="box-header with-border">

                    <h3 class="box-title"><i class="fa fa-plus"></i> Add Essays By Work-Flows</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>

                </div>

                <div class="box-body">
                    <div class="col-md-7">

                        <?= GridView::widget([
							'dataProvider' => $dataProviderEssays,
							//'filterModel'  => $searchModelPathogens,
							// 'layout'=>"{summary}\n{items}\n{pager}"
							'layout'       => "{items}",
							'options' => ['style' => 'font-size:12px;'],
							'columns' => [
								['class' => 'yii\grid\SerialColumn'],

								[
									'attribute'     => 'shortName',
									'enableSorting' => false,
								],
								[
									'attribute'     => 'longName',
									'enableSorting' => false,
								],
								[
									'attribute'     => 'description',
									'enableSorting' => false,
								],
								[
									'attribute'     => 'costId',
									'value'     => 'cost.code',
									'enableSorting' => false,
								],
								[
									'attribute'     => 'costId',
									'value'         => 'cost.cost',
									'enableSorting' => false,
								],
								['class' => 'yii\grid\CheckboxColumn'],
							],
							/**************************************************/
							'tableOptions' => [
								'class' => 'table table-bordered table-striped dataTable',
								'id'    => "gvEssayList",
							],
							/**************************************************/
						]); ?>

                    </div>

                    <div class="col-md-1 text-center">

                        <?= Html::submitButton(
							'<i class="fa fa-plus "></i> Add & Save',
							[
								'class' => 'btn btn-defrault-cip btn-app',
							]
						)
						?>

                    </div>

                    <div class="col-md-4">
                        <?= GridView::widget([
							'dataProvider' => $dataProvider,
							//'filterModel' => $searchModel,
							'layout'       => "{items}",
							'options' => ['style' => 'font-size:12px;'],
							'columns' => [
								['class' => 'yii\grid\SerialColumn'],












								[
									'attribute'     => 'workFlowId',
									'value'         => 'workFlow.shortName',
									'enableSorting' => false,
								],

								[
									'attribute'     => 'cropId',
									'value'         => 'crop.shortName',
									'enableSorting' => false,
								],

								[
									'attribute'     => 'essayId',
									'value'         => 'essay.shortName',
									'enableSorting' => false,
								],

								[
									'class' => 'yii\grid\ActionColumn',
									'header'        => 'Delete',
									'template'      => '{delete}',
									'buttons'       => [

										'delete' => function ($url, $model, $key) {
											return Html::a(
												'<span class="glyphicon glyphicon-trash"></span>',
												null,
												[
													'title' => Yii::t('yii', 'Delete'),
													'href' => Yii::$app->urlManager->createUrl(
														[
															'configuration/work-flow/deleteebw',
															'workFlowId'  => ArrayHelper::getValue($key, 'workFlowId'),
															'cropId' => ArrayHelper::getValue($key, 'cropId'),
															'essayId' => ArrayHelper::getValue($key, 'essayId'),

														]
													),
													'aria-label' => "Delete",
													'data-pjax' => "0",
													'data-confirm' => "Are you sure you want to delete this item?",
													'data-method' => "post",
												]
											);
										},
									]
								],
							],
							/**************************************************/
							'tableOptions' => [
								'class' => 'table table-bordered table-striped dataTable',
								'id'    => "gvEssayByWorkFlowList",
							],
							/**************************************************/
						]); ?>

                    </div>

                </div>

                <div class="box-footer">
                    <div class="col-md-12">

                        <?= Html::a(
							'<i class="fa fa-bars "></i> Go List',
							[
								'index',
							],
							[
								'class' => 'btn btn-success-cip btn-app'
							]
						) ?>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div> 