<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\WorkFlow */

$this->title = 'Create Work Flow';
$this->params['breadcrumbs'][] = ['label' => 'Work Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-flow-create">

    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>