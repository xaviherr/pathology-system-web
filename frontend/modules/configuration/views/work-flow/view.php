<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\WorkFlow */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="work-flow-view">
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            'id',
            'shortName',
            'longName',
            'description',
            'status',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'repetition',
        ],
    ]) ?>
</div>