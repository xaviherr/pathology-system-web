<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

use yii\helpers\ArrayHelper;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\WorkFlow;

use yii\bootstrap\Modal;
use yii\helpers\Url;

use yii\widgets\ActiveForm;



$this->title = $model->shortName;

?>
<div class="essay-by-work-flow">

    <span>
        <h2> <?= Html::encode($this->title) ?> </h2>
    </span>

    <div class="row">





        <div class="col-md-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title"> Details WorkFlow</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'shortName',
                            'longName',
                            'description',

                            'registeredBy',
                            'registeredAt',
                        ],
                    ]) ?>

                </div>

            </div>
        </div>

    </div>



    <div class="row">



        <div class="col-md-8">
            <div class="box">

                <div class="box-header with-border">
                    <!-- <h3 class="box-title"> Essays By Diagnostics</h3>  -->
                    <h3 class="box-title"> Essays By WorkFlow</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => ['style' => 'font-size:12px;'],
                        //'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'workFlowId',
                                'value' => 'workFlow.shortName',
                            ],
                            [
                                'attribute' => 'essayId',
                                'value' => 'essay.shortName',
                            ],
                            'registeredBy',
                            'registeredAt',
                        ],
                    ]); ?>

                </div>

            </div>
        </div>




        <div class="col-md-4">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title"> Add Essays</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($modelAddEssay, 'essayId')->dropDownList(
                        ArrayHelper::map(
                            Essay::find()
                                ->where(['deletedBy' => null, 'deletedAt' => null])
                                ->all(),
                            'id',
                            'shortName'
                        ),
                        ['prompt' => 'Select...', ]
                    ) ?>
                    <?= $form->field($modelAddEssay, 'numOrder')->textInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton('Add', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>

            </div>
        </div>

    </div>


</div><!-- essay-by-work-flow --> 