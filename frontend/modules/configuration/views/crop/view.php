<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="crop-view">

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            'id',
            'shortName',
            'longName',
            'description',
            'status',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
        ],
    ]) ?>

</div>