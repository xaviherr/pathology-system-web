<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Activity */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-view">

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            'id',
            'shortName',
            'longName',
            'description',
            'status',
            'details',
            [
                'label' => 'Type',
                'value' => $model->activityType->shortName,
            ],
            [
                'label' => 'Data Source',
                'value' => $model->activityDataSource->shortName,
            ],

            [
                'label' => 'Property',
                'value' => $model->activityProperty->shortName,
            ],
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
        ],
    ]) ?>

</div>