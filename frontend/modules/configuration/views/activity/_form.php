<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shortName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'activityTypeId')->dropDownList(
        $model->getActivityTypeArray(),
        [
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'activityDataSourceId')->dropDownList(
        $model->getActivityDataSourceArray(),
        [
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'activityPropertyId')->dropDownList(
        $model->getActivityPropertyArray(),
        [
            'prompt' => 'Select...',
        ]
    ) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>