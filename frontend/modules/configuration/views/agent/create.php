<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Agent */

$this->title = 'Create Agent';
$this->params['breadcrumbs'][] = ['label' => 'Agents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-create">
    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>