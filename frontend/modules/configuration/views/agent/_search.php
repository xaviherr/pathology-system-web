<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agent-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'id') ?>
            <?= $form->field($model, 'shortName') ?>
            <?= $form->field($model, 'longName') ?>
            <?= $form->field($model, 'description') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(
                [
                    'active' => 'Active',
                    'disabled' => 'Disabled',
                ],
                [
                    'prompt' => 'All',
                ]
            ) ?>
            <?php echo $form->field($model, 'agentGroupId') ?>
            <?php echo $form->field($model, 'agentTypeId') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <i class="icon fa fa-info-circle"></i>
            You may optionally enter a comparison operator (<, <=,>, >=, <> or =) at the beginning of each of your search values to specify how the comparison should be done.
                    <br />
                    <br />
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>