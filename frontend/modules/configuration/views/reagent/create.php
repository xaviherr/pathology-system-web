<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Reagent */

$this->title = 'Create Reagent';
$this->params['breadcrumbs'][] = ['label' => 'Reagents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reagent-create">
    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>