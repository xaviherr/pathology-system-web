<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Parameter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parameter-form">

    <?php
    $form = ActiveForm::begin([]);
    ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shortName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'singularity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'entity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <hr>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php
    ActiveForm::end();
    ?>

</div>