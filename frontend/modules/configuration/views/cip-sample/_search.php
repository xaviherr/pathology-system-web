<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\CipSampleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cip-sample-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sampleId') ?>

    <?= $form->field($model, 'CIPNUMBER') ?>

    <?= $form->field($model, 'CIPNUMBER_key') ?>

    <?= $form->field($model, 'CROP') ?>

    <?= $form->field($model, 'WebSMTA_bool') ?>

    <?php // echo $form->field($model, 'WebSMTA2') ?>

    <?php // echo $form->field($model, 'FAO') ?>

    <?php // echo $form->field($model, 'CULTVRNAME') ?>

    <?php // echo $form->field($model, 'COLNUMBER') ?>

    <?php // echo $form->field($model, 'ALTERID') ?>

    <?php // echo $form->field($model, 'OTHER_IDENT') ?>

    <?php // echo $form->field($model, 'SINONIMO') ?>

    <?php // echo $form->field($model, 'CCODE') ?>

    <?php // echo $form->field($model, 'MISSCODE') ?>

    <?php // echo $form->field($model, 'COUNTRY') ?>

    <?php // echo $form->field($model, 'STATE') ?>

    <?php // echo $form->field($model, 'REGION') ?>

    <?php // echo $form->field($model, 'REGION2') ?>

    <?php // echo $form->field($model, 'LOCALITY') ?>

    <?php // echo $form->field($model, 'SOURCE') ?>

    <?php // echo $form->field($model, 'SOURCIP') ?>

    <?php // echo $form->field($model, 'ALT') ?>

    <?php // echo $form->field($model, 'LATCOOR') ?>

    <?php // echo $form->field($model, 'LONGCOOR') ?>

    <?php // echo $form->field($model, 'BIOSTAT') ?>

    <?php // echo $form->field($model, 'SPP') ?>

    <?php // echo $form->field($model, 'SPECIES') ?>

    <?php // echo $form->field($model, 'GENUS') ?>

    <?php // echo $form->field($model, 'PopulationGroup') ?>

    <?php // echo $form->field($model, 'FEMALE') ?>

    <?php // echo $form->field($model, 'MALE') ?>

    <?php // echo $form->field($model, 'DCCODE') ?>

    <?php // echo $form->field($model, 'DCTRY') ?>

    <?php // echo $form->field($model, 'DATERCVD') ?>

    <?php // echo $form->field($model, 'DATECOL') ?>

    <?php // echo $form->field($model, 'DONOR') ?>

    <?php // echo $form->field($model, 'COMMENTS') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <?php // echo $form->field($model, 'Health_Status') ?>

    <?php // echo $form->field($model, 'Distribution_Status') ?>

    <?php // echo $form->field($model, 'STORA') ?>

    <?php // echo $form->field($model, 'BIOSTAT2') ?>

    <?php // echo $form->field($model, 'corporate') ?>

    <?php // echo $form->field($model, 'CULTVRNAME2') ?>

    <?php // echo $form->field($model, 'FEMALE2') ?>

    <?php // echo $form->field($model, 'MALE2') ?>

    <?php // echo $form->field($model, 'COLNUMBER2') ?>

    <?php // echo $form->field($model, 'Health_Status2') ?>

    <?php // echo $form->field($model, 'Corporativa') ?>

    <?php // echo $form->field($model, 'CREATE') ?>

    <?php // echo $form->field($model, 'CIPFBS') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
