<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\CipSample */

$this->title = 'Update Cip Sample: ' . $model->sampleId;
$this->params['breadcrumbs'][] = ['label' => 'Cip Samples', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sampleId, 'url' => ['view', 'id' => $model->sampleId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cip-sample-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
