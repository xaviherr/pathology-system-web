<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\CipSample */

$this->title = $model->sampleId;
$this->params['breadcrumbs'][] = ['label' => 'Cip Samples', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cip-sample-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sampleId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sampleId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sampleId',
            'CIPNUMBER',
            'CIPNUMBER_key',
            'CROP',
            'WebSMTA_bool',
            'WebSMTA2',
            'FAO',
            'CULTVRNAME',
            'COLNUMBER',
            'ALTERID',
            'OTHER_IDENT',
            'SINONIMO',
            'CCODE',
            'MISSCODE',
            'COUNTRY',
            'STATE',
            'REGION',
            'REGION2',
            'LOCALITY',
            'SOURCE',
            'SOURCIP',
            'ALT',
            'LATCOOR',
            'LONGCOOR',
            'BIOSTAT',
            'SPP',
            'SPECIES',
            'GENUS',
            'PopulationGroup',
            'FEMALE',
            'MALE',
            'DCCODE',
            'DCTRY',
            'DATERCVD',
            'DATECOL',
            'DONOR',
            'COMMENTS',
            'STATUS',
            'Health_Status',
            'Distribution_Status',
            'STORA',
            'BIOSTAT2',
            'corporate',
            'CULTVRNAME2',
            'FEMALE2',
            'MALE2',
            'COLNUMBER2',
            'Health_Status2',
            'Corporativa',
            'CREATE',
            'CIPFBS',
        ],
    ]) ?>

</div>
