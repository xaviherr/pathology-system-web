<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "WorkFlow".
 *
 * @property int $id
 * @property string $shortName
 * @property string $longName
 * @property string $description
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property EssayByWorkFlow[] $essayByWorkFlows
 * @property Essay[] $essays
 * @property WorkFlowByCrop[] $workFlowByCrops
 * @property Crop[] $crops
 */
class WorkFlow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'WorkFlow';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shortName', 'longName', 'status'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['shortName', 'longName', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 245],
            [['repetition'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'repetition' => 'Repetition',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssayByWorkFlows()
    {
        return $this->hasMany(EssayByWorkFlow::className(), ['workFlowId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::className(), ['id' => 'essayId'])->viaTable('EssayByWorkFlow', ['workFlowId' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkFlowByCrops()
    {
        return $this->hasMany(WorkFlowByCrop::className(), ['workFlowId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrops()
    {
        return $this->hasMany(Crop::className(), ['id' => 'cropId'])->viaTable('WorkFlowByCrop', ['workFlowId' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
