<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Cost;

/**
 * CostSearch represents the model behind the search form of `frontend\modules\configuration\models\Cost`.
 */
class CostSearch extends Cost
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'period'], 'integer'],
            [['code', 'description', 'status', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'currencyTypeId'], 'safe'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cost::find();

        // add conditions that should always apply here
        $query->andWhere(['Cost.deletedBy' => null, 'Cost.deletedAt' => null, 'Cost.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'code' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Cost.id' => $this->id,
            'Cost.cost' => $this->cost,
            'Cost.period' => $this->period,
            'Cost.registeredAt' => $this->registeredAt,
            'Cost.updatedAt' => $this->updatedAt,
            'Cost.deletedAt' => $this->deletedAt,
            'Cost.currencyTypeId' => $this->currencyTypeId,
        ]);

        $query->andFilterWhere(['like', 'Cost.code', $this->code])
            ->andFilterWhere(['like', 'Cost.description', $this->description])
            ->andFilterWhere(['like', 'Cost.status', $this->status])
            ->andFilterWhere(['like', 'Cost.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Cost.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Cost.deletedBy', $this->deletedBy]);

        return $dataProvider;
    }
}
