<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Parameter;

/**
 * ParameterSearch represents the model behind the search form of `frontend\modules\configuration\models\Parameter`.
 */
class ParameterSearch extends Parameter
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['singularity', 'code', 'entity', 'shortName', 'longName', 'description', 'details', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Parameter::find();

        // add conditions that should always apply here
        $query->andWhere(
            [
                'Parameter.deletedBy' => null,
                'Parameter.deletedAt' => null,
                //'Parameter.status' => 'active'
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' =>
            [
                'defaultOrder' =>
                [
                    'code' => SORT_ASC,
                    'entity' => SORT_ASC,
                    'singularity' => SORT_ASC,
                    'shortName' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query
            ->andFilterWhere(['like', 'singularity', $this->singularity])
            ->andFilterWhere(['like', 'entity', $this->entity])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'shortName', $this->shortName])
            ->andFilterWhere(['like', 'longName', $this->longName])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
