<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Symptom;

/**
 * SymptomSearch represents the model behind the search form of `frontend\modules\configuration\models\Symptom`.
 */
class SymptomSearch extends Symptom
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['shortName', 'longName', 'description', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Symptom::find();

        // add conditions that should always apply here
        $query->andWhere(['Symptom.deletedBy' => null, 'Symptom.deletedAt' => null, 'Symptom.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['shortName' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Symptom.id' => $this->id,
            'Symptom.registeredAt' => $this->registeredAt,
            'Symptom.updatedAt' => $this->updatedAt,
            'Symptom.deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'Symptom.shortName', $this->shortName])
            ->andFilterWhere(['like', 'Symptom.longName', $this->longName])
            ->andFilterWhere(['like', 'Symptom.description', $this->description])
            ->andFilterWhere(['like', 'Symptom.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Symptom.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Symptom.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'Symptom.status', $this->status]);

        return $dataProvider;
    }
}
