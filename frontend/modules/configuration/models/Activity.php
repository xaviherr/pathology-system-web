<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;
use frontend\modules\control\models\ActivityByEssay;

/**
 * This is the model class for table "{{%Activity}}".
 *
 * @property int $id
 * @property string $shortName
 * @property string $longName
 * @property string $description
 * @property string $details
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property ActivityByEssay[] $activityByEssays
 * @property Essay[] $essays
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%Activity}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityTypeId', 'activityDataSourceId', 'activityPropertyId'], 'integer'],
            [['shortName', 'longName'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['shortName', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['longName'], 'string', 'max' => 145],
            [['description', 'details'], 'string', 'max' => 245],
            [['activityTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['activityTypeId' => 'id']],
            [['activityDataSourceId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['activityDataSourceId' => 'id']],
            [['activityPropertyId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['activityPropertyId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'details' => 'Details',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'activityTypeId' => 'Type',
            'activityDataSourceId' => 'Data Source',
            'activityPropertyId' => 'Property',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityByEssays()
    {
        return $this
            ->hasMany(
                ActivityByEssay::className(),
                [
                    'activityId' => 'id'
                ]
            )
            ->andWhere(
                [
                    'status' => 'active'
                ]
            );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(
            Essay::className(),
            [
                'id' => 'essayId'
            ]
        )
            ->viaTable(
                '{{%ActivityByEssay}}',
                [
                    'activityId' => 'id',
                    'status' => 'status'
                ]
            )
            ->andWhere(
                [
                    'status' => 'active'
                ]
            );
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function  getActivityType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'activityTypeId']);
    }

    public function getActivityTypeArray()
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
                'entity' => 'ACTIVITY',
                'singularity' => 'TYPE',
            ]
        )->all(), 'id', 'shortName');
    }

    public function  getActivityDataSource()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'activityDataSourceId']);
    }

    public function getActivityDataSourceArray()
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
                'entity' => 'ACTIVITY',
                'singularity' => 'DATASOURCE',
            ]
        )->all(), 'id', 'shortName');
    }

    public function  getActivityProperty()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'activityPropertyId']);
    }

    public function getActivityPropertyArray()
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
                'entity' => 'ACTIVITY',
                'singularity' => 'PROPERTY',
            ]
        )->all(), 'id', 'shortName');
    }
}
