<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\CipSample;

/**
 * CipSampleSearch represents the model behind the search form of `frontend\modules\configuration\models\CipSample`.
 */
class CipSampleSearch extends CipSample
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sampleId', 'SOURCE', 'ALT', 'DATERCVD', 'DATECOL'], 'integer'],
            [['CIPNUMBER', 'CIPNUMBER_key', 'CROP', 'WebSMTA_bool', 'WebSMTA2', 'FAO', 'CULTVRNAME', 'COLNUMBER', 'ALTERID', 'OTHER_IDENT', 'SINONIMO', 'CCODE', 'MISSCODE', 'COUNTRY', 'STATE', 'REGION', 'REGION2', 'LOCALITY', 'SOURCIP', 'BIOSTAT', 'SPP', 'SPECIES', 'GENUS', 'PopulationGroup', 'FEMALE', 'MALE', 'DCCODE', 'DCTRY', 'DONOR', 'COMMENTS', 'STATUS', 'Health_Status', 'Distribution_Status', 'STORA', 'BIOSTAT2', 'corporate', 'CULTVRNAME2', 'FEMALE2', 'MALE2', 'COLNUMBER2', 'Health_Status2', 'Corporativa', 'CREATE', 'CIPFBS'], 'safe'],
            [['LATCOOR', 'LONGCOOR'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CipSample::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sampleId' => $this->sampleId,
            'SOURCE' => $this->SOURCE,
            'ALT' => $this->ALT,
            'LATCOOR' => $this->LATCOOR,
            'LONGCOOR' => $this->LONGCOOR,
            'DATERCVD' => $this->DATERCVD,
            'DATECOL' => $this->DATECOL,
            'CREATE' => $this->CREATE,
        ]);

        $query->andFilterWhere(['like', 'CIPNUMBER', $this->CIPNUMBER])
            ->andFilterWhere(['like', 'CIPNUMBER_key', $this->CIPNUMBER_key])
            ->andFilterWhere(['like', 'CROP', $this->CROP])
            ->andFilterWhere(['like', 'WebSMTA_bool', $this->WebSMTA_bool])
            ->andFilterWhere(['like', 'WebSMTA2', $this->WebSMTA2])
            ->andFilterWhere(['like', 'FAO', $this->FAO])
            ->andFilterWhere(['like', 'CULTVRNAME', $this->CULTVRNAME])
            ->andFilterWhere(['like', 'COLNUMBER', $this->COLNUMBER])
            ->andFilterWhere(['like', 'ALTERID', $this->ALTERID])
            ->andFilterWhere(['like', 'OTHER_IDENT', $this->OTHER_IDENT])
            ->andFilterWhere(['like', 'SINONIMO', $this->SINONIMO])
            ->andFilterWhere(['like', 'CCODE', $this->CCODE])
            ->andFilterWhere(['like', 'MISSCODE', $this->MISSCODE])
            ->andFilterWhere(['like', 'COUNTRY', $this->COUNTRY])
            ->andFilterWhere(['like', 'STATE', $this->STATE])
            ->andFilterWhere(['like', 'REGION', $this->REGION])
            ->andFilterWhere(['like', 'REGION2', $this->REGION2])
            ->andFilterWhere(['like', 'LOCALITY', $this->LOCALITY])
            ->andFilterWhere(['like', 'SOURCIP', $this->SOURCIP])
            ->andFilterWhere(['like', 'BIOSTAT', $this->BIOSTAT])
            ->andFilterWhere(['like', 'SPP', $this->SPP])
            ->andFilterWhere(['like', 'SPECIES', $this->SPECIES])
            ->andFilterWhere(['like', 'GENUS', $this->GENUS])
            ->andFilterWhere(['like', 'PopulationGroup', $this->PopulationGroup])
            ->andFilterWhere(['like', 'FEMALE', $this->FEMALE])
            ->andFilterWhere(['like', 'MALE', $this->MALE])
            ->andFilterWhere(['like', 'DCCODE', $this->DCCODE])
            ->andFilterWhere(['like', 'DCTRY', $this->DCTRY])
            ->andFilterWhere(['like', 'DONOR', $this->DONOR])
            ->andFilterWhere(['like', 'COMMENTS', $this->COMMENTS])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS])
            ->andFilterWhere(['like', 'Health_Status', $this->Health_Status])
            ->andFilterWhere(['like', 'Distribution_Status', $this->Distribution_Status])
            ->andFilterWhere(['like', 'STORA', $this->STORA])
            ->andFilterWhere(['like', 'BIOSTAT2', $this->BIOSTAT2])
            ->andFilterWhere(['like', 'corporate', $this->corporate])
            ->andFilterWhere(['like', 'CULTVRNAME2', $this->CULTVRNAME2])
            ->andFilterWhere(['like', 'FEMALE2', $this->FEMALE2])
            ->andFilterWhere(['like', 'MALE2', $this->MALE2])
            ->andFilterWhere(['like', 'COLNUMBER2', $this->COLNUMBER2])
            ->andFilterWhere(['like', 'Health_Status2', $this->Health_Status2])
            ->andFilterWhere(['like', 'Corporativa', $this->Corporativa])
            ->andFilterWhere(['like', 'CIPFBS', $this->CIPFBS]);

        return $dataProvider;
    }
}
