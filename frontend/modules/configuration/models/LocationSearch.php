<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Location;

/**
 * LocationSearch represents the model behind the search form of `frontend\modules\configuration\models\Location`.
 */
class LocationSearch extends Location
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['shortName', 'longName', 'description', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Location::find();

        // add conditions that should always apply here
        $query->andWhere(['Location.deletedBy' => null, 'Location.deletedAt' => null, 'Location.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['shortName' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Location.id' => $this->id,
            'Location.registeredAt' => $this->registeredAt,
            'Location.updatedAt' => $this->updatedAt,
            'Location.deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'Location.shortName', $this->shortName])
            ->andFilterWhere(['like', 'Location.longName', $this->longName])
            ->andFilterWhere(['like', 'Location.description', $this->description])
            ->andFilterWhere(['like', 'Location.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Location.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Location.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'Location.status', $this->status]);

        return $dataProvider;
    }
}
