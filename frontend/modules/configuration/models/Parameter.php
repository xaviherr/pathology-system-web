<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Parameter".
 *
 * @property int $id
 * @property string $singularity
 * @property string $entity
 * @property string $shortName
 * @property string $longName
 * @property string $description
 * @property string $details
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property Cost[] $costs
 * @property Pathogen[] $pathogens
 * @property ReadingData[] $readingDatas
 * @property Request[] $requests
 * @property Request[] $requests0
 * @property RequestProcess[] $requestProcesses
 * @property Sample[] $samples
 * @property Support[] $supports
 */
class Parameter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Parameter';
    }


    /********************************************************************************************/
    public static function getUserList()
    {
        return ArrayHelper::map(Parameter::find()->all(), 'entity', 'fullName');
    }

    public function getFullName()
    {
        return $this->entity . ' - ' . $this->singularity;
    }
    /********************************************************************************************/


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['singularity', 'entity', 'shortName', 'longName', 'status'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['code', 'singularity', 'entity', 'shortName', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['longName'], 'string', 'max' => 145],
            [['description', 'details'], 'string', 'max' => 245],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'singularity' => 'Singularity',
            'entity' => 'Entity',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'details' => 'Details',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCosts()
    {
        return $this->hasMany(Cost::className(), ['costCurrencyId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPathogens()
    {
        return $this->hasMany(Pathogen::className(), ['pathogenTypeId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::className(), ['readingDataTypeId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['paymentTypeId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests0()
    {
        return $this->hasMany(Request::className(), ['requestStatusId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::className(), ['processStatusId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSamples()
    {
        return $this->hasMany(Sample::className(), ['sampleTypeId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['supportTypeId' => 'id'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
