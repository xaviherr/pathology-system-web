<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\MainIp */

$this->title = 'Update Main Ip: ' . $model->accession_id_key;
$this->params['breadcrumbs'][] = ['label' => 'Main Ips', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->accession_id_key, 'url' => ['view', 'id' => $model->accession_id_key]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="main-ip-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
