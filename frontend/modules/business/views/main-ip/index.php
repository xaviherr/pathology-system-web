<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\MainIpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Main Ips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-ip-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Main Ip', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'accession_id_key',
            'CIPNUMBER',
            'CROP',
            'GENUS',
            'SPECIES',
            //'SUBTAXON',
            //'CULTVRNAME',
            //'OTHER_IDENT',
            //'FEMALE',
            //'MALE',
            //'BIOSTAT',
            //'DATERCVD',
            //'DATECOL',
            //'COLNUMBER',
            //'COUNTRY',
            //'ADMIN1',
            //'ADMIN2',
            //'ADMIN3',
            //'LOCALITY',
            //'LATCOOR',
            //'LONGCOOR',
            //'CATALOGUE',
            //'ORDERING',
            //'AVAILABLE',
            //'STATUS',
            //'IDENTITY_RESULT',
            //'COMMENTS',
            //'MLSSTAT',
            //'TI',
            //'ANEXO1',
            //'Health_Status',
            //'PopulationGroup',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
