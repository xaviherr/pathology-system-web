<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\MainIp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-ip-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'accession_id_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CIPNUMBER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CROP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'GENUS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SPECIES')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SUBTAXON')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CULTVRNAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OTHER_IDENT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FEMALE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MALE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BIOSTAT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DATERCVD')->textInput() ?>

    <?= $form->field($model, 'DATECOL')->textInput() ?>

    <?= $form->field($model, 'COLNUMBER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COUNTRY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADMIN1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADMIN2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADMIN3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LOCALITY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LATCOOR')->textInput() ?>

    <?= $form->field($model, 'LONGCOOR')->textInput() ?>

    <?= $form->field($model, 'CATALOGUE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ORDERING')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'AVAILABLE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STATUS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IDENTITY_RESULT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COMMENTS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MLSSTAT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TI')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ANEXO1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Health_Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PopulationGroup')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
