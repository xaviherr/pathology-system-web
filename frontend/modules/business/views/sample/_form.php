<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Sample */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sample-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numOrder')->textInput() ?>

    <?= $form->field($model, 'accessionCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accessionNumber')->textInput() ?>

    <?= $form->field($model, 'accessionName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collectingCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collectingNumber')->textInput() ?>

    <?= $form->field($model, 'labCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'labNumber')->textInput() ?>

    <?= $form->field($model, 'female')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'male')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isBulk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'acquisitionRequest')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'distributionRequest')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredAt')->textInput() ?>

    <?= $form->field($model, 'updatedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'disabled' => 'Disabled',], ['prompt' => '']) ?>

    <?= $form->field($model, 'plate')->textInput() ?>

    <?= $form->field($model, 'requestId')->textInput() ?>

    <?= $form->field($model, 'cropId')->textInput() ?>

    <?= $form->field($model, 'workFlowId')->textInput() ?>

    <?= $form->field($model, 'numOrderId')->textInput() ?>

    <?= $form->field($model, 'sampleTypeId')->textInput() ?>

    <?= $form->field($model, 'sourceId')->textInput() ?>

    <?= $form->field($model, 'sampleCrossId')->textInput() ?>

    <?= $form->field($model, 'graftingCount')->textInput() ?>

    <?= $form->field($model, 'firstUserCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondUserCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thirdUserCode')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>