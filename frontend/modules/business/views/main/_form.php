<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Main */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CIPNUMBER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CIPNUMBER_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CROP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'WebSMTA_bool')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'WebSMTA2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FAO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CULTVRNAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COLNUMBER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ALTERID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OTHER_IDENT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SINONIMO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CCODE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MISSCODE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COUNTRY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STATE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REGION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REGION2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LOCALITY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SOURCE')->textInput() ?>

    <?= $form->field($model, 'SOURCIP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ALT')->textInput() ?>

    <?= $form->field($model, 'LATCOOR')->textInput() ?>

    <?= $form->field($model, 'LONGCOOR')->textInput() ?>

    <?= $form->field($model, 'BIOSTAT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SPP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SPECIES')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'GENUS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PopulationGroup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FEMALE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MALE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DCCODE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DCTRY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DATERCVD')->textInput() ?>

    <?= $form->field($model, 'DATECOL')->textInput() ?>

    <?= $form->field($model, 'DONOR')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COMMENTS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STATUS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Health_Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Distribution_Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STORA')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BIOSTAT2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'corporate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CULTVRNAME2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FEMALE2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MALE2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COLNUMBER2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Health_Status2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Corporativa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CREATE')->textInput() ?>

    <?= $form->field($model, 'CIPFBS')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
