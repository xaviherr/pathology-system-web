<?php

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
?>
<div class="upload">
  <?php Pjax::begin(['id' => 'pjax-manage-request-upload']); ?>
  <?php $form_manage_request_upload = ActiveForm::begin(
    [
      'id' => 'form-manage-request-upload',
      'options' =>
      [
        'data-pjax' => true,
        'id' => 'dynamic-form-manage-request-upload',
      ]
    ]
  ); ?>
  <div class="box-body">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <?= $form_manage_request_upload->field($model, 'documentFile')->fileInput() ?>
    </div>
  </div>
  <div class="box-footer">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="btn-group pull-right" style="margin-top: 25px;">
        <?= Html::a(
          'Save absorbance',
          [
            'upload',
            'requestId' => Yii::$app->request->get('requestId'),
            'numOrderId' => Yii::$app->request->get('numOrderId'),
            'activityId' =>  Yii::$app->request->get('activityId'),
            'essayId' =>  Yii::$app->request->get('essayId'),
            'supportOrder' =>  Yii::$app->request->get('supportOrder'),
          ],
          [
            'title' => 'Results in support format',
            'data-method' => 'post',
            'class' => 'btn btn-primary',
          ]
        ); ?>

        <?= Html::a(
          'Close',
          null,
          [
            'class' => 'btn btn-default',
            'title' => 'Close',
            'data-dismiss' => 'modal',
          ]
        ); ?>
      </div>
    </div>
  </div>
  <?php ActiveForm::end() ?>
  <?php Pjax::end(); ?>
</div>