<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Essay;

$js_code_config_workflow  = <<<JS
<script language="JavaScript">
$('.dataTableDefaultClass').DataTable({
  'paging'      : true,
  'lengthChange': true,
  'searching'   : true,
  'ordering'    : true,
  'info'        : true,
  'autoWidth'   : true
});
</script>
JS;
?>

<div class="config-workflow">
  <?php Pjax::begin(['id' => 'pjax-config-workflow']); ?>
  <?php $form_config_workflow = ActiveForm::begin(['id' => 'form-config-workflow', 'options' => ['data-pjax' => true, 'id' => 'dynamic-form-config-workflow',]]); ?>
  <?= $js_code_config_workflow ?>

  <div class="box-body">

    <div class="row">
      <div class="col-md-12">
        <?= Crop::findOne($arrayRequestProcess['cropId'])->longName ?>
      </div>
      <div class="col-md-12">
        <i class="icon fa fa-cubes"></i>
        <?= WorkFlow::findOne($arrayRequestProcess['workFlowId'])->shortName ?>
      </div>
    </div>
    <?php foreach ($dataProviders as $key => $dataProvider) { ?>
      <hr>
      <div class="row">
        <div class="col-md-6">
          <i class="icon fa fa-cube"></i>
          <?= Essay::findOne($key)->shortName ?>
        </div>
        <div class="col-md-6">
        </div>
      </div>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'showOnEmpty' => false,
          'emptyText' => '<table><tbody></tbody></table>',
          // 'options' => ['style' => 'font-size:11px;'],
          'options' => ['style' => 'font-size:1em;'],
          'tableOptions' => [
            'class' => 'table table-bordered table-striped dataTable dataTableDefaultClass',
          ],
        ]); ?>
    <?php } ?>

  </div>

  <div class="box-footer">

    <?= Html::a(
      'Close',
      null,
      [
        'class' => 'btn btn-primary pull-right',
        'data-dismiss' => 'modal',
        'title' => 'Close',
      ]
    ); ?>

  </div>

  <?php ActiveForm::end(); ?>
  <?php Pjax::end(); ?>
</div>