<?php

use yii\helpers\Html;
?>
<div class="render-content">


  <?php
  $creationCondition =  false;
  $approvalCondition = false;
  $rejectionCondition = false;
  $uploadCondition = false;
  $validationCondition = false;
  $reprocessCondition =  false;
  $notificationCondition = false;
  $verificationCondition = false;
  if (!is_null($modelRequest->creationDate)) $creationCondition = true;
  if (!is_null($modelRequest->approvalDate)) $approvalCondition = true;
  if (!is_null($modelRequest->rejectionDate)) $rejectionCondition = true;
  if (!is_null($modelRequest->uploadDate)) $uploadCondition = true;
  if (!is_null($modelRequest->validationDate)) $validationCondition = true;
  if (!is_null($modelRequest->reprocessDate)) $reprocessCondition = true;
  if (!is_null($modelRequest->notificationDate)) $notificationCondition = true;
  if (!is_null($modelRequest->verificationDate)) $verificationCondition = true;
  ?>


  <div class="row">
    <div class="col-lg-12">
      <ul class="timeline">
        <li>
          <span class="badge bg-green" style="padding: 10px 15px;"> START</span>
        </li>
        <br />
        <li>
          <i class="fa fa-plus bg-green"></i>
          <div class="timeline-item enable">
            <span class="time"><i class="fa fa-clock-o"></i> <?= $modelRequest->creationDate ?></span>
            <h3 class="timeline-header">
              <i class="fa fa-user text-aqua"></i>
              <b> CREATED</b> Request Creation
            </h3>
            <div class="timeline-footer" style="padding-bottom: 30px">
            </div>
          </div>
        </li>
        <br />
        <li>
          <?php if ($approvalCondition) { ?>
            <i class="fa fa-check bg-green"></i>
            <div class="timeline-item enable">
            <?php } else if ($rejectionCondition) { ?>
              <i class="fa fa-ban bg-red"></i>
              <div class="timeline-item disable">
              <?php } else { ?>
                <i class="fa fa-check bg-gray"></i>
                <div class="timeline-item disable">
                <?php } ?>

                <span class="time"><i class="fa fa-clock-o"></i>
                  <?php if ($approvalCondition) { ?>
                    <?= $modelRequest->approvalDate ?>
                  <?php } else if ($rejectionCondition) { ?>
                    <?= $modelRequest->rejectionDate ?>
                  <?php } ?>
                </span>
                <h3 class="timeline-header">
                  <i class="fa fa-user text-orange"></i>
                  <b>
                    <?php if ($approvalCondition) { ?>
                      APPROVED
                    <?php } else if ($rejectionCondition) { ?>
                      REJECTED
                    <?php } else { ?>
                      APPROVED / REJECTED
                    <?php } ?>
                  </b> Approve request pending analysis
                </h3>
                <div class="timeline-footer" style="padding-bottom: 30px">
                  <!-- ---------------- -->
                  <?php if (Yii::$app->user->can("request_manage-request-tracking-approve")) { ?>
                    <?php if ($approvalCondition) { ?>
                    <?php } else if ($rejectionCondition) { ?>
                      <div class="btn-group  pull-right">
                        <?= Html::a(
                              'Approve',
                              [
                                'manage-request-tracking-approve',
                                'requestId' => $modelRequest->id,
                                '#' => 'tracking',
                              ],
                              [
                                'class' => 'btn btn-primary btn-xs',
                                'data-method' => 'post',
                              ]
                            ); ?>
                      </div>
                    <?php } else if ($creationCondition) { ?>
                      <div class="btn-group  pull-right">
                        <?= Html::a(
                              'Approve',
                              [
                                'manage-request-tracking-approve',
                                'requestId' => $modelRequest->id,
                                '#' => 'tracking',
                              ],
                              [
                                'class' => 'btn btn-primary btn-xs',
                                'data-method' => 'post',
                              ]
                            ); ?>
                        <?= Html::a(
                              'To refuse',
                              [
                                'manage-request-tracking-refuse',
                                'requestId' => $modelRequest->id,
                                '#' => 'tracking',
                              ],
                              [
                                'class' => 'btn btn-danger btn-xs',
                                'data-method' => 'post',
                              ]
                            ); ?>
                      </div>
                    <?php } ?>
                  <?php } ?>
                  <!-- ---------------- -->
                </div>

                </div>
        </li>
        <br />
        <li>
          <?php if ($uploadCondition) { ?>
            <i class="fa fa-cloud-upload bg-green"></i>
            <div class="timeline-item enable">
            <?php } else { ?>
              <i class="fa fa-cloud-upload bg-gray"></i>
              <div class="timeline-item disable">
              <?php } ?>
              <span class="time"><i class="fa fa-clock-o"></i>
                <?= $modelRequest->uploadDate ?>
              </span>
              <h3 class="timeline-header">
                <i class="fa fa-user text-purple"></i>
                <b>
                  UPLOADED
                </b> Upload and send analysis results
              </h3>
              <div class="timeline-footer" style="padding-bottom: 30px">
                <!-- ---------------- -->
                <?php if (Yii::$app->user->can("request_manage-request-tracking-upload")) { ?>
                  <?php if ($uploadCondition) { ?>
                  <?php } else if ($approvalCondition) { ?>
                    <div class="btn-group  pull-right">
                      <?= Html::a(
                            'Upload results',
                            [
                              'manage-request-tracking-upload',
                              'requestId' => $modelRequest->id,
                              '#' => 'tracking',
                            ],
                            [
                              'class' => 'btn btn-primary btn-xs',
                              'data-method' => 'post',
                            ]
                          ); ?>
                    </div>
                  <?php } ?>
                <?php } ?>
                <!-- ---------------- -->
              </div>

              </div>
        </li>
        <br />
        <li>
          <?php if ($validationCondition) { ?>
            <i class="fa fa-check bg-green"></i>
            <div class="timeline-item enable">
            <?php } else if ($reprocessCondition) { ?>
              <i class="fa fa-refresh bg-red"></i>
              <div class="timeline-item enable">
              <?php } else { ?>
                <i class="fa fa-check bg-gray"></i>
                <div class="timeline-item disable">
                <?php } ?>
                <span class="time"><i class="fa fa-clock-o"></i>
                  <?php if ($validationCondition) { ?>
                    <?= $modelRequest->approvalDate ?>
                  <?php } else if ($reprocessCondition) { ?>
                    <?= $modelRequest->rejectionDate ?>
                  <?php } ?>
                </span>
                <h3 class="timeline-header">
                  <i class="fa fa-user text-orange"></i>
                  <b>
                    <?php if ($validationCondition) { ?>
                      VALIDATED
                    <?php } else if ($reprocessCondition) { ?>
                      REPROCESSING
                    <?php } else { ?>
                      VALIDATED / REPROCESSING
                    <?php } ?>
                  </b> Validate results and approve, pending delivery
                </h3>
                <div class="timeline-footer" style="padding-bottom: 30px">
                  <!-- ---------------- -->
                  <?php if (Yii::$app->user->can("request_manage-request-tracking-validate")) { ?>
                    <?php if ($validationCondition) { ?>
                    <?php } else if ($reprocessCondition) { ?>
                      <div class="btn-group  pull-right">
                        <?= Html::a(
                              'Validate',
                              [
                                'manage-request-tracking-validate',
                                'requestId' => $modelRequest->id,
                                '#' => 'tracking',
                              ],
                              [
                                'class' => 'btn btn-primary btn-xs',
                                'data-method' => 'post',
                              ]
                            ); ?>
                      </div>
                    <?php } else if ($uploadCondition) { ?>
                      <div class="btn-group  pull-right">
                        <?= Html::a(
                              'Validate',
                              [
                                'manage-request-tracking-validate',
                                'requestId' => $modelRequest->id,
                                '#' => 'tracking',
                              ],
                              [
                                'class' => 'btn btn-primary btn-xs',
                                'data-method' => 'post',
                              ]
                            ); ?>
                        <?= Html::a(
                              'Reprocess',
                              [
                                'manage-request-tracking-reprocess',
                                'requestId' => $modelRequest->id,
                                '#' => 'tracking',
                              ],
                              [
                                'class' => 'btn btn-danger btn-xs',
                                'data-method' => 'post',
                              ]
                            ); ?>
                      </div>
                    <?php } ?>
                  <?php } ?>
                  <!-- ---------------- -->
                </div>

                </div>
        </li>
        <br />
        <li>
          <?php if ($notificationCondition) { ?>
            <i class="fa fa-envelope bg-green"></i>
            <div class="timeline-item enable">
            <?php } else { ?>
              <i class="fa fa-envelope bg-gray"></i>
              <div class="timeline-item disable">
              <?php } ?>
              <span class="time"><i class="fa fa-clock-o"></i>
                <?= $modelRequest->notificationDate ?>
              </span>
              <h3 class="timeline-header">
                <i class="fa fa-user text-orange"></i>
                <b>SENT</b>
                Send results to the client user
              </h3>
              <div class="timeline-footer" style="padding-bottom: 30px">
                <!-- ---------------- -->
                <?php if (Yii::$app->user->can("request_manage-request-tracking-send")) { ?>
                  <?php if ($notificationCondition) { ?>
                  <?php } else if ($validationCondition) { ?>
                    <div class="btn-group  pull-right">
                      <?= Html::a(
                            'Send Results',
                            [
                              'manage-request-tracking-send',
                              'requestId' => $modelRequest->id,
                              '#' => 'tracking',
                            ],
                            [
                              'class' => 'btn btn-primary btn-xs',
                              'data-method' => 'post',
                            ]
                          ); ?>
                    </div>
                  <?php } ?>
                <?php } ?>
                <!-- ---------------- -->
              </div>
              </div>
        </li>
        <br />
        <li>
          <?php if ($verificationCondition) { ?>
            <i class="fa fa-check bg-green"></i>
            <div class="timeline-item enable">
            <?php } else { ?>
              <i class="fa fa-check bg-gray"></i>
              <div class="timeline-item disable">
              <?php } ?>
              <span class="time"><i class="fa fa-clock-o"></i>
                <?= $modelRequest->verificationDate ?>
              </span>
              <h3 class="timeline-header">
                <i class="fa fa-user text-aqua"></i>
                <b>
                  VERIFIED
                </b> Verify results by the client user
              </h3>
              <div class="timeline-footer" style="padding-bottom: 30px">
                <!-- ---------------- -->
                <?php if (Yii::$app->user->can("request_manage-request-tracking-verify")) { ?>
                  <?php if ($verificationCondition) { ?>
                  <?php } else if ($notificationCondition) { ?>
                    <div class="btn-group  pull-right">
                      <?= Html::a(
                            'Verified results',
                            [
                              'manage-request-tracking-verify',
                              'requestId' => $modelRequest->id,
                              '#' => 'tracking',
                            ],
                            [
                              'class' => 'btn btn-primary btn-xs',
                              'data-method' => 'post',
                            ]
                          ); ?>
                    </div>
                  <?php } ?>
                <?php } ?>
                <!-- ---------------- -->
              </div>
              </div>
        </li>
        <br />
        <li>
          <?php if (!is_null($modelRequest->verificationDate)) { ?>
            <span class="badge bg-green" style="padding: 10px 15px;"> FINISH</span>
          <?php } else { ?>
            <span class="badge bg-gray" style="padding: 10px 15px;"> FINISH</span>
          <?php } ?>
        </li>
      </ul>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-sm-5">
      <p>
        <i class="fa fa-user text-orange"></i>
        <span style="font-size:10px;"> ADMINISTRATIVE USERS</span>
      </p>
      <p>
        <i class="fa fa-user text-purple"></i>
        <span style="font-size:10px;"> TECHNICAL USERS</span>
      </p>
      <p>
        <i class="fa fa-user text-aqua"></i>
        <span style="font-size:10px;"> APPLICANT USERS</span>
      </p>
    </div>
  </div>
</div>