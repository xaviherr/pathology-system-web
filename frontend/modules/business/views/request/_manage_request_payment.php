<?php

use yii\helpers\Html;
use yii\grid\GridView;
?>

<div class="render-content">
  <div class="row">
    <div class="col-sm-12">


      <?= GridView::widget(
        [
          'dataProvider' => $dataProviderFinancialConceptOne,
          'layout'       => "{items}",
          'options'       => [
            'style' => 'overflow-x: auto; padding-right: 0px; padding-left: 0px; font-size:1em;'
          ],
          'rowOptions' => function ($model) {
            if ($model->status == "disabled") {
              return ['class' => 'danger'];
            } else if ($model->status == "active") {
              return ['class' => 'success'];
            }
          },
          'columns'       => [
            [
              'class' => 'yii\grid\SerialColumn'
            ],
            [
              'attribute'     => 'requestId',
              'header'        => 'Request',
              'value' => 'request.code',
              'enableSorting' => false,
            ],
            // [
            //   'attribute'     => 'statusConcept',
            //   'value'     =>  function ($model) {
            //     return  Html::tag(
            //       'span',
            //       Html::encode($model->statusConcept),
            //       ['class' => 'label label-' . $model->statusConcept]
            //     );
            //   },
            //   'enableSorting' => false,
            //   'format' => 'raw',
            // ],
            [
              'attribute'     => 'statusBus',
              'value'     =>  function ($model) {
                return  Html::tag(
                  'span',
                  Html::encode($model->statusBus),
                  ['class' => 'label label-' . $model->statusBus]
                );
              },
              'enableSorting' => false,
              'format' => 'raw',
            ],
            // [
            //   'header' => 'Expiry date',
            //   'value' => function ($model) {
            //     return
            //       date("d-M-Y",  strtotime($model->dateTo));
            //   },
            // ],
            // [
            //   'attribute'     => 'period',
            //   'enableSorting' => false,
            // ],
            // [
            //   'attribute'     => 'account',
            //   'enableSorting' => false,
            // ],
            // [
            //   'attribute'     => 'resno',
            //   'enableSorting' => false,
            // ],
            [
              'attribute'     => 'bus',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'task',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'crop',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'testOfLaboratory',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'owner',
              'enableSorting' => false,
            ],
            // [
            //     'attribute'     => 'pathogens',
            //     'enableSorting' => false,
            // ],
            [
              'attribute'     => 'numberOfExperiments',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'numberOfPathogens',
              'enableSorting' => false,
            ],
            // [
            //     'attribute'     => 'descriptionFinal',
            //     'enableSorting' => false,
            // ],
            // [
            //   'attribute'     => 'ACE',
            //   'enableSorting' => false,
            // ],
            [
              'attribute'     => 'agreement',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'samples',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'fee',
              'enableSorting' => false,
            ],
            [
              'attribute'     => 'percent',
              'header' => 'Percent %',
              'enableSorting' => false,
            ],
            // [
            //   'attribute'     =>   'totalPercentage',
            //   'header' => 'Sub total Cost USD',
            //   'enableSorting' => false,
            // ],
            [
              'header'        => 'Sub total Cost USD',
              'value'     =>  function ($model) {
                return     Yii::$app->formatter->asCurrency($model['totalPercentage'], '$');
              },
              'enableSorting' => false,
              'contentOptions' => ['style' => 'width:100px;'],
            ],
            // [
            //   'attribute'     => 'total',
            //   'enableSorting' => false,
            // ],
            [
              'header'        => 'Total',
              'value'     =>  function ($model) {
                return     Yii::$app->formatter->asCurrency($model['total'], '$');
              },
              'enableSorting' => false,
            ],
            /*
              [
                  'attribute'     => 'costId',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'resnoLab',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'cropId',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'workFlowId',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'numOrderId',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'financialBusId',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'registeredBy',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'registeredAt',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'updatedBy',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'updatedAt',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'deletedBy',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'deletedAt',
                  'enableSorting' => false,
              ],
              [
                  'attribute'     => 'status',
                  'enableSorting' => false,
              ],

              */
            // [
            //   'class' => 'yii\grid\CheckboxColumn',
            //   // 'checkboxOptions' => function ($model, $key, $index, $column) {
            //   //     return ['checked' => true];
            //   // }
            //   'checkboxOptions' => function ($model, $key, $index, $column) {

            //     if (!is_null(Yii::$app->request->post('selection'))) {
            //       return [
            //         'value' => $model->id,
            //         'checked' => in_array($model->id, Yii::$app->request->post('selection')) ? true : false
            //       ];
            //     } else {
            //       return [
            //         'value' => $model->id,
            //       ];
            //     }
            //   },
            // ],

          ],
          'tableOptions' => [
            'class' => 'table table-bordered table-striped dataTable gvDefault',
            'id'    => "gvConcepts",
          ],
        ]
      ); ?>

    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 ">
      <span class="info">
        <i class="icon fa fa-info"></i> Legend information about the status of the financial concepts and the buses entered by the request owner.
      </span>
    </div>
  </div>
  <div class="row">
    <!-- <div class="col-sm-5">
      <p>
        <span class="label label-to_send" style="font-size: 10px;">to_send</span>
        <span style="font-size:10px;"> Financial concept of the list that has not yet been sent to finance for processing.</span>
      </p>
      <p>
        <span class="label label-sent" style="font-size: 10px;">sent</span>
        <span style="font-size:10px;"> Financial concept of the list that was already sent to finance.</span>
      </p>
    </div> -->
    <div class="col-sm-5">
      <p>
        <span class="label label-active" style="font-size: 10px;">active</span>
        <span style="font-size:10px;"> Active bus status. Financial concept ready to be sent.</span>
      </p>
      <p>
        <span class="label label-inactive" style="font-size: 10px;">inactive</span>
        <span style="font-size:10px;">Bus status deactivated or inactive, it is recommended to notify the user who registered the request.</span>
      </p>
    </div>
  </div>
</div>