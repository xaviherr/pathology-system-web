<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Agent;
use kartik\select2\Select2;
?>
<div class="row">

  <!-- PASSPORT -->
  <div class="col-xs-12 col-md-3 col-lg-3 " style="min-width:420px;">
    <b> <span>Filter passport columns</span></b>
    <br />
    <?php if (!is_null($listNavigation['passportFilter'])) { ?>
      <div class="content-border row">
        <ul class="todo-list ">
          <?php foreach ($listNavigation['passportFilter'] as $key => $value) { ?>
            <li style="display:inline-flex">
              <label class="check-label">
                <input type="checkbox" name="passportFilter[<?= $key ?>]" value=<?= $value ?> <?= $value ?>>
                <span style="font-size: 0.75em;" class="text"> <?= $key ?></span>
              </label>
            </li>
          <?php  } ?>
        </ul>
      </div>
    <?php  } ?>
  </div>

  <!-- ACTIVITY -->
  <div class="col-xs-12 col-md-3 col-lg-3 border-left" style="min-width:420px;">
    <b> <span>Filter activity columns</span></b>
    <br />
    <?php if (!is_null($listNavigation['activityFilter'])) { ?>
      <div class="content-border row">
        <ul class="todo-list ">
          <?php foreach ($listNavigation['activityFilter'] as $key => $value) { ?>
            <li style="display:inline-flex">
              <label class="check-label">
                <input type="checkbox" name="activityFilter[<?= $key ?>]" value=<?= $value ?> <?= $value ?>>
                <span style="font-size: 0.75em;" class="text"> <?= Activity::findOne($key)->shortName ?></span>
              </label>
            </li>
          <?php  } ?>
        </ul>
      </div>
    <?php  } ?>
  </div>

  <!-- AGENTS -->
  <div class="col-xs-12 col-md-3 col-lg-3 border-left" style="min-width:320px;">
    <b><span>Filter agent columns</span></b>
    <br />
    <?php if (!is_null($listNavigation['agentFilter'])) { ?>
      <div class="content-border row">
        <ul class="todo-list ">
          <?php foreach ($listNavigation['agentFilter'] as $key => $value) { ?>
            <li style="display:inline-flex">
              <label class="check-label">
                <input type="checkbox" name="agentFilter[<?= $key ?>]" value=<?= $value ?> <?= $value ?>>
                <span style="font-size: 0.75em;" class="text"> <?= Agent::findOne($key)->shortName ?></span>
              </label>
            </li>
          <?php  } ?>
        </ul>
      </div>
    <?php  } ?>
  </div>

  <!-- ABSORBANCES -->
  <?php if (!is_null($listNavigation['absorbance'])) { ?>
    <div class="col-xs-12 col-md-3 col-lg-3 border-left" style="min-width:420px;">
      <b> <span>Set absorbance data</span></b><br />
      <?php foreach ($listNavigation['absorbance'] as $key => $value) { ?>
        <?php if (empty($value)) { ?>
          <div class="row no-margin">
            <span class="text-red">
              Empty distribution in activity <?= Activity::findOne($key)->shortName ?>
            </span>
          </div>
          <br />
        <?php } else { ?>
          <div class="row no-margin">
            <span class="text-black">
              Distribution <?= Activity::findOne($key)->shortName ?>
            </span>
            <div class="content-border row">
              <ul class="todo-list ">
                <?php foreach ($listNavigation['absorbance'][$key]     as $suppId => $suppValue) { ?>
                  <li style="display:inline-flex; margin-bottom:2px;">
                    <?php $suppValue == "empty" ? $btnClass = 'default' : $btnClass = 'success' ?>
                    <?= Html::button(
                              '<span>  Support ' . $suppId  . ' </span>',
                              [
                                'title' => 'Upload support absorbances',
                                'style' => 'font-size: 0.85em; width: 75px;',
                                'class' => 'btn  btn-' . $btnClass . ' btn-xs modal-upload-action',
                                'value' => Url::to(
                                  [
                                    'request/upload',
                                    'requestId' =>  $requestId,
                                    'numOrderId' =>  $numOrderId,
                                    'essayId' => $essayId,
                                    'activityId' => $key,
                                    'supportOrder' => $suppId,
                                  ]
                                ),
                              ]
                            ); ?>
                  </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
    </div>
  <?php  } ?>

</div>