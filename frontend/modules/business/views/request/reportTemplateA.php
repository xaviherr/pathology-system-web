<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="report.xls"');
header('Cache-Control: max-age=0');
?>
<table>
  <tr>
    <?php foreach ($columns as $key_h => $value_h) { ?>
      <th style="border: 1px dashed #bbbbbb !important; font-size: 14px; background-color: #eeeeee;"> <?= $value_h['attribute'] ?> </th>
    <?php } ?>
  </tr>
  <?php foreach ($dataProvider->allModels as $key_b => $value_b) { ?>
    <tr style='font-size: 12px'>
      <?php foreach ($columns as $key_h_aux => $value_h_aux) { ?>
        <td style="border: 1px dashed #eeeeee !important"> <?= $value_b[$value_h_aux['attribute']] ?> </td>
      <?php } ?>
    </tr>
  <?php } ?>
</table>