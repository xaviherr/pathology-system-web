<?php

use frontend\modules\configuration\models\Essay;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>
<div class="render-content">

  <div class="row">
    <div class="col-lg-4">
      <div>
        <?php foreach ($modelListRequestProcess as  $value1) { ?>
          <div style="margin-left:  20px;">
            <h4><b><span data-toggle="tooltip">Workflow <?= $value1->numOrderId ?></span>. <?= $value1->workFlow->shortName ?></b></h4>

            <?php foreach (ArrayHelper::map($value1->requestProcessDetails, 'essayId', 'essayId') as  $value2) { ?>
              <div style="margin-left:  20px;">

                <h5>
                  <?= Essay::findOne($value2)->shortName ?>
                  <?= Html::a(
                        '<i class="fa fa-list"></i> Advance report',
                        [
                          'manage-request',
                          'requestId' => $requestId,
                          'numOrderId' => $value1->numOrderId,
                          'essayId' => $value2,
                          'type' => 'advance',
                          '#' => 'results',

                        ],
                        [
                          'title' => 'Results in report format',
                          'data-method' => 'post',
                        ]
                      ); ?>
                  |
                  <?= Html::a(
                        '<i class="fa fa-th-list"></i> Resume report',
                        [
                          'manage-request',
                          'requestId' => $requestId,
                          'numOrderId' => $value1->numOrderId,
                          'essayId' => $value2,
                          'type' => 'resume',
                          '#' => 'results',
                        ],
                        [
                          'title' => 'Results in report format',
                          'data-method' => 'post',
                        ]
                      ); ?>
                </h5>

              </div>
            <?php } ?>

          </div>
        <?php } ?>

      </div>
    </div>
  </div>

  <hr>


  <!-- COLLAPSE -->

  <?php if (Yii::$app->user->can("request_result-by-activity")) { ?>
    <div class="row">
      <div class="col-lg-12">
        <b>
          <?php if (!is_null($listNavigation['absorbance'])) { ?>
            <a role="button" data-toggle="collapse" href="#filterView" aria-expanded="false" aria-controls="collapseExample">
              <span>Advance filter</span><span class="text-red"> [View absorbances]</span>
            </a>
          <?php } else { ?>
            <a role="button" data-toggle="collapse" href="#filterView" aria-expanded="false" aria-controls="collapseExample">
              <span>Advance filter</span>
            </a>
          <?php } ?>
        </b>
        <div class="collapse" id="filterView">
          <br />
          <?php
            echo $this->render('_manage-request-tree-view-filter', [
              'requestId' => $requestId,
              'listNavigation' => $listNavigation,
              'numOrderId' => Yii::$app->request->get('numOrderId'),
              'essayId' => Yii::$app->request->get('essayId'),
            ]); ?>
        </div>
      </div>
    </div>

    <hr>
  <?php } ?>



  <div class="row">
    <div class="col-lg-12">

      <div class="btn-group  pull-right">
        <?= Html::a(
          '<i class="fa fa-filter"></i> Apply filters',
          [
            'manage-request-filter',
            'requestId' =>  Yii::$app->request->get('requestId'),
            'numOrderId' =>  Yii::$app->request->get('numOrderId'),
            'essayId' => Yii::$app->request->get('essayId'),
            '#' => 'results',
          ],
          [
            'class' => 'btn btn-primary',
            'data-method' => 'post',
          ]
        ); ?>

        <?= Html::a(
          '<i class="fa fa-file-excel-o"></i> Download report',
          [
            'manage-report',
            'requestId' =>  Yii::$app->request->get('requestId'),
            'numOrderId' =>  Yii::$app->request->get('numOrderId'),
            'essayId' => Yii::$app->request->get('essayId'),
            '#' => 'results',
          ],
          [
            'class' => 'btn btn-success',
            // 'data-pjax' => 0,
            'data-method' => 'post',
            'target' => '_blank',
          ]
        ); ?>
      </div>


    </div>
  </div>





</div>