<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use frontend\modules\configuration\models\Parameter;

$this->title = $modelRequest->code;
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="request-report">

    <div class='row'>
        <div class="col-sm-6">
            <h4 class="pull-left">
                REQUEST REPORT ID: <span class="text-teal"> <?= $modelRequest->code ?> </span>
            </h4>
        </div>
        <div class="col-sm-6">
            <h5 class="pull-right">

                <?php
                $statusCode = Parameter::findOne($modelRequest['requestStatusId'])->code;
                $statusValue = Parameter::findOne($modelRequest['requestStatusId'])->shortName;
                ?>

                <?php if ($statusCode == '1.1') { ?>
                    <span class="label label-info"> <?= $statusValue ?> <i class="fa fa-edit"></i> </span>
                <?php } else if ($statusCode == '2.1') { ?>
                    <span class="label label-success"> <?= $statusValue ?> <i class="fa fa-check"></i> </span>
                <?php } else if ($statusCode == '3.0') { ?>
                    <span class="label label-danger"> <?= $statusValue ?> <i class="fa fa-exclamation"></i> </span>
                <?php } else if ($statusCode == '3.1') { ?>
                    <span class="label label-info"> <?= $statusValue ?><i class="fa fa-hourglass-2"></i> </span>
                <?php } else if ($statusCode == '3.2') { ?>
                    <span class="label label-success"> <?= $statusValue ?> <i class="fa fa-check"></i> </span>
                <?php  } else if ($statusCode == '4.1') { ?>
                    <span class="label label-info"> <?= $statusValue ?><i class="fa fa-hourglass-2"></i> </span>
                <?php } else if ($statusCode == '5.1') { ?>
                    <span class="label label-success"> <?= $statusValue ?> <i class="fa fa-check"></i> </span>
                <?php } else { ?>
                    <span class="label label-info"> <?= $statusValue ?> <i class="fa fa-exclamation"></i> </span>
                <?php } ?>

            </h5>
        </div>
    </div>


    <div class="box-header with-border">
        <h5>Request details</h5>
    </div>

    <div class="box-body">
        <div class='row'>
            <div class="col-sm-6 col-xs-6 description-block border-right">
                <?= DetailView::widget([
                    'model' => $modelRequest,
                    'options' => ['class' => 'table table-condensed', 'style' => 'font-size:11px;'],
                    'attributes' => [
                        'registeredBy',
                        //'requestTypeId',
                        [
                            'attribute' => 'requestTypeId',
                            'value' => $modelRequest->requestType->shortName,
                        ],
                        'details',
                        'diagnostic',
                        // [
                        //     'attribute' => 'requestStatusId',
                        //     'value' => $model->requestStatus->shortName,
                        // ],
                    ],
                ]) ?>
            </div>

            <div class="col-sm-6 col-xs-6 description-block border-right">

                <?= DetailView::widget([
                    'model' => $modelRequest,
                    'options' => ['class' => 'table table-condensed', 'style' => 'font-size:11px;'],
                    'attributes' => [

                        'sampleQty',
                        'totalCost',
                        // 'paymentTypeId',
                        [
                            'attribute' => 'paymentTypeId',
                            'value' => $modelRequest->paymentType->shortName,
                        ],
                    ],
                ]) ?>



            </div>
        </div>

    </div>

    <div class="box-header with-border">
        <h5>Tracking</h5>
    </div>

    <div class="box-body">

        <div class='row'>
            <div class="col-sm-6 col-xs-6 description-block border-right">
                <?= DetailView::widget([
                    'model' => $modelRequest,
                    'options' => ['class' => 'table table-condensed', 'style' => 'font-size:11px;'],
                    'attributes' => [
                        [
                            'label' => 'REGISTERED',
                            'attribute' => 'creationDate',
                        ],
                        [
                            'label' => 'CREATED',
                            'attribute' => 'requiredDate',
                        ],
                        [
                            'label' => 'APPROVED/REJECTED',
                            'attribute' => 'approvalDate',
                        ],
                        [
                            'label' => 'PROCESS START',
                            'attribute' => 'initDate',
                        ],
                        [
                            'label' => 'FINISHED',
                            'attribute' => 'completionDate',
                        ],
                    ],
                ]) ?>
            </div>
            <div class="col-sm-6 col-xs-6 description-block">
                <div class="progress-group">
                    <span class="progress-text">Progress Status Bar (REQUIRED)</span>
                    <span class="progress-number"><b>1</b>/5</span>

                    <div class="progress sm">
                        <div class="progress-bar progress-bar-green" style="width: 40%"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="box-header with-border">
        <h5>Payment</h5>
    </div>

    <div class="box-body">
    </div>

    <div class="box-header with-border">
        <h5>Results</h5>
    </div>

    <div class="box-body">



        <div class="row justify-content-md-justify">

            <div class="col-lg-12">
                <!-- <table class="new-table">
                    <thead>
                        <tr role="row">

                            <th class="col-md-1">Num Order</th>
                            <th class="col-md-1">Crop</th>
                            <th class="col-md-1">Workflow</th>
                            <th class="col-md-1">Essay #</th>
                            <th class="col-md-1">Agent #</th>
                            <th class="col-md-2">Sample Status</th>
                            <th class="col-md-1">Sample #</th>
                            <th class="col-md-1">Sub Total</th>
                            <th class="col-md-1">Process Status</th>
                            <th class="col-md-2">Sample Detail</th>
                        </tr>
                    </thead>
                </table>
                <br /> -->

                <div class="new-table">
                    <?= ListView::widget(
                        [
                            'dataProvider' => $dataProviderRequestProcess,
                            'itemView' => '_overview-request-process',
                            'layout' => '{items}{pager}{summary}',

                            'pager' => [
                                'firstPageLabel' => 'first',
                                'lastPageLabel' => 'last',
                                'prevPageLabel' => 'previous',
                                'nextPageLabel' => 'next',
                            ],
                        ]
                    );

                    ?>
                </div>


            </div>

        </div>







        <!-- <div class='row'>
            <div class=" description-block "> -->

        <?
        // = GridView::widget([
        //     'dataProvider' => $dataProviderRequestProcessDetail,
        //     'showOnEmpty' => false,
        //     'emptyText' => '<table><tbody></tbody></table>',
        //     'options' => ['style' => 'font-size:1em;'],
        //     'columns' => [
        //         ['class' => 'yii\grid\SerialColumn'],
        //         [
        //             'attribute'     => 'requestId',
        //             //'value'     => 'request.code',
        //             // 'value'     => $model->numOrder,
        //             'header' => 'REQUEST ID',
        //             'enableSorting' => false,
        //         ],
        //         // [
        //         //     'attribute'     => 'cropId',
        //         //     'value'     => 'crop.shortName',
        //         //     'header' => 'CROP',
        //         //     'enableSorting' => false,
        //         // ],
        //         // [
        //         //     'attribute'     => 'workFlowId',
        //         //     'value'     => 'workFlow.shortName',
        //         //     'header' => 'ESSAY',
        //         //     'enableSorting' => false,
        //         // ],
        //         // [
        //         //     'attribute'     => 'essayId',
        //         //     'value'     => 'essay.shortName',
        //         //     'header' => 'SUB. ESSAY',
        //         //     'enableSorting' => false,
        //         // ],
        //         // [
        //         //     'attribute'     => 'agentId',
        //         //     'value'     => 'agent.shortName',
        //         //     'header' => 'AGENT',
        //         //     'enableSorting' => false,
        //         // ],
        //         // [
        //         //     'attribute'     => 'processStatusDetailId',
        //         //     'value'     => 'processStatusDetail.shortName',
        //         //     'header' => 'STATUS',
        //         //     'enableSorting' => false,
        //         // ],
        //     ],
        //     'tableOptions' => [
        //         'class' => 'table table-bordered table-striped dataTable',
        //         'id'    => "gvReportRequestProcessDetails",
        //     ],
        // ]); 
        ?>

        <!-- </div>
        </div> -->

    </div>

    <div class="box-footer">

        <?php
        $urlButton = [];
        if ($statusCode == 1.1) {
            $titleButton = 'Edit <i class="fa fa-edit"></i>';
            $classButton = 'btn btn-primary pull-right';
            $urlButton = [
                'update',
                'requestId' => $modelRequest->id,
            ];
        } else if ($statusCode == 2.1) {
            $titleButton = 'Notify <i class="fa fa-paper-plane"></i>';
            $classButton = 'btn btn-success pull-right';
            $urlButton = ['notify'];
        }
        ?>
        <?= Html::a(
            $titleButton,

            $urlButton,
            [
                'class' =>  $classButton,
                'style' => 'margin:0px 5px;'
            ]
        ) ?>



    </div>


</div>