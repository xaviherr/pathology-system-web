<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = 'Update Request';
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$js_code_request_update =
  <<<JS
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <script language="JavaScript">
  $(".modal-add-action").click(function() {
    $("#modal-add")
      .modal("show")
      .find(".modal-add-content")
      .load($(this).attr("value"));
    return false;
  });
  $(".modal-config-action").click(function() {
    $("#modal-config")
      .modal("show")
      .find(".modal-config-content")
      .load($(this).attr("value"));
    return false;
  });
  $(".modal-payment-action").click(function() {
    $("#modal-payment")
      .modal("show")
      .find(".modal-payment-content")
      .load($(this).attr("value"));
    return false;
  });
  </script>

  JS;
?>

<div class="request-update">

  <h1>
    <?= Html::encode($this->title) ?>
  </h1>
  <!-- ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ MODAL-->

  <div class="row">

    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Add sample(s)</h3>
          </div>
          <div class="modal-body">
            <div class="modal-add-content"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-config" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Config workflow</h3>
          </div>
          <div class="modal-body">
            <div class="modal-config-content"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Add payment(s)</h3>
          </div>
          <div class="modal-body">
            <div class="modal-payment-content"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-save" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Save request</h3>
          </div>
          <div class="modal-body">
            <div class="modal-save-content"></div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ FORM-->

  <?php Pjax::begin(['id' => 'pjax-request-update']); ?>
  <?php $form_request_update = ActiveForm::begin(['id' => 'form-request-update', 'options' => ['data-pjax' => true, 'id' => 'dynamic-form-request-update',]]); ?>


  <!-- CROP-WORKFLOW-ESSAY-AGENTS -->
  <div class="box">

    <?= $js_code_request_update ?>

    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-plus-circle"></i> Create a new Request</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>

    <div class="box-body">
      <div class="row justify-content-md-justify">
        <div class="col-lg-12 ">
          <span class="info">
            <i class="icon fa fa-info"></i> Set information about the Request. It is necessary to select at least one box from the list of "Pathogens" / "Indicator plants" of each "Test" that belongs to the selected Workflow.
          </span>
        </div>
      </div>
      <br />
      <div class="row justify-content-md-justify">
        <?= $this->render('_form-add-essay-agent', [
          'selectCropValue' => $selectCropValue,
          'selectWorkFlowValue' => $selectWorkFlowValue,
          'arrayMapCrop' => $arrayMapCrop,
          'arrayMapWorkFlow' => $arrayMapWorkFlow,
          'arrayMapEssayAgent' => $arrayMapEssayAgent,
        ]) ?>
      </div>
    </div>

    <div class="box-footer">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <?= Html::a('Add essay', ['detail', 'requestId' => $requestId,], ['class' => 'btn btn-primary pull-right', 'title' => 'Add essay', 'data-method' => 'post',]); ?>
      </div>
    </div>

  </div>

  <!-- SAMPLES -->
  <div class="box">

    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add samples</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>

    <div class="box-body">
      <div class="row justify-content-md-justify">
        <div class="col-lg-12 ">
          <span class="info">
            <i class="icon fa fa-info"></i> Add samples to the request by clicking the plus button:
          </span>
        </div>
      </div>
      <br />
      <div class="row justify-content-md-justify">
        <div class="col-lg-12">
          <table class="new-table">
            <thead>
              <tr role="row">
                <th class="col-md-1">Nbr. Order</th>
                <th class="col-md-1">Crop</th>
                <th class="col-md-2">Test set</th>
                <th class="col-md-1">Test #</th>
                <th class="col-md-2">Pathogen #</th>
                <th class="col-md-1">Sample #</th>
                <th class="col-md-2">Status</th>
                <th class="col-md-2">Actions</th>
              </tr>
            </thead>
          </table>
          <br />
          <div class="new-table">
            <?= ListView::widget(
              [
                'dataProvider' => $dataProviderRequestProcess,
                'itemView' => '_form-add-sample',
                'layout' => '{items}{pager}{summary}',

                'pager' => [
                  'firstPageLabel' => 'first',
                  'lastPageLabel' => 'last',
                  'prevPageLabel' => 'previous',
                  'nextPageLabel' => 'next',
                ],
              ]
            );
            ?>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- PAYMENT -->
  <div class="box">

    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add payment</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>

    <div class="box-body">

      <div class="row">
        <div class="col-lg-12">
          <label class="control-label">Payment detail</label>
          <div class="new-table">
            <?= ListView::widget(
              [
                'dataProvider' => $dataProviderRequestProcess,
                'itemView' => '_form-add-payment',
                'layout' => '{items}',
              ]
            );
            ?>
          </div>
          <label class="control-label">Total Cost</label>
          <span style="font-size: 12px;">
            <?php
            $totalCost = 0;
            foreach ($dataProviderRequestProcess->allModels as $key => $value) {
              $totalCost = $totalCost + $value['subTotalCost'];
            } ?>
            <?= Html::textInput(
              'totalCostTextInput',
              $totalCost,
              [
                'class' => 'form-control',
                'readOnly' => 'true',
                'style' => 'text-align:right; font-weight: bold'
              ]
            ) ?>
          </span>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <label class="control-label">Details</label>
          <?= Html::textArea(
            'textAreaDetail',
            $requestData['textAreaDetail'],
            [
              'class' => 'form-control',
            ]
          ) ?>
        </div>
      </div>

    </div>

  </div>

  <!-- SAVE -->
  <div class="row">

    <div class="col-md-6 col-lg-6">
      <?= Html::checkbox('agreeChecked', true, ['label' => 'I have read and agreed to the above information, create a new request', 'class' => 'btn btn-primary pull-left', 'disabled' => "disabled"]) ?>
    </div>

    <div class="col-md-6 col-lg-6">
      <?= Html::button(
        'Save',
        [
          'title' => Yii::t('yii', 'Save'),
          'class' => 'btn btn-primary pull-right modal-config-action',
          'value' => Url::to(
            [
              'request/save-request',
              'requestId' => $requestId
            ]
          ),
        ]
      ); ?>
    </div>

  </div>


  <?php ActiveForm::end(); ?>
  <?php Pjax::end(); ?>

  <!-- ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ END-->


</div>