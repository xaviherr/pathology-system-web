<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Cost;
use yii\widgets\MaskedInput;
use yii\widgets\ListView;
?>
<div class="add-payment">
  <?php Pjax::begin(
    [
      'id' => 'pjax-add-payment'
    ]
  ); ?>
  <?php $form_add_payment = ActiveForm::begin(
    [
      'id' => 'form-add-payment',
      'options' => [
        'data-pjax' => true,
        'id' => 'dynamic-form-add-payment',
      ]
    ]
  ); ?>

  <div class="box-body">
    <div class="row">
      <?php foreach ($arrayEssayAgentValue as $essayId => $value) { ?>
        <fieldset class="groupbox-border">
          <legend class="groupbox-border">
            <span class='text-teal'>
              <h4><?= $value['testName'] ?> payment(s)</h4>
            </span>
          </legend>
          <div class="content">

            <div class="row">

              <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: center">
                <span style="font-size: 12px;"><i class="icon fa fa-cube"></i> Test name:</span>
                <br />

                <span style="font-size: 12px;"><?= $value['testName'] ?></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: center">
                <span style="font-size: 12px;"><i class="icon fa fa-dollar"></i> Test cost:</span>
                <br />

                <span style="font-size: 12px;"><?= $value['testCost'] ?></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: center">
                <span style="font-size: 12px;"><i class="icon fa fa-bug"></i> Agent Qty:</span>
                <br />

                <span style="font-size: 12px;"><?= $value['agentQty'] . " - " . $value['agentDetail'] ?></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: center">
                <span style="font-size: 12px;"><i class="icon fa fa-leaf"></i> Sample Qty.:</span>
                <br />

                <span style="font-size: 12px;"><?= $value['sampleQty'] ?></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: center">
                <span style="font-size: 12px;"><i class="icon fa fa-money"></i> Sub total test:</span>
                <br />

                <span style="font-size: 12px;"><?= $value['subTotalByTest'] ?></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: center">
                <span style="font-size: 12px;"><i class="icon fa fa-th-list"></i> Payment concepts:</span>
                <br />

                <?php if ($value['paymentConcept'] == 100) { ?>
                  <span class="label label-success" style="font-size: 12px;"><?= (string) $value['paymentConcept'] . "%" ?> </span>
                <?php } else if ($value['paymentConcept'] == 0) { ?>
                  <span class="label label-danger" style="font-size: 12px;"><?= (string) $value['paymentConcept'] . "%" ?> </span>
                <?php } else { ?>
                  <span class="label label-warning" style="font-size: 12px;"><?= (string) $value['paymentConcept'] . "%" ?> </span>
                <?php } ?>
              </div>

            </div>

            <br />
            <br />

            <div class="row">
              <div class="col-lg-12" style="text-align: left">
                <span><b>Check and add payment(s):</b></span>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12">
                <?php if (!is_null($message)) { ?>

                  <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Bus status inactive!</h4>
                    <span> <?= $message ?> </span>
                    <?= Yii::$app->session->getFlash('error') ?>
                  </div>

                <?php } ?>

              </div>
            </div>

            <br />

            <div class="row">

              <div class="col-lg-6 col-md-6">
                <div class="input-group">
                  <span class="input-group-addon">Task</span>
                  <?= MaskedInput::widget(['id' => 'task_payment_' . $essayId, 'name' => 'taskPayment' . $essayId, 'mask' => '9999-9999-99',]); ?>
                </div>
              </div>

              <div class="col-lg-3 col-md-3">
                <div class="input-group">
                  <span class="input-group-addon">%</span>

                  <?php // MaskedInput::widget(['id' => 'task_percent_' . $essayId, 'name' => 'percentPayment' . $essayId, 'mask' => '999',]); 
                    ?>

                  <?= Html::textInput(
                      'percentPayment' . $essayId,
                      null,
                      [
                        'class' => 'form-control',
                        'name' => 'percentPayment' . $essayId,
                      ]
                    ) ?>

                </div>
              </div>

              <div class="col-lg-3 col-md-3">
                <?= Html::a(
                    'Search',
                    [
                      'add-payment',
                      'requestId' => $requestId,
                      'numOrderId' => $numOrderId,
                      'essayId' => $essayId,
                    ],
                    [
                      'class' => 'btn btn-default btn-sm pull-right',
                      'title' => 'Search in financial source',
                      'data-method'  => 'post',
                    ]
                  ); ?>
              </div>

            </div>

            <br />

            <div class="row">

              <div class="col-md-12">
                <!-- HERE -->
                <div style="overflow-x: auto;">
                  <?= GridView::widget([
                      'dataProvider' => $dataProviderRequestProcessPayment[$essayId],
                      'options' => ['style' => 'font-size:1em;'],
                      'layout'       => "{items}",
                      'columns' => [
                        [
                          'class' => 'yii\grid\SerialColumn',
                          'header'        => 'Order',
                        ],

                        [
                          'attribute'     => 'agreement',
                          'header'        => 'Agreement',
                          'enableSorting' => false,
                        ],
                        [
                          'attribute'     => 'bus',
                          'header'        => 'Bus',
                          'enableSorting' => false,
                        ],
                        [
                          'attribute'     => 'task',
                          'header'        => 'Task',
                          'enableSorting' => false,
                        ],
                        [
                          'header' => 'Expiry date',
                          'value' => function ($model) {
                            return date("d-M-Y", strtotime($model['dateTo']));
                          },
                        ],
                        [
                          'attribute'     => 'statusBus',
                          'value'     =>  function ($model) {
                            return  Html::tag(
                              'span',
                              Html::encode($model['statusBus']),
                              ['class' => 'label label-' . $model['statusBus']]
                            );
                          },
                          'enableSorting' => false,
                          'format' => 'raw',
                        ],
                        [
                          'attribute'     => 'owner',
                          'header'        => 'Owner',
                          'enableSorting' => false,
                        ],
                        [
                          'header'        => 'Percent',
                          'value'     =>  function ($model) {
                            return  Yii::$app->formatter->asPercent($model['percent'] / 100, 2);
                          },
                          'enableSorting' => false,
                        ],
                        [
                          'header'        => 'Total cost (USD)',
                          'value'     =>  function ($model) {
                            return     Yii::$app->formatter->asCurrency($model['totalPercentage'], '$');
                          },
                          'enableSorting' => false,
                        ],
                      ],
                      'tableOptions' => [
                        'class' => 'table table-bordered table-striped dataTable',
                        'id'    => "gvPayment" . $essayId,
                      ],
                    ]);
                    ?>

                </div>
              </div>

            </div>

          </div>

        </fieldset>

      <?php  } ?>

    </div>

  </div>

  <div class="box-footer">

    <div class="btn-group  pull-right">
      <?= Html::a(
        'Add payment(s)',
        [
          'add-payment',
          'requestId' => $requestId,
          'numOrderId' => $numOrderId,
        ],
        [
          'class' => 'btn btn-primary',
          'title' => 'Add payment(s)',
          'data-method' => 'post',
        ]
      ); ?>


      <?= Html::a(
        'Close',
        null,
        [
          'class' => 'btn btn-default',
          'title' => 'Close',
          'data-dismiss' => 'modal',

        ]
      ); ?>
    </div>



  </div>

  <?php ActiveForm::end(); ?>

  <?php Pjax::end(); ?>

</div>