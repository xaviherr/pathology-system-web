<?php

use kartik\select2\Select2;
?>
<div class="render-content">
  <div class="col col-sm-6 col-md-4 col-lg-4">
    <label class="control-label">Crop</label>
    <?php $iconClass = "class = 'fa fa-refresh fa-spin'"; ?>
    <?= Select2::widget([
      'id' => 'selectCrop',
      'name' => 'selectCropName',
      'data' => $arrayMapCrop,
      'value' => $selectCropValue,
      'options' => [
        'placeholder' => 'Select...',
      ],
      'pluginEvents' => [
        'select2:select' => 'function() {
            $( "#selectWorkFlow" ).html("<option value=0>loading essays set by crop...</option>");

            $.post("index.php?r=configuration/work-flow/drop-down-lists&id="  +   $(this).val(),
              function( data )
              {
                $( "#selectWorkFlow" ).html(data);
              }
            ); 

            $( "#cbxlEssayAgent" ).html("loading essays set by crop...");

            $.post("index.php?r=control/essay-by-work-flow/check-lists&id=' . '"+$("#selectWorkFlow").val(), 
              function( data ) {
                $( "#cbxlEssayAgent" ).html(data);
              }
            ); 

          }',
      ]
    ]);
    ?>
    <br />
    <label class="control-label">WorkFlow</label>
    <?php $iconClass = "class = 'fa fa-refresh fa-spin'"; ?>
    <?= Select2::widget([
      'id' => 'selectWorkFlow',
      'name' => 'selectWorkFlowName',
      'data' => $arrayMapWorkFlow,
      'value' => $selectWorkFlowValue,
      'options' => [
        'placeholder' => 'Select...',
      ],
      'pluginEvents' => [
        'select2:select' => 'function() {
                                    
          $( "#cbxlEssayAgent" ).html("loading agents by essay...<i ' . $iconClass . '></i>");

          $.post("index.php?r=control/essay-by-work-flow/check-lists&id=' . '"+$("#selectWorkFlow").val(), 
              function( data ) {
                  $( "#cbxlEssayAgent" ).html(data);
              }
          ); 

        }',
      ]
    ]);
    ?>
  </div>
  <div class="col col-sm-6 col-md-8 col-lg-8" id="divRequestProcessDetails">
    <?= $arrayMapEssayAgent ?>
  </div>
</div>