<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="save-request">
  <?php $form = ActiveForm::begin(); ?>

  <div class="body">

    <div class="row">
      <div class="col-lg-12">
        <label class="control-label">Details and comments</label>
        <?= Html::textArea(
          'textAreaDetail',
          null,
          [
            'class' => 'form-control',
          ]
        ) ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-lg-12">
        <?= Html::checkbox(
          'agreeChecked',
          false,
          [
            'label' => ' I have read and agreed to the above information, create a new request',
            'class' => 'btn btn-primary pull-left',
          ]
        ); ?>
      </div>
    </div>

    <hr>

    <div class="row">
      <!-- <div class="col-md-6">
        <span class="info">
          <i class="icon fa fa-info"></i> The request is saved in draft mode for future modifications, consider this option when you do not have all the complete data or if you want to obtain a cost estimate.
        </span>
        <? //= Html::a(
        //   'Save as draft',
        //   [
        //     'save',
        //     'requestId' => $requestId,
        //     'created' => false,
        //   ],
        //   [
        //     'class' => 'btn btn-block btn-primary btn-lg pull-right',
        //     'title' => 'Save',
        //     'data-method' => 'post',
        //   ]
        // ); 
        ?>
      </div> -->
      <div class="col-md-12">
        <span class="info">
          <i class="icon fa fa-info"></i> The request is saved and sent to those responsible for approving it. If you have inconsistent information, this request will be rejected.
        </span>
        <?= Html::a(
          'Create as request',
          [
            'save',
            'requestId' => $requestId,
            'created' => true,
          ],
          [
            'class' => 'btn btn-block btn-success btn-lg pull-left',
            'title' => 'Save',
            'data-method' => 'post',
          ]
        ); ?>
      </div>
    </div>

  </div>

  <?php ActiveForm::end(); ?>
</div>