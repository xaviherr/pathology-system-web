<?php

use yii\grid\GridView;
?>
<?php
$js_code_manage_request_result =
  <<<JS
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script language="JavaScript">
  $(".modal-upload-action").click(function() {
    $("#modal-upload")
      .modal("show")
      .find(".modal-upload-content")
      .load($(this).attr("value"));
    return false;
  });
  $('.gvDefaultResult').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
  });
  $(function () {
  $('[data-toggle="popover"]').popover()
  })
  $("[data-toggle=popover]")
  .popover({html:true})
</script>
JS;
?>
<?= $js_code_manage_request_result ?>
<div class="render-content">
  <!-- ENABLE -->
  <?php if (Yii::$app->user->can("request_result-by-activity")) { ?>
    <div class="row">
      <div class="col-md-12">
        <?php if (!is_null($dataProvider[1]) and isset($dataProvider[1])) { ?>
          <?= GridView::widget([
                'dataProvider' => $dataProvider[1],
                'options' => ['style' => 'font-size:1em;'],
                'layout'       => "{items}",
                'columns' =>   $columns[1],
                'tableOptions' => [
                  'class' => 'table table-bordered table-striped dataTable gvDefault gvDefaultResult',
                ],
              ]);
              ?>
        <?php } else { ?>
          <span title="Is necessary set reading datas" class="link-black">No data results</span>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <!-- ENABLE -->
  <?php if (Yii::$app->user->can("request_result-by-activity-resume")) { ?>
    <?php if (!is_null($modelRequest->notificationDate)) { ?>
      <div class="row">
        <div class="col-md-12">
          <?php if (isset($dataProvider[2]) and !is_null($dataProvider[2])) { ?>
            <?= GridView::widget([
                    'dataProvider' =>  $dataProvider[2],
                    'options' => ['style' => 'font-size:1em;'],
                    'layout'       => "{items}",
                    'columns' =>  $columns[2],
                    'tableOptions' => [
                      'class' => 'table table-bordered table-striped dataTable gvDefault gvDefaultResult',
                    ],
                  ]);
                  ?>
          <?php } else { ?>
            <span title="Is necessary set reading datas" class="link-black">No data results</span>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
  <!-- DISABLE -->
  <?php if (Yii::$app->user->can("request_result-by-agent")) { ?>
    <div class="row">
      <div class="col-md-12">
        <?php if (isset($dataProvider[3]) and !is_null($dataProvider[3])) { ?>
          <?= GridView::widget([
                'dataProvider' => $dataProvider[3],
                'options' => ['style' => 'font-size:1em;'],
                'layout'       => "{items}",
                'columns' =>  $columns[3],
                'tableOptions' => [
                  'class' => 'table table-bordered table-striped dataTable gvDefault gvDefaultResult',
                ],
              ]);
              ?>
        <?php } else { ?>
          <span title="Is necessary set reading datas" class="link-black"> </span>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <!-- DISABLE -->
  <?php if (Yii::$app->user->can("request_result-by-agent-resume")) { ?>
    <div class="row">
      <div class="col-md-12">
        <?php if (isset($dataProvider[4]) and !is_null($dataProvider[4])) { ?>
          <?= GridView::widget([
                'dataProvider' =>  $dataProvider[4],
                'options' => ['style' => 'font-size:1em;'],
                'layout'       => "{items}",
                'columns' =>  $columns[4],
                'tableOptions' => [
                  'class' => 'table table-bordered table-striped dataTable gvDefault gvDefaultResult',
                ],
              ]);
              ?>
        <?php } else { ?>
          <span title="Is necessary set reading data" class="link-black"> </span>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
</div>