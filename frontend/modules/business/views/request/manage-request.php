<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\modules\configuration\models\Essay;

$this->title = 'Manage Request';
$this->params['breadcrumbs'][] = ['label' => 'All my Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manage-request">
  <h1>
    <?= Html::encode($this->title) ?> ID:<span class="text-teal"> <?= $modelRequest->code ?> </span>
  </h1>
  <div class="row">
    <div class="modal fade" id="modal-test" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-mm modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Title test</h3>
          </div>
          <div class="modal-body">
            <div class="modal-test-content"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-mm modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Upload absorbance <?= Html::encode($this->title) ?></h3>
          </div>
          <div class="modal-body">
            <div class="modal-upload-content"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box">


    <!-- nav nav-pills nav-justified -->
    <div class="box-body">
      <ul class="nav nav-tabs nav-pills nav-justified" id="myTab">
        <li class="active"><a data-toggle="tab" href="#detail">REQUEST DETAILS</a></li>
        <li class=""><a data-toggle="tab" href="#tracking">TRACKING</a></li>
        <li class=""><a data-toggle="tab" href="#payment">PAYMENT</a></li>
        <li class=""><a data-toggle="tab" href="#results">RESULTS</a></li>
        <!-- <li><a data-toggle="tab" href="#statistics">STATISTICS</a></li> -->
        <!-- <li><a data-toggle="tab" href="#notifications">NOTIFICATIONS</a></li> -->
      </ul>
      <div class="tab-content">
        <!-- ♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦-->
        <div id="detail" class="tab-pane fade in active">
          <h3>REQUEST DETAILS</h3>
          <div class="col-lg-12 small-padding">
            <fieldset class="groupbox-border">
              <legend class="groupbox-border">
                <span class='text-teal'>
                  <h4>Detailed information</h4>
                </span>
              </legend>
              <div class="content">
                <?= $this->render('_manage-request-detail', [
                  'modelRequest' => $modelRequest,
                  'arrayRequestProcess' => $arrayRequestProcess,
                  'arrayListFinancialConceptOne' => $arrayListFinancialConceptOne,
                ]) ?>
              </div>
            </fieldset>
          </div>
          <div class="col-lg-12 small-padding">
            <?php foreach ($arrayRequestProcess as $key => $value) { ?>
              <fieldset class="groupbox-border">
                <legend class="groupbox-border">
                  <span class='text-teal'>
                    <h4>Workflows</h4>
                  </span>
                </legend>
                <div class="content">
                  <?= $this->render('_manage-request-workflow', [
                      'arrayValueRequestProcess' => $value,
                      'arrayAgentByRequestProcessDetail' => array_filter($arrayAgentByRequestProcessDetail, function ($element) use ($value) {
                        return ($element['numOrderId'] == $value['numOrderId']);
                      }),
                      'arrayListSample' =>  array_filter($arrayListSample, function ($element) use ($value) {
                        return ($element['numOrderId'] == $value['numOrderId']);
                      }),
                    ]) ?>
                </div>
              </fieldset>
            <?php } ?>
          </div>
        </div>
        <!-- ♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦-->
        <div id="tracking" class="tab-pane fade">
          <?php Pjax::begin(['id' => 'pjax-manage-request-tracking']); ?>
          <?php $form_manage_request_tracking = ActiveForm::begin(
            [
              'id' => 'form-manage-request-tracking',
              'options' => [
                'data-pjax' => true,
                'id' => 'dynamic-form-manage-request-tracking',
              ]
            ]
          ); ?>
          <h3>TRACKING</h3>
          <div class=" col-lg-12 small-padding">
            <fieldset class="groupbox-border">
              <legend class="groupbox-border">
                <span class='text-teal'>
                  <h4>Tracking detail</h4>
                </span>
              </legend>
              <div class="content">
                <?= $this->render('_manage_request_tracking', [
                  'modelRequest' => $modelRequest,
                ]);
                ?>
              </div>
            </fieldset>
          </div>
          <?php ActiveForm::end(); ?>
          <?php Pjax::end(); ?>
        </div>
        <!-- ♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦-->
        <div id="payment" class="tab-pane fade">
          <h3>PAYMENT</h3>
          <div class="col-lg-12 small-padding">
            <fieldset class="groupbox-border">
              <legend class="groupbox-border">
                <span class='text-teal'>
                  <h4>Payment detail</h4>
                </span>
              </legend>
              <div class="content">
                <?= $this->render('_manage_request_payment', [
                  'dataProviderFinancialConceptOne' => $dataProviderFinancialConceptOne,
                ]) ?>
              </div>
            </fieldset>
          </div>
        </div>
        <!-- ♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦-->
        <div id="results" class="tab-pane fade ">
          <?php //Pjax::begin(['id' => 'pjax-manage-request-results']); 
          ?>
          <?php $form_manage_request_results = ActiveForm::begin(
            [
              'id' => 'form-manage-request-results',
              'method' => 'post',
              'options' => [
                // 'data-pjax' => true,
                'id' => 'dynamic-form-manage-request-results',
              ]
            ]
          ); ?>

          <h3>RESULTS</h3>


          <div class="col-lg-12 small-padding">
            <fieldset class="groupbox-border">
              <legend class="groupbox-border">
                <span class='text-teal'>
                  <h4>Report navigation</h4>
                </span>
              </legend>
              <div class="content">
                <?= $this->render('_manage-request-tree-view', [
                  'requestId' => $requestId,
                  'listNavigation' => $listNavigation,
                  'modelListRequestProcess' => $modelListRequestProcess,
                ]) ?>
              </div>
            </fieldset>
          </div>


          <div class="col-lg-12 small-padding">
            <fieldset class="groupbox-border">
              <legend class="groupbox-border">
                <span class='text-teal'>
                  <?php $data = Yii::$app->request->get();
                  if (isset($data['numOrderId']) and isset($data['essayId'])) {
                    $numOrderId = $data['numOrderId'];
                    $essayId = $data['essayId']; ?>
                    <h4>Report results (<?= $numOrderId ?>. <?= Essay::findOne($essayId)->shortName; ?>)</h4>
                  <?php } else { ?>
                    <h4>Report results</h4>
                  <?php }  ?>
                </span>
              </legend>
              <div class="content">
                <?= $this->render('_manage-request-result', [
                  'modelRequest' => $modelRequest,
                  'arrayRequestProcess' => $arrayRequestProcess,
                  'arrayRequestProcessDetail' => $arrayRequestProcessDetail,
                  'arrayAgentByRequestProcessDetail' => $arrayAgentByRequestProcessDetail,
                  'dataProvider' => $dataProvider,
                  'columns' => $columns,
                ]) ?>
              </div>
            </fieldset>
          </div>

          <?php ActiveForm::end(); ?>
          <?php //Pjax::end(); 
          ?>
        </div>
        <!-- ♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦-->
        <!-- <div id="statistics" class="tab-pane fade">
          <h3>STATISTICS</h3>
        </div> -->
        <!-- ♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦♠♦-->
        <!-- <div id="notifications" class="tab-pane fade">
          <h3>NOTIFICATIONS</h3>
          <p></p>
        </div> -->
      </div>

    </div>
  </div>

</div>