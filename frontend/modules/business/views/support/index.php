<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\SupportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Supports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="support-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Support', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'columns',
            'rows',
            'totalSlots',
            'usedSlots',
            //'availableSlots',
            //'ctrlQty',
            //'registeredBy',
            //'registeredAt',
            //'updatedBy',
            //'updatedAt',
            //'deletedBy',
            //'deletedAt',
            //'status',
            //'requestId',
            //'cropId',
            //'workFlowId',
            //'numOrderId',
            //'activityId',
            //'essayId',
            //'supportTypeId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
