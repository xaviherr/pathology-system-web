<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Support */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="support-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'columns')->textInput() ?>

    <?= $form->field($model, 'rows')->textInput() ?>

    <?= $form->field($model, 'totalSlots')->textInput() ?>

    <?= $form->field($model, 'usedSlots')->textInput() ?>

    <?= $form->field($model, 'availableSlots')->textInput() ?>

    <?= $form->field($model, 'ctrlQty')->textInput() ?>

    <?= $form->field($model, 'registeredBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredAt')->textInput() ?>

    <?= $form->field($model, 'updatedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'disabled' => 'Disabled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'requestId')->textInput() ?>

    <?= $form->field($model, 'cropId')->textInput() ?>

    <?= $form->field($model, 'workFlowId')->textInput() ?>

    <?= $form->field($model, 'numOrderId')->textInput() ?>

    <?= $form->field($model, 'activityId')->textInput() ?>

    <?= $form->field($model, 'essayId')->textInput() ?>

    <?= $form->field($model, 'supportTypeId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
