<?php

namespace frontend\modules\business\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\intelligence\models\RequestProcess;

/**
 * This is the model class for table "Sample".
 *
 * @property int $id
 * @property int $numOrder
 * @property string $accessionCode
 * @property int $accessionNumber
 * @property string $accessionName
 * @property string $collectingCode
 * @property int $collectingNumber
 * @property string $labCode
 * @property int $labNumber
 * @property string $female
 * @property string $male
 * @property string $isBulk
 * @property string $observation
 * @property string $acquisitionRequest
 * @property string $distributionRequest
 * @property string $details
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $plate
 * @property int $requestId
 * @property int $cropId
 * @property int $workFlowId
 * @property int $numOrderId
 * @property int $sampleTypeId
 * @property int $sourceId
 * @property int $sampleCrossId
 * @property int $graftingCount
 * @property string $firstUserCode
 * @property string $secondUserCode
 * @property string $thirdUserCode
 *
 * @property ReadingData[] $readingDatas
 * @property Parameter $sampleType
 * @property Parameter $source
 * @property Parameter $sampleCross
 * @property RequestProcess $request
 */
class Sample extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Sample';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numOrder', 'accessionNumber', 'collectingNumber', 'labNumber', 'plate', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'sampleTypeId', 'sourceId', 'sampleCrossId', 'graftingCount'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['requestId', 'cropId', 'workFlowId', 'numOrderId'], 'required'],
            [['accessionCode', 'accessionName', 'collectingCode', 'labCode', 'isBulk', 'acquisitionRequest', 'distributionRequest', 'registeredBy', 'updatedBy', 'deletedBy', 'firstUserCode', 'secondUserCode', 'thirdUserCode'], 'string', 'max' => 45],
            [['female', 'male',], 'string', 'max' => 100],
            [['observation', 'details'], 'string', 'max' => 245],
            [['sampleTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['sampleTypeId' => 'id']],
            [['sourceId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['sourceId' => 'id']],
            [['sampleCrossId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['sampleCrossId' => 'id']],

            // [
            //     [
            //         'requestId',
            //         'cropId',
            //         'workFlowId',
            //         'numOrderId'
            //     ],
            //     'exist',
            //     'skipOnError' => true,
            //     'targetClass' => RequestProcess::className(),
            //     'targetAttribute' => [
            //         'requestId' => 'requestId',
            //         'cropId' => 'cropId',
            //         'workFlowId' => 'workFlowId',
            //         'numOrderId' => 'numOrderId'
            //     ]
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numOrder' => 'Num Order',
            'accessionCode' => 'Accession Code',
            'accessionNumber' => 'Accession Number',
            'accessionName' => 'Accession Name',
            'collectingCode' => 'Collecting Code',
            'collectingNumber' => 'Collecting Number',
            'labCode' => 'Lab Code',
            'labNumber' => 'Lab Number',
            'female' => 'Female',
            'male' => 'Male',
            'isBulk' => 'Is Bulk',
            'observation' => 'Observation',
            'acquisitionRequest' => 'Acquisition Request',
            'distributionRequest' => 'Distribution Request',
            'details' => 'Details',

            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',

            'plate' => 'Plate',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
            'sampleTypeId' => 'Sample Type ID',
            'sourceId' => 'Source ID',
            'sampleCrossId' => 'Sample Cross ID',
            'graftingCount' => 'Grafting Count',
            'firstUserCode' => 'Other Code 1',
            'secondUserCode' => 'Other Code 2',
            'thirdUserCode' => 'Observation',
        ];
    }

    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::className(), ['sampleId' => 'id']);
    }

    public function getSampleType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'sampleTypeId']);
    }

    public function getSource()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'sourceId']);
    }

    public function getSampleCross()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'sampleCrossId']);
    }

    public function getRequest()
    {
        return $this->hasOne(RequestProcess::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId']);
    }

    public function getCrop()
    {
        return $this->hasOne(Crop::className(), ['id' => 'cropId']);
    }


    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
