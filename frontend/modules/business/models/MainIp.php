<?php

namespace frontend\modules\business\models;

use Yii;

/**
 * This is the model class for table "{{%main}}".
 *
 * @property string $accession_id_key Accession Number
 * @property string $CIPNUMBER Accession Number
 * @property string $CROP Common Crop Name
 * @property string $GENUS Genus
 * @property string $SPECIES Species
 * @property string $SUBTAXON Subtaxon
 * @property string $CULTVRNAME Accession Name
 * @property string $OTHER_IDENT Other identification
 * @property string $FEMALE Parent Female
 * @property string $MALE Parent Male
 * @property string $BIOSTAT Biological Status
 * @property int $DATERCVD Acquisition date
 * @property int $DATECOL Collecting date of sample
 * @property string $COLNUMBER Collecting Number/Breeder Code
 * @property string $COUNTRY Country of Origin
 * @property string $ADMIN1 Administrative subdivision 1
 * @property string $ADMIN2 Administrative subdivision 2
 * @property string $ADMIN3 Administrative subdivision 3
 * @property string $LOCALITY Location of collecting site
 * @property double $LATCOOR Latitude of collecting site
 * @property double $LONGCOOR Longitude of collecting site
 * @property string $CATALOGUE Link to catalogue o potato
 * @property string $ORDERING Link to ordering system
 * @property string $AVAILABLE Available
 * @property string $STATUS Genebank accession status
 * @property string $IDENTITY_RESULT Identity result
 * @property string $COMMENTS Remarks
 * @property string $MLSSTAT MLS status
 * @property string $TI International Treaty
 * @property string $ANEXO1 Annex 1
 * @property string $Health_Status Health Status
 * @property string $PopulationGroup
 */
class MainIp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%main}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db3');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['accession_id_key'], 'required'],
            [['DATERCVD', 'DATECOL'], 'integer'],
            [['LATCOOR', 'LONGCOOR'], 'number'],
            [['accession_id_key', 'CIPNUMBER'], 'string', 'max' => 20],
            [['CROP', 'Health_Status'], 'string', 'max' => 15],
            [['GENUS', 'SPECIES', 'SUBTAXON', 'COLNUMBER', 'COUNTRY'], 'string', 'max' => 50],
            [['CULTVRNAME'], 'string', 'max' => 80],
            [['OTHER_IDENT', 'BIOSTAT'], 'string', 'max' => 35],
            [['FEMALE', 'MALE', 'ADMIN1', 'ADMIN2', 'ADMIN3', 'LOCALITY'], 'string', 'max' => 100],
            [['CATALOGUE', 'ORDERING', 'COMMENTS'], 'string', 'max' => 200],
            [['AVAILABLE', 'IDENTITY_RESULT'], 'string', 'max' => 30],
            [['STATUS'], 'string', 'max' => 10],
            [['MLSSTAT'], 'string', 'max' => 2],
            [['TI', 'ANEXO1'], 'string', 'max' => 1],
            [['PopulationGroup'], 'string', 'max' => 70],
            [['accession_id_key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'accession_id_key' => 'Accession Id Key',
            'CIPNUMBER' => 'Accession Number', //NEED
            'CROP' => 'Crop',
            'GENUS' => 'Genus',
            'SPECIES' => 'Species',
            'SUBTAXON' => 'Subtaxon',
            'CULTVRNAME' => 'Accession Name', //NEED
            'OTHER_IDENT' => 'Other  Ident',
            'FEMALE' => 'Female Parent', //NEED
            'MALE' => 'Male Parent', //NEED
            'BIOSTAT' => 'Biostat',
            'DATERCVD' => 'Datercvd',
            'DATECOL' => 'Datecol',
            'COLNUMBER' => 'ColNumber / BreederCode', //NEED
            'COUNTRY' => 'Country',
            'ADMIN1' => 'Admin1',
            'ADMIN2' => 'Admin2',
            'ADMIN3' => 'Admin3',
            'LOCALITY' => 'Locality',
            'LATCOOR' => 'Latcoor',
            'LONGCOOR' => 'Longcoor',
            'CATALOGUE' => 'Catalogue',
            'ORDERING' => 'Ordering',
            'AVAILABLE' => 'Available',
            'STATUS' => 'Status',
            'IDENTITY_RESULT' => 'Identity  Result',
            'COMMENTS' => 'Comments',
            'MLSSTAT' => 'Mlsstat',
            'TI' => 'Ti',
            'ANEXO1' => 'Anexo1',
            'Health_Status' => 'Health  Status',
            'PopulationGroup' => 'Population Group',
        ];
    }
}
