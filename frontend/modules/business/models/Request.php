<?php

namespace frontend\modules\business\models;

use Yii;
use common\models\User;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\financial\models\FinancialConceptOne;

use frontend\modules\intelligence\models\RequestProcess;


/**
 * This is the model class for table "Request".
 *
 * @property int $id
 * @property string $code
 * @property string $details
 * @property string $diagnostic
 * @property int $agentQty
 * @property int $sampleQty
 * @property double $totalCost
 * @property string $requiredDate Date on which the requirement is recorded
 * @property string $approvalDate Date on which the request was approved
 * @property string $initDate Date when the process starts
 * @property string $completionDate Date when the process completes
 * @property int $requestTypeId
 * @property int $paymentTypeId
 * @property int $requestStatusId
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $userId
 * @property string $payNumber
 *
 * @property Parameter $requestType
 * @property Parameter $paymentType
 * @property Parameter $requestStatus
 * @property User $user
 * @property RequestProcess[] $requestProcesses
 * @property WorkFlowByCrop[] $crops
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agentQty', 'sampleQty', 'requestTypeId', 'paymentTypeId', 'requestStatusId', 'userId'], 'integer'],
            [['totalCost'], 'number'],
            [['creationDate', 'approvalDate', 'rejectionDate', 'uploadDate', 'validationDate', 'reprocessDate', 'notificationDate', 'verificationDate', 'registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['userId'], 'required'],
            [['status'], 'string'],
            [['code', 'registeredBy', 'updatedBy', 'deletedBy', 'payNumber'], 'string', 'max' => 45],
            [['details', 'diagnostic'], 'string', 'max' => 450],
            [['requestTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['requestTypeId' => 'id']],
            [['paymentTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['paymentTypeId' => 'id']],
            [['requestStatusId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['requestStatusId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'details' => 'Note',
            'diagnostic' => 'Diagnostic',
            'agentQty' => 'Agent Qty',
            'sampleQty' => 'Sample Qty',
            'totalCost' => 'Total fee',
            'creationDate' => 'Creation Date',
            'approvalDate' => 'Approval Date',
            'rejectionDate' => 'Rejection Date',
            'uploadDate' => 'Upload Date',
            'validationDate' => 'Validation Date',
            'reprocessDate' => 'Reprocess Date',
            'notificationDate' => 'Notification Date',
            'verificationDate' => 'Verification Date',
            'requestTypeId' => 'Request Type ID',
            'paymentTypeId' => 'Payment type',
            'requestStatusId' => 'Request Status ID',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'userId' => 'User ID',
            'payNumber' => 'Payment detail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'requestTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'paymentTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestStatus()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'requestStatusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::className(), ['requestId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialConceptsOne()
    {
        return $this->hasMany(FinancialConceptOne::className(), ['requestId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrops()
    {
        return $this->hasMany(WorkFlowByCrop::className(), ['cropId' => 'cropId', 'workFlowId' => 'workFlowId'])->viaTable('RequestProcess', ['requestId' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->email;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            // $this->deletedBy = null;
            // $this->deletedAt = null;
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->email;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
