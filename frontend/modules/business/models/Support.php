<?php

namespace frontend\modules\business\models;

use Yii;
use frontend\modules\intelligence\models\RequestProcessDetail;
use frontend\modules\intelligence\models\ReadingData;


/**
 * This is the model class for table "Support".
 *
 * @property int $id
 * @property int $columns
 * @property int $rows
 * @property int $totalSlots
 * @property int $usedSlots
 * @property int $availableSlots
 * @property int $ctrlQty
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $requestId
 * @property int $cropId
 * @property int $workFlowId
 * @property int $numOrderId
 * @property int $activityId
 * @property int $essayId
 * @property int $supportTypeId
 *
 * @property ReadingData[] $readingDatas
 * @property RequestProcessDetail $request
 */
class Support extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Support';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId'], 'required'],
            [['id', 'columns', 'rows', 'totalSlots', 'usedSlots', 'availableSlots', 'ctrlQty', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'supportTypeId'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['id', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId'], 'unique', 'targetAttribute' => ['id', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId']],
            [['requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessDetail::className(), 'targetAttribute' => ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId', 'activityId' => 'activityId', 'essayId' => 'essayId']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'columns' => 'Columns',
            'rows' => 'Rows',
            'totalSlots' => 'Total Slots',
            'usedSlots' => 'Used Slots',
            'availableSlots' => 'Available Slots',
            'ctrlQty' => 'Ctrl Qty',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
            'activityId' => 'Activity ID',
            'essayId' => 'Essay ID',
            'supportTypeId' => 'Support Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::className(), ['supportId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(RequestProcessDetail::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId', 'activityId' => 'activityId', 'essayId' => 'essayId']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
