<?php

namespace frontend\modules\business\models;

use Yii;

/**
 * This is the model class for table "{{%main}}".
 *
 * @property string $CIPNUMBER
 * @property string $CIPNUMBER_key
 * @property string $CROP
 * @property string $WebSMTA_bool
 * @property string $WebSMTA2
 * @property string $FAO
 * @property string $CULTVRNAME
 * @property string $COLNUMBER
 * @property string $ALTERID
 * @property string $OTHER_IDENT
 * @property string $SINONIMO
 * @property string $CCODE
 * @property string $MISSCODE
 * @property string $COUNTRY
 * @property string $STATE
 * @property string $REGION
 * @property string $REGION2
 * @property string $LOCALITY
 * @property int $SOURCE
 * @property string $SOURCIP
 * @property int $ALT
 * @property double $LATCOOR
 * @property double $LONGCOOR
 * @property string $BIOSTAT
 * @property string $SPP
 * @property string $SPECIES
 * @property string $GENUS
 * @property string $PopulationGroup
 * @property string $FEMALE
 * @property string $MALE
 * @property string $DCCODE
 * @property string $DCTRY
 * @property int $DATERCVD
 * @property int $DATECOL
 * @property string $DONOR
 * @property string $COMMENTS
 * @property string $STATUS
 * @property string $Health_Status
 * @property string $Distribution_Status
 * @property string $STORA
 * @property string $BIOSTAT2
 * @property string $corporate
 * @property string $CULTVRNAME2
 * @property string $FEMALE2
 * @property string $MALE2
 * @property string $COLNUMBER2
 * @property string $Health_Status2
 * @property string $Corporativa
 * @property string $CREATE Creation date
 * @property string $CIPFBS
 */
class Main extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%main}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CIPNUMBER_key'], 'required'],
            [['SOURCE', 'ALT', 'DATERCVD', 'DATECOL'], 'integer'],
            [['LATCOOR', 'LONGCOOR'], 'number'],
            [['CREATE'], 'safe'],
            [['CIPNUMBER', 'CIPNUMBER_key', 'STATE', 'Distribution_Status'], 'string', 'max' => 20],
            [['CROP', 'SPP', 'Health_Status'], 'string', 'max' => 15],
            [['WebSMTA_bool', 'WebSMTA2', 'FAO', 'STORA', 'corporate', 'Corporativa'], 'string', 'max' => 1],
            [['CULTVRNAME'], 'string', 'max' => 80],
            [['COLNUMBER', 'ALTERID', 'MISSCODE', 'SPECIES', 'GENUS', 'DCCODE', 'CIPFBS'], 'string', 'max' => 50],
            [['OTHER_IDENT', 'SINONIMO', 'BIOSTAT', 'BIOSTAT2'], 'string', 'max' => 35],
            [['CCODE', 'SOURCIP'], 'string', 'max' => 12],
            [['COUNTRY', 'DCTRY'], 'string', 'max' => 7],
            [['REGION'], 'string', 'max' => 30],
            [['REGION2', 'LOCALITY'], 'string', 'max' => 90],
            [['PopulationGroup'], 'string', 'max' => 71],
            [['FEMALE', 'MALE'], 'string', 'max' => 100],
            [['DONOR'], 'string', 'max' => 150],
            [['COMMENTS'], 'string', 'max' => 200],
            [['STATUS'], 'string', 'max' => 4],
            [['CULTVRNAME2', 'FEMALE2', 'MALE2', 'COLNUMBER2', 'Health_Status2'], 'string', 'max' => 22],
            [['CIPNUMBER_key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CIPNUMBER' => 'Accession Number', //NEED
            'CIPNUMBER_key' => 'Cipnumber Key',
            'CROP' => 'Crop',
            'WebSMTA_bool' => 'Web Smta Bool',
            'WebSMTA2' => 'Web Smta2',
            'FAO' => 'Fao',
            'CULTVRNAME' => 'Accession Name', //NEED
            'COLNUMBER' => 'ColNumber / BreederCode', //NEED
            'ALTERID' => 'Alterid',
            'OTHER_IDENT' => 'Other  Ident',
            'SINONIMO' => 'Sinonimo',
            'CCODE' => 'Ccode',
            'MISSCODE' => 'Misscode',
            'COUNTRY' => 'Country',
            'STATE' => 'State',
            'REGION' => 'Region',
            'REGION2' => 'Region2',
            'LOCALITY' => 'Locality',
            'SOURCE' => 'Source',
            'SOURCIP' => 'Sourcip',
            'ALT' => 'Alt',
            'LATCOOR' => 'Latcoor',
            'LONGCOOR' => 'Longcoor',
            'BIOSTAT' => 'Biostat',
            'SPP' => 'Spp',
            'SPECIES' => 'Species',
            'GENUS' => 'Genus',
            'PopulationGroup' => 'Population Group',
            'FEMALE' => 'Female Parent', //NEED
            'MALE' => 'Male Parent', //NEED
            'DCCODE' => 'Dccode',
            'DCTRY' => 'Dctry',
            'DATERCVD' => 'Datercvd',
            'DATECOL' => 'Datecol',
            'DONOR' => 'Donor',
            'COMMENTS' => 'Comments',
            'STATUS' => 'Status',
            'Health_Status' => 'Health  Status',
            'Distribution_Status' => 'Distribution  Status',
            'STORA' => 'Stora',
            'BIOSTAT2' => 'Biostat2',
            'corporate' => 'Corporate',
            'CULTVRNAME2' => 'Cultvrname2',
            'FEMALE2' => 'Female2',
            'MALE2' => 'Male2',
            'COLNUMBER2' => 'Colnumber2',
            'Health_Status2' => 'Health  Status2',
            'Corporativa' => 'Corporativa',
            'CREATE' => 'Create',
            'CIPFBS' => 'Cipfbs',
        ];
    }
}
