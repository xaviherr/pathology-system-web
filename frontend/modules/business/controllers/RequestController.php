<?php

namespace frontend\modules\business\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\User;
use app\models\UploadFile;
// ---------------------------------------------------------------------
use frontend\modules\intelligence\models\RequestProcess;
use frontend\modules\intelligence\models\RequestProcessSearch;
use frontend\modules\intelligence\models\RequestProcessDetail;
use frontend\modules\intelligence\models\RequestProcessDetailSearch;
use frontend\modules\intelligence\models\AgentByRequestProcessDetail;
use frontend\modules\intelligence\models\ReadingData;
use frontend\modules\intelligence\models\ReadingDataSearch;
// ---------------------------------------------------------------------
use frontend\modules\control\models\ReadingDataTypeByEssay;
use frontend\modules\control\models\EssayByWorkFlow;
// ---------------------------------------------------------------------
use frontend\modules\configuration\models\ReadingDataType;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Cost;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Notification;
use frontend\modules\configuration\models\Composition;
use frontend\modules\configuration\models\Activity;
// ---------------------------------------------------------------------
use frontend\modules\business\models\Request;
use frontend\modules\business\models\RequestSearch;
use frontend\modules\business\models\MainSearch;
use frontend\modules\business\models\MainIpSearch;
use frontend\modules\business\models\Sample;
use frontend\modules\business\models\SampleSearch;
// ---------------------------------------------------------------------
use frontend\modules\financial\models\FinancialTransaction;
use frontend\modules\financial\models\FinancialConceptOne;
use frontend\complements\Financial;
use frontend\modules\business\models\Support;
use frontend\modules\configuration\models\Symptom;
use frontend\modules\control\models\ActivityByEssay;
use frontend\modules\financial\models\FinancialAccount;
use PHPExcel_IOFactory;
// ---------------------------------------------------------------------
class RequestController extends Controller
{

    public $enableCsrfValidation = false;

    //BEHAVIORS

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionOverview($id)
    {
        $modelRequest = $this->findModel($id);

        $searchModelRequestProcess = new RequestProcessSearch();
        $dataProviderRequestProcess = $searchModelRequestProcess->search(Yii::$app->request->queryParams);
        $dataProviderRequestProcess->query->andWhere(['requestId' => $id]);
        $dataProviderRequestProcess->query->andWhere(['deletedBy' => null]);
        $dataProviderRequestProcess->query->andWhere(['deletedAt' => null]);
        $dataProviderRequestProcess->pagination = false;

        $searchModelRequestProcessDetail = new RequestProcessDetailSearch();
        $dataProviderRequestProcessDetail = $searchModelRequestProcessDetail->search(Yii::$app->request->queryParams);
        $dataProviderRequestProcessDetail->query->andWhere(['requestId' => $id]);
        $dataProviderRequestProcessDetail->query->andWhere(['deletedBy' => null]);
        $dataProviderRequestProcessDetail->query->andWhere(['deletedAt' => null]);
        $dataProviderRequestProcessDetail->pagination = false;

        return $this->renderAjax('overview', [
            'modelRequest' => $modelRequest,
            'dataProviderRequestProcess' => $dataProviderRequestProcess,
            'dataProviderRequestProcessDetail' => $dataProviderRequestProcessDetail
        ]);
    }

    protected function convert_key(&$array, $newId)
    {
        $array_Ids = array_column($array, $newId);
        array_walk($array_Ids, function (&$value, $key) {
            return $value = '_' . $value;
        });
        return $array = array_combine($array_Ids, $array);
    }

    protected function delete_col(&$array, $offset)
    {
        return array_walk($array, function (&$v) use ($offset) {
            array_splice($v, $offset, 1);
        });
    }

    protected function delete_cols($array, $array_columns)
    {
        if (!is_null($array) and !empty($array)) {
            $keys1 = array_keys($array[array_keys($array)[0]]);
            krsort($keys1);
            foreach ($keys1 as $key => $value) {
                if (!in_array($value, $array_columns)) {
                    $this->delete_col($array, $key);
                }
            }
        }
        return  $array;
    }

    protected function merge_left($new_id_01,   $array_01,  $new_id_02,  $array_02, $new_attribute)
    {
        $this->convert_key($array_01, $new_id_01);
        $sub_array_01 = array_column($array_01, 'id', 'id');
        array_walk($sub_array_01, function (&$value, $key) {
            return $value = ['sampleId' =>  $key, 'result' => null];
        });
        $this->convert_key($sub_array_01, 'sampleId');
        $this->convert_key($array_02, $new_id_02);
        $sub_array_02 = array_merge($sub_array_01, $array_02);
        array_walk($sub_array_02, function (&$value, $key) use ($new_attribute) {
            return $value = [$new_attribute => '<span class = "td-' . $value['result'] . '">' . $value['result'] . '</span>'];
        });
        $result = array_merge_recursive($array_01, $sub_array_02);
        return $result;
    }

    protected function get_columns($array)
    {

        $columns = [];
        if (!empty($array)) {
            $columns = array_keys($array[array_keys($array)[0]]);
            array_walk($columns, function (&$value, $key) {
                return $value =
                    [
                        'header'        => $value,
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'text-center'],
                        'enableSorting' => 'false',
                        'value'     =>  $value,
                    ];
            });
        }
        return $columns;
    }

    public function actionReportProcess($id)
    {
        $modelRequest = $this->findModel($id);
        $searchModelRequestProcess = new RequestProcessSearch();
        $dataProviderRequestProcess = $searchModelRequestProcess->search(Yii::$app->request->queryParams);
        $dataProviderRequestProcess->query->andWhere(['requestId' => $id]);
        $dataProviderRequestProcess->query->andWhere(['deletedBy' => null]);
        $dataProviderRequestProcess->query->andWhere(['deletedAt' => null]);
        $dataProviderRequestProcess->pagination = false;
        $searchModelRequestProcessDetail = new RequestProcessDetailSearch();
        $dataProviderRequestProcessDetail = $searchModelRequestProcessDetail->search(Yii::$app->request->queryParams);
        $dataProviderRequestProcessDetail->query->andWhere(['requestId' => $id]);
        $dataProviderRequestProcessDetail->query->andWhere(['deletedBy' => null]);
        $dataProviderRequestProcessDetail->query->andWhere(['deletedAt' => null]);
        $dataProviderRequestProcessDetail->pagination = false;
        $searchModelReadingData = new ReadingDataSearch();
        $dataProviderReadingData = $searchModelReadingData->search(Yii::$app->request->queryParams);
        $dataProviderReadingData->query->andWhere(['requestId' => $id]);
        $dataProviderReadingData->query->andWhere(['deletedBy' => null]);
        $dataProviderReadingData->query->andWhere(['deletedAt' => null]);
        $dataProviderReadingData->pagination = false;
        $searchModelSample = new SampleSearch();
        $dataProviderSample = $searchModelSample->search(Yii::$app->request->queryParams);
        $dataProviderSample->query->andWhere(['requestId' => $id]);
        $dataProviderSample->query->andWhere(['deletedBy' => null]);
        $dataProviderSample->query->andWhere(['deletedAt' => null]);
        $dataProviderSample->pagination = false;
        $dataProviders = [];

        /***SAMPLES***/
        $arraySample = Sample::find()->where(['requestId' => $id, 'status' => 'active'])->asArray()->all();
        /*************/

        /***READING_DATAS***/
        $arrayReadingData = ReadingData::find()->where(['requestId' => $id, 'status' => 'active'])->asArray()->all();
        /*************/

        /***REQUEST_PROCESS***/
        $arrayRequestProcess = RequestProcess::find()->where(['requestId' => $id, 'status' => 'active'])->asArray()->all();
        /*************/

        /***REQUEST_PROCESS_DETAIL***/
        $arrayRequestProcessDetail = RequestProcessDetail::find()->where(['requestId' => $id, 'status' => 'active'])->asArray()->all();
        /*************/


        $arrayDataSampleDetail = [];
        $arrayDesign = [];



        $arrayRequestProcesses =  array_column($arrayRequestProcess, 'workFlowId');
        foreach ($arrayRequestProcesses as $key01 => $value_workFlowId) {
            // -------------------------------------------------------------------------------------------
            $oneWorkFlow = WorkFlow::find()->where(['id' => $value_workFlowId, 'status' => 'active',])->asArray()->one();
            $arrayDesign[$value_workFlowId] =
                [
                    'workFlowId' =>  $value_workFlowId,
                    'value' => $oneWorkFlow['shortName'],
                    'essays' => []
                ];
            // -------------------------------------------------------------------------------------------
            $arraySample_WorkFlow = array_filter($arraySample, function ($element) use ($value_workFlowId) {
                return ($element['workFlowId'] == $value_workFlowId);
            });
            $arraySample_WorkFlow_New = $this->delete_cols(
                $arraySample_WorkFlow,
                [
                    'id',
                    'accessionCode',
                    'accessionName',
                    'collectingCode',
                    'female',
                    'male'
                ]
            );
            // -------------------------------------------------------------------------------------------
            $arrayReadingData_WorkFlow = array_filter($arrayReadingData, function ($element) use ($value_workFlowId) {
                return ($element['workFlowId'] == $value_workFlowId);
            });

            $arrayRequestProcessDetail_WorkFlow = array_filter($arrayRequestProcessDetail, function ($element) use ($value_workFlowId) {
                return ($element['workFlowId'] == $value_workFlowId);
            });

            $arrayEssayByWorkFlow = EssayByWorkFlow::find()->where(['workFlowId' => $value_workFlowId, 'status' => 'active',])->asArray()->all();
            $arrayEssays =  array_column($arrayEssayByWorkFlow, 'essayId');
            foreach ($arrayEssays as $key02 => $value_essayId) {
                // -------------------------------------------------------------------------------------------
                $oneEssay = Essay::find()->where(['id' => $value_essayId, 'status' => 'active',])->asArray()->one();
                $arrayDesign[$value_workFlowId]['essays'][$value_essayId] =
                    [
                        'essayId' =>  $value_essayId,
                        'value' => $oneEssay['shortName'],
                    ];
                // -------------------------------------------------------------------------------------------



                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                $arrayReadingData_WorkFlow_Essay = array_filter($arrayReadingData_WorkFlow, function ($element) use ($value_essayId) {
                    return ($element['essayId'] == $value_essayId);
                });

                $arrayRequestProcessDetail_WorkFlow_Essay = array_filter($arrayRequestProcessDetail_WorkFlow, function ($element) use ($value_essayId) {
                    return ($element['essayId'] == $value_essayId);
                });
                $arrayAgents =  array_column($arrayRequestProcessDetail_WorkFlow_Essay, 'agentId');

                $arrayReadingDataTypeByEssay = ReadingDataTypeByEssay::find()->where(['essayId' => $value_essayId, 'status' => 'active',])->asArray()->all();
                $arrayReadingDataTypes =  array_column($arrayReadingDataTypeByEssay, 'readingDataTypeId');
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

                $arraySample_WorkFlow_New_View02 = [];
                $arraySample_WorkFlow_New_View02 =  $arraySample_WorkFlow_New;
                /************************************************************BY AGENT************************************************************/
                foreach ($arrayAgents as $key03 => $value_agentId) {
                    $oneAgent = Agent::find()->where(['id' => $value_agentId, 'status' => 'active',])->asArray()->one();
                    $arrayReadingData_WorkFlow_Essay_Agent = array_filter($arrayReadingData_WorkFlow_Essay, function ($element) use ($value_agentId) {
                        return ($element['agentId'] == $value_agentId);
                    });
                    foreach ($arrayReadingDataTypes as $key04 => $value_readingDataTypeId) {
                        $oneReadingDataType = ReadingDataType::find()->where(['id' => $value_readingDataTypeId, 'status' => 'active',])->asArray()->one();
                        $arrayReadingData_WorkFlow_Essay_Agent_Type = array_filter($arrayReadingData_WorkFlow_Essay_Agent, function ($element) use ($value_readingDataTypeId) {
                            return ($element['readingDataTypeId'] == $value_readingDataTypeId);
                        });
                        $arrayReadingData_WorkFlow_Essay_Agent_Type_New = $this->delete_cols(
                            $arrayReadingData_WorkFlow_Essay_Agent_Type,
                            [
                                'result',
                                'sampleId',
                            ]
                        );
                        $columnHeader = $oneReadingDataType['shortName'] . " " . $oneAgent['shortName'];
                        $arraySample_WorkFlow_New_View02 =  $this->merge_left(
                            'id',
                            $arraySample_WorkFlow_New_View02,
                            'sampleId',
                            $arrayReadingData_WorkFlow_Essay_Agent_Type_New,
                            $columnHeader
                        );
                    }
                }
                /************************************************************END AGENT************************************************************/
                $arrayDataSampleDetail[$value_workFlowId][$value_essayId]['view01'] =  $arraySample_WorkFlow_New_View02;

                $arraySample_WorkFlow_New_View01 = [];
                $arraySample_WorkFlow_New_View01 =  $arraySample_WorkFlow_New;
                /************************************************************BY TYPE************************************************************/
                foreach ($arrayReadingDataTypes as $key03 => $value_readingDataTypeId) {
                    $oneReadingDataType = ReadingDataType::find()->where(['id' => $value_readingDataTypeId, 'status' => 'active',])->asArray()->one();
                    $arrayReadingData_WorkFlow_Essay_Type = array_filter($arrayReadingData_WorkFlow_Essay, function ($element) use ($value_readingDataTypeId) {
                        return ($element['readingDataTypeId'] == $value_readingDataTypeId);
                    });
                    foreach ($arrayAgents as $key04 => $value_agentId) {
                        $oneAgent = Agent::find()->where(['id' => $value_agentId, 'status' => 'active',])->asArray()->one();
                        $arrayReadingData_WorkFlow_Essay_Type_Agent = array_filter($arrayReadingData_WorkFlow_Essay_Type, function ($element) use ($value_agentId) {
                            return ($element['agentId'] == $value_agentId);
                        });
                        $arrayReadingData_WorkFlow_Essay_Type_Agent_New = $this->delete_cols(
                            $arrayReadingData_WorkFlow_Essay_Type_Agent,
                            [
                                'result',
                                'sampleId',
                            ]
                        );
                        $columnHeader = $oneReadingDataType['shortName'] . " " . $oneAgent['shortName'];
                        $arraySample_WorkFlow_New_View01 =  $this->merge_left(
                            'id',
                            $arraySample_WorkFlow_New_View01,
                            'sampleId',
                            $arrayReadingData_WorkFlow_Essay_Type_Agent_New,
                            $columnHeader
                        );
                    }
                }
                /************************************************************END TYPE************************************************************/
                $arrayDataSampleDetail[$value_workFlowId][$value_essayId]['view02'] =  $arraySample_WorkFlow_New_View01;

                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            }




            foreach ($arrayDataSampleDetail as $key_1 => $value_1) {
                foreach ($value_1 as $key_2 => $value_2) {
                    $keyProvider01 = (string) $key_1 . '_' . (string) $key_2 . '_' . 'view01';
                    $dataProviders[$keyProvider01] = new ArrayDataProvider([
                        'allModels' => $value_2['view01'],
                        'id' => $keyProvider01,
                        'sort' => ['attributes' => ['numOrder'],],
                        'pagination' => ['pageSize' => 1000]
                    ]);
                    $columns[$keyProvider01] = $this->get_columns($value_2['view01']);
                    $keyProvider02 = (string) $key_1 . '_' . (string) $key_2 . '_' . 'view02';
                    $dataProviders[$keyProvider02] = new ArrayDataProvider([
                        'allModels' => $value_2['view02'],
                        'id' => $keyProvider02,
                        'sort' => ['attributes' => ['numOrder'],],
                        'pagination' => ['pageSize' => 1000]
                    ]);
                    $columns[$keyProvider02] = $this->get_columns($value_2['view02']);
                }
            }
        }

        $rdTestAll = [];


        $supportArray = [];



        foreach ($dataProviderReadingData->models as $readingDataKey => $readingDataValue) {

            if (!array_key_exists($readingDataValue['supportId'],  $supportArray)) {
                $supportArray[$readingDataValue['supportId']] = $readingDataValue['workFlowId'] . '_' . $readingDataValue['essayId'] . '_support';
            }
            $oneAgentGraph = Agent::find()->where(['id' =>  $readingDataValue['agentId'], 'status' => 'active',])->asArray()->one();
            $oneSampleGraph = Sample::find()->where(['id' => $readingDataValue['sampleId'], 'status' => 'active',])->asArray()->one();
            $readingDataArray = [
                'supportId' =>  $readingDataValue['supportId'],
                'cellPosition' =>  $readingDataValue['cellPosition'],
                'agentId' =>  $readingDataValue['agentId'],
                'agentValue' =>  $oneAgentGraph['shortName'],
                'result' =>  $readingDataValue['result'],
                'sampleId' => $readingDataValue['sampleId'],
                'sampleValue' =>  $oneSampleGraph['accessionCode']
            ];
            $rdTestAll[$readingDataValue['supportId']][$readingDataValue['cellPosition']] = $readingDataArray;
        }
        $maxColumn = 12;
        $maxRow = 8;
        $control = 6;
        $total = 96;
        $row = "";
        $column = "";
        $value = "";
        $supportArrayGraph = [];
        foreach ($supportArray as $key => $value) {
            $support = '<div class="row"><div class="col-md-12"><table class="table-center">';
            for ($r = 1; $r <= $maxRow; $r++) {
                $row =  $row . "<tr class='tr-row'>";
                $support = $support . $row;
                for ($c = 0; $c < $maxColumn; $c++) {
                    $column =  $column . "<td class='td-column'>";
                    $support = $support . $column;
                    $classCell = "div-cell";
                    if (array_key_exists($c * $maxRow + ($r), $rdTestAll[$key])) {
                        $resultTest = " " . $rdTestAll[$key][$c * $maxRow + ($r)]['result'];
                        $sampleValue = " " . (string) $rdTestAll[$key][$c * $maxRow + ($r)]['sampleValue'];
                        $agentValue = " " . (string) $rdTestAll[$key][$c * $maxRow + ($r)]['agentValue'];
                    } else {
                        $resultTest = " available";
                        $sampleValue = "";
                        $agentValue = "";
                    }
                    $classCell = $classCell . $resultTest;
                    $text = "(" . (string) ($c * $maxRow + ($r)) . ") " . (string) $sampleValue . " " . (string) $agentValue;
                    $title = (string) $sampleValue . " - " . (string) $agentValue;
                    $support = $support .  "<div class='" . $classCell . "'>
                        <label class='span-cell' style='font-size: 8px;' title = '" . $title . "'>" . $text . "</label></div>";
                    $column = "</td>";
                    $support = $support . $column;
                }
                $row = " </tr>";
                $support = $support . $row;
            }
            $support = $support . "</table></div></div>";

            $supportArrayGraph[$value][$key] = $support;
        }
        return $this->render('report-process', [
            'modelRequest' => $modelRequest,
            'dataProviderRequestProcess' => $dataProviderRequestProcess,
            'dataProviderRequestProcessDetail' => $dataProviderRequestProcessDetail,
            'dataProviderReadingData' => $dataProviderReadingData,
            'dataProviders' => $dataProviders,
            'columns' => $columns,
            'supportArrayGraph' => $supportArrayGraph,
            'arrayDesign' => $arrayDesign,
        ]);
    }

    //♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦//
    //CREATE - UPDATE (FUNCTIONS)

    protected function getMapCrop()
    {
        $arrayMapCrop =  ArrayHelper::map(
            Crop::find()->where(
                [
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active',
                ]
            )
                ->orderBy('longName')
                ->all(),
            'id',
            'longName'
        );
        return  $arrayMapCrop;
    }

    protected function getMapWorkFlow($cropId)
    {
        $arrayMapWorkFlow = null;
        if (!is_null($cropId)) {
            $modelCrop = Crop::find()->where(['id' => $cropId, 'status' => 'active',])->one();
            $arrayMapWorkFlow =
                ArrayHelper::map(
                    $modelCrop->getWorkFlows()->where(
                        [
                            'deletedBy' => null,
                            'deletedAt' => null,
                            'status' => 'active',
                        ]
                    )->all(),
                    'id',
                    'shortName'
                );
        }
        return  $arrayMapWorkFlow;
    }

    protected function getMapEssayAgent($selectWorkFlowValue, $selectEssayAgentValue)
    {
        $arrayMapEssayAgent  =  "<div id='cbxlEssayAgent' class='cbxlStyle' disabled='disabled' aria-invalid='false'><br/><br/></div>";
        if (!is_null($selectEssayAgentValue)) {

            $arrayMapEssayAgent = $this->getCheckLists($selectWorkFlowValue, $selectEssayAgentValue);
            $arrayMapEssayAgent = "<div id='cbxlEssayAgent' class='cbxlStyle' disabled='disabled' aria-invalid='false'>" . $arrayMapEssayAgent . "</div>";
        }
        return $arrayMapEssayAgent;
    }

    protected function actionCheckLists($id, $arrayAgents  = null)
    {
        return  $this->getCheckLists($id, $arrayAgents);
    }

    protected function getCheckLists($id, $arrayAgents  = null)
    {
        $countWorkFlows = WorkFlow::find()->where(['id' => $id, 'deletedBy' => null, 'deletedAt' => null, 'status' => 'active'])->count();
        $workFlow = WorkFlow::find()->where(['id' => $id, 'deletedBy' => null, 'deletedAt' => null, 'status' => 'active'])->one();
        $result = "";
        if ($countWorkFlows > 0) {
            $result = "<fieldset class='groupbox-border'>
            <legend class='groupbox-border'><h4><i class='icon fa fa-cubes'></i> " . $workFlow->shortName . " </h4></legend>";
            $essays = ArrayHelper::map($workFlow->essays, 'id', 'shortName');
            foreach ($essays as $key0 => $value0) {
                $countEssays = Essay::find()->where(['id' => $key0, 'deletedBy' => null, 'deletedAt' => null, 'status' => 'active'])->count();
                $essay = Essay::find()->where(['id' => $key0, 'deletedBy' => null, 'deletedAt' => null, 'status' => 'active'])->one();


                if ($workFlow->id == 14 and $essay->essayTypeId == 95) {
                    $result = $result . "
                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='position: absolute; visibility: hidden;'>
                    <hr style='margin-bottom: 0px; margin-top: 0px;'>
                    <label class='cbxlStyle'>
                        <span  style='font-size: 12px;' ><i class='icon fa fa-cube'></i> " . $value0 .  "</span> 
                    </label>";
                } else {
                    $result = $result . "
                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                    <hr style='margin-bottom: 0px; margin-top: 0px;'>
                    <label class='cbxlStyle'>
                        <span  style='font-size: 12px;' ><i class='icon fa fa-cube'></i> " . $value0 .  "</span> 
                    </label>";
                }



                if ($countEssays > 0) {
                    $agents = ArrayHelper::map($essay->agents, 'id', 'shortName');
                    $agentsLongName = ArrayHelper::map($essay->agents, 'id', 'longName');
                    foreach ($agents as $key1 => $value1) {
                        $result = $result . "<div class='col-xs-6 col-sm-6 col-md-4 col-lg-3'>";
                        $result = $result . "  <label  class='cbxStyle' title='" .  $agentsLongName[$key1] .  "'>";
                        $extra = " ";
                        // first exception
                        if (!is_null($arrayAgents)) {
                            foreach ($arrayAgents as $key2 => $value2) {
                                if ($key0 == $key2  and array_key_exists($key1, $value2)) {
                                    $extra = " checked='true' ";
                                }
                            }
                        }
                        // second exception
                        if ($essay->essayTypeId == 95) {
                            $extra = " checked='true' ";
                        }
                        $result = $result . "      <input type='checkbox' name='selectEssayAgentName[" . $key0 . "][" . $key1 . "]' " . $extra . " value='" .  $value1  . "'>";
                        $result = $result . "          <span style='font-size: 10px;' class = 'badge bg-teal-gradient' style='white-space: normal;' >" . $value1 . "</span>";
                        $result = $result . "  </label>";
                        $result = $result . "</div>";
                    }
                } else {
                    $result = $result . "";
                }
                $result = $result . "</div>";
                // $result = $result . "<br /><br />";
            }
            $result = $result . "</fieldset>";
        }
        // $result = $result . "<br /><br />";
        return $result;
    }

    public function getValidated($post)
    {
        if (!is_null(Yii::$app->request->post($post))) {
            if (!empty(Yii::$app->request->post($post))) {
                return true;
            }
        }
        return false;
    }

    public function getSessionOpen()
    {

        $session = Yii::$app->session->get('sessionListRequestProcess');

        if (is_null($session) or empty($session)) {
            Yii::$app->session->setFlash('warning',  "<h4><i class='icon fa fa-check'></i>Session closed!</h4>Session time ended!");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    protected function sendNotification($fromRole, $toRole, $subject, $note, $code, $requestId, $toUserIds = null)
    {
        $auth = Yii::$app->authManager;

        if ($fromRole == 'system_admin') {
            $fromUserId =  Yii::$app->user->identity->id;
            $fromUserMail = 'CippathologyNotification@cgiar.org';
        }

        $messages = [];
        $arrayToUserId = $auth->getUserIdsByRole($toRole);

        if (!is_null($toUserIds)) {
            $toUserIdsArray =  explode(",", $toUserIds);
            foreach ($toUserIdsArray as $key => $value) {
                $arrayToUserId[] = $value;
            }
        }

        foreach ($arrayToUserId as $key1 => $value1) {
            $modelNotification = new Notification();
            $modelNotification->subject = $subject;
            $modelNotification->fromUserId = $fromUserId;
            $modelNotification->from = $fromUserMail;

            $modelUser = User::find()->where(
                [
                    'id' => $value1,
                    'status' => 10,
                ]
            )->one();

            if (isset($modelUser)) {
                $modelNotification->to  = $modelUser->email;
                $modelNotification->toUserId  = $modelUser->id;
                $modelNotification->description = $modelUser->firstName . ' ' . $modelUser->lastName;
            } else {

                $modelNotification->to  = $value1;
                $modelNotification->toUserId  = 1;
                $modelNotification->description = $value1;
            }

            $modelNotification->code = $code;
            $modelNotification->details = "ID: " .  $requestId  . "; REQ.CODE: " . Request::findOne($requestId)->code;
            $modelNotification->save();
            $notificationId =  $modelNotification->id;
            $paramsMail = [];

            foreach ($note as $key2 => $value2) {
                $modelComposition = new Composition();
                $modelComposition->notificationId = $notificationId;
                $modelComposition->header  = $value2['header'];
                $modelComposition->body = $value2['body'];
                $modelComposition->save();
                $paramsMail = array_merge($paramsMail,  array($modelComposition->header => $modelComposition->body));
            }

            $messages[] = Yii::$app->mailer->compose(
                $modelNotification->code,
                $paramsMail
            )->setTo(
                [
                    $modelNotification->to => $modelNotification->description,
                ]
            )->setFrom($modelNotification->from)
                ->setSubject($modelNotification->subject);
        }
        try {
            Yii::$app->mailer->sendMultiple($messages);
        } catch (\Throwable $th) { }
    }

    protected function getArrayFromString($txt)
    {
        $array = [];
        $txt = str_replace(" ", "", $txt);
        if (strpos($txt, ",") !== false and strpos($txt, "-") !== false) {
            $variable1 = explode(",", $txt);
            foreach ($variable1 as $key1 => $value1) {
                $variable2 = explode("-", $value1);
                if (count($variable2) > 1) {
                    for ($i = $variable2[0]; $i <= $variable2[1]; $i++) {
                        $array[] = $i;
                    }
                } else {
                    $array[] =  $variable2[0];
                }
            }
        } else if (strpos($txt, "-") !== false) {
            $variable = explode("-", $txt);
            for ($i = $variable[0]; $i <= $variable[1]; $i++) {
                $array[] = $i;
            }
        } else if (strpos($txt, ",") !== false) {
            $variable = explode(",", $txt);
            foreach ($variable as $key => $value) {
                $array[] =  $value;
            }
        } else {
            $array[] = $txt;
        }
        $array =  array_unique($array);
        return $array;
    }

    //♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦//
    //CREATE - UPDATE (ACTIONS)

    public function actionManageRemove($requestId)
    {

        $requestModel = Request::findOne($requestId);
        $requestModel->status = 'disabled';
        $requestModel->save();

        return $this->redirect(
            [
                'index',
            ]
        );
    }

    public function actionIndex($id = null)
    {
        // $searchModel = new RequestSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);




        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(
                [
                    'Request.status' => 'active',
                ]
            );
        // ->orderBy(
        //     // [
        //     //   'request_id' => SORT_DESC,
        //     //   'sequential_code' => SORT_DESC,
        //     // ]
        //     [
        //         'registered_at' => SORT_DESC,
        //     ]
        // )



        $dataProvider->pagination = false;




        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionId' => $id,
        ]);
    }

    public function actionUpdate($requestId)
    {
        if (Request::findOne($requestId)->requestStatus->code == 1.1) {
            // $userId = Yii::$app->user->identity->id;
            // $userRole =   Yii::$app->user->identity->role;

            $arrayRequest = null;
            $arrayListRequestProcess = null;
            $arrayListRequestProcessDetail = null;
            $arrayListAgentByRequestProcessDetail = null;

            $modelRequest = $this->findModel($requestId);
            $arrayRequest = $modelRequest->attributes;
            Yii::$app->session->set('sessionRequest', $arrayRequest); //SAVE
            $requestData['textAreaDetail'] = $arrayRequest['details'];


            $modelRequestProcess = new RequestProcess();
            $modelRequestProcess->load(null, '');
            $arrayRequestProcess = $modelRequestProcess->attributes;
            $arrayListRequestProcess = RequestProcess::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListRequestProcess =  ArrayHelper::index($arrayListRequestProcess, 'numOrderId');
            $arrayListRequestProcess[0] = $arrayRequestProcess;
            ksort($arrayListRequestProcess);
            Yii::$app->session->set('sessionListRequestProcess', $arrayListRequestProcess); //SAVE

            $modelRequestProcessDetail = new RequestProcessDetail();
            $modelRequestProcessDetail->load(null, '');
            $arrayRequestProcessDetail = $modelRequestProcessDetail->attributes;
            $arrayListRequestProcessDetail = RequestProcessDetail::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListRequestProcessDetail =  ArrayHelper::index($arrayListRequestProcessDetail, 'numOrder');
            $arrayListRequestProcessDetail[0] = $arrayRequestProcessDetail;
            ksort($arrayListRequestProcessDetail);
            Yii::$app->session->set('sessionListRequestProcessDetail', $arrayListRequestProcessDetail); //SAVE

            $modelAgentByRequestProcessDetail = new AgentByRequestProcessDetail();
            $modelAgentByRequestProcessDetail->load(null, '');
            $arrayAgentByRequestProcessDetail = $modelAgentByRequestProcessDetail->attributes;
            $arrayListAgentByRequestProcessDetail = AgentByRequestProcessDetail::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListAgentByRequestProcessDetail =  ArrayHelper::index($arrayListAgentByRequestProcessDetail, 'numOrder');
            $arrayListAgentByRequestProcessDetail[0] = $arrayAgentByRequestProcessDetail;
            ksort($arrayListAgentByRequestProcessDetail);
            Yii::$app->session->set('sessionListAgentByRequestProcessDetail', $arrayListAgentByRequestProcessDetail); //SAVE

            $modelSample = new Sample();
            $modelSample->load(null, '');
            $arraySample = $modelSample->attributes;
            $arrayListSample = Sample::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListSample =  ArrayHelper::index($arrayListSample, 'id');
            $arrayListSample[0] = $arraySample;
            ksort($arrayListSample);
            Yii::$app->session->set('sessionListSample', $arrayListSample); //SAVE

            // ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ FINANCIAL ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ //
            // $modelFinancialBus = new FinancialBus();
            // $modelFinancialBus->load(null, '');
            // $arrayFinancialBus =  $modelFinancialBus->attributes;
            // $arrayListFinancialBus[] = $arrayFinancialBus;
            // Yii::$app->session->set('arrayListFinancialBus', $arrayListFinancialBus); //SAVE
            // ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ FINANCIAL ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ //

            $modelFinancialTransaction = new Sample();
            $modelFinancialTransaction->load(null, '');
            $arrayFinancialTransaction = $modelFinancialTransaction->attributes;
            $arrayListFinancialTransaction = Sample::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListFinancialTransaction =  ArrayHelper::index($arrayListFinancialTransaction, 'numOrderId');
            $arrayListFinancialTransaction[0] = $arrayFinancialTransaction;
            ksort($arrayListFinancialTransaction);
            Yii::$app->session->set('sessionListFinancialTransaction', $arrayListFinancialTransaction); //SAVE

            $modelFinancialConceptOne = new Sample();
            $modelFinancialConceptOne->load(null, '');
            $arrayFinancialConceptOne = $modelFinancialConceptOne->attributes;
            $arrayListFinancialConceptOne = Sample::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListFinancialConceptOne =  ArrayHelper::index($arrayListFinancialConceptOne, 'id');
            $arrayListFinancialConceptOne[0] = $arrayFinancialConceptOne;
            ksort($arrayListFinancialConceptOne);
            Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne); //SAVE

            $selectCropValue = Yii::$app->request->post('selectCropName');
            $selectWorkFlowValue = Yii::$app->request->post('selectWorkFlowName');
            $selectEssayAgentValue = Yii::$app->request->post('selectEssayAgentName');

            $arrayMapCrop = $this->getMapCrop();
            $arrayMapWorkFlow = $this->getMapWorkFlow($selectCropValue);
            $arrayMapEssayAgent = $this->getMapEssayAgent($selectWorkFlowValue, $selectEssayAgentValue);

            ArrayHelper::remove($arrayListRequestProcess, '0');

            $dataProviderRequestProcess = new ArrayDataProvider(
                [
                    'allModels' => $arrayListRequestProcess,
                    'pagination' => ['pageSize' => 10,],
                    'sort' => ['attributes' => ['cropValue', 'workFlowValue', 'essayQty', 'agentQty', 'sampleQty', 'sampleStatus'],],
                    'key' => 'numOrderId',
                    'id' => $requestId
                ]
            );

            return $this->render('update', [
                'requestId' => $requestId,
                'selectCropValue' => $selectCropValue,
                'selectWorkFlowValue' => $selectWorkFlowValue,


                'arrayMapCrop' => $arrayMapCrop,
                'arrayMapWorkFlow' => $arrayMapWorkFlow,
                'arrayMapEssayAgent' => $arrayMapEssayAgent,

                'dataProviderRequestProcess' => $dataProviderRequestProcess,
                'requestData' => $requestData,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionCreate($requestId = null)
    {

        if ($requestId == 0 or is_null($requestId)) {

            $arrayRequest = null;
            $arrayListRequestProcess = null;
            $arrayListRequestProcessDetail = null;
            $arrayListAgentByRequestProcessDetail = null;

            if (is_null($requestId)) {

                // REQUEST
                $modelRequest = new Request();
                $modelRequest->load(null, '');
                $arrayRequest = $modelRequest->attributes;
                Yii::$app->session->set('sessionRequest', $arrayRequest); //SAVE

                $modelRequestProcess = new RequestProcess();
                $modelRequestProcess->load(null, '');
                $arrayRequestProcess = $modelRequestProcess->attributes;
                $arrayListRequestProcess[] = $arrayRequestProcess;
                Yii::$app->session->set('sessionListRequestProcess', $arrayListRequestProcess); //SAVE

                $modelRequestProcessDetail = new RequestProcessDetail();
                $modelRequestProcessDetail->load(null, '');
                $arrayRequestProcessDetail =  $modelRequestProcessDetail->attributes;
                $arrayListRequestProcessDetail[] = $arrayRequestProcessDetail;
                Yii::$app->session->set('sessionListRequestProcessDetail', $arrayListRequestProcessDetail); //SAVE

                $modelAgentByRequestProcessDetail = new AgentByRequestProcessDetail();
                $modelAgentByRequestProcessDetail->load(null, '');
                $arrayAgentByRequestProcessDetail =  $modelAgentByRequestProcessDetail->attributes;
                $arrayListAgentByRequestProcessDetail[] = $arrayAgentByRequestProcessDetail;
                Yii::$app->session->set('sessionListAgentByRequestProcessDetail', $arrayListAgentByRequestProcessDetail); //SAVE

                // SAMPLE
                $modelSample = new Sample();
                $modelSample->load(null, '');
                $arraySample =  $modelSample->attributes;
                $arrayListSample[] = $arraySample;
                Yii::$app->session->set('sessionListSample', $arrayListSample); //SAVE`

                $modelFinancialTransaction = new FinancialTransaction();
                $modelFinancialTransaction->load(null, '');
                $arrayFinancialTransaction =  $modelFinancialTransaction->attributes;
                $arrayListFinancialTransaction[] = $arrayFinancialTransaction;
                Yii::$app->session->set('sessionListFinancialTransaction', $arrayListFinancialTransaction); //SAVE

                $modelFinancialConceptOne = new FinancialConceptOne();
                $modelFinancialConceptOne->load(null, '');
                $arrayFinancialConceptOne =  $modelFinancialConceptOne->attributes;
                $arrayListFinancialConceptOne[] = $arrayFinancialConceptOne;
                Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne); //SAVE
                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect(
                    [
                        'create',
                        'requestId' => 0
                    ]
                );
            } else {

                $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
                $selectCropValue = Yii::$app->request->post('selectCropName');
                $selectWorkFlowValue = Yii::$app->request->post('selectWorkFlowName');
                $selectEssayAgentValue = Yii::$app->request->post('selectEssayAgentName');
                $arrayMapCrop = $this->getMapCrop();
                $arrayMapWorkFlow = $this->getMapWorkFlow($selectCropValue);
                $arrayMapEssayAgent = $this->getMapEssayAgent($selectWorkFlowValue, $selectEssayAgentValue);
                ArrayHelper::remove($arrayListRequestProcess, '0');
                $dataProviderRequestProcess = new ArrayDataProvider(
                    [
                        'allModels' => $arrayListRequestProcess,
                        'pagination' => ['pageSize' => 10,],
                        'sort' => ['attributes' => ['cropValue', 'workFlowValue', 'essayQty', 'agentQty', 'sampleQty', 'sampleStatus'],],
                        'key' => 'numOrderId',
                        'id' => $requestId
                    ]
                );
            }

            return $this->render('create', [
                'requestId' => $requestId,
                'selectCropValue' => $selectCropValue,
                'selectWorkFlowValue' => $selectWorkFlowValue,


                'arrayMapCrop' => $arrayMapCrop,
                'arrayMapWorkFlow' => $arrayMapWorkFlow,
                'arrayMapEssayAgent' => $arrayMapEssayAgent,

                'dataProviderRequestProcess' => $dataProviderRequestProcess,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionDetail($requestId)
    {
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {

            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayListRequestProcessDetail = Yii::$app->session->get('sessionListRequestProcessDetail');
            $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
            $arrayListFinancialTransaction = Yii::$app->session->get('sessionListFinancialTransaction');

            if (
                $this->getValidated('selectCropName')  and
                $this->getValidated('selectWorkFlowName') and
                $this->getValidated('selectEssayAgentName')
            ) {

                $selectCropValue = Yii::$app->request->post('selectCropName');
                $selectWorkFlowValue = Yii::$app->request->post('selectWorkFlowName');
                $selectEssayAgentValue = Yii::$app->request->post('selectEssayAgentName');

                $numOrderId = array_keys($arrayListRequestProcess)[count($arrayListRequestProcess) - 1] + 1;
                $arrayListRequestProcess[$numOrderId] = $arrayListRequestProcess[0];
                $arrayListRequestProcess[$numOrderId]['requestId'] = $requestId;
                $arrayListRequestProcess[$numOrderId]['numOrderId'] = $numOrderId;
                $arrayListRequestProcess[$numOrderId]['cropId'] = $selectCropValue;
                $arrayListRequestProcess[$numOrderId]['workFlowId'] = $selectWorkFlowValue;
                $arrayListRequestProcess[$numOrderId]['essayQty'] =   count($selectEssayAgentValue);
                $arrayListRequestProcess[$numOrderId]['agentQty'] =   0;
                $arrayListRequestProcess[$numOrderId]['processStatusId'] = 8;
                $arrayListRequestProcess[$numOrderId]['status'] = 'active';
                $arrayListRequestProcess[$numOrderId]['essayDetail'] = WorkFlow::findOne($selectWorkFlowValue)->shortName;
                // 8		STATUS	REQUESTPROCESS	PENDING
                // 9		STATUS	REQUESTPROCESS	PROCESSING
                // 10		STATUS	REQUESTPROCESS	FINISHED
                // 70		STATUS	REQUESTPROCESS	CHECKED

                $arrayListFinancialTransaction[$numOrderId] = $arrayListFinancialTransaction[0];
                if (Crop::findOne($selectCropValue)->shortName == 'SO') {
                    $arrayListFinancialTransaction[$numOrderId]['resnoLab'] = "P19016";
                } else if (Crop::findOne($selectCropValue)->shortName == 'IP') {
                    $arrayListFinancialTransaction[$numOrderId]['resnoLab'] = "P19009";
                } else {
                    $arrayListFinancialTransaction[$numOrderId]['resnoLab'] = "P19016";
                }
                // P19009	Virology, Sweetpotato Laboratory
                // P19016	Virology, Potato Laboratory

                $arrayListFinancialTransaction[$numOrderId]['requestId'] = $requestId;
                $arrayListFinancialTransaction[$numOrderId]['cropId'] = $selectCropValue;
                $arrayListFinancialTransaction[$numOrderId]['workFlowId'] = $selectWorkFlowValue;
                $arrayListFinancialTransaction[$numOrderId]['numOrderId'] = $numOrderId;
                $arrayListFinancialTransaction[$numOrderId]['status'] = 'active';

                $arrayEssayList = [];


                $arrayMapActivity = [];

                foreach ($selectEssayAgentValue as $essayId => $arrayMapAgent) {
                    $modelEssay = Essay::findOne($essayId);
                    $arrayMapActivity = ArrayHelper::map($modelEssay->getActivities(), 'id', 'shortName');

                    foreach ($arrayMapActivity as $activityId => $activityValue) { //All Activities By Essay
                        // $indexRequestProcessDetail = count($arrayListRequestProcessDetail);
                        $indexRequestProcessDetail = array_keys($arrayListRequestProcessDetail)[count($arrayListRequestProcessDetail) - 1] + 1;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail] = $arrayListRequestProcessDetail[0];
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['requestId'] = $requestId;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['numOrderId'] = $numOrderId;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['numOrder'] = $indexRequestProcessDetail;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['cropId'] = $selectCropValue;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['workFlowId'] = $selectWorkFlowValue;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['essayId'] = $essayId;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['activityId'] = $activityId;
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['status'] = 'active';
                        $arrayListRequestProcessDetail[$indexRequestProcessDetail]['processStatusDetailId'] = 67; //<------------------------------------------------NEW VALUE HERE
                        // 67		STATUS	REQUESTPROCESSDETAIL	PENDING
                        // 68		STATUS	REQUESTPROCESSDETAIL	PROCESSING
                        // 69		STATUS	REQUESTPROCESSDETAIL	FINISHED
                        // 71		STATUS	REQUESTPROCESSDETAIL	CHECKED
                    }

                    foreach ($arrayMapAgent as $agentId => $agentValue) { //All Agents By Essay
                        // $indexAgentByRequestProcessDetail = count($arrayListAgentByRequestProcessDetail);
                        $indexAgentByRequestProcessDetail = array_keys($arrayListAgentByRequestProcessDetail)[count($arrayListAgentByRequestProcessDetail) - 1] + 1;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail] = $arrayListAgentByRequestProcessDetail[0];
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['requestId'] = $requestId;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['numOrderId'] = $numOrderId;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['numOrder'] = $indexAgentByRequestProcessDetail;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['cropId'] = $selectCropValue;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['workFlowId'] = $selectWorkFlowValue;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['essayId'] = $essayId;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['agentId'] = $agentId;
                        $arrayListAgentByRequestProcessDetail[$indexAgentByRequestProcessDetail]['status'] = 'active';
                        // $agentDetail = $agentDetail . $agentValue . ", ";
                    }

                    $arrayListRequestProcess[$numOrderId]['agentQty'] = $arrayListRequestProcess[$numOrderId]['agentQty'] + count($arrayMapAgent);
                    $arrayListRequestProcess[$numOrderId]['agentDetail'] = $arrayListRequestProcess[$numOrderId]['agentDetail']  . "(" . Essay::findOne($essayId)->shortName . ": " . implode(", ", $arrayMapAgent) . ") ";
                    $arrayEssayList[] =  Essay::findOne($essayId)->shortName;
                }
                // $arrayListRequestProcess[$numOrderId]['essayDetail'] = $arrayListRequestProcess[$numOrderId]['essayDetail'] . " (" .  implode(", ", array_values($arrayEssayList)) . ") ";
                Yii::$app->session->set('sessionListRequestProcess', $arrayListRequestProcess);
                Yii::$app->session->set('sessionListRequestProcessDetail', $arrayListRequestProcessDetail);
                Yii::$app->session->set('sessionListAgentByRequestProcessDetail', $arrayListAgentByRequestProcessDetail);
                Yii::$app->session->set('sessionListFinancialTransaction', $arrayListFinancialTransaction);

                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect(
                    [
                        'edit',
                        'requestId' => 0,
                        'numOrderId' => $numOrderId
                    ]
                );
            } else {
                $message = [];
                if (!$this->getValidated('selectCropName'))  $message[] =   'You must select the crop to display the list of workflows';
                if (!$this->getValidated('selectWorkFlowName'))   $message[] =   'You must select the workflow to display the list of "Pathogens" / "Indicator plants"';
                if (!$this->getValidated('selectEssayAgentName'))   $message[] =   'You must select at least one "Pathogen" / "Indicator Plant" to add a new process to the order';

                Yii::$app->session->setFlash('danger',  '<h4><i class="icon fa fa-check"></i>Error!</h4>' . implode("<br/>", $message));
                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect(
                    [
                        'create',
                        'requestId' => 0
                    ]
                );
            }
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionEdit($requestId, $numOrderId = null)
    {

        $this->getSessionOpen();

        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {

            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');

            if (is_null($numOrderId)) {
                foreach ($arrayListRequestProcess as $key => $value) {
                    if ($key > 0)
                        $numOrderId = $value['numOrderId'];
                }
            }

            $arrayRequestProcess = $arrayListRequestProcess[$numOrderId];
            $selectCropValue =  $arrayRequestProcess['cropId'];
            $selectWorkFlowValue =  $arrayRequestProcess['workFlowId'];

            $arrayListAgentByRequestProcessDetail = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });

            $selectEssayAgentValue = ArrayHelper::map($arrayListAgentByRequestProcessDetail, 'agentId', 'numOrder', 'essayId');
            $arrayMapCrop = $this->getMapCrop();
            $arrayMapWorkFlow = $this->getMapWorkFlow($selectCropValue);
            $arrayMapEssayAgent = $this->getMapEssayAgent($selectWorkFlowValue, $selectEssayAgentValue);
            ArrayHelper::remove($arrayListRequestProcess, '0');

            $dataProviderRequestProcess = new ArrayDataProvider(
                [
                    'allModels' => $arrayListRequestProcess,
                    'pagination' => ['pageSize' => 10,],
                    'sort' => ['attributes' => ['cropValue', 'workFlowValue', 'essayQty', 'agentQty', 'sampleQty', 'sampleStatus'],],
                    'key' => 'numOrderId',
                    'id' => $requestId
                ]
            );

            return $this->render('create', [
                'requestId' => $requestId,
                'selectCropValue' => $selectCropValue,
                'selectWorkFlowValue' => $selectWorkFlowValue,
                'arrayMapCrop' => $arrayMapCrop,
                'arrayMapWorkFlow' => $arrayMapWorkFlow,
                'arrayMapEssayAgent' => $arrayMapEssayAgent,
                'dataProviderRequestProcess' => $dataProviderRequestProcess,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionRemove($requestId, $numOrderId)
    {

        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {

            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayListRequestProcess = array_filter($arrayListRequestProcess, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] != $numOrderId);
            });
            Yii::$app->session->set('sessionListRequestProcess', $arrayListRequestProcess);


            $arrayListRequestProcessDetail = Yii::$app->session->get('sessionListRequestProcessDetail');
            $arrayListRequestProcessDetail = array_filter($arrayListRequestProcessDetail, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] != $numOrderId);
            });
            Yii::$app->session->set('sessionListRequestProcessDetail', $arrayListRequestProcessDetail);


            $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
            $arrayListAgentByRequestProcessDetail = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] != $numOrderId);
            });
            Yii::$app->session->set('sessionListAgentByRequestProcessDetail', $arrayListAgentByRequestProcessDetail);


            $arrayListSample = Yii::$app->session->get('sessionListSample');
            $arrayListSample = array_filter($arrayListSample, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] != $numOrderId);
            });
            Yii::$app->session->set('sessionListSample', $arrayListSample);

            $arrayListFinancialTransaction = Yii::$app->session->get('sessionListFinancialTransaction');
            $arrayListFinancialTransaction = array_filter($arrayListFinancialTransaction, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] != $numOrderId);
            });
            Yii::$app->session->set('sessionListFinancialTransaction', $arrayListFinancialTransaction);


            $arrayListFinancialConceptOne = Yii::$app->session->get('sessionListFinancialConceptOne');
            $arrayListFinancialConceptOne = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] != $numOrderId);
            });
            Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne);


            ArrayHelper::remove($arrayListRequestProcess, '0');

            $selectCropValue = Yii::$app->request->post('selectCropName');
            $selectWorkFlowValue = Yii::$app->request->post('selectWorkFlowName');
            $selectEssayAgentValue = Yii::$app->request->post('selectEssayAgentName');

            $arrayMapCrop = $this->getMapCrop();
            $arrayMapWorkFlow = $this->getMapWorkFlow($selectCropValue);
            $arrayMapEssayAgent = $this->getMapEssayAgent($selectWorkFlowValue, $selectEssayAgentValue);

            $dataProviderRequestProcess = new ArrayDataProvider(
                [
                    'allModels' => $arrayListRequestProcess,
                    'pagination' => ['pageSize' => 10,],
                    'key' => 'numOrderId',
                    'id' => $requestId
                ]
            );

            return $this->render('create', [
                'requestId' => $requestId,
                'selectCropValue' => $selectCropValue,
                'selectWorkFlowValue' => $selectWorkFlowValue,
                'arrayMapCrop' => $arrayMapCrop,
                'arrayMapWorkFlow' => $arrayMapWorkFlow,
                'arrayMapEssayAgent' => $arrayMapEssayAgent,
                'dataProviderRequestProcess' => $dataProviderRequestProcess,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionSearchSample($requestId, $numOrderId)
    {
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {

            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayRequestProcess = $arrayListRequestProcess[$numOrderId];
            $cropId = $arrayRequestProcess['cropId'];
            $workFlowId = $arrayRequestProcess['workFlowId'];


            $modelCrop = Crop::findOne($cropId);
            $modelWorkFlow = WorkFlow::findOne($workFlowId);
            $sWorkFlow =  (string) $modelWorkFlow->shortName . " (" . (string) $modelWorkFlow->longName . ")";
            $sCrop =  (string) $modelCrop->shortName . " (" . (string) $modelCrop->longName . ")";
            $samples = null;
            $summary = null;


            if ($this->getValidated('samples')) {

                $samples = Yii::$app->request->post('samples');
                $samples = str_replace('CIP ', 'CIP', $samples);
                $summary =  "SAMPLES SUCCESSFULLY FILTERED<br/>";

                $searchModelIp = new MainIpSearch();
                $queryParamsIp = array_merge(array(), Yii::$app->request->getQueryParams());
                $queryParamsIp["MainIpSearch"]["CIPNUMBER"] = $samples;
                $dataProviderIp = $searchModelIp->search($queryParamsIp);
                $dataProviderIp->pagination->pageSize = 1000;

                $searchModelSo = new MainSearch();
                $queryParamsSo = array_merge(array(), Yii::$app->request->getQueryParams());
                $queryParamsSo["MainSearch"]["CIPNUMBER"] = $samples;
                $dataProviderSo = $searchModelSo->search($queryParamsSo);
                $dataProviderSo->pagination->pageSize = 1000;

                if ($this->getValidated('sampleTypeId')) {
                    if ($this->getValidated('sampleIsBulk')) {

                        $numFoundSample = 0;
                        $numOrderAllSample = 0;

                        $arraySamples = [];
                        $arraySamplesAll = [];
                        $arraySampleData = [];

                        $arraySamplesAll = preg_split('/\r\n?/', $samples, -1, PREG_SPLIT_NO_EMPTY);

                        foreach ($arraySamplesAll as $key => $value) {
                            $arraySamples[] = preg_split('/[\s,]+/', $value, -1, PREG_SPLIT_NO_EMPTY)[0];
                        }

                        $summary_detail = "<h5>DETAILS:</h5>";
                        $summary_detail = $summary_detail . "#Quantity of values entered to review: " . (string) count($arraySamples) . "<br/>";

                        if ($modelCrop->shortName == "IP") {
                            foreach ($dataProviderSo->getModels() as $key => $value) {
                                $arraySampleData[$value->getAttributes()['CIPNUMBER']] =  $value->getAttributes();
                            }

                            foreach ($dataProviderIp->getModels() as $key => $value) {
                                $arraySampleData[$value->getAttributes()['CIPNUMBER']] =  $value->getAttributes();
                            }
                        } else if ($modelCrop->shortName == "SO") {

                            foreach ($dataProviderSo->getModels() as $key => $value) {
                                $arraySampleData[$value->getAttributes()['CIPNUMBER']] =  $value->getAttributes();
                            }
                        }

                        $dataProvider =  new ArrayDataProvider(
                            [
                                'allModels' => $arraySampleData,
                                'pagination' => ['pageSize' => 0,],
                            ]
                        );

                        $summary_list = "<h5>SAMPLE LIST:</h5>";

                        foreach ($arraySamples as $key => $value) {
                            $numOrderAllSample = $numOrderAllSample + 1;
                            if (ArrayHelper::keyExists($value, $arraySampleData, false)) {
                                $numFoundSample = $numFoundSample + 1;
                                $summary_list = $summary_list . "<p><span class='text-green'>" . (string) $numOrderAllSample . " - " . (string) $value . "</span></p>";
                            } else {


                                $summary_list = $summary_list . "<p><span class='text-red font-weight-bold'>" . (string) $numOrderAllSample . " - " . (string) $value . " (NOT FOUND)" . "</span></p>";
                            }
                        }

                        $summary_detail = $summary_detail . "#Quantity of values found: " . (string) $numFoundSample . "<br/>";

                        if ($numOrderAllSample - $numFoundSample > 0)
                            $summary_detail = $summary_detail . "<span class='text-red font-weight-bold'>#Quantity of values not found: " . (string) ($numOrderAllSample - $numFoundSample) . "</span><br/>";

                        $summary_detail = $summary_detail . "#Number of unique instruments entered: " . (string) count($arraySampleData) . "<br/>";
                        $summary_list = $summary_list . "<br/>";
                        $summary = $summary . $summary_detail . $summary_list;
                    } else {
                        $summary = $summary .  "<p><span class='text-red font-weight-bold'>Select if sample is bulk</span></p>";
                    }
                } else {
                    $summary = $summary . "<p><span class='text-red font-weight-bold'>Select a sample type</span></p>";
                }
            } else {


                $summary = "<p>No filtered samples for " . $sWorkFlow . "/" . $sCrop . "</p>";
                $summary = $summary . "<p><span class='text-red font-weight-bold'>Samples need to be selected, enter the name of the accessions to search them in our database.</span></p>";
                /* SAMPLE DATA CONDITIONS */
                if ($modelCrop->shortName == "IP") {
                    $searchModel = new MainIpSearch();
                    $queryParams = array_merge(array(), Yii::$app->request->getQueryParams());
                    $queryParams["MainIpSearch"]["CIPNUMBER"] = $samples;
                    $dataProvider = $searchModel->search($queryParams);
                    $dataProvider->pagination->pageSize = 1000;
                } else if ($modelCrop->shortName == "SO") {
                    $searchModel = new MainSearch();
                    $queryParams = array_merge(array(), Yii::$app->request->getQueryParams());
                    $queryParams["MainSearch"]["CIPNUMBER"] = $samples;
                    $dataProvider = $searchModel->search($queryParams);
                    $dataProvider->pagination->pageSize = 1000;
                }
            }

            return $this->renderAjax('add-sample', [
                'dataProvider' => $dataProvider,
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'summary' => $summary,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }


    public function actionAddSample($requestId, $numOrderId)
    {
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {

            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayRequestProcess = $arrayListRequestProcess[$numOrderId];
            $cropId = $arrayRequestProcess['cropId'];
            $workFlowId = $arrayRequestProcess['workFlowId'];


            $modelCrop = Crop::findOne($cropId);
            $modelWorkFlow = WorkFlow::findOne($workFlowId);
            $sWorkFlow =  (string) $modelWorkFlow->shortName . " (" . (string) $modelWorkFlow->longName . ")";
            $sCrop =  (string) $modelCrop->shortName . " (" . (string) $modelCrop->longName . ")";
            $samples = null;
            $summary = null;
            $grafts =  $modelWorkFlow->repetition;


            if ($this->getValidated('samples')) {

                $samples = Yii::$app->request->post('samples');
                $samples = str_replace('CIP ', 'CIP',  $samples);
                $summary =  "SAMPLES SUCCESSFULLY FILTERED<br/>";

                $searchModelIp = new MainIpSearch();
                $queryParamsIp = array_merge(array(), Yii::$app->request->getQueryParams());
                $queryParamsIp["MainIpSearch"]["CIPNUMBER"] = $samples;
                $dataProviderIp = $searchModelIp->search($queryParamsIp);
                $dataProviderIp->pagination->pageSize = 1000;

                $searchModelSo = new MainSearch();
                $queryParamsSo = array_merge(array(), Yii::$app->request->getQueryParams());
                $queryParamsSo["MainSearch"]["CIPNUMBER"] = $samples;
                $dataProviderSo = $searchModelSo->search($queryParamsSo);
                $dataProviderSo->pagination->pageSize = 1000;

                if ($this->getValidated('sampleTypeId')) {
                    if ($this->getValidated('sampleIsBulk')) {

                        $sampleTypeId = Yii::$app->request->post('sampleTypeId');
                        $sampleIsBulk = Yii::$app->request->post('sampleIsBulk');
                        $arrayListSample = Yii::$app->session->get('sessionListSample');

                        $arraySamples = [];
                        $arraySamplesAll = [];
                        $arraySamplesOtherCode1 = [];
                        $arraySamplesOtherCode2 = [];

                        $arrayListSample = array_filter($arrayListSample, function ($element) use ($workFlowId, $cropId, $numOrderId) {
                            return ($element['numOrderId'] != $numOrderId);
                        });

                        $arrayListFinancialConceptOne = Yii::$app->session->get('sessionListFinancialConceptOne');
                        $arrayListFinancialConceptOne = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                            return ($element['numOrderId'] != $numOrderId);
                        });

                        $arraySamplesAll = preg_split('/\r\n?/', $samples, -1, PREG_SPLIT_NO_EMPTY);
                        foreach ($arraySamplesAll as $key => $value) {

                            $arraySamplesOtherCode1[$key] = null;
                            $arraySamplesOtherCode2[$key] = null;

                            $arraySamples[$key] = preg_split('/[\s,]+/', $value, -1, PREG_SPLIT_NO_EMPTY)[0];

                            try {
                                $arraySamplesOtherCode1[$key] = preg_split('/[\s,]+/', $value, -1, PREG_SPLIT_NO_EMPTY)[1];
                            } catch (\Throwable $th) {
                                $arraySamplesOtherCode1[$key] = null;
                            }

                            try {
                                $arraySamplesOtherCode2[$key] = preg_split('/[\s,]+/', $value, -1, PREG_SPLIT_NO_EMPTY)[2];
                            } catch (\Throwable $th) {
                                $arraySamplesOtherCode2[$key] = null;
                            }
                        }

                        $summary_detail = "Details:";
                        $numFoundSample = 0;
                        $numOrderAllSample = 0;
                        $arraySampleData = [];


                        if ($modelCrop->shortName == "IP") {
                            foreach ($dataProviderSo->getModels() as $key => $value) {
                                $arraySampleData[$value->getAttributes()['CIPNUMBER']] =  $value->getAttributes();
                            }

                            foreach ($dataProviderIp->getModels() as $key => $value) {
                                $arraySampleData[$value->getAttributes()['CIPNUMBER']] =  $value->getAttributes();
                            }
                        } else if ($modelCrop->shortName == "SO") {

                            foreach ($dataProviderSo->getModels() as $key => $value) {
                                $arraySampleData[$value->getAttributes()['CIPNUMBER']] =  $value->getAttributes();
                            }
                        }

                        $dataProvider =  new ArrayDataProvider(
                            [
                                'allModels' => $arraySampleData,
                                'pagination' => ['pageSize' => 0,],
                            ]
                        );

                        $numOrder = 0;
                        foreach ($arraySamples as $key => $value) {

                            $numOrderAllSample = $numOrderAllSample + 1;
                            $indexSample = array_keys($arrayListSample)[count($arrayListSample) - 1] + 1;

                            $arrayListSample[$indexSample] = $arrayListSample[0];
                            $arrayListSample[$indexSample]['accessionCode'] = $value;
                            $arrayListSample[$indexSample]['numOrderId'] = $numOrderId;
                            $arrayListSample[$indexSample]['numOrder'] = $numOrder = $numOrder + 1;

                            $arrayListSample[$indexSample]['firstUserCode'] = null;
                            $arrayListSample[$indexSample]['secondUserCode'] = null;
                            $arrayListSample[$indexSample]['thirdUserCode'] = null;

                            if (ArrayHelper::keyExists($value, $arraySampleData, false)) {
                                $numFoundSample = $numFoundSample + 1;
                                $arrayListSample[$indexSample]['accessionCode'] = $value;
                                $arrayListSample[$indexSample]['firstUserCode'] =  $arraySamplesOtherCode1[$key];
                                $arrayListSample[$indexSample]['secondUserCode'] =  $arraySamplesOtherCode2[$key];

                                $arrayListSample[$indexSample]['sourceId'] =     $modelCrop->shortName == "SO" ? 92 : 93;
                                $arrayListSample[$indexSample]['collectingCode'] = $arraySampleData[$value]['COLNUMBER'];
                                $arrayListSample[$indexSample]['accessionName'] = $arraySampleData[$value]['CULTVRNAME'];
                                $arrayListSample[$indexSample]['female'] = $arraySampleData[$value]['FEMALE'];
                                $arrayListSample[$indexSample]['male'] = $arraySampleData[$value]['MALE'];
                                $arrayListSample[$indexSample]['cropId'] =  $cropId;
                                $arrayListSample[$indexSample]['workFlowId'] =  $workFlowId;
                                $arrayListSample[$indexSample]['sampleTypeId'] =  $sampleTypeId;
                                $arrayListSample[$indexSample]['sampleCrossId'] =  72;
                                $arrayListSample[$indexSample]['isBulk'] = $sampleIsBulk;
                                $arrayListSample[$indexSample]['graftingCount'] = $grafts;
                            } else {

                                if (is_null($arraySamplesOtherCode2[$key])) {
                                    $arrayListSample[$indexSample]['accessionCode'] = null;
                                    $arrayListSample[$indexSample]['firstUserCode'] = $value;
                                    $arrayListSample[$indexSample]['secondUserCode'] =  $arraySamplesOtherCode1[$key];

                                    $arrayListSample[$indexSample]['sourceId'] = 82;
                                    $arrayListSample[$indexSample]['colNumber'] = "";
                                    $arrayListSample[$indexSample]['cultvrName'] = "";
                                    $arrayListSample[$indexSample]['female'] = "";
                                    $arrayListSample[$indexSample]['male'] = "";
                                    $arrayListSample[$indexSample]['cropId'] =  $cropId;
                                    $arrayListSample[$indexSample]['workFlowId'] =  $workFlowId;
                                    $arrayListSample[$indexSample]['sampleTypeId'] =  $sampleTypeId;
                                    $arrayListSample[$indexSample]['sampleCrossId'] =  72;
                                    $arrayListSample[$indexSample]['isBulk'] = $sampleIsBulk;
                                    $arrayListSample[$indexSample]['graftingCount'] = $grafts;
                                } else {
                                    $arrayListSample[$indexSample]['accessionCode'] = $value;
                                    $arrayListSample[$indexSample]['firstUserCode'] =  $arraySamplesOtherCode1[$key];
                                    $arrayListSample[$indexSample]['secondUserCode'] =  $arraySamplesOtherCode2[$key];

                                    $arrayListSample[$indexSample]['sourceId'] = 82;
                                    $arrayListSample[$indexSample]['colNumber'] = "";
                                    $arrayListSample[$indexSample]['cultvrName'] = "";
                                    $arrayListSample[$indexSample]['female'] = "";
                                    $arrayListSample[$indexSample]['male'] = "";
                                    $arrayListSample[$indexSample]['cropId'] =  $cropId;
                                    $arrayListSample[$indexSample]['workFlowId'] =  $workFlowId;
                                    $arrayListSample[$indexSample]['sampleTypeId'] =  $sampleTypeId;
                                    $arrayListSample[$indexSample]['sampleCrossId'] =  72;
                                    $arrayListSample[$indexSample]['isBulk'] = $sampleIsBulk;
                                    $arrayListSample[$indexSample]['graftingCount'] = $grafts;
                                }
                            }
                        }

                        Yii::$app->session->set('sessionListSample', $arrayListSample);
                        Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne);
                        ArrayHelper::remove($arrayListSample, '0');

                        $subTotalCost = 0;
                        if (count($arraySamples) > 0) {

                            $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
                            $arrayListAgentByRequestProcessDetail = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($numOrderId) {
                                return ($element['numOrderId'] == $numOrderId);
                            });

                            foreach ($modelWorkFlow->essays  as $key => $value) {
                                if ($value->essayType->code == 'RSGL') {
                                    $essayId = $value->id;
                                    $arrayListAgentByRequestProcessDetailByEssay = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($essayId) {
                                        return ($element['essayId'] == $essayId);
                                    });
                                    $agentCount = count($arrayListAgentByRequestProcessDetailByEssay);
                                } else if ($value->essayType->code == 'RMUL') {
                                    $agentCount = 1;
                                }
                                $subTotalCost = $subTotalCost +
                                    count($arraySamples)  *
                                    Cost::findOne($value->costId)->cost *
                                    $agentCount;
                            }
                        }
                        $arrayRequestProcess['subTotalCost'] = $subTotalCost;
                        $arrayRequestProcess['sampleQty'] = count($arraySamples);
                        $arrayRequestProcess['sampleStatus'] = "Sample loaded";
                        $arrayRequestProcess['paymentStatus'] = null;
                        $arrayRequestProcess['paymentPercentage'] =  null;
                        $arrayRequestProcess['sampleDetail'] = strlen(implode(",", $arraySamples)) > 240 ? substr(implode(",", $arraySamples), 0, 240) . "..." : implode(",", $arraySamples);
                        $arrayListRequestProcess[$numOrderId] =  $arrayRequestProcess;
                        Yii::$app->session->set('sessionListRequestProcess', $arrayListRequestProcess);

                        Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                        return $this->redirect(
                            [
                                'edit',
                                'requestId' => $requestId,
                                'numOrderId' => $numOrderId,
                            ]
                        );
                    } else {
                        $summary = $summary .  "Select if sample is bulk<br/>";
                    }
                } else {
                    $summary = $summary . "Select a sample type<br/>";
                }
            } else {
                $summary = "No filtered samples for " . $sWorkFlow . "/" . $sCrop . "<br/>";
                $summary = $summary . "Samples need to be selected<br/>";

                /* SAMPLE DATA CONDITIONS */
                if ($modelCrop->shortName == "IP") {
                    $searchModel = new MainIpSearch();
                    $queryParams = array_merge(array(), Yii::$app->request->getQueryParams());
                    $queryParams["MainIpSearch"]["CIPNUMBER"] = $samples;
                } else if ($modelCrop->shortName == "SO") {
                    $searchModel = new MainSearch();
                    $queryParams = array_merge(array(), Yii::$app->request->getQueryParams());
                    $queryParams["MainSearch"]["CIPNUMBER"] = $samples;
                }
                $dataProvider = $searchModel->search($queryParams);
                $dataProvider->pagination->pageSize = 1000;
            }

            return $this->renderAjax('add-sample', [
                'dataProvider' => $dataProvider,
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'summary' => $summary,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionConfigWorkflow($requestId, $numOrderId)
    {
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {
            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayListRequestProcessFilter = array_filter($arrayListRequestProcess, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            $arrayRequestProcess = $arrayListRequestProcessFilter[$numOrderId];

            $arrayListRequestProcessDetail = Yii::$app->session->get('sessionListRequestProcessDetail');
            $arrayListRequestProcessDetailFilter = array_filter($arrayListRequestProcessDetail, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            $arrayRequestProcessDetail = $arrayListRequestProcessDetailFilter;

            $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
            $arrayListAgentByRequestProcessDetailFilter = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            $arrayEssayAgentValue = ArrayHelper::map($arrayListAgentByRequestProcessDetailFilter, 'agentId', 'numOrderId', 'essayId');

            $arrayListSample = Yii::$app->session->get('sessionListSample');

            $arrayListSampleFilter = array_filter($arrayListSample, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            $arraySample = $arrayListSampleFilter;

            $arrayListDataProvider = [];

            foreach ($arrayEssayAgentValue as $essayId => $arrayAgentByEssay) {

                $arrayColumns = [];
                $arrayColumns = array(
                    'Nbr. Order' => null,
                    'Accession number' => null,
                    //'Lab code' => null,
                    'Other Code 1' => null,
                    'Other Code 2' => null,
                    // 'Third User code' => null,
                    'Collecting number' => null,
                    'Accession name' => null,
                    'Female parent' => null,
                    'Male parent' => null,
                    'Is bulk' => null,
                    'Crop' => null,
                    'Sample type' => null,
                );

                // foreach ($arrayAgentByEssay as $agentId => $value) {
                //     $arrayActivity = array_filter($arrayRequestProcessDetail, function ($element) use ($essayId) {
                //         return ($element['essayId'] == $essayId);
                //     });
                //     foreach ($arrayActivity as $key => $value) {
                //         $arrayColumns[str_replace("Agent", Agent::findOne($agentId)->shortName, Activity::findOne($value['activityId'])->shortName)] = null;
                //     }
                // }

                foreach ($arraySample as $key => $value) {

                    $arrayColumns['Nbr. Order'] = $value['numOrder'];
                    $arrayColumns['Accession number'] = $value['accessionCode'];
                    //$arrayColumns['Lab code'] = $value['labCode'];
                    $arrayColumns['Other Code 1'] = $value['firstUserCode'];
                    $arrayColumns['Other Code 2'] = $value['secondUserCode'];
                    // $arrayColumns['User code'] = $value['thirdUserCode'];
                    $arrayColumns['Collecting number'] = $value['collectingCode'];
                    $arrayColumns['Accession name'] = $value['accessionName'];
                    $arrayColumns['Female parent'] = $value['female'];
                    $arrayColumns['Male parent'] = $value['male'];
                    $arrayColumns['Is bulk'] = $value['isBulk'];
                    $arrayColumns['Crop'] =   Crop::findOne($value['cropId'])->longName;
                    $arrayColumns['Sample type'] = Parameter::findOne($value['sampleTypeId'])->shortName;

                    $arrayListDataProvider[$essayId][] =  $arrayColumns;
                }
            }

            $dataProviders = [];
            foreach ($arrayListDataProvider as $essayId => $data) {
                $dataProviders[$essayId] = new ArrayDataProvider([
                    'allModels' => $data,
                    //'key' =>  'index',
                    'id' => $essayId,
                    'sort' => ['attributes' => ['numOrder'],],
                    'pagination' => ['pageSize' => 1000]
                ]);
            }

            return $this->renderAjax('config-workflow', [
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'arrayRequestProcess' => $arrayRequestProcess,
                'arrayRequestProcessDetail' => $arrayRequestProcessDetail,
                'arrayEssayAgentValue' => $arrayEssayAgentValue,
                'arraySample' => $arraySample,
                'arrayListDataProvider' => $arrayListDataProvider,
                'dataProviders' => $dataProviders,

            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionSearchPayment($requestId, $numOrderId)
    {
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {
            //=============================================================================================//
            $arrayListFinancialConceptOne = Yii::$app->session->get('sessionListFinancialConceptOne');

            $arrayListFinancialConceptOne = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                return ($element['status'] != 'disabled');
            });

            Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne);

            $arrayPercentage = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            //---------------------------------------------------------------------------------------------//
            $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
            $arrayListRequestProcessFilter = array_filter($arrayListRequestProcess, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            $arrayRequestProcess = $arrayListRequestProcessFilter[$numOrderId];
            //---------------------------------------------------------------------------------------------//
            $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
            $arrayListAgentByRequestProcessDetailFilter = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            $arrayEssayAgentValue = ArrayHelper::map($arrayListAgentByRequestProcessDetailFilter, 'agentId', 'agentId', 'essayId');
            foreach ($arrayEssayAgentValue as $keyEssayId => $valueAgentId) {
                array_walk($valueAgentId, function (&$value, $key) {
                    return $value = Agent::findOne($value)->shortName;
                });
                //-----------------------------//
                $arrayPercentageByEssay = array_filter($arrayPercentage, function ($element) use ($numOrderId, $keyEssayId) {
                    return ($element['essayId'] == $keyEssayId);
                });
                $percentageAccumulated = array_sum(array_column($arrayPercentageByEssay, 'percent'));
                //-----------------------------//
                if (Essay::findOne($keyEssayId)->essayType->code == 'RSGL') {
                    $countAgent = count($valueAgentId);
                } else if (Essay::findOne($keyEssayId)->essayType->code  == 'RMUL') {
                    $countAgent = 1;
                }
                //-----------------------------//
                $arrayEssayAgentValue[$keyEssayId] = array(
                    'testName' => Essay::findOne($keyEssayId)->shortName,
                    'testCost' => Essay::findOne($keyEssayId)->cost->cost,
                    'agentQty' => $countAgent,
                    'sampleQty' =>  $arrayRequestProcess['sampleQty'],
                    'subTotalByTest' => Essay::findOne($keyEssayId)->cost->cost * $countAgent * $arrayRequestProcess['sampleQty'],
                    'agentDetail' => implode(", ", $valueAgentId),
                    'paymentConcept' => $percentageAccumulated,
                    'keyEssayId' => $keyEssayId,
                );
            }
            $arrayListFinancialConceptOneFilter = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                return ($element['numOrderId'] == $numOrderId);
            });
            foreach ($arrayEssayAgentValue as $essayId => $valueData) {
                $arrayListFinancialConceptOneDataProviderByEssay = array_filter($arrayListFinancialConceptOneFilter, function ($element) use ($essayId) {
                    return ($element['essayId'] == $essayId);
                });
                $dataProviderRequestProcessPayment[$essayId] = new ArrayDataProvider(
                    [
                        'allModels' => $arrayListFinancialConceptOneDataProviderByEssay,
                        'pagination' => ['pageSize' => 5,],
                    ]
                );
            }



            //=============================================================================================//
            return $this->renderAjax('add-payment', [
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'arrayEssayAgentValue' => $arrayEssayAgentValue,
                'dataProviderRequestProcessPayment' => $dataProviderRequestProcessPayment,
                'message' => null,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function actionAddPayment($requestId, $numOrderId, $essayId = null)
    {
        $message = null;
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {


            if (is_null($essayId)) {
                $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');

                $essayQty =  $arrayListRequestProcess[$numOrderId]['essayQty'];

                $totalPercentageAllEssays = 0;

                $arrayListFinancialConceptOne = Yii::$app->session->get('sessionListFinancialConceptOne');




                foreach ($arrayListFinancialConceptOne as $key => $value) {
                    if ($key > 0) {
                        $arrayListFinancialConceptOne[$key]['status'] = 'active';

                        if ($value['numOrderId'] == $numOrderId)
                            $totalPercentageAllEssays =  $totalPercentageAllEssays + $value['percent'];
                    }
                }






                $arrayListRequestProcess[$numOrderId]['paymentPercentage'] =   $totalPercentageAllEssays / $essayQty;
                if ($totalPercentageAllEssays / $essayQty == 100)
                    $arrayListRequestProcess[$numOrderId]['paymentStatus'] =  "Payment charged";
                else
                    $arrayListRequestProcess[$numOrderId]['paymentStatus'] =  "Underpayment";






                Yii::$app->session->set('sessionListRequestProcess', $arrayListRequestProcess);
                Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne);

                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect(
                    [
                        'edit',
                        'requestId' => $requestId,
                        'numOrderId' => $numOrderId,
                    ]
                );
            } else {
                //=============================================================================================//
                $arrayListFinancialConceptOne = Yii::$app->session->get('sessionListFinancialConceptOne');
                $arrayPercentage = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                    return ($element['numOrderId'] == $numOrderId);
                });
                //---------------------------------------------------------------------------------------------//
                $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
                $arrayListRequestProcessFilter = array_filter($arrayListRequestProcess, function ($element) use ($numOrderId) {
                    return ($element['numOrderId'] == $numOrderId);
                });
                $arrayRequestProcess = $arrayListRequestProcessFilter[$numOrderId];
                //---------------------------------------------------------------------------------------------//
                $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
                $arrayListAgentByRequestProcessDetailFilter = array_filter($arrayListAgentByRequestProcessDetail, function ($element) use ($numOrderId) {
                    return ($element['numOrderId'] == $numOrderId);
                });
                $arrayEssayAgentValue = ArrayHelper::map($arrayListAgentByRequestProcessDetailFilter, 'agentId', 'agentId', 'essayId');
                foreach ($arrayEssayAgentValue as $keyEssayId => $valueAgentId) {
                    array_walk($valueAgentId, function (&$value, $key) {
                        return $value = Agent::findOne($value)->shortName;
                    });
                    //-----------------------------//
                    $arrayPercentageByEssay = array_filter($arrayPercentage, function ($element) use ($keyEssayId) {
                        return ($element['essayId'] == $keyEssayId);
                    });
                    $percentageAccumulatedByEssay = array_sum(array_column($arrayPercentageByEssay, 'percent'));
                    //-----------------------------//
                    if (Essay::findOne($keyEssayId)->essayType->code == 'RSGL') {
                        $countAgent = count($valueAgentId);
                    } else if (Essay::findOne($keyEssayId)->essayType->code  == 'RMUL') {
                        $countAgent = 1;
                    }
                    //-----------------------------//
                    $arrayEssayAgentValue[$keyEssayId] = array(
                        'testName' => Essay::findOne($keyEssayId)->shortName,
                        'testCost' => Essay::findOne($keyEssayId)->cost->cost,
                        'agentQty' =>  $countAgent,
                        'sampleQty' =>  $arrayRequestProcess['sampleQty'],
                        'subTotalByTest' => Essay::findOne($keyEssayId)->cost->cost *  $countAgent * $arrayRequestProcess['sampleQty'],
                        'agentDetail' => implode(", ", $valueAgentId),
                        'paymentConcept' => $percentageAccumulatedByEssay,
                        'keyEssayId' => $keyEssayId,
                    );
                }

                //---------------------------------------------------------------------------------------------//
                if (!is_null(Yii::$app->request->post('taskPayment' . $essayId))) {
                    if (!is_null(Yii::$app->request->post('percentPayment' . $essayId)) and Yii::$app->request->post('percentPayment' . $essayId) <= 100 and Yii::$app->request->post('percentPayment' . $essayId) > 0) {
                        $taskPayment = Yii::$app->request->post('taskPayment' . $essayId);
                        $percentPayment = (float) Yii::$app->request->post('percentPayment' . $essayId);
                        //-----------------------------//
                        $arrayPercentageInEssay = array_filter($arrayPercentage, function ($element) use ($essayId) {
                            return ($element['essayId'] == $essayId);
                        });
                        $percentageAccumulatedInEssay = array_sum(array_column($arrayPercentageInEssay, 'percent'));
                        //-----------------------------//
                        if ((100 - $percentageAccumulatedInEssay) >= $percentPayment) {
                            $modelFinancial = new Financial();
                            $arrayFinancialBusService = $modelFinancial->getBus($taskPayment);
                            if (!is_null($arrayFinancialBusService)) {

                                if ($arrayFinancialBusService['bus_status'] == 'N') {
                                    $arrayEssayAgentValue[$essayId]['paymentConcept'] = $percentageAccumulatedInEssay + $percentPayment; //New
                                    $arrayListFinancialTransaction = Yii::$app->session->get('sessionListFinancialTransaction');
                                    $arrayListFinancialTransactionFilter = array_filter($arrayListFinancialTransaction, function ($element) use ($numOrderId) {
                                        return ($element['numOrderId'] == $numOrderId);
                                    });
                                    $arrayListFinancialConceptOne = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId, $essayId, $taskPayment) {
                                        return ($element['numOrderId'] . $element['essayId'] . $element['task']  != $numOrderId . $essayId . $taskPayment);
                                    });
                                    $indexFinancialConceptOne = array_keys($arrayListFinancialConceptOne)[count($arrayListFinancialConceptOne) - 1] + 1;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne] = $arrayListFinancialConceptOne[0];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['id'] = $indexFinancialConceptOne;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['essayId'] = $essayId;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['statusConcept'] = 'to_send';
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['statusBus'] = "active"; //New
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['period'] = (string) date("Y") . (string) date("m");
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['account'] = FinancialAccount::findOne($arrayListFinancialTransactionFilter[$numOrderId]['resnoLab'])->accountToCharge;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['resno'] =  $arrayListFinancialTransactionFilter[$numOrderId]['resnoLab'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['bus'] =  empty($arrayFinancialBusService['bus_id']) ? substr($arrayFinancialBusService['task_id'], 0, 9)  : $arrayFinancialBusService['bus_id'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['task'] = $arrayFinancialBusService['task_id'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['crop'] = Crop::findOne($arrayListFinancialTransactionFilter[$numOrderId]['cropId'])->longName;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['testOfLaboratory'] = Essay::findOne($essayId)->shortName;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['owner'] =  $arrayFinancialBusService['description'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['pathogens'] =  $arrayEssayAgentValue[$essayId]['agentDetail'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['numberOfExperiments'] = 1;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['numberOfPathogens'] = $arrayEssayAgentValue[$essayId]['agentQty'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['descriptionFinal'] = "Request=requestId; Test=" . Essay::findOne($essayId)->shortName
                                        . "; " . Crop::findOne($arrayListFinancialTransactionFilter[$numOrderId]['cropId'])->longName
                                        . "; " . $arrayFinancialBusService['description']
                                        . "; Pathogen= " . $arrayEssayAgentValue[$essayId]['agentDetail']
                                        . "; Experiment= " . (string) 1
                                        . "; Sample= " . $arrayRequestProcess['sampleQty']
                                        . "; ACE= " . Essay::findOne($essayId)->cost->code
                                        . "; Total= " . (string) ($arrayEssayAgentValue[$essayId]['subTotalByTest']);
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['ACE'] = Essay::findOne($essayId)->cost->code;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['agreement'] = $arrayFinancialBusService['project_id'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['samples'] = $arrayRequestProcess['sampleQty'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['costId'] = Essay::findOne($essayId)->costId;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['fee'] =  $arrayEssayAgentValue[$essayId]['testCost'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['total'] =  $arrayEssayAgentValue[$essayId]['subTotalByTest'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['resnoLab'] = $arrayListFinancialTransactionFilter[$numOrderId]['resnoLab'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['requestId'] = $arrayListFinancialTransactionFilter[$numOrderId]['requestId'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['cropId'] = $arrayListFinancialTransactionFilter[$numOrderId]['cropId'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['workFlowId'] = $arrayListFinancialTransactionFilter[$numOrderId]['workFlowId'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['numOrderId'] = $arrayListFinancialTransactionFilter[$numOrderId]['numOrderId'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['percent'] = $percentPayment;
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['totalPercentage'] = round((($percentPayment *  $arrayEssayAgentValue[$essayId]['testCost'] * $arrayRequestProcess['sampleQty'] * $arrayEssayAgentValue[$essayId]['agentQty']) / 100), 2);
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['dateTo'] = $arrayFinancialBusService['date_to'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['status'] = 'disabled';
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['tab'] = $arrayFinancialBusService['tab'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['projectDescription'] = $arrayFinancialBusService['project'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['busDescription'] = $arrayFinancialBusService['bus'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['busStatus'] = $arrayFinancialBusService['bus_status'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['resourceId'] = $arrayFinancialBusService['resource_id'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['taskDescription'] = $arrayFinancialBusService['task'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['taskStatus'] = $arrayFinancialBusService['task_status'];
                                    $arrayListFinancialConceptOne[$indexFinancialConceptOne]['email'] = $arrayFinancialBusService['email'];

                                    Yii::$app->session->set('sessionListFinancialConceptOne', $arrayListFinancialConceptOne);
                                } else {

                                    $variableMessage = $arrayFinancialBusService['bus_status'];
                                    $expiryDateMessage =   (string) date("d-M-Y",  strtotime($arrayFinancialBusService['date_to']));
                                    $taskMessage =  $arrayFinancialBusService['task_id'];

                                    switch ($variableMessage) {
                                        case 'P':
                                            $message = "The status of the financial bus is not active. "
                                                . '<br/>' . " Status bus: " .  $variableMessage . " (Parked)"
                                                . '<br/>' . " Expiry date: " .  $expiryDateMessage
                                                . '<br/>' . " Task: " .  $taskMessage;
                                            break;
                                        case 'C':
                                            $message = "The status of the financial bus is not active. "
                                                . '<br/>' . " Status bus: " .  $variableMessage . " (Closed)"
                                                . '<br/>' . " Expiry date: " .  $expiryDateMessage
                                                . '<br/>' . " Task: " .  $taskMessage;
                                            break;
                                        case 'T':
                                            $message = "The status of the financial bus is not active. "
                                                . '<br/>' . " Status bus: " .  $variableMessage . " (Terminated)"
                                                . '<br/>' . " Expiry date: " .  $expiryDateMessage
                                                . '<br/>' . " Task: " .  $taskMessage;
                                            break;
                                        default:
                                            $message = "The status of the financial bus is not active. ";
                                            break;
                                    }
                                }
                            } else {
                                $message = "The financial bus status does not exist.";
                            }
                        }
                    }
                }
                //---------------------------------------------------------------------------------------------//
                $arrayListFinancialConceptOneFilter = array_filter($arrayListFinancialConceptOne, function ($element) use ($numOrderId) {
                    return ($element['numOrderId'] == $numOrderId);
                });
                foreach ($arrayEssayAgentValue as $essayId => $valueData) {
                    $arrayListFinancialConceptOneDataProviderByEssay = array_filter($arrayListFinancialConceptOneFilter, function ($element) use ($essayId) {
                        return ($element['essayId'] == $essayId);
                    });
                    $dataProviderRequestProcessPayment[$essayId] = new ArrayDataProvider(
                        [
                            'allModels' => $arrayListFinancialConceptOneDataProviderByEssay,
                            'pagination' => ['pageSize' => 10,],
                        ]
                    );
                }
                //=============================================================================================//
                return $this->renderAjax('add-payment', [
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'arrayEssayAgentValue' => $arrayEssayAgentValue,
                    'dataProviderRequestProcessPayment' => $dataProviderRequestProcessPayment,
                    'message' => $message,
                ]);
            }
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    //------------------------------------------------------------------------------------------------------------------------------------//
    //SAVE REQUEST
    public function actionSaveRequest($requestId)
    {
        if ($requestId == 0 or Request::findOne($requestId)->requestStatus->code == 1.1) {
            return $this->renderAjax('save-request', [
                'requestId' => $requestId,
            ]);
        } else {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                ]
            );
        }
    }

    public function getSaveRequestId($newRequest, $created, $requestId, $save)
    {
        $intRequestValidate = 1;
        $arrayRequest = Yii::$app->session->get('sessionRequest');

        $arrayRequest['userId'] =  Yii::$app->user->identity->id;
        $arrayRequest['details'] =    is_null(Yii::$app->request->post('textAreaDetail')) ? "" : Yii::$app->request->post('textAreaDetail');
        $arrayRequest['status'] = 'active';
        $arrayRequest['requestTypeId'] = 49;
        // 47		TYPE	REQUEST	INTRODUCTION
        // 48		TYPE	REQUEST	DISTRIBUTION
        // 49		TYPE	REQUEST	OTHER
        $arrayRequest['paymentTypeId'] = 46;
        // 23		PAYMENT	REQUEST	Cash
        // 24		PAYMENT	REQUEST	Credit card
        // 25		PAYMENT	REQUEST	Deposit
        // 46		PAYMENT	REQUEST	Other
        if ($created) {
            $arrayRequest['requestStatusId'] = 4;
            $arrayRequest['creationDate'] = date("Y-m-d H:i:s", time());
        } else {
            $arrayRequest['requestStatusId'] = 65;
            // $arrayRequest['creationDate']  = date("Y-m-d H:i:s", time());
        }

        // 65	1.1	STATUS	REQUEST	DRAFT
        // 4	2.1	STATUS	REQUEST	CREATED
        // 120	3.0	STATUS	REQUEST	REJECTED
        // 122	3.1	STATUS	REQUEST	APPROVED
        // 6	4.1	STATUS	REQUEST	UPLOADED
        // 5	5.0	STATUS	REQUEST	REPROCESSING
        // 7	5.1	STATUS	REQUEST	VALIDATED
        // 142	6.1	STATUS	REQUEST	SENT
        // 143	7.1	STATUS	REQUEST	VERIFIED

        // creationDate
        // rejectionDate
        // approvalDate
        // uploadDate
        // reprocessDate
        // validationDate
        // notificationDate
        // verificationDate

        $listModelRequest =  Request::find()->where(
            [
                'status' => 'active',
            ]
        )->andFilterWhere(
            [
                '>=', 'registeredAt', date('Y-01-01')
            ]
        )->asArray()->all();

        if (count($listModelRequest) > 0) {
            $listModelRequest = ArrayHelper::map($listModelRequest, 'id', 'code');
            $postCode =   substr('00' . (substr($listModelRequest[max(array_keys($listModelRequest))], 5, 3) + 1),  -3);
            $arrayRequest['code'] = date("Y") . "-" .  $postCode;
        } else {
            $arrayRequest['code'] = date("Y") . "-001";
        }

        $modelRequest = ($newRequest ? new Request() : Request::findOne($requestId));
        $modelRequest->attributes = $arrayRequest;

        if ($save) {
            $modelRequest->save();
            $requestId = $modelRequest->id;
        } else {
            if (!$modelRequest->validate())  $intRequestValidate  = 0;
            if ($intRequestValidate) {
                Yii::$app->session->set('sessionRequest', $arrayRequest);
            }
        }

        return $requestId;
    }

    public function getSaveRequestProcess($newRequest, $requestId, $intValidate, $save)
    {
        $intRequestProcessValidated = 1;
        $agentQty = 0;
        $sampleQty = 0;
        $totalCost = 0;

        $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
        ArrayHelper::remove($arrayListRequestProcess, '0');


        foreach ($arrayListRequestProcess as $key => $value) {
            $agentQty = $agentQty + $value['agentQty'];
            $sampleQty = $sampleQty + $value['sampleQty'];
            $totalCost = $totalCost + $value['subTotalCost'];
            $modelRequestProcess = ($newRequest ?
                new RequestProcess()
                : RequestProcess::find()->where([
                    'requestId' => $value['requestId'],
                    'cropId' => $value['cropId'],
                    'workFlowId' => $value['workFlowId'],
                    'numOrderId' => $value['numOrderId'],
                    'status' => 'active',
                ])->one());
            $modelRequestProcess->attributes = $value;
            $modelRequestProcess->requestId = $requestId;

            if ($save) {
                $modelRequestProcess->save();
            } else {
                if (!$modelRequestProcess->validate())  $intValidate = 0;
                $intRequestProcessValidated  = $intRequestProcessValidated * $intValidate;
            }
        }
        //$modelRequest = Request::findOne($requestId);
        $arrayRequest = Yii::$app->session->get('sessionRequest');
        $arrayRequest['agentQty']  = $agentQty;
        $arrayRequest['sampleQty'] = $sampleQty;
        $arrayRequest['totalCost'] = $totalCost;
        //$modelRequest->save();
        Yii::$app->session->set('sessionRequest', $arrayRequest); //SAVE

        return $intRequestProcessValidated;
    }

    public function getSaveRequestProcessDetail($newRequest, $requestId, $intValidate, $save)
    {
        $intRequestProcessDetailValidated = 1;
        $arrayListRequestProcessDetail = Yii::$app->session->get('sessionListRequestProcessDetail');
        ArrayHelper::remove($arrayListRequestProcessDetail, '0');
        foreach ($arrayListRequestProcessDetail as $key => $value) {
            $modelRequestProcessDetail = ($newRequest ?
                new RequestProcessDetail()
                : RequestProcessDetail::find()->where([
                    'requestId' => $value['requestId'],
                    'cropId' => $value['cropId'],
                    'workFlowId' => $value['workFlowId'],
                    'numOrderId' => $value['numOrderId'],
                    'activityId' => $value['activityId'],
                    'essayId' => $value['essayId'],
                    'status' => 'active',
                ])->one());
            $modelRequestProcessDetail->attributes = $value;
            $modelRequestProcessDetail->requestId = $requestId;
            if ($save) {
                $modelRequestProcessDetail->save();
            } else {
                if (!$modelRequestProcessDetail->validate())  $intValidate = 0;
                $intRequestProcessDetailValidated  = $intRequestProcessDetailValidated * $intValidate;
            }
        }
        return $intRequestProcessDetailValidated;
    }

    public function getSaveAgentByRequestProcessDetail($newRequest, $requestId, $intValidate, $save)
    {
        $intAgentByRequestProcessDetailValidated = 1;
        $arrayListAgentByRequestProcessDetail = Yii::$app->session->get('sessionListAgentByRequestProcessDetail');
        ArrayHelper::remove($arrayListAgentByRequestProcessDetail, '0');
        foreach ($arrayListAgentByRequestProcessDetail as $key => $value) {
            $modelAgentByRequestProcessDetail = ($newRequest ?
                new AgentByRequestProcessDetail()
                : AgentByRequestProcessDetail::find()->where([
                    'requestId' => $value['requestId'],
                    'cropId' => $value['cropId'],
                    'workFlowId' => $value['workFlowId'],
                    'numOrderId' => $value['numOrderId'],
                    'agentId' => $value['agentId'],
                    'essayId' => $value['essayId'],
                    'status' => 'active',
                ])->one());
            $modelAgentByRequestProcessDetail->attributes = $value;
            $modelAgentByRequestProcessDetail->requestId = $requestId;
            if ($save) {
                $modelAgentByRequestProcessDetail->save();
            } else {
                if (!$modelAgentByRequestProcessDetail->validate())  $intValidate = 0;
                $intAgentByRequestProcessDetailValidated  = $intAgentByRequestProcessDetailValidated * $intValidate;
            }
        }
        return $intAgentByRequestProcessDetailValidated;
    }

    public function getSaveSample($newRequest, $requestId, $intValidate, $save)
    {
        $intSampleValidated = 1;
        $arrayListSample = Yii::$app->session->get('sessionListSample');
        ArrayHelper::remove($arrayListSample, '0');

        foreach ($arrayListSample as $key => $value) {
            $modelSample = ($newRequest ?
                new Sample()
                : Sample::find()->where([
                    'id' => $value['id'],
                    'requestId' => $value['requestId'],
                    'cropId' => $value['cropId'],
                    'workFlowId' => $value['workFlowId'],
                    'numOrderId' => $value['numOrderId'],
                    'status' => 'active',
                ])->one());
            $modelSample->attributes = $value;
            $modelSample->requestId = $requestId;
            if ($save) {
                $modelSample->save();
            } else {
                if (!$modelSample->validate())  $intValidate = 0;
                $intSampleValidated  = $intSampleValidated * $intValidate;
            }
        }
        return $intSampleValidated;
    }

    public function getSaveFinancialTransaction($newRequest, $requestId, $intValidate, $save)
    {
        $intFinancialTransactionValidated = 1;
        $arrayListFinancialTransaction = Yii::$app->session->get('sessionListFinancialTransaction');
        ArrayHelper::remove($arrayListFinancialTransaction, '0');

        foreach ($arrayListFinancialTransaction as $key => $value) {
            $modelFinancialTransaction = ($newRequest ?
                new FinancialTransaction()
                : FinancialTransaction::find()->where([
                    'resnoLab' => $value['resnoLab'],
                    'requestId' => $value['requestId'],
                    'cropId' => $value['cropId'],
                    'workFlowId' => $value['workFlowId'],
                    'numOrderId' => $value['numOrderId'],
                    'status' => 'active',
                ])->one());

            $modelFinancialTransaction->attributes = $value;
            $modelFinancialTransaction->requestId = $requestId;
            if ($save) {
                $modelFinancialTransaction->save();
            } else {
                if (!$modelFinancialTransaction->validate())  $intValidate = 0;
                $intFinancialTransactionValidated  = $intFinancialTransactionValidated  * $intValidate;
            }
        }
        return $intFinancialTransactionValidated;
    }

    public function getSaveFinancialConceptOne($newRequest, $requestId, $intValidate, $save)
    {
        $intFinancialConceptOneValidated = 1;
        $arrayListFinancialConceptOne = Yii::$app->session->get('sessionListFinancialConceptOne');
        ArrayHelper::remove($arrayListFinancialConceptOne, '0');

        foreach ($arrayListFinancialConceptOne as $key => $value) {
            $modelFinancialConceptOne = ($newRequest ?
                new FinancialConceptOne()
                : FinancialConceptOne::find()->where([
                    'id' => $value['id'],
                    'requestId' => $value['requestId'],
                    'cropId' => $value['cropId'],
                    'workFlowId' => $value['workFlowId'],
                    'numOrderId' => $value['numOrderId'],
                    'status' => 'active',
                ])->one());
            $modelFinancialConceptOne->attributes = $value;
            $modelFinancialConceptOne->requestId = $requestId;
            $newDescription  = str_replace("requestId", Yii::$app->session->get('sessionRequest')['code'], $modelFinancialConceptOne->descriptionFinal);
            $modelFinancialConceptOne->descriptionFinal =  $newDescription;
            if ($save) {
                $modelFinancialConceptOne->save();
            } else {
                if (!$modelFinancialConceptOne->validate())  $intValidate = 0;
                $intFinancialConceptOneValidated  = $intFinancialConceptOneValidated  * $intValidate;
            }
        }
        return $intFinancialConceptOneValidated;
    }

    public function getRequestValidate()
    {
        $intSample = 1;
        $intPayment = 1;

        $intValidate = 1;
        $arrayListRequestProcess = Yii::$app->session->get('sessionListRequestProcess');
        ArrayHelper::remove($arrayListRequestProcess, '0');

        foreach ($arrayListRequestProcess as $key => $value) {
            if ($value['sampleStatus'] != "Sample loaded") $intValidate = 0;
            $intSample  = $intSample * $intValidate;
        }

        $intValidate = 1;
        foreach ($arrayListRequestProcess as $key => $value) {
            if ($value['paymentStatus'] != "Payment charged") $intValidate = 0;
            $intPayment  = $intPayment * $intValidate;
        }

        $result = 10 * $intSample + $intPayment;
        return  $result;
    }

    public function actionSave($requestId, $created)
    {
        $intValidate = 1;

        $newRequest = $requestId == 0 ? true : false;

        if (Yii::$app->request->post('agreeChecked')) {

            if ($newRequest or Request::findOne($requestId)->requestStatus->code == 1.1) {

                //------------------------------------------------------------------------------------------------------------------------------------//

                $requestId = $this->getSaveRequestId($newRequest, $created, $requestId, false);
                $intRequestProcessValidated =  $this->getSaveRequestProcess($newRequest, $requestId, $intValidate,  false);
                $intRequestProcessDetailValidated =  $this->getSaveRequestProcessDetail($newRequest, $requestId, $intValidate,  false);
                $intAgentByRequestProcessDetailValidated =  $this->getSaveAgentByRequestProcessDetail($newRequest, $requestId, $intValidate,  false);
                $intSampleValidated =   $this->getSaveSample($newRequest, $requestId, $intValidate,  false);
                $intFinancialTransactionValidated = $this->getSaveFinancialTransaction($newRequest, $requestId, $intValidate,  false);
                $intFinancialConceptOneValidated = $this->getSaveFinancialConceptOne($newRequest, $requestId, $intValidate,  false);
                //------------------------------------------------------------------------------------------------------------------------------------//

                $intValidate =  $intRequestProcessValidated *
                    $intRequestProcessDetailValidated *
                    $intAgentByRequestProcessDetailValidated *
                    $intSampleValidated *
                    $intFinancialTransactionValidated *
                    $intFinancialConceptOneValidated;

                if ($intValidate) {

                    $requestChecked = $this->getRequestValidate();

                    if ($requestChecked == 11) {
                        //------------------------------------------------------------------------------------------------------------------------------------//

                        $requestId = $this->getSaveRequestId($newRequest, $created, $requestId, true);
                        $intRequestProcessValidated =  $this->getSaveRequestProcess($newRequest, $requestId, $intValidate,  true);
                        $intRequestProcessDetailValidated =  $this->getSaveRequestProcessDetail($newRequest, $requestId, $intValidate, true);
                        $intAgentByRequestProcessDetailValidated =  $this->getSaveAgentByRequestProcessDetail($newRequest, $requestId, $intValidate, true);
                        $intSampleValidated =   $this->getSaveSample($newRequest, $requestId, $intValidate, true);
                        $intFinancialTransactionValidated = $this->getSaveFinancialTransaction($newRequest, $requestId, $intValidate, true);
                        $intFinancialConceptOneValidated = $this->getSaveFinancialConceptOne($newRequest, $requestId, $intValidate, true);

                        //------------------------------------------------------------------------------------------------------------------------------------//

                        $code = Yii::$app->session->get('sessionRequest')['code'];
                        $newRequest ? $message = "New request created!</h4>The values entered in the new application were registered successfully." : "Changes saved!</h4>New changes entered in the application were successfully registered.";
                        Yii::$app->session->setFlash('success',  "<h4><i class='icon fa fa-check'></i>(" .  $code . ") " . $message);


                        $toUserIdsArray[] =  Yii::$app->user->identity->id;
                        $toUserIds = implode(",", $toUserIdsArray);

                        try {

                            $this->sendNotification(
                                'system_admin',
                                'business_admin',
                                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                                array(
                                    [
                                        'header' => 'id',
                                        'body' => $requestId
                                    ],
                                    [
                                        'header' => 'code',
                                        'body' =>  $code
                                    ],
                                ),
                                'mail01',
                                $requestId,
                                $toUserIds
                            );
                        } catch (\Throwable $th) { }

                        Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                        return $this->redirect(
                            [
                                'manage-request',
                                'requestId' => $requestId,
                            ]
                        );
                    } else if ($requestChecked == 10) {
                        Yii::$app->session->setFlash('danger', "<h4><i class='icon fa fa-check'></i>No saved!</h4>Error saving payment information");
                    } else if ($requestChecked == 1) {
                        Yii::$app->session->setFlash('danger', "<h4><i class='icon fa fa-check'></i>No saved!</h4>Error saving sample information");
                    } else if ($requestChecked == 0) {
                        Yii::$app->session->setFlash('danger', "<h4><i class='icon fa fa-check'></i>No saved!</h4>Error saving sample and payment information");
                    }
                    Request::findOne($requestId)->delete();
                    $requestId = 0;
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->redirect(
                        [
                            'edit',
                            'requestId' => $requestId,
                        ]
                    );
                } else {
                    Yii::$app->session->setFlash('danger', "<h4><i class='icon fa fa-check'></i>Invalid Information!</h4>Error saving, one or more data is not or is invalid in the creation of the request");
                }
                //------------------------------------------------------------------------------------------------------------------------------------//
            } else {
                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->redirect(
                    [
                        'edit',
                        'requestId' => $requestId,
                    ]
                );
            }
        } else {
            Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No saved!</h4>You must agree with the conditions mentioned in the request");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    $newRequest ? 'create' : 'update',
                    'requestId' => $requestId
                ]
            );
        }
    }

    //♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦//
    //MANAGE-INDEX-VIEW

    public function actionManageRequestTrackingApprove($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->approvalDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  122;
            $userClientId =  $modelRequest->userId;
            //-----------------------
            /*
            $arrayListFinancialConceptOne = FinancialConceptOne::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListFinancialConceptOneOwner = array_unique(ArrayHelper::map($arrayListFinancialConceptOne, 'owner', 'email'));
            */

            $code = Request::findOne($requestId)->code;

            $toUserIdsArray[] = $userClientId;
            /*
            foreach ($arrayListFinancialConceptOneOwner as $key => $value) {
                $toUserIdsArray[] =  $value;
            }
            */
            $toUserIds = implode(",", $toUserIdsArray);
            $this->sendNotification(
                'system_admin',
                'tech_role',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail02',
                $requestId,
                $toUserIds
            );
            //-----------------------
            $modelRequest->save();
            Yii::$app->session->setFlash('info', "<h4><i class='icon fa fa-check'></i>Information!</h4> Approved by the quarantine unit");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionManageRequestTrackingRefuse($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->rejectionDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  120;
            $userClientId =  $modelRequest->userId;
            //-----------------------
            /*
            $arrayListFinancialConceptOne = FinancialConceptOne::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListFinancialConceptOneOwner = array_unique(ArrayHelper::map($arrayListFinancialConceptOne, 'owner', 'email'));
            */
            $code = Request::findOne($requestId)->code;
            $toUserIdsArray[] = $userClientId;
            /*
            foreach ($arrayListFinancialConceptOneOwner as $key => $value) {
                $toUserIdsArray[] =  $value;
            }
            */
            $toUserIds = implode(",", $toUserIdsArray);
            $this->sendNotification(
                'system_admin',
                'system_admin',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail03',
                $requestId,
                $toUserIds
            );
            //-----------------------
            $modelRequest->save();
            Yii::$app->session->setFlash('danger', "<h4><i class='icon fa fa-check'></i>Information!</h4> Approved by the quarantine unit");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionManageRequestTrackingUpload($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->uploadDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  6;
            //-----------------------
            $code = Request::findOne($requestId)->code;
            $this->sendNotification(
                'system_admin',
                'business_admin',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail04',
                $requestId
            );
            //-----------------------
            Yii::$app->session->setFlash('info', "<h4><i class='icon fa fa-check'></i>Information!</h4> Uploaded by technical staff");
            $modelRequest->save();
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionManageRequestTrackingValidate($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->validationDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  7;
            //-----------------------
            $code = Request::findOne($requestId)->code;
            $this->sendNotification(
                'system_admin',
                'tech_role',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail05',
                $requestId
            );
            //-----------------------
            $modelRequest->save();
            Yii::$app->session->setFlash('success', "<h4><i class='icon fa fa-check'></i>Information!</h4> Validated by the quarantine unit");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionManageRequestTrackingReprocess($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->reprocessDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  5;
            //-----------------------
            $code = Request::findOne($requestId)->code;
            $this->sendNotification(
                'system_admin',
                'tech_role',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail06',
                $requestId
            );
            //-----------------------
            $modelRequest->save();
            Yii::$app->session->setFlash('danger', "<h4><i class='icon fa fa-check'></i>Information!</h4> Invalidated by the quarantine unit, pending to reprocess");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionManageRequestTrackingSend($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->notificationDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  142;
            $userClientId =  $modelRequest->userId;
            //-----------------------
            /*
            $arrayListFinancialConceptOne = FinancialConceptOne::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
            $arrayListFinancialConceptOneOwner = array_unique(ArrayHelper::map($arrayListFinancialConceptOne, 'owner', 'email'));
            */
            $code = Request::findOne($requestId)->code;
            $toUserIdsArray[] = $userClientId;
            /*
            foreach ($arrayListFinancialConceptOneOwner as $key => $value) {
                $toUserIdsArray[] =  $value;
            }
            */
            $toUserIds = implode(",", $toUserIdsArray);
            $this->sendNotification(
                'system_admin',
                'system_admin',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail07',
                $requestId,
                $toUserIds
            );
            //-----------------------
            $modelRequest->save();
            Yii::$app->session->setFlash('info', "<h4><i class='icon fa fa-check'></i>Information!</h4> Sent notification to client user");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionManageRequestTrackingVerify($requestId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $modelRequest = Request::findOne($requestId);
            $modelRequest->verificationDate = date("Y-m-d H:i:s", time());
            $modelRequest->requestStatusId =  143;
            //-----------------------
            $code = Request::findOne($requestId)->code;
            $this->sendNotification(
                'system_admin',
                'business_admin',
                'CIPPATHOLOGY System: CIP Phytosanitary Certificate, Order ' . $code . ' addressed to HQU.',
                array(
                    [
                        'header' => 'id',
                        'body' => $requestId
                    ],
                    [
                        'header' => 'code',
                        'body' =>  $code
                    ],
                ),
                'mail08',
                $requestId
            );
            //-----------------------
            $modelRequest->save();
            Yii::$app->session->setFlash('success', "<h4><i class='icon fa fa-check'></i>Information!</h4> Verified by the customer user
            ");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    '#' => 'tracking',
                ]
            );
            /***********************************************/
        }
    }

    public function actionUpload(
        $requestId,
        $numOrderId,
        $essayId,
        $activityId,
        $supportOrder
    ) {
        $model = new UploadFile();
        $serial =  $requestId . "." . $numOrderId . "." . $activityId . "." . $essayId . "." . $supportOrder;
        /************************************************/
        $model->documentFile = UploadedFile::getInstance($model, 'documentFile');
        $uploadResult = $model->upload("_" . $serial);
        /************************************************/
        if ($uploadResult) {
            $fileName =   $uploadResult['path'] . $uploadResult['name'];
            $excelReader = PHPExcel_IOFactory::createReaderForFile($fileName);
            $excelObj = $excelReader->load($fileName);
            $worksheet = $excelObj->getSheet(0);
            $lastRow = 9; //Defined by the tests carried out in the CIP laboratory.
            for ($row = 2; $row <= $lastRow; $row++) {
                $arrayAbsorbancesResult[($row - 1)] = $worksheet->getCell('B' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 7)] =  $worksheet->getCell('C' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 15)] = $worksheet->getCell('D' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 23)] =  $worksheet->getCell('E' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 31)] =  $worksheet->getCell('F' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 39)] = $worksheet->getCell('G' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 47)] = $worksheet->getCell('H' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 55)] =  $worksheet->getCell('I' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 63)] = $worksheet->getCell('J' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 71)] =   $worksheet->getCell('K' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 79)] =  $worksheet->getCell('L' . $row)->getValue();
                $arrayAbsorbancesResult[($row + 87)] =  $worksheet->getCell('M' . $row)->getValue();
            }
            // *********************
            $arrayReadingData = ReadingData::find()->where([
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'activityId' => $activityId,
                'essayId' => $essayId,
                'supportId' => $supportOrder,
                'status' => 'active'

            ])->orderBy(
                [
                    'activityId' => SORT_ASC,
                    'agentId' => SORT_ASC,
                    'supportId' => SORT_ASC,
                    'cellPosition' => SORT_ASC,
                ]
            )->asArray()->all();
            // *********************
            $arrayReadingData = ArrayHelper::index($arrayReadingData, 'cellPosition');
            $arrayControls = array_filter($arrayReadingData, function ($element) {
                return ($element['readingDataTypeId'] == 11); //DECLARE IN THE INIT
            });
            $arrayMapAgentControl = ArrayHelper::map($arrayControls, 'cellPosition', 'cellPosition', 'agentId');

            // pvx   41, 42, 43, 44, 45, 46, 47, 48

            // pvx   89, 90, 91, 92, 93, 94, 95, 96

            // get sane positions
            $arraySanePositions = [];
            foreach ($arrayMapAgentControl  as $agentIdKey => $arrayCellPositionValue) {
                $cellControls = count($arrayCellPositionValue); // get cell control count by agent (3, 6, 8)

                // pvx   41, 42, 43, 44, 45, 46, 47, 48
                foreach ($arrayCellPositionValue as $cellPositionKey => $cellPositionValue) {
                    // pvx   41 (cellPositionValue)

                    $numOrderControlByAgent = $cellPositionValue -  8 * intdiv($cellPositionValue, 8);
                    //  1 (numOrderControlByAgent)

                    if ($cellControls == 3 and $numOrderControlByAgent == 7) {
                        // if only 3 controls by agent in one support -> the 7th position in a column on support is a sane control

                        // 1.
                        // 2.
                        // 3.
                        // 4.
                        // 5
                        // 6.
                        // 7        -
                        // 8+

                        $arraySanePositions[$agentIdKey][$cellPositionKey] = $arrayAbsorbancesResult[$cellPositionValue];
                    } else if ($numOrderControlByAgent  == 5 or $numOrderControlByAgent == 6) {
                        // else the 5th and 6th positions in a column on support are the sane controls

                        // 1.
                        // 2.
                        // 3.
                        // 4.
                        // 5        -
                        // 6        -
                        // 7+
                        // 8+

                        $arraySanePositions[$agentIdKey][$cellPositionKey] =  $arrayAbsorbancesResult[$cellPositionValue];
                    }
                }
            }
            // *********************
            foreach ($arraySanePositions as $agentIdKey => $averageValue) {
                $arraySaneControls[$agentIdKey] = array_sum($averageValue) / count($averageValue);
            }

            // Rules from absorbances
            // Valor	Range 
            // -	abs > 1.5x
            // ? 	1.5x <= abs < 2x
            // +	abs >= 2x
            // X: average sane control

            // Formula with sane positions and results
            foreach ($arrayReadingData  as $cellPositionKey => $arrayReadingDataValue) {
                $modelReadingData =  ReadingData::find()->where(
                    [
                        'requestId' => $requestId,
                        'numOrderId' => $numOrderId,
                        'activityId' => $activityId,
                        'essayId' => $essayId,
                        'supportId' => $supportOrder,
                        'cellPosition' => $cellPositionKey,
                        'status' => 'active'
                    ]
                )->one();
                $readingDataResult = $arrayAbsorbancesResult[$cellPositionKey];

                $modelReadingData->qResult = $readingDataResult;
                $saneResult = $arraySaneControls[$modelReadingData->agentId];

                if ($readingDataResult < 1.5 * $saneResult) {
                    $modelReadingData->cResult = "negative";
                } else if ($readingDataResult < 2 * $saneResult) {
                    $modelReadingData->cResult = "doubt";
                } else {
                    $modelReadingData->cResult = "positive";
                }
                // $modelReadingData->evidence = $saneResult;
                $modelReadingData->save();
            }
        } else {
            return $this->renderAjax(
                'upload',
                [
                    'serial' => $serial,
                    'model' => $model,
                ]
            );
        }
        Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
        return $this->redirect(
            [
                'manage-request',
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                '#' => 'results',
            ]
        );
    }

    //♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ RESULTS ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦//

    public function actionManageRequestFilter(
        $requestId,
        $numOrderId,
        $essayId
    ) {
        return $this->redirect(
            [
                'manage-request',
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'passportFilters' =>  !is_null(Yii::$app->request->post('passportFilter')) ?  implode('|', array_keys(Yii::$app->request->post('passportFilter'))) : null,
                'activityFilters' =>  !is_null(Yii::$app->request->post('activityFilter')) ?  implode('|', array_keys(Yii::$app->request->post('activityFilter'))) : null,
                'agentFilters' => !is_null(Yii::$app->request->post('agentFilter')) ?  implode('|', array_keys(Yii::$app->request->post('agentFilter'))) : null,
            ]
        );
    }

    public function actionManageRequest(
        $requestId,
        $numOrderId = null,
        $essayId = null,
        $type = null,
        $passportFilters = null,
        $activityFilters = null,
        $agentFilters = null
    ) {
        /*************************************** NEW ***************************************/
        $listNavigation['absorbance'] = null;

        $listNavigation['passportFilter'] = null;
        $listNavigation['activityFilter'] = null;
        $listNavigation['agentFilter'] = null;
        /***********************************************************************************/



        if (is_null($type)) {
            if (isset(Yii::$app->session['sessionType'])) {
                $type =  Yii::$app->session['sessionType'];
            }
        } else {
            Yii::$app->session['sessionType'] =  $type;
        }








        /*VALUE_ESSAY*/
        if (!is_null($essayId) and !is_null($numOrderId)) {







            /*FILTER PASSPORT*/
            if (is_null($passportFilters)) {

                if ($essayId == 2 or $essayId  == 3) {
                    $listNavigation['passportFilter'] = [
                        'order' => "",
                        'numOrder' =>  "checked",
                        'sampleId' => "",
                        'accession_number' => "checked",
                        'other_code_1' => "checked",
                        'other_code_2' => "checked",
                        'collecting_number' => "checked",
                    ];
                } else {
                    $listNavigation['passportFilter'] = [
                        'order' => "checked",
                        'numOrder' =>  "checked",
                        'sampleId' => "checked",
                        'accession_number' => "checked",
                        'other_code_1' => "checked",
                        'other_code_2' => "checked",
                        'collecting_number' => "checked",
                        'observation' => "checked",
                    ];
                }

                $passportFilters = implode('|', array_keys($listNavigation['passportFilter']));
            } else {
                if ($essayId == 2 or $essayId  == 3) {
                    $listNavigation['passportFilter'] = [
                        'order' => "",
                        'numOrder' =>  "",
                        'sampleId' => "",
                        'accession_number' => "",
                        'other_code_1' => "",
                        'other_code_2' => "",
                        'collecting_number' => "",
                    ];
                } else {
                    $listNavigation['passportFilter'] = [
                        'order' => "",
                        'numOrder' =>  "",
                        'sampleId' => "",
                        'accession_number' => "",
                        'other_code_1' => "",
                        'other_code_2' => "",
                        'collecting_number' => "",
                        'observation' => "",
                    ];
                }

                foreach (array_keys($listNavigation['passportFilter']) as $key => $value) {
                    if (in_array($value, explode("|", $passportFilters))) {
                        $listNavigation['passportFilter'][$value] = "checked";
                    }
                }
            }



            /*FILTER ACTIVITY*/
            if (is_null($activityFilters)) {

                foreach (Essay::findOne($essayId)->activities  as $key => $modelActivity) {

                    $listNavigation['activityFilter'][$modelActivity->id] =  "checked";
                }
                $activityFilters = implode('|', array_keys($listNavigation['activityFilter']));
            } else {
                foreach (Essay::findOne($essayId)->activities  as $key => $modelActivity) {
                    $listNavigation['activityFilter'][$modelActivity->id] =  "";
                }
                foreach (array_keys($listNavigation['activityFilter']) as $key => $value) {
                    if (in_array($value, explode("|", $activityFilters))) {
                        $listNavigation['activityFilter'][$value] = "checked";
                    }
                }
            }



            /*FILTER AGENT*/
            if (is_null($agentFilters)) {
                foreach (AgentByRequestProcessDetail::find()->where([
                    'requestId' =>   $requestId,
                    'numOrderId' => $numOrderId,
                    'essayId' => $essayId,
                ])->all() as $key => $modelAgentByRequestProcessDetail) {
                    $listNavigation['agentFilter'][$modelAgentByRequestProcessDetail->agentId] =  "checked";
                }
                $agentFilters = implode('|', array_keys($listNavigation['agentFilter']));
            } else {
                foreach (AgentByRequestProcessDetail::find()->where([
                    'requestId' =>   $requestId,
                    'numOrderId' => $numOrderId,
                    'essayId' => $essayId,
                ])->all() as $key => $modelAgentByRequestProcessDetail) {
                    $listNavigation['agentFilter'][$modelAgentByRequestProcessDetail->agentId] =  "";
                }
                foreach (array_keys($listNavigation['agentFilter']) as $key => $value) {
                    if (in_array($value, explode("|", $agentFilters))) {
                        $listNavigation['agentFilter'][$value] = "checked";
                    }
                }
            }



            /*ABSORBANCES*/
            foreach (Essay::findOne($essayId)->activities  as $key => $modelActivity) {
                if ($modelActivity->activityType->code == 'quantitative' and $modelActivity->activityDataSource->code == 'WEB') {
                    $listNavigation['absorbance'][$modelActivity->id] = $this->actionGenerateDistribution(
                        $requestId,
                        $numOrderId,
                        $essayId,
                        $modelActivity->id
                    );
                }
            }
        }



        /*************************************** NEW ***************************************/
        $modelRequest = Request::findOne($requestId);
        $arrayRequestProcess = RequestProcess::find()->where(['requestId' => $requestId, 'status' => 'active',])->orderBy(['numOrderId' => SORT_ASC,])->asArray()->all();
        $modelListRequestProcess =  RequestProcess::find()->where(['requestId' => $requestId, 'status' => 'active',])->orderBy(['numOrderId' => SORT_ASC,])->all();

        $arrayRequestProcessDetail = RequestProcessDetail::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
        $arrayAgentByRequestProcessDetail = AgentByRequestProcessDetail::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
        $arrayListSample = Sample::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();

        $arrayListFinancialConceptOne = FinancialConceptOne::find()->where(['requestId' => $requestId, 'status' => 'active',])->asArray()->all();
        $modelListFinancialConceptOne = FinancialConceptOne::find()->where(['requestId' => $requestId, 'status' => 'active',]);
        $dataProviderFinancialConceptOne = new ActiveDataProvider(['query' => $modelListFinancialConceptOne, 'pagination' => ['pageSize' => 0,], 'sort' => ['defaultOrder' => ['statusBus' => SORT_ASC,]],]);
        //****************************************************************************************************************************//
        $resultByActivity = $this->actionResultByActivity(
            $requestId,
            $numOrderId,
            $essayId,
            $listNavigation['passportFilter'],
            $listNavigation['activityFilter'],
            $listNavigation['agentFilter'],
            $type
            //'advance'
            // 'resume'
        );

        if ($resultByActivity == false) {
            $dataProvider[1] = null;
            $columns[1] = null;
        } else {
            $dataResults[1] = $resultByActivity;
            $dataProvider[1] =  new ArrayDataProvider(
                [
                    'allModels' => $dataResults[1]['dataProvider'],
                    'pagination' => ['pageSize' => 0,],
                ]
            );
            if (isset($dataResults[1]['columns'])) {
                $columns[1] =  $dataResults[1]['columns'];
            } else {
                $dataProvider[1] = null;
                $columns[1] = null;
            }
        }
        //****************************************************************************************************************************//
        // $resultByActivityResume =  $this->actionResultByActivityResume(
        //     $requestId,
        //     $numOrderId,
        //     $essayId,
        //     $listNavigation['passportFilter'],
        //     $listNavigation['activityFilter'],
        //     $listNavigation['agentFilter'],
        //     "resume"
        // );
        // if ($resultByActivityResume  == false) {
        //     $dataProvider[2] = null;
        //     $columns[2] = null;
        // } else {
        //     $dataResults[2] = $resultByActivityResume;
        //     $dataProvider[2] =  new ArrayDataProvider(
        //         [
        //             'allModels' => $dataResults[2]['dataProvider'],
        //             'pagination' => ['pageSize' => 0,],
        //         ]
        //     );
        //     if (isset($dataResults[2]['columns'])) {
        //         $columns[2] =  $dataResults[2]['columns'];
        //     } else {
        //         $dataProvider[2] = null;
        //         $columns[2] = null;
        //     }
        // }
        //****************************************************************************************************************************//
        // $ResultByAgent =  $this->actionResultByAgent(
        //     $requestId,
        //     $numOrderId,
        //     $essayId,
        //     $listNavigation['passportFilter'],
        //     $listNavigation['activityFilter'],
        //     $listNavigation['agentFilter']
        // );
        // if ($ResultByAgent  == false) {
        //     $dataProvider[3] = null;
        //     $columns[3] = null;
        // } else {
        //     $dataResults[3] =   $ResultByAgent;
        //     $dataProvider[3] =  new ArrayDataProvider(
        //         [
        //             'allModels' => $dataResults[1]['dataProvider'],
        //             'pagination' => ['pageSize' => 0,],
        //         ]
        //     );
        //     $columns[3] =  $dataResults[3]['columns'];
        // }
        //****************************************************************************************************************************//
        // $resultByAgentResume =  $this->actionResultByAgentResume(
        //     $requestId,
        //     $numOrderId,
        //     $essayId,
        //     $listNavigation['passportFilter'],
        //     $listNavigation['activityFilter'],
        //     $listNavigation['agentFilter']
        // );
        // if ($resultByAgentResume  == false) {
        //     $dataProvider[4] = null;
        //     $columns[4] = null;
        // } else {
        //     $dataResults[4] =   $resultByAgentResume;
        //     $dataProvider[4] =  new ArrayDataProvider(
        //         [
        //             'allModels' => $dataResults[4]['dataProvider'],
        //             'pagination' => ['pageSize' => 0,],
        //         ]
        //     );
        //     $columns[4] =  $dataResults[4]['columns'];
        // }
        //****************************************************************************************************************************//
        // $resultBySupport =    $this->actionResultBySupport(
        //     $requestId,
        //     $numOrderId,
        //     $essayId,
        //     $listNavigation['passportFilter'],
        //     $listNavigation['activityFilter'],
        //     $listNavigation['agentFilter']
        // );
        // if ($resultBySupport !== false) {
        //     $dataResults[5] = $resultBySupport;
        //     $dataProvider[5] =  $dataResults[5]['readingDataByActivities'];
        //     $columns[5]  =  $dataResults[5]['activities'];
        // }
        //****************************************************************************************************************************//
        return $this->render('manage-request', [
            'requestId' => $requestId,
            'modelRequest' => $modelRequest,
            //----------------------------------
            'arrayRequestProcess' => $arrayRequestProcess,
            'arrayRequestProcessDetail' => $arrayRequestProcessDetail,
            'arrayAgentByRequestProcessDetail' => $arrayAgentByRequestProcessDetail,
            //----------------------------------
            'listNavigation' => $listNavigation,
            //----------------------------------
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            //----------------------------------
            'arrayListSample' => $arrayListSample,
            'arrayListFinancialConceptOne' => $arrayListFinancialConceptOne,
            'dataProviderFinancialConceptOne' => $dataProviderFinancialConceptOne,
            //--------------------------------------
            'modelListRequestProcess' => $modelListRequestProcess,
            // 'passportFilters' => $passportFilters,
            // 'activityFilters' => $activityFilters,
            // 'agentFilters' => $agentFilters,
            //--------------------------------------
        ]);
    }

    public function actionManageReport(
        $requestId,
        $numOrderId = null,
        $essayId = null
    ) {
        if (
            is_null($numOrderId) or
            is_null($essayId)
        ) {
            return $this->redirect(
                [
                    'manage-request',
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'essayId' => $essayId
                ]
            );
        } else if (isset(Yii::$app->session['sessionType'])) {
            $type =  Yii::$app->session['sessionType'];
        }

        return $this->redirect(
            [
                'report',
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'type' => $type,
                'passportFilters' =>  !is_null(Yii::$app->request->post('passportFilter')) ?  implode('|', array_keys(Yii::$app->request->post('passportFilter'))) : null,
                'activityFilters' =>  !is_null(Yii::$app->request->post('activityFilter')) ?  implode('|', array_keys(Yii::$app->request->post('activityFilter'))) : null,
                'agentFilters' => !is_null(Yii::$app->request->post('agentFilter')) ?  implode('|', array_keys(Yii::$app->request->post('agentFilter'))) : null,
            ]
        );
    }

    public function actionReport(
        $requestId,
        $numOrderId = null,
        $essayId = null,
        $type = null,

        $passportFilters = null,
        $activityFilters = null,
        $agentFilters = null
    ) {
        /*************************************** NEW ***************************************/
        $listNavigation['passportFilter'] = null;
        $listNavigation['activityFilter'] = null;
        $listNavigation['agentFilter'] = null;

        /*VALUE_ESSAY*/
        if (!is_null($essayId) and !is_null($numOrderId)) {

            /*FILTER PASSPORT*/
            if (is_null($passportFilters)) {
                $listNavigation['passportFilter'] = ['order' => "checked", 'numOrder' =>  "checked", 'sampleId' => "checked", 'accession_number' => "checked", 'other_code_1' => "checked", 'other_code_2' => "checked", 'collecting_number' => "checked", 'observation' => "checked",];
                $passportFilters = implode('|', array_keys($listNavigation['passportFilter']));
            } else {
                $listNavigation['passportFilter'] = ['order' => "", 'numOrder' =>  "", 'sampleId' => "", 'accession_number' => "", 'other_code_1' => "", 'other_code_2' => "", 'collecting_number' => "", 'observation' => "",];
                foreach (array_keys($listNavigation['passportFilter']) as $key => $value) {
                    if (in_array($value, explode("|", $passportFilters))) {
                        $listNavigation['passportFilter'][$value] = "checked";
                    }
                }
            }

            /*FILTER ACTIVITY*/
            if (is_null($activityFilters)) {
                foreach (Essay::findOne($essayId)->activities  as $key => $modelActivity) {
                    $listNavigation['activityFilter'][$modelActivity->id] =  "checked";
                }
                $activityFilters = implode('|', array_keys($listNavigation['activityFilter']));
            } else {
                foreach (Essay::findOne($essayId)->activities  as $key => $modelActivity) {
                    $listNavigation['activityFilter'][$modelActivity->id] =  "";
                }
                foreach (array_keys($listNavigation['activityFilter']) as $key => $value) {
                    if (in_array($value, explode("|", $activityFilters))) {
                        $listNavigation['activityFilter'][$value] = "checked";
                    }
                }
            }

            /*FILTER AGENT*/
            if (is_null($agentFilters)) {
                foreach (AgentByRequestProcessDetail::find()->where([
                    'requestId' =>   $requestId,
                    'numOrderId' => $numOrderId,
                    'essayId' => $essayId,
                ])->all() as $key => $modelAgentByRequestProcessDetail) {
                    $listNavigation['agentFilter'][$modelAgentByRequestProcessDetail->agentId] =  "checked";
                }
                $agentFilters = implode('|', array_keys($listNavigation['agentFilter']));
            } else {
                foreach (AgentByRequestProcessDetail::find()->where([
                    'requestId' =>   $requestId,
                    'numOrderId' => $numOrderId,
                    'essayId' => $essayId,
                ])->all() as $key => $modelAgentByRequestProcessDetail) {
                    $listNavigation['agentFilter'][$modelAgentByRequestProcessDetail->agentId] =  "";
                }
                foreach (array_keys($listNavigation['agentFilter']) as $key => $value) {
                    if (in_array($value, explode("|", $agentFilters))) {
                        $listNavigation['agentFilter'][$value] = "checked";
                    }
                }
            }

            /*ABSORBANCES*/
            foreach (Essay::findOne($essayId)->activities  as $key => $modelActivity) {
                if ($modelActivity->activityType->code == 'quantitative' and $modelActivity->activityDataSource->code == 'WEB') {
                    $listNavigation['absorbance'][$modelActivity->id] = $this->actionGenerateDistribution(
                        $requestId,
                        $numOrderId,
                        $essayId,
                        $modelActivity->id
                    );
                }
            }
        }

        $resultByActivity = $this->actionResultByActivity(
            $requestId,
            $numOrderId,
            $essayId,
            $listNavigation['passportFilter'],
            $listNavigation['activityFilter'],
            $listNavigation['agentFilter'],
            $type
            //'advance'
            // 'resume'
        );

        if ($resultByActivity == false) {
            $dataProvider = null;
            $columns = null;
        } else {
            $dataResults = $resultByActivity;
            $dataProvider =  new ArrayDataProvider(
                [
                    'allModels' => $dataResults['dataProvider'],
                    'pagination' => ['pageSize' => 0,],
                ]
            );
            if (isset($dataResults['columns'])) {
                $columns =  $dataResults['columns'];
            } else {
                $dataProvider = null;
                $columns = null;
            }
        }

        return $this->renderPartial(
            'reportTemplateA',
            [
                'dataProvider' => $dataProvider,
                'columns' => $columns,
            ]
        );
    }

    // ------------------------- RESULTS VALUES ------------------------- //

    public function actionResultByActivity(
        $requestId,
        $numOrderId = null,
        $essayId = null,

        $passportHeader = null,
        $activityHeader = null,
        $agentHeader = null,
        $type = null
    ) {

        if (Yii::$app->user->can("request_result-by-activity")) {

            if (is_null($numOrderId) or is_null($essayId)) {
                $session = Yii::$app->session;
                $session->destroy();
                return false;
            } else {

                if ($type == "advance") {
                    $arrayReadingData = ReadingData::find()->where([
                        'requestId' => $requestId,
                        'numOrderId' => $numOrderId,
                        'essayId' => $essayId,
                        'status' => 'active',
                    ])->orderBy(
                        [
                            'activityId' => SORT_ASC,
                            'agentId' => SORT_ASC,
                            'supportId' => SORT_ASC,
                            'cellPosition' => SORT_ASC,
                        ]
                    )->asArray()->all();
                } else if ($type == "resume") {
                    $arrayReadingData = ReadingData::find()->where([
                        'requestId' => $requestId,
                        'numOrderId' => $numOrderId,
                        'essayId' => $essayId,
                        'status' => 'active',
                        'readingDataTypeId' => 13, // READING DATA|RESULT
                    ])->orderBy(
                        [
                            'activityId' => SORT_ASC,
                            'agentId' => SORT_ASC,
                            'supportId' => SORT_ASC,
                            'cellPosition' => SORT_ASC,
                        ]
                    )->asArray()->all();
                }

                $oldValue = null;
                $oldValueAux = null;
                $currentOrder = 0;
                $arrayReadingDataIndexed = [];
                $symptomList = [];
                $symptomNameList = [];




                if ($essayId == 2 or $essayId  == 3) {

                    $arraySamplesAux = Sample::find()
                        ->where(
                            [
                                'requestId' => $requestId
                            ]
                        )->asArray()
                        ->all();

                    $arraySamples = array_keys(ArrayHelper::map($arraySamplesAux, 'id', 'id'));
                    $arraySamples = array_flip($arraySamples);
                    array_walk($arraySamples, function (&$value, $key) {
                        return $value =  $value + 1;
                    });

                    foreach ($arrayReadingData as $readingDataKey => $readingDataValue) {

                        $currentOrder = $arraySamples[$readingDataValue['sampleId']];

                        $currentValue = $readingDataValue['activityId'] . '_' . $readingDataValue['agentId'] . '_' . $readingDataValue['sampleId'];
                        if ($oldValue != $currentValue) {
                            $oldValue =  $currentValue;
                            $symptomList = [];
                            $symptomNameList = [];
                        }
                        if ($readingDataValue['sampleId'])
                            if ($readingDataValue['cResult'] == 'positive') {
                                $symptomList[] = Symptom::findOne($readingDataValue['symptomId'])->shortName;
                                $symptomNameList[] =  Symptom::findOne($readingDataValue['symptomId'])->longName;
                            }
                        $arrayReadingData[$readingDataKey]['cellPositionUser'] = $currentOrder;
                        $arrayReadingDataIndexed[$readingDataValue['activityId']][$readingDataValue['agentId']][$readingDataValue['sampleId']][$readingDataValue['graftingNumberId']][$currentOrder]
                            = [
                                'supportId' => $readingDataValue['supportId'],
                                'cellPosition' => $readingDataValue['cellPosition'],
                                'cResult' => $readingDataValue['cResult'],
                                'qResult' => $readingDataValue['qResult'],
                                'readingDataTypeId' => $readingDataValue['readingDataTypeId'],
                                'symptomIds' => $symptomList,
                                'symptomNames' => $symptomNameList,
                            ];
                    }
                } else {
                    foreach ($arrayReadingData as $readingDataKey => $readingDataValue) {
                        $currentValue = $readingDataValue['activityId'] . '_' . $readingDataValue['agentId'];
                        if ($oldValue != $currentValue) {
                            $oldValue =  $currentValue;
                            $currentOrder = 1;
                        } else {
                            $currentOrder = $currentOrder  + 1;
                        }
                        $arrayReadingData[$readingDataKey]['cellPositionUser'] = $currentOrder;
                        $arrayReadingDataIndexed[$readingDataValue['activityId']][$readingDataValue['agentId']][$readingDataValue['sampleId']][$readingDataValue['graftingNumberId']][$currentOrder]
                            = [
                                'supportId' => $readingDataValue['supportId'],
                                'cellPosition' => $readingDataValue['cellPosition'],
                                'cResult' => $readingDataValue['cResult'],
                                'qResult' => $readingDataValue['qResult'],
                                'readingDataTypeId' => $readingDataValue['readingDataTypeId'],
                                'symptomIds' => $symptomList,
                                'symptomNames' => $symptomNameList,
                            ];
                    }
                }

                //Save in sessions
                $session = Yii::$app->session;
                $sessionRowsReport = "sessionRowsReport" . "_" . $type . "_" . $requestId . "_" . $numOrderId . "_" . $essayId;
                $sessionColumnsReportByActivity = "sessionColumnsReportByActivity" . "_" . $type .  "_" . $requestId . "_" . $numOrderId . "_" . $essayId;

                if (isset($session[$sessionRowsReport])) {
                    $arrayRowsReport =  $session[$sessionRowsReport];
                } else {
                    $arrayRowsReport = $this->getRowsReport($arrayReadingData, $requestId, $numOrderId, $essayId);
                    $session[$sessionRowsReport] =  $arrayRowsReport;
                }

                if (isset($session[$sessionColumnsReportByActivity])) {
                    $arrayColumnsReportByActivity =   $session[$sessionColumnsReportByActivity];
                } else {
                    $arrayColumnsReportByActivity = $this->getColumnsReportByActivity($requestId, $numOrderId, $essayId);
                    $session[$sessionColumnsReportByActivity] =   $arrayColumnsReportByActivity;
                }

                //Set result filters headers in report columns passport
                if (!is_null($passportHeader)) {
                    foreach ($passportHeader as $keyPassport => $valuePassport) {
                        if ($valuePassport == "checked") {
                            $report['columns'][] = [
                                'attribute'     => $keyPassport,
                                'enableSorting' => false,
                                'format' => 'raw',
                            ];
                        }
                    }
                }

                //Set result filters headers in report columns activities & agents
                if ($type == "advance") {
                    if (!is_null($activityHeader) and !is_null($agentHeader)) {
                        foreach ($arrayColumnsReportByActivity as $keyColumn => $valueColumn) {
                            if (
                                $activityHeader[$valueColumn['activityId']]   == "checked" and (is_null($valueColumn['agentId']) or $agentHeader[$valueColumn['agentId']]  == "checked")
                            ) {
                                $report['columns'][] =
                                    [
                                        'attribute'     => $valueColumn['headerValue'],
                                        'enableSorting' => false,
                                        'format' => 'raw',
                                    ];
                            }
                        }
                    }
                } else if ($type == "resume") {
                    if (!is_null($activityHeader) and !is_null($agentHeader)) {
                        foreach ($arrayColumnsReportByActivity as $keyColumn => $valueColumn) {
                            if (is_null($valueColumn['agentId'])) {
                                $report['columns'][] =
                                    [
                                        'attribute'     => $valueColumn['headerValue'],
                                        'enableSorting' => false,
                                        'format' => 'raw',
                                    ];
                            }
                        }
                    }
                }



                //Set results in report
                foreach ($arrayRowsReport as $keyRow => $valueRow) {
                    $lastClass9 = null;
                    $lastValueResult9 = [];

                    $lastClass47 = null;
                    $lastActivity47 = null;
                    $lastTitle47 = [];
                    $lastValueResult47 = [];

                    $lastClass48 = null;
                    $lastActivity48 = null;
                    $lastTitle48 = [];
                    $lastValueResult48 = [];

                    $lastClass49 = null;
                    $lastActivity49 = null;
                    $lastTitle49 = [];
                    $lastValueResult49 = [];

                    $lastClass50 = null;
                    $lastActivity50 = null;
                    $lastTitle50 = [];
                    $lastValueResult50 = [];

                    $lastClass51 = null;
                    $lastActivity51 = null;
                    $lastTitle51 = [];
                    $lastValueResult51 = [];

                    foreach ($arrayColumnsReportByActivity as $keyColumn => $valueColumn) {

                        // ROWS
                        $order = $valueRow['order'];
                        $sampleId = $valueRow['sampleId'];
                        $readingDataTypeId =  $valueRow['readingDataTypeId'];
                        // COLUMNS
                        $activityId = $valueColumn['activityId'];
                        $agentId = $valueColumn['agentId'];
                        $headerValue = $valueColumn['headerValue'];
                        $resultType = $valueColumn['resultType'];
                        $essayType = $valueColumn['essayType'];
                        $absorbance = $valueColumn['absorbance'];
                        $calculate = $valueColumn['calculate'];
                        $format = $valueColumn['format'];
                        $activityDataSource = $valueColumn['activityDataSource'];
                        //---------

                        $qualitativeResult = null;
                        $quantitativeResult = null;
                        $arrayRowsReport[$keyRow][$headerValue]  = "<span></span>";

                        if (isset($arrayReadingDataIndexed[$activityId][$agentId][$sampleId][0][$order])) {

                            $arrayReadingDataSelect =  $arrayReadingDataIndexed[$activityId][$agentId][$sampleId][0][$order];

                            $supportId = $arrayReadingDataSelect['supportId'];
                            $cellPosition = $arrayReadingDataSelect['cellPosition'];
                            $position = $supportId . "-" .  $cellPosition;
                            $qualitativeResult = $arrayReadingDataSelect['cResult'];
                            $quantitativeResult = $arrayReadingDataSelect['qResult'];
                            $class = $qualitativeResult;

                            if ($essayType == 'RMUL') { // SYMPTOMS
                                $arrayResult = count($arrayReadingDataSelect['symptomIds']) > 0 ? $arrayReadingDataSelect['symptomIds'] : ['NA'];
                                $valueResult = implode(", ", $arrayResult);
                                $symptomNames = count($arrayReadingDataSelect['symptomNames'])  > 0 ? $arrayReadingDataSelect['symptomNames'] : ['NA'];
                                $title =  implode(", ", $symptomNames);
                                $class = 'positive';
                            } else if ($essayType == 'RSGL' &&  $resultType == 'qualitative') { //LABORATORY
                                $valueResult = strtoupper(substr($qualitativeResult, 0, 3));
                                $title = "The result in the reading " . $headerValue . " is " . $qualitativeResult . "\n Support: " . $supportId . "\n Cell Position: " . $cellPosition;
                            } else if ($essayType == 'RSGL' &&  $resultType == 'quantitative') { // ABSORBANCES
                                $valueResult =  number_format($quantitativeResult, 3, '.', '');
                                $title = "The result in the absorbance " . $headerValue . " is " . $valueResult;
                                $class = $class . "-light";
                            }

                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span style='margin-right:5px; font-size:10px;'>"
                                . $position  . "</span><span class='label label-"
                                . $class . "' title=' " . $title . "'>" . $valueResult . "</span>";

                            $agentModel = Agent::findOne($agentId);

                            //activity 9
                            $lastClass9 = $class;
                            $lastValueResult9[] = substr($agentModel->shortName, 0, 2) . '(' . $valueResult . ')';

                            //activity47
                            if ($agentModel->agentType->id == 144) {
                                $lastClass47 = $class;
                                $lastActivity47 = Activity::findOne($activityId)->shortName;
                                $lastTitle47[$agentModel->id] = $agentModel->shortName . ' (' . $title . ')';
                                $lastValueResult47[$agentModel->id] = substr($agentModel->shortName, 0, 2) . '(' . $valueResult . ')';
                            }

                            //activity48
                            if ($agentModel->agentType->id == 145) {
                                $lastClass48 = $class;
                                $lastActivity48 = Activity::findOne($activityId)->shortName;
                                $lastTitle48[$agentModel->id] = $agentModel->shortName . ' (' . $title . ')';
                                $lastValueResult48[$agentModel->id] = substr($agentModel->shortName, 0, 2) . '(' . $valueResult . ')';
                            }

                            //activity49
                            if ($agentModel->agentType->id == 146) {
                                $lastClass49 = $class;
                                $lastActivity49 = Activity::findOne($activityId)->shortName;
                                $lastTitle49[$agentModel->id] = $agentModel->shortName . ' (' . $title . ')';
                                $lastValueResult49[$agentModel->id] = substr($agentModel->shortName, 0, 2) . '(' . $valueResult . ')';
                            }

                            //activity50
                            if ($agentModel->agentType->id == 147) {
                                $lastClass50 = $class;
                                $lastActivity50 = Activity::findOne($activityId)->shortName;
                                $lastTitle50[$agentModel->id] = $agentModel->shortName . ' (' . $title . ')';
                                $lastValueResult50[$agentModel->id] = substr($agentModel->shortName, 0, 2) . '(' . $valueResult . ')';
                            }

                            //activity51
                            if ($agentModel->agentType->id == 148) {
                                $lastClass51 = $class;
                                $lastActivity51 = Activity::findOne($activityId)->shortName;
                                $lastTitle51[$agentModel->id] = $agentModel->shortName . ' (' . $title . ')';
                                $lastValueResult51[$agentModel->id] = substr($agentModel->shortName, 0, 7) . '(' . $valueResult . ')';
                            }

                            //
                        } else if ($calculate) {

                            //VALUES 21 and 22 PENDING TO AUTOMATIC 
                            $result1 = null;
                            $result2 = null;

                            $position = null;
                            $class = null;
                            $title =  null;
                            $valueResult =   null;

                            if (isset($arrayReadingDataIndexed[21][$agentId][$sampleId][0][$order])) {

                                $results1 =  $arrayReadingDataIndexed[21][$agentId][$sampleId][0][$order];
                                $result1 = $results1['cResult'];
                                $supportId = $results1['supportId'];
                                $cellPosition = $results1['cellPosition'];
                                $position =  $supportId . "-" .   $cellPosition;
                                // $position =  "";

                                if (isset($arrayReadingDataIndexed[22][$agentId][$sampleId][0][$order]['cResult'])) {
                                    $result2 =  $arrayReadingDataIndexed[22][$agentId][$sampleId][0][$order]['cResult'];

                                    if ($result1 == "positive" and $result2 == "positive") {
                                        $valueResult = "positive";
                                    } else if ($result1 == "positive" and $result2 == "negative") {
                                        $valueResult = "doubt";
                                    } else if ($result1 == "positive" and $result2 == "doubt") {
                                        $valueResult = "doubt";
                                    } else if ($result1 == "negative" and $result2 == "positive") {
                                        $valueResult = "positive";
                                    } else if ($result1 == "negative" and $result2 == "negative") {
                                        $valueResult = "negative";
                                    } else if ($result1 == "negative" and $result2 == "doubt") {
                                        $valueResult = "doubt";
                                    } else if ($result1 == "doubt" and $result2 == "positive") {
                                        $valueResult = "positive";
                                    } else if ($result1 == "doubt" and $result2 == "negative") {
                                        $valueResult = "doubt";
                                    } else if ($result1 == "doubt" and $result2 == "doubt") {
                                        $valueResult = "doubt";
                                    } else {
                                        $valueResult = $result1;
                                    }
                                } else {
                                    $valueResult = $result1;
                                }
                                $class = $valueResult;
                                $title = "The result in the reading " . $headerValue . " is " . $valueResult . "\n Support: " . $supportId . "\n Cell Position: " . $cellPosition;
                                $valueResult =  strtoupper(substr($valueResult, 0, 3));
                            }

                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span style='margin-right:5px; font-size:10px;'>" . $position  . "</span><span class='label label-" . $class . "' title=' " . $title . "'>" . $valueResult . "</span>";
                        }

                        //activity 9
                        if ($activityId == 9 and ($essayId == '2' or $essayId == '3')) {
                            if (count($lastValueResult9) > 0) {
                                $arrayRowsReport[$keyRow][$headerValue]  = "<span class='label label-positive'>POS</span>";
                            } else {
                                $arrayRowsReport[$keyRow][$headerValue]  = "<span class='label label-negative'>NEG</span>";
                            }
                        }

                        //activity47
                        if ($activityId == 47) {
                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span class='label label-" . $lastClass47 . "' title=' "  . $lastActivity47 . ": " . implode("; ", $lastTitle47) . "'>"
                                . implode("; ", $lastValueResult47) . "</span>";
                        }

                        //activity48
                        if ($activityId == 48) {
                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span class='label label-" . $lastClass48 . "' title=' "  . $lastActivity48 . ": " . implode("; ", $lastTitle48) . "'>"
                                . implode("; ", $lastValueResult48) . "</span>";
                        }

                        //activity49
                        if ($activityId == 49) {
                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span class='label label-" . $lastClass49 . "' title=' "  . $lastActivity49 . ": " . implode("; ", $lastTitle49) . "'>"
                                . implode("; ", $lastValueResult49) . "</span>";
                        }

                        //activity50
                        if ($activityId == 50) {
                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span class='label label-" . $lastClass50 . "' title=' "  . $lastActivity50 . ": " . implode("; ", $lastTitle50) . "'>"
                                . implode("; ", $lastValueResult50) . "</span>";
                        }

                        //activity51
                        if ($activityId == 51) {
                            $arrayRowsReport[$keyRow][$headerValue]  =
                                "<span class='label label-" . $lastClass51 . "' title=' "  . $lastActivity51 . ": " . implode("; ", $lastTitle51) . "'>"
                                . implode("; ", $lastValueResult51) . "</span>";
                        }

                        //
                    }
                }

                $report['dataProvider'] =  $arrayRowsReport;
                return $report;
                // 

            }
        } else {
            return false;
        }
    }

    //******* UNDER CONSTRUCTION *******/

    public function actionResultByActivityResume($requestId, $numOrderId = null, $essayId = null, $passportHeader = null, $activityHeader = null, $agentHeader = null, $type)
    {
        // if (Yii::$app->user->can("request_result-by-agent")) { } else {
        //     return false;
        // }
    }

    public function actionResultByAgent($requestId, $numOrderId = null, $essayId = null, $passportHeader = null, $activityHeader = null, $agentHeader = null)
    {
        // if (Yii::$app->user->can("request_result-by-agent")) { } else {
        //     return false;
        // }
    }

    public function actionResultByAgentResume($requestId, $numOrderId = null, $essayId = null, $passportHeader = null, $activityHeader = null, $agentHeader = null)
    {
        // if (Yii::$app->user->can("request_result-by-agent-resume")) { } else {
        //     return false;
        // }
    }

    public function actionResultBySupport($requestId, $numOrderId = null, $essayId = null, $passportHeader = null, $activityHeader = null, $agentHeader = null)
    {
        // if (Yii::$app->user->can("request_result-by-support")) { } else {
        //     return false;
        // }
    }

    // ------------------------- RESULTS EXTRAS ------------------------- //

    public function actionGenerateDistribution($requestId, $numOrderId, $essayId, $activityId)
    {

        $activityOrderTemplate = ActivityByEssay::findOne([
            'activityId' => $activityId,
            'essayId' => $essayId
        ])->activityOrder - 1;
        $activityIdTemplate = ActivityByEssay::find()->where(
            [
                'essayId' => $essayId,
                'activityOrder' => $activityOrderTemplate
            ]
        )->asArray()->one()['activityId'];

        //--------------------------------------------------------------------------------------

        $arraySupportTemplate = Support::find()->where([
            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
            'essayId' => $essayId,
            'activityId' => $activityIdTemplate,
            'status' => 'active',
        ])->orderBy(
            [
                'id' => SORT_ASC,
            ]
        )->asArray()->all();

        $arrayReadingDataTemplate = ReadingData::find()->where([
            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
            'essayId' => $essayId,
            'activityId' => $activityIdTemplate,
            'status' => 'active',
        ])->orderBy(
            [
                'activityId' => SORT_ASC,
                'agentId' => SORT_ASC,
                'supportId' => SORT_ASC,
                'cellPosition' => SORT_ASC,
            ]
        )->asArray()->all();

        //--------------------------------------------------------------------------------------

        $arraySupport = Support::find()->where([
            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
            'essayId' => $essayId,
            'activityId' => $activityId,
            'status' => 'active',
        ])->orderBy(
            [
                'id' => SORT_ASC,
            ]
        )->asArray()->all();

        $arrayReadingData = ReadingData::find()->where([
            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
            'essayId' => $essayId,
            'activityId' => $activityId,
            'status' => 'active',
        ])->orderBy(
            [
                'activityId' => SORT_ASC,
                'agentId' => SORT_ASC,
                'supportId' => SORT_ASC,
                'cellPosition' => SORT_ASC,
            ]
        )->asArray()->all();

        //--------------------------------------------------------------------------------------

        if (!empty($arraySupportTemplate) and !empty($arrayReadingDataTemplate)) {
            if (empty($arraySupport) and empty($arrayReadingData)) {
                ini_set('memory_limit', '1024M');
                ini_set('max_execution_time', 300);
                try {
                    foreach ($arraySupportTemplate as $key => $value) {
                        $modelSupport = new Support();
                        $modelSupport->attributes = $value;
                        $modelSupport->activityId = $activityId;
                        $modelSupport->save();
                        unset($modelSupport);
                    }
                } catch (\Throwable $th) { }
                try {
                    foreach ($arrayReadingDataTemplate as $key => $value) {
                        $modelReadingData = new ReadingData();
                        $modelReadingData->attributes = $value;
                        $modelReadingData->activityId = $activityId;
                        $modelReadingData->cResult = "empty";
                        $modelReadingData->qResult = 0.000;
                        $modelReadingData->result = 'quantitative';
                        $modelReadingData->graftingNumberId = 0; // 0 is the first grafting
                        $modelReadingData->save();
                        unset($modelReadingData);
                    }
                } catch (\Throwable $th) { }
            }
        }

        $data = null;
        if (!empty($arraySupport) and !empty($arrayReadingData)) {
            $data =  ArrayHelper::map($arrayReadingData, 'supportId', 'cResult');
            ksort($data);
        }

        return $data;
    }

    public function getRowsReport($arrayReadingData, $requestId = null, $numOrderId = null, $essayId   = null)
    {

        if ($essayId == 2 or $essayId  == 3) {

            $arraySamples = Sample::find()
                ->where(
                    [
                        'requestId' => $requestId
                    ]
                )->asArray()
                ->all();

            foreach ($arraySamples as $key => $sampleValue) {

                $sampleId = $sampleValue['id'];

                $arrayReadingDataFilter1Aux = array_filter($arrayReadingData, function ($element) use ($sampleId) {
                    return ($element['sampleId'] == $sampleId);
                });

                if (empty($arrayReadingDataFilter1Aux)) {

                    $arrayReadingDataFilter2[] = [
                        'sampleId' => $sampleValue['id'],
                        'readingDataTypeId' => '13',
                        'observation' => '',
                        'cellPosition' => '1',
                        'essayId' => $essayId,
                        'graftingNumberId' => '0',
                        'numOrderId' => $numOrderId,
                        'requestId' => $requestId,
                        'supportId' => '1',
                    ];
                } else {

                    $arrayReadingDataFilter1 = array_values($arrayReadingDataFilter1Aux)[0];

                    $arrayReadingDataFilter2[] = [
                        'sampleId' => $arrayReadingDataFilter1['sampleId'],
                        'readingDataTypeId' => $arrayReadingDataFilter1['readingDataTypeId'],
                        'observation' => $arrayReadingDataFilter1['observation'],
                        'cellPosition' => $arrayReadingDataFilter1['cellPosition'],
                        'essayId' => $arrayReadingDataFilter1['essayId'],
                        'graftingNumberId' => $arrayReadingDataFilter1['graftingNumberId'],
                        'numOrderId' => $arrayReadingDataFilter1['numOrderId'],
                        'requestId' => $arrayReadingDataFilter1['requestId'],
                        'supportId' => $arrayReadingDataFilter1['supportId'],
                    ];
                }
            }
        } else {
            $arrayAgents = ArrayHelper::map($arrayReadingData, 'agentId', 'agentId');
            $arrayActivities = ArrayHelper::map($arrayReadingData, 'activityId', 'activityId');

            if (empty($arrayAgents) or empty($arrayActivities)) {
                $arraySamples = Sample::find()
                    ->where(
                        [
                            'requestId' => $requestId
                        ]
                    )->asArray()
                    ->all();

                foreach ($arraySamples as $key => $sampleValue) {
                    $arrayReadingDataFilter2[] = [
                        'sampleId' => $sampleValue['id'],
                        'readingDataTypeId' => '13',
                        'observation' => '',
                        'cellPosition' => '1',
                        'essayId' => $essayId,
                        'graftingNumberId' => '0',
                        'numOrderId' => $numOrderId,
                        'requestId' => $requestId,
                        'supportId' => '1',
                    ];
                }
            } else {
                $agent = array_values($arrayAgents)[0];
                $activity = array_values($arrayActivities)[0];

                $arrayReadingDataFilter1 = array_filter($arrayReadingData, function ($element) use ($activity) {
                    return ($element['activityId'] == $activity);
                });
                $arrayReadingDataFilter2 = array_filter($arrayReadingDataFilter1, function ($element) use ($agent) {
                    return ($element['agentId'] == $agent);
                });
            }
        }

        $order = 0;

        $oldValue = null;
        foreach ($arrayReadingDataFilter2 as $key => $value) {
            $currentValue = $value['supportId'] . '_' . $value['cellPosition'] . '_' . $value['sampleId'] . '_' . $value['graftingNumberId'];

            // supportId
            // cellPosition
            // sampleId
            // graftingNumberId for the moment this value is zero

            if ($oldValue != $currentValue) {
                $oldValue = $currentValue;
                //----PART 1 ----//

                $sampleModel = Sample::findOne($value['sampleId']);

                try {
                    $crop =  $sampleModel->crop->longName;
                } catch (\Throwable $th) {
                    $crop = "";
                }
                try {
                    $sample_type =   $sampleModel->sampleType->shortName;
                } catch (\Throwable $th) {
                    $sample_type = "";
                }
                try {
                    $reading_type =   Parameter::findOne($value['readingDataTypeId'])->shortName;
                } catch (\Throwable $th) {
                    $reading_type = "";
                }

                //----PART 2 ----//

                if ($reading_type == 'RESULT') {
                    $order = $order  + 1;
                    $accessionCode = $sampleModel->accessionCode;
                    $numOrder = $sampleModel->numOrder;
                } else {
                    $order = $order  + 1;
                    $accessionCode = $reading_type;
                    $numOrder = "";
                }

                //----PART 3 ----//

                if (empty($value['observation'])) {
                    $observation = '<a id="test" title="Edit observation"href=index.php?r=business/request/observation-edit&'
                        . 'requestId=' . $value['requestId']
                        . '&numOrderId=' . $value['numOrderId']
                        . '&essayId=' . $value['essayId']
                        . '&sampleId=' . $value['sampleId']
                        . '>...<i class="fa fa-fw fa-edit"></i></a>';
                } else {
                    $observation = '<a id="test" title="Edit observation"href=index.php?r=business/request/observation-edit&'
                        . 'requestId=' . $value['requestId']
                        . '&numOrderId=' . $value['numOrderId']
                        . '&essayId=' . $value['essayId']
                        . '&sampleId=' . $value['sampleId'] . '>'
                        . $value['observation'] . '...<i class="fa fa-fw fa-edit"></i></a>';
                }

                //----PART 4 ----//

                $arrayReadingDataFilter2[$key] =
                    [
                        'order' => $order,
                        'numOrder' => $numOrder,
                        'sampleId' => $sampleModel->id,
                        'readingDataTypeId' => $value['readingDataTypeId'],
                        'accession_number' =>  $accessionCode,
                        'other_code_1' =>  $sampleModel->firstUserCode,
                        'other_code_2' =>  $sampleModel->secondUserCode,
                        'collecting_number' =>  $sampleModel->collectingCode,
                        'accession_name' =>  $sampleModel->accessionName,
                        'female_parent' =>  $sampleModel->female,
                        'male_parent' =>  $sampleModel->male,
                        'is_bulk' =>  $sampleModel->isBulk,
                        'crop' =>   $crop,
                        'sample_type' => $sample_type,
                        'reading_type' => $reading_type,
                        'observation' =>   $observation,
                    ];
            }
        }

        $arrayReadingDataFilter2 =   ArrayHelper::index($arrayReadingDataFilter2, 'order');

        return $arrayReadingDataFilter2;
    }

    public function actionObservationEdit($requestId = null, $numOrderId = null, $essayId = null, $sampleId = null)
    {
        $readingDataArrayList = ReadingData::find()->where(
            [
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'sampleId' => $sampleId,
            ]
        )->asArray()->all();

        $requestProcessDetailArrayList = RequestProcessDetail::find()->where(
            [
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
            ]
        )->asArray()->all();

        $agentByRequestProcessDetailArrayList =  AgentByRequestProcessDetail::find()->where(
            [
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
            ]
        )->asArray()->all();

        $observation = ArrayHelper::map($readingDataArrayList, 'requestId', 'observation')[$requestId];

        return $this->render('observation-edit', [
            'readingDataArrayList' => $readingDataArrayList,
            'requestProcessDetailArrayList' => $requestProcessDetailArrayList,
            'agentByRequestProcessDetailArrayList' => $agentByRequestProcessDetailArrayList,

            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
            'essayId' => $essayId,
            'sampleId' => $sampleId,

            'observation' => $observation
        ]);
    }

    public function actionUpdateReadingData(
        $requestId = null,
        $numOrderId = null,
        $essayId = null,
        $sampleId = null
    ) {
        // --------------------------------------------------------------
        $data = Yii::$app->request->post();
        $observation = Yii::$app->request->post('observation');
        $readingDataScript = ReadingData::find()
            ->where(
                [
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'essayId' => $essayId,
                    'sampleId' => $sampleId
                ]
            );
        $workFlowId = $readingDataScript->one()->workFlowId;
        $cropId = $readingDataScript->one()->cropId;
        $readingDataTypeId = '13';
        // --------------------------------------------------------------
        // save qualitative | quantitative | observation BY READING-DATA
        foreach ($readingDataScript->all() as $key => $readingDataModel) {
            $activityDataSourceCode = $readingDataModel->activity->activityDataSource->code;
            $activityTypeName = $readingDataModel->activity->activityType->shortName;
            $requestId = $readingDataModel->requestId;
            $numOrderId = $readingDataModel->numOrderId;
            $essayId = $readingDataModel->essayId;
            $sampleId = $readingDataModel->sampleId;
            $activityId = $readingDataModel->activityId;
            $agentId = $readingDataModel->agentId;
            $graftingNumberId = $readingDataModel->graftingNumberId;
            $symptomId = $readingDataModel->symptomId;
            $cellPosition = $readingDataModel->cellPosition;
            $supportId = $readingDataModel->supportId;
            $index = $activityDataSourceCode . '|' . $activityTypeName
                . '_' . $requestId
                . '_' . $numOrderId
                . '_' . $essayId
                . '_' . $sampleId
                . '_' . $activityId
                . '_' . $agentId
                . '_' . $graftingNumberId
                . '_' . $symptomId
                . '_' . $cellPosition
                . '_' . $supportId;

            if (isset($data[$index])) {
                if ($activityTypeName === 'qualitative') {
                    $readingDataModel->cResult   =  $data[$index];
                } else if ($activityTypeName === 'quantitative') {
                    $readingDataArrayControls = ReadingData::find()
                        ->where(
                            [
                                'requestId' => $requestId,
                                'numOrderId' => $numOrderId,
                                'essayId' => $essayId,
                                'activityId' => $activityId,
                                'agentId' => $agentId,
                                'supportId' => $supportId,
                                'readingDataTypeId' =>  '11'
                            ]
                        )->asArray()->all();
                    $arrayCellPositionValue = ArrayHelper::map($readingDataArrayControls, 'cellPosition', 'qResult');
                    $arraySanePositions = [];
                    $cellControls = count($arrayCellPositionValue); // get cell control count by agent (3, 6, 8)
                    foreach ($arrayCellPositionValue as $cellPosition => $cellPositionResult) {
                        $numOrderControlByAgent = $cellPosition -  8 * intdiv($cellPosition, 8);
                        if ($cellControls == 3 and $numOrderControlByAgent == 7) {
                            $arraySanePositions[$cellPosition] = $cellPositionResult; // if only 3 controls cells by agent in one support -> the 7th position in a column on support is a sane control
                        } else if ($numOrderControlByAgent  == 5 or $numOrderControlByAgent == 6) {
                            $arraySanePositions[$cellPosition] = $cellPositionResult; // else the 5th and 6th positions in a column on support are the sane controls
                        }
                    }
                    $saneResult  = array_sum($arraySanePositions) / count($arraySanePositions);
                    // Rules from absorbances
                    // Valor	Range 
                    // -	abs > 1.5x
                    // ? 	1.5x <= abs < 2x
                    // +	abs >= 2x
                    // X: average sane control
                    $readingDataModel->qResult     =  $data[$index];
                    if ($readingDataModel->qResult < 1.5 * $saneResult) {
                        $readingDataModel->cResult = "negative";
                    } else if ($readingDataModel->qResult < 2 * $saneResult) {
                        $readingDataModel->cResult = "doubt";
                    } else {
                        $readingDataModel->cResult = "positive";
                    }
                }
            }
            $readingDataModel->observation = $observation;
            $readingDataModel->save();
        }

        // filter (web) in DATA-POST; get POSITIONS
        $auxPosArray = [];
        foreach ($data as $key => $value) {
            if (substr($key, 0, 3) == 'WEB') {
                $auxPos = explode("_", $key)[9];
                if (!in_array($auxPos, $auxPosArray)) {
                    array_push($auxPosArray, $auxPos);
                }
            }
        }

        // filter (positions, calculates) in DATA-POST; set calculate results 
        foreach ($data as $key1 => $value1) {
            if (substr($key1, 0, 3) == 'CAL') {
                foreach ($auxPosArray as $key2 => $cellPosition) {
                    $activityDataSourceCode_activityTypeName =  explode("_", $key1)[0];
                    $activityTypeName = explode("|", $activityDataSourceCode_activityTypeName)[1];
                    $requestId =  explode("_", $key1)[1];
                    $numOrderId =  explode("_", $key1)[2];
                    $essayId =  explode("_", $key1)[3];
                    $sampleId =  explode("_", $key1)[4];
                    $activityId =  explode("_", $key1)[5];
                    $agentId =  explode("_", $key1)[6];
                    $graftingNumberId =  '0';
                    $symptomId = '1';
                    $supportId =  '1';

                    //--//
                    $readingDataModelUpdate = ReadingData::findOne(
                        [
                            'requestId' => $requestId,
                            'cropId' => $cropId,
                            'workFlowId' => $workFlowId,
                            'essayId' => $essayId,
                            'activityId' => $activityId,
                            'agentId' => $agentId,
                            'sampleId' => $sampleId,
                            'cellPosition' => $cellPosition,
                            'supportId' => $supportId,
                            'numOrderId' => $numOrderId,
                            'graftingNumberId' => $graftingNumberId,
                            'symptomId' => $symptomId
                        ]
                    );
                    if (($readingDataModelUpdate) !== null) {
                        $readingDataModelUpdate->observation = $observation;
                        $readingDataModelUpdate->cResult = $value1;
                        $readingDataModelUpdate->save();
                    } else {
                        $readingDataModelNew = new ReadingData();
                        $readingDataModelNew->requestId = $requestId;
                        $readingDataModelNew->cropId = $cropId;
                        $readingDataModelNew->workFlowId = $workFlowId;
                        $readingDataModelNew->essayId = $essayId;
                        $readingDataModelNew->activityId = $activityId;
                        $readingDataModelNew->agentId = $agentId;
                        $readingDataModelNew->sampleId = $sampleId;
                        $readingDataModelNew->cellPosition = $cellPosition;
                        $readingDataModelNew->supportId = $supportId;
                        $readingDataModelNew->numOrderId = $numOrderId;
                        $readingDataModelNew->graftingNumberId = $graftingNumberId;
                        $readingDataModelNew->symptomId = $symptomId;
                        $readingDataModelNew->result = $activityTypeName;
                        $readingDataModelNew->cResult = $value1;
                        $readingDataModelNew->readingDataTypeId = $readingDataTypeId;
                        $readingDataModelNew->observation = $observation;
                        $readingDataModelNew->save();
                    }
                    //--//
                }
            }
        }

        return $this->redirect(
            [
                'manage-request',
                'requestId' => $requestId,
            ]
        );
    }

    public function getColumnsReportByActivity($requestId, $numOrderId, $essayId)
    {
        $arrayActivityByEssay = ActivityByEssay::find()->where([
            'ActivityByEssay.essayId' => $essayId,
            'status' => 'active',
        ])->orderBy('activityOrder')->asArray()->all();

        $arrayAgentByEssay = AgentByRequestProcessDetail::find()->where([
            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
            'essayId' => $essayId,
            'status' => 'active',
        ])->asArray()->all();

        foreach ($arrayActivityByEssay as $arrayActivityByEssayKey => $arrayActivityByEssayValue) {

            $modelActivity = Activity::findOne($arrayActivityByEssayValue['activityId']);
            $activityType = Parameter::findOne($modelActivity->activityTypeId)->code;
            $activityDataSource = Parameter::findOne($modelActivity->activityDataSourceId)->code;
            $activityProperty = Parameter::findOne($modelActivity->activityPropertyId)->code;
            $essayType = Essay::findOne($arrayActivityByEssayValue['essayId'])->essayType->code;

            // id |code|shortName                          
            // ---|----|-----------------------------------
            // 95 |RMUL|Multiple Results Per Reading (RMUL)
            // 127|RSGL|Single Results Per Reading (RSGL)  

            $absorbance = false;
            $calculate = false;

            if ($modelActivity->activityType->code == 'quantitative' and $modelActivity->activityDataSource->code == 'WEB')
                $absorbance = true;

            if ($modelActivity->activityDataSource->code == 'CAL')
                $calculate = true;

            foreach ($arrayAgentByEssay as $arrayAgentByEssayKey => $arrayAgentByEssayValue) {

                if ($activityProperty == 'ACTPA') {

                    $pos = strpos(strtolower($modelActivity->shortName), "agent");
                    if ($pos === false) {
                        $headerValue = Agent::findOne($arrayAgentByEssayValue['agentId'])->shortName . ' ' . strtolower($modelActivity->shortName);
                    } else {
                        $headerValue = str_replace("agent", Agent::findOne($arrayAgentByEssayValue['agentId'])->shortName, strtolower($modelActivity->shortName));
                    }
                    $arrayColumnsReportByActivity[] = [
                        'agentId' => $arrayAgentByEssayValue['agentId'],
                        'activityId' => $arrayActivityByEssayValue['activityId'],
                        'resultType' => $activityType,
                        'essayType' => $essayType,
                        'activityDataSource' => $activityDataSource,
                        'format' => true,
                        'headerValue' =>   $headerValue,
                        'absorbance' => $absorbance,
                        'calculate' => $calculate,
                    ];
                } else if ($activityProperty == 'ASEND' and $arrayAgentByEssayKey  == count($arrayAgentByEssay) - 1) {

                    $headerValue = $modelActivity->shortName;
                    $arrayColumnsReportByActivity[] = [
                        'agentId' => null,
                        'activityId' => $arrayActivityByEssayValue['activityId'],
                        'resultType' => $activityType,
                        'essayType' => $essayType,
                        'activityDataSource' => $activityDataSource,
                        'format' => false,
                        'headerValue' =>   $headerValue,
                        'absorbance' => $absorbance,
                        'calculate' => $calculate,
                    ];
                }
            }
        }
        return $arrayColumnsReportByActivity;
    }

    public function getColumnsReportByAgent($requestId, $numOrderId, $essayId)
    {
        // // ActivityByEssay
        // $arrayActivityByEssay = ActivityByEssay::find()->where([
        //     'ActivityByEssay.essayId' => $essayId,
        //     'status' => 'active',
        // ])->orderBy('activityOrder')->asArray()->all();
        // // AgentByRequestProcessDetail
        // $arrayAgentByEssay = AgentByRequestProcessDetail::find()->where([
        //     'requestId' => $requestId,
        //     'numOrderId' => $numOrderId,
        //     'essayId' => $essayId,
        //     'status' => 'active',
        // ])->asArray()->all();
        // foreach ($arrayAgentByEssay as $arrayAgentByEssayKey => $arrayAgentByEssayValue) {
        //     foreach ($arrayActivityByEssay as $arrayActivityByEssayKey => $arrayActivityByEssayValue) {
        //         $modelActivity = Activity::findOne($arrayActivityByEssayValue['activityId']);
        //         $activityType = Parameter::findOne($modelActivity->activityTypeId)->code;
        //         $activityDataSource = Parameter::findOne($modelActivity->activityDataSourceId)->code;
        //         $activityProperty = Parameter::findOne($modelActivity->activityPropertyId)->code;
        //         if ($activityProperty == 'ACTPA') {
        //             // Activity per sample at the beginning
        //             //by agent
        //             $headerValue = str_replace("agent", Agent::findOne($arrayAgentByEssayValue['agentId'])->shortName, strtolower($modelActivity->shortName));
        //             $arrayColumnsReportByActivity[] = [
        //                 'agentId' => $arrayAgentByEssayValue['agentId'],
        //                 'activityId' => $arrayActivityByEssayValue['activityId'],
        //                 'resultType' => $activityType,
        //                 'activityDataSource' => $activityDataSource,
        //                 'format' => true,
        //                 'headerValue' =>   $headerValue,
        //             ];
        //         } else if ($activityProperty == 'ASEND' and $arrayAgentByEssayKey  == count($arrayAgentByEssay) - 1) {
        //             // Activity by agent
        //             //by sample
        //             $headerValue = $modelActivity->shortName;
        //             $arrayColumnsReportByActivity[] = [
        //                 'agentId' => null,
        //                 'activityId' => $arrayActivityByEssayValue['activityId'],
        //                 'resultType' => $activityType,
        //                 'activityDataSource' => $activityDataSource,
        //                 'format' => false,
        //                 'headerValue' =>   $headerValue,
        //             ];
        //         }
        //     }
        // }
        // return $arrayColumnsReportByActivity;
    }

    public function actionEssaySelect($requestId, $numOrderId)
    {
        $modelRequestProcess = RequestProcess::find()->where([
            'requestId' => $requestId,
            'numOrderId' => $numOrderId,
        ])->one();
        $listModelEssaysByWorkFlow = EssayByWorkFlow::find()->where([
            'workFlowId' => $modelRequestProcess->workFlow->id,
        ])->all();
        $result = '<option value="0">Select...</option>';
        foreach ($listModelEssaysByWorkFlow as $key => $value) {
            $id =  $value->essay->id;
            $shortName = $value->essay->shortName;
            $result =  $result . "\n" .  '<option value="' . $id . '">' .  $shortName . '</option>';
        }
        return $result;
    }
}
