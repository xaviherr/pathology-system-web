<?php

namespace frontend\modules\business\controllers;

use Yii;
use frontend\modules\business\models\Support;
use frontend\modules\business\models\SupportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupportController implements the CRUD actions for Support model.
 */
class SupportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Support models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Support model.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId),
        ]);
    }

    /**
     * Creates a new Support model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Support();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Support model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        $model = $this->findModel($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Support model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        $this->findModel($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Support model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return Support the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        if (($model = Support::findOne(['id' => $id, 'requestId' => $requestId, 'cropId' => $cropId, 'workFlowId' => $workFlowId, 'numOrderId' => $numOrderId, 'activityId' => $activityId, 'essayId' => $essayId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
