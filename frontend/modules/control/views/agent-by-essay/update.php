<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\AgentByEssay */

$this->title = 'Update Agent By Essay: ' . $model->essayId;
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->essayId, 'url' => ['view', 'essayId' => $model->essayId, 'agentId' => $model->agentId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agent-by-essay-update">

    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>