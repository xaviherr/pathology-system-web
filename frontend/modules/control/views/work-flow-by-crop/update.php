<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\WorkFlowByCrop */

$this->title = 'Update Work Flow By Crop: ' . $model->cropId;
$this->params['breadcrumbs'][] = ['label' => 'Work Flow By Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cropId, 'url' => ['view', 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-flow-by-crop-update">

    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>