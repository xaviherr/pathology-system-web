<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\control\models\WorkFlowByCropSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Work Flow By Crops';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-flow-by-crop-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box collapsed-box">
        <div class="box-header">
            <button type="button" class="btn btn-primary" data-widget="collapse">
                <i class="fa fa-search"></i> Advanced Search
            </button>
        </div>
        <div class="box-body">
            <?php
            echo $this->render('_search', ['model' => $searchModel]);
            ?>
        </div>
    </div>


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-bars "></i> <?= Html::encode($this->title) ?> List</h3>
        </div>

        <div class="box-body">
            <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title"><i class="fa fa-edit"></i>Create <?php echo Html::encode($this->title) ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="modal-create-content"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title"><i class="fa fa-edit"></i>View <?php echo Html::encode($this->title) ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="modal-view-content"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title"><i class="fa fa-edit"></i>Edit <?php echo Html::encode($this->title) ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="modal-update-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-body">
            <?= GridView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'layout'       => "{items}",
                    'options'       => ['style' => 'font-size:12px;'],
                    'rowOptions' => function ($model) {
                        if ($model->status == "disabled") {
                            return ['class' => 'danger'];
                        } else if ($model->status == "active") {
                            return ['class' => 'success'];
                        }
                    },
                    'columns'       => [
                        [
                            'class' => 'yii\grid\SerialColumn'
                        ],
                        [
                            'attribute'     => 'cropId',
                            'value'         => 'crop.longName',
                            'enableSorting' => false,
                        ],
                        [
                            'attribute'     => 'workFlowId',
                            'value'         => 'workFlow.shortName',
                            'enableSorting' => false,
                        ],
                        [
                            'attribute'     => 'status',
                            'enableSorting' => false,
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header'        => 'Action',
                            'template'      => '{view} {update} {delete}',
                            'headerOptions' => ['width' => '70'],
                            'buttons'       => [
                                'view' => function ($url, $model, $key) {
                                    return Html::button(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        [
                                            'title' => Yii::t('yii', 'View'),
                                            'class' => 'btn btn-default btn-xs modal-view-action',
                                            'value' => Url::to(
                                                [
                                                    'work-flow-by-crop/view',
                                                    //'id' => $key
                                                    'cropId' => ArrayHelper::getValue($key, 'cropId'),
                                                    'workFlowId' => ArrayHelper::getValue($key, 'workFlowId'),
                                                ]
                                            ),
                                        ]
                                    );
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::button(
                                        '<span class="glyphicon glyphicon glyphicon-pencil"></span>',
                                        [
                                            'title' => Yii::t('yii', 'Update'),
                                            'class' => 'btn btn-default btn-xs modal-update-action',
                                            'value' => Url::to(
                                                [
                                                    'work-flow-by-crop/update',
                                                    //'id' => $key
                                                    'cropId' => ArrayHelper::getValue($key, 'cropId'),
                                                    'workFlowId' => ArrayHelper::getValue($key, 'workFlowId'),
                                                ]
                                            ),

                                        ]
                                    );
                                },
                            ],
                        ],
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-striped dataTable',
                        'id'    => "gvWorkFLow",
                    ],
                ]
            ); ?>
        </div>

        <div class="box-footer">
            <div class="col-md-12">
                <?= Html::button(
                    'Create ' . Html::encode($this->title),
                    [
                        'value' => Url::to(['work-flow-by-crop/create']),
                        'class' => 'btn btn-success modal-create-action',
                        // 'id' => 'modalButton'
                    ]
                )
                ?>
            </div>
        </div>
    </div>


</div>