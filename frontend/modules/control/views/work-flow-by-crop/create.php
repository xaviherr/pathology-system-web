<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\WorkFlowByCrop */

$this->title = 'Create Work Flow By Crop';
$this->params['breadcrumbs'][] = ['label' => 'Work Flow By Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-flow-by-crop-create">

    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>