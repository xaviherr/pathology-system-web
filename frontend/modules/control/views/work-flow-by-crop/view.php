<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\WorkFlowByCrop */

$this->title = $model->cropId;
$this->params['breadcrumbs'][] = ['label' => 'Work Flow By Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="work-flow-by-crop-view">

    <h1><?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <p>
        <?php
        // echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) 
        ?>
        <?php
        // echo  Html::a('Delete', ['delete', 'id' => $model->id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ])
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            // 'workFlow.shortName',
            [
                'attribute' => 'cropId',
                'value' => $model->crop->longName,
            ],
            // 'essay.shortName',
            [
                'attribute' => 'workFlowId',
                'value' => $model->workFlow->shortName,
            ],
            'status',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
        ],
    ]) ?>

</div>