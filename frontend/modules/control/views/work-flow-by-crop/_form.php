<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\WorkFlowByCrop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-flow-by-crop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'cropId')->dropDownList(
        $model->getCropArray(),
        [
            'id' => 'ddlCrop',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?php echo $form->field($model, 'workFlowId')->dropDownList(
        $model->getWorkFlowArray(),
        [
            'id' => 'ddlWorkFlow',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>