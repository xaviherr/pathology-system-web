<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="activity-by-essay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'essayId')->dropDownList(
        $model->getEssayArray(),
        [
            'id' => 'ddlEssay',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'activityId')->dropDownList(
        $model->getActivityArray(),
        [
            'id' => 'ddlAgent',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'activityOrder')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>