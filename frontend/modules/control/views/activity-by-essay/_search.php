<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\ActivityByEssaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-by-essay-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'activityId') ?>

    <?= $form->field($model, 'essayId') ?>

    <?= $form->field($model, 'status') ?>

    <? //= $form->field($model, 'dataSource') 
    ?>

    <? //= $form->field($model, 'dataType') 
    ?>

    <?= $form->field($model, 'activityOrder') ?>

    <? //= $form->field($model, 'activityTypeId') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>