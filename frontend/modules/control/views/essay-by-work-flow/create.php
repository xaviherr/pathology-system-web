<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\EssayByWorkFlow */

$this->title = 'Create Essay By Work Flow';
$this->params['breadcrumbs'][] = ['label' => 'Essay By Work Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="essay-by-work-flow-create">

    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>