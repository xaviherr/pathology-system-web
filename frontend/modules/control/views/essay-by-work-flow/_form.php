<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\EssayByWorkFlow */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="essay-by-work-flow-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'workFlowId')->dropDownList(
        $model->getWorkFlowArray(),
        [
            'id' => 'ddlWorkFlow',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?php echo $form->field($model, 'essayId')->dropDownList(
        $model->getEssayArray(),
        [
            'id' => 'ddlEssay',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>