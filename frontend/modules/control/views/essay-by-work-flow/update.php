<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\EssayByWorkFlow */

$this->title = 'Update Essay By Work Flow: ' . $model->workFlowId;
$this->params['breadcrumbs'][] = ['label' => 'Essay By Work Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->workFlowId, 'url' => ['view', 'workFlowId' => $model->workFlowId, 'essayId' => $model->essayId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="essay-by-work-flow-update">

    <h1>
        <?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>