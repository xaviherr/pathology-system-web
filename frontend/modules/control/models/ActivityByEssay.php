<?php

namespace frontend\modules\control\models;

use Yii;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Essay;
use yii\helpers\ArrayHelper;

class ActivityByEssay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ActivityByEssay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityId', 'essayId'], 'required'],
            [['activityId', 'essayId', 'activityOrder'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['activityId', 'essayId'], 'unique', 'targetAttribute' => ['activityId', 'essayId']],
            [['essayId'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essayId' => 'id']],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activityId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'activityId' => 'Activity ID',
            'essayId' => 'Essay ID',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'activityOrder' => 'Activity Order',
        ];
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essayId']);
    }

    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activityId']);
    }

    public function getRequestProcessDetails()
    {
        return $this->hasMany(RequestProcessDetail::className(), ['activityId' => 'activityId', 'essayId' => 'essayId'])->andWhere(['status' => 'active'])->andWhere(['status' => 'active']);
    }

    public function getRequests()
    {
        return $this->hasMany(RequestProcess::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId'])->viaTable('{{%RequestProcessDetail}}', ['activityId' => 'activityId', 'essayId' => 'essayId', 'status' => 'status'])->andWhere(['status' => 'active'])->andWhere(['status' => 'active']);
    }

    public function getEssayArray()
    {
        return ArrayHelper::map(Essay::find()->where(
            [
                'Essay.deletedBy' => null,
                'Essay.deletedAt' => null,
                'Essay.status' => 'active',
            ]
        )->orderBy(['shortName' => SORT_ASC])->all(), 'id', 'shortName');
    }

    public function getActivityArray()
    {
        return ArrayHelper::map(Activity::find()->where(
            [
                'Activity.deletedBy' => null,
                'Activity.deletedAt' => null,
                'Activity.status' => 'active',
            ]
        )->orderBy(['shortName' => SORT_ASC])->all(), 'id', 'shortName');
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
