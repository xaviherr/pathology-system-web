<?php

namespace frontend\modules\control\models;

use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\WorkFlow;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "WorkFlowByCrop".
 *
 * @property int $cropId
 * @property int $workFlowId
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property RequestProcess[] $requestProcesses
 * @property Request[] $requests
 * @property Crop $crop
 * @property WorkFlow $workFlow
 */
class WorkFlowByCrop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'WorkFlowByCrop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cropId', 'workFlowId'], 'required'],
            [['cropId', 'workFlowId'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['cropId', 'workFlowId'], 'unique', 'targetAttribute' => ['cropId', 'workFlowId']],
            [['cropId'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::className(), 'targetAttribute' => ['cropId' => 'id']],
            [['workFlowId'], 'exist', 'skipOnError' => true, 'targetClass' => WorkFlow::className(), 'targetAttribute' => ['workFlowId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cropId' => 'Crop',
            'workFlowId' => 'Work Flow',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::className(), ['cropId' => 'cropId', 'workFlowId' => 'workFlowId'])->andWhere(['status' => 'active'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['id' => 'requestId'])->viaTable('RequestProcess', ['cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::className(), ['id' => 'cropId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkFlow()
    {
        return $this->hasOne(WorkFlow::className(), ['id' => 'workFlowId']);
    }

    public function getWorkFlowArray()
    {
        return ArrayHelper::map(WorkFlow::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
            ]
        )->all(), 'id', 'shortName');
    }

    public function getCropArray()
    {
        return ArrayHelper::map(Crop::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
            ]
        )->all(), 'id', 'longName');
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
