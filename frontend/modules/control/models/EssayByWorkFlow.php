<?php

namespace frontend\modules\control\models;

use Yii;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Essay;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "EssayByWorkFlow".
 *
 * @property int $workFlowId
 * @property int $essayId
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property WorkFlow $workFlow
 * @property Essay $essay
 */
class EssayByWorkFlow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'EssayByWorkFlow';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['workFlowId', 'essayId', 'status'], 'required'],
            [['workFlowId', 'essayId'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['workFlowId', 'essayId'], 'unique', 'targetAttribute' => ['workFlowId', 'essayId']],
            [['workFlowId'], 'exist', 'skipOnError' => true, 'targetClass' => WorkFlow::className(), 'targetAttribute' => ['workFlowId' => 'id']],
            [['essayId'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essayId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'workFlowId' => 'Work Flow',
            'essayId' => 'Essay',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkFlow()
    {
        return $this->hasOne(WorkFlow::className(), ['id' => 'workFlowId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essayId']);
    }

    public function getWorkFlowArray()
    {
        return ArrayHelper::map(WorkFlow::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
            ]
        )->all(), 'id', 'shortName');
    }

    public function getEssayArray()
    {
        return ArrayHelper::map(Essay::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
            ]
        )->all(), 'id', 'shortName');
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
