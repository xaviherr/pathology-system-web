<?php

namespace frontend\modules\control\controllers;

use Yii;
use frontend\modules\control\models\AgentByEssay;
use frontend\modules\control\models\AgentByEssaySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AgentByEssayController extends Controller
{
    //BEHAVIORS
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {

            $searchModel = new AgentByEssaySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination = false;
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->goHome();
        }
    }

    public function actionView($essayId, $agentId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {

            return $this->renderAjax('view', [
                'model' => $this->findModel($essayId, $agentId),
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionCreate()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {

            $model = new AgentByEssay();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionUpdate($essayId, $agentId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {

            $model = $this->findModel($essayId, $agentId);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'essayId' => $model->essayId, 'agentId' => $model->agentId]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionDelete($essayId, $agentId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $this->findModel($essayId, $agentId)->delete();
            // $model = $this->findModel($essayId, $agentId);
            // $model->deletedAt = date("Y-m-d H:i:s", time());
            // $model->deletedBy = Yii::$app->user->identity->username;
            // $model->save();
            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    protected function findModel($essayId, $agentId)
    {
        if (($model = AgentByEssay::findOne(['essayId' => $essayId, 'agentId' => $agentId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
