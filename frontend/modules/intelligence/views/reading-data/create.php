<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\ReadingData */

$this->title = 'Create Reading Data';
$this->params['breadcrumbs'][] = ['label' => 'Reading Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reading-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
