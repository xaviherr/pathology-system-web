<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\intelligence\models\RequestProcessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Request Processes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-process-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <p>
        <?= Html::a('Create Request Process', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'requestId',
            'numOrderId',
            'cropId',
            'workFlowId',
            'startDate',
            //'finishDate',
            //'checkDate',
            //'essayQty',
            //'agentQty',
            //'sampleQty',
            //'sampleStatus',
            //'essayDetail:ntext',
            //'agentDetail:ntext',
            //'sampleDetail:ntext',
            //'registeredBy',
            //'registeredAt',
            //'updatedBy',
            //'updatedAt',
            //'deletedBy',
            //'deletedAt',
            //'status',
            //'subTotalCost',
            //'processStatusId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>