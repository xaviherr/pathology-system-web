<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcessSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-process-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'requestId') ?>

    <?= $form->field($model, 'numOrderId') ?>

    <?= $form->field($model, 'cropId') ?>

    <?= $form->field($model, 'workFlowId') ?>

    <?= $form->field($model, 'startDate') ?>

    <?php // echo $form->field($model, 'finishDate') ?>

    <?php // echo $form->field($model, 'checkDate') ?>

    <?php // echo $form->field($model, 'essayQty') ?>

    <?php // echo $form->field($model, 'agentQty') ?>

    <?php // echo $form->field($model, 'sampleQty') ?>

    <?php // echo $form->field($model, 'sampleStatus') ?>

    <?php // echo $form->field($model, 'essayDetail') ?>

    <?php // echo $form->field($model, 'agentDetail') ?>

    <?php // echo $form->field($model, 'sampleDetail') ?>

    <?php // echo $form->field($model, 'registeredBy') ?>

    <?php // echo $form->field($model, 'registeredAt') ?>

    <?php // echo $form->field($model, 'updatedBy') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedBy') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'subTotalCost') ?>

    <?php // echo $form->field($model, 'processStatusId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
