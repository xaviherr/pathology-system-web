<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcess */

$this->title = 'Update Request Process: ' . $model->requestId;
$this->params['breadcrumbs'][] = ['label' => 'Request Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->requestId, 'url' => ['view', 'requestId' => $model->requestId, 'numOrderId' => $model->numOrderId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="request-process-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
