<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcessDetail */

$this->title = $model->requestId;
$this->params['breadcrumbs'][] = ['label' => 'Request Process Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-process-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numOrder',
            'startDate',
            'finishDate',
            'checkDate',
            'controlArea',
            'controllerDirection',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
            'status',
            'requestId',
            'cropId',
            'workFlowId',
            'numOrderId',
            'activityId',
            'essayId',
            'processStatusDetailId',
        ],
    ]) ?>

</div>
