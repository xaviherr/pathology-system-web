<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcessDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-process-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numOrder')->textInput() ?>

    <?= $form->field($model, 'startDate')->textInput() ?>

    <?= $form->field($model, 'finishDate')->textInput() ?>

    <?= $form->field($model, 'checkDate')->textInput() ?>

    <?= $form->field($model, 'controlArea')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'controllerDirection')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredAt')->textInput() ?>

    <?= $form->field($model, 'updatedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'disabled' => 'Disabled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'requestId')->textInput() ?>

    <?= $form->field($model, 'cropId')->textInput() ?>

    <?= $form->field($model, 'workFlowId')->textInput() ?>

    <?= $form->field($model, 'numOrderId')->textInput() ?>

    <?= $form->field($model, 'activityId')->textInput() ?>

    <?= $form->field($model, 'essayId')->textInput() ?>

    <?= $form->field($model, 'processStatusDetailId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
