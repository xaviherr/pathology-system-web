<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcessDetail */

$this->title = 'Create Request Process Detail';
$this->params['breadcrumbs'][] = ['label' => 'Request Process Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-process-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
