<?php

namespace frontend\modules\intelligence\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\intelligence\models\ReadingData;

/**
 * ReadingDataSearch represents the model behind the search form of `frontend\modules\intelligence\models\ReadingData`.
 */
class ReadingDataSearch extends ReadingData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sampleId', 'supportIdUser', 'cellPositionUser', 'cellPosition', 'supportId', 'plantSectionId', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'agentId', 'graftingNumberId', 'symptomId', 'readingDataTypeId'], 'integer'],
            [['observation', 'evidence', 'result', 'cResult', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
            [['qResult'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReadingData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(
            [
                'readingDataType' => function ($query) {
                    $query->from(['A' => 'Parameter']);
                }
            ]
        );

        $query->joinWith(
            [
                'plantSection' => function ($query) {
                    $query->from(['B' => 'Parameter']);
                }
            ]
        );

        // grid filtering conditions
        $query->andFilterWhere([
            'sampleId' => $this->sampleId,
            'supportIdUser' => $this->supportIdUser,
            'cellPositionUser' => $this->cellPositionUser,
            'cellPosition' => $this->cellPosition,
            'supportId' => $this->supportId,
            'qResult' => $this->qResult,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'requestId' => $this->requestId,
            'cropId' => $this->cropId,
            'workFlowId' => $this->workFlowId,
            'numOrderId' => $this->numOrderId,
            'activityId' => $this->activityId,
            'essayId' => $this->essayId,
            'agentId' => $this->agentId,
            'graftingNumberId' => $this->graftingNumberId,
            'symptomId' => $this->symptomId,
            // 'plantSectionId' => $this->plantSectionId,
            // 'readingDataTypeId' => $this->readingDataTypeId,
        ]);

        $query->andFilterWhere(['like', 'observation', $this->observation])
            ->andFilterWhere(['like', 'evidence', $this->evidence])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'cResult', $this->cResult])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'A.shortName', $this->readingDataTypeId])
            ->andFilterWhere(['like', 'B.shortName', $this->plantSectionId]);

        return $dataProvider;
    }
}
