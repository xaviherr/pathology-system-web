<?php

namespace frontend\modules\intelligence\models;

use frontend\modules\configuration\models\Essay;
use Yii;
use frontend\modules\control\models\ActivityByEssay;
use frontend\modules\configuration\models\Parameter;

class RequestProcessDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%RequestProcessDetail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numOrder', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'processStatusDetailId'], 'integer'],
            [['startDate', 'finishDate', 'checkDate', 'registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId'], 'required'],
            [['controlArea', 'controllerDirection', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId'], 'unique', 'targetAttribute' => ['requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId']],
            [['activityId', 'essayId'], 'exist', 'skipOnError' => true, 'targetClass' => ActivityByEssay::className(), 'targetAttribute' => ['activityId' => 'activityId', 'essayId' => 'essayId']],
            [['processStatusDetailId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['processStatusDetailId' => 'id']],
            [['essayId'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essayId' => 'id']],
            // [
            //     [
            //         'requestId',
            //         'cropId',
            //         'workFlowId',
            //         'numOrderId'
            //     ],
            //     'exist',
            //     'skipOnError' => true,
            //     'targetClass' => RequestProcess::className(),
            //     'targetAttribute' =>
            //     [
            //         'requestId' => 'requestId',
            //         'cropId' => 'cropId',
            //         'workFlowId' => 'workFlowId',
            //         'numOrderId' => 'numOrderId'
            //     ]
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numOrder' => 'Num Order',
            'startDate' => 'Start Date',
            'finishDate' => 'Finish Date',
            'checkDate' => 'Check Date',
            'controlArea' => 'Control Area',
            'controllerDirection' => 'Controller Direction',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
            'activityId' => 'Activity ID',
            'essayId' => 'Essay ID',
            'processStatusDetailId' => 'Process Status Detail ID',
        ];
    }

    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId', 'activityId' => 'activityId', 'essayId' => 'essayId'])->andWhere(['status' => 'active']);
    }

    public function getActivity()
    {
        return $this->hasOne(ActivityByEssay::className(), ['activityId' => 'activityId', 'essayId' => 'essayId']);
    }

    public function getProcessStatusDetail()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'processStatusDetailId']);
    }

    public function getRequest()
    {
        return $this->hasOne(RequestProcess::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId']);
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essayId']);
    }

    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId', 'activityId' => 'activityId', 'essayId' => 'essayId'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
