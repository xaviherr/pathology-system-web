<?php

namespace frontend\modules\intelligence\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\intelligence\models\RequestProcess;

/**
 * RequestProcessSearch represents the model behind the search form of `frontend\modules\intelligence\models\RequestProcess`.
 */
class RequestProcessSearch extends RequestProcess
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requestId', 'numOrderId', 'cropId', 'workFlowId', 'essayQty', 'agentQty', 'sampleQty', 'paymentPercentage', 'processStatusId'], 'integer'],
            [['startDate', 'finishDate', 'checkDate', 'sampleStatus', 'paymentStatus', 'essayDetail', 'agentDetail', 'sampleDetail', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
            [['subTotalCost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcess::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'requestId' => $this->requestId,
            'numOrderId' => $this->numOrderId,
            'cropId' => $this->cropId,
            'workFlowId' => $this->workFlowId,
            'startDate' => $this->startDate,
            'finishDate' => $this->finishDate,
            'checkDate' => $this->checkDate,
            'essayQty' => $this->essayQty,
            'agentQty' => $this->agentQty,
            'sampleQty' => $this->sampleQty,
            'paymentPercentage' => $this->paymentPercentage,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'subTotalCost' => $this->subTotalCost,
            'processStatusId' => $this->processStatusId,
        ]);

        $query->andFilterWhere(['like', 'sampleStatus', $this->sampleStatus])
            ->andFilterWhere(['like', 'paymentStatus', $this->paymentStatus])
            ->andFilterWhere(['like', 'essayDetail', $this->essayDetail])
            ->andFilterWhere(['like', 'agentDetail', $this->agentDetail])
            ->andFilterWhere(['like', 'sampleDetail', $this->sampleDetail])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
