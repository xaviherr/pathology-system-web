<?php

namespace frontend\modules\intelligence\models;

use Yii;

use frontend\modules\configuration\models\Activity;
use frontend\modules\control\models\AgentByEssay;

/**
 * This is the model class for table "{{%AgentByRequestProcessDetail}}".
 *
 * @property int $essayId
 * @property int $agentId
 * @property int $requestId
 * @property int $cropId
 * @property int $workFlowId
 * @property int $numOrderId
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $numOrder
 * @property int $intPosition
 * @property int $endPosition
 * @property int $initContent
 * @property int $endContent
 * @property int $activityId
 *
 * @property Activity $activity
 * @property AgentByEssay $essay
 * @property RequestProcess $request
 */
class AgentByRequestProcessDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%AgentByRequestProcessDetail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['essayId', 'agentId', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'required'],
            [['essayId', 'agentId', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'numOrder', 'intPosition', 'endPosition', 'initContent', 'endContent', 'activityId'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['essayId', 'agentId', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'unique', 'targetAttribute' => ['essayId', 'agentId', 'requestId', 'cropId', 'workFlowId', 'numOrderId']],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activityId' => 'id']],
            [['essayId', 'agentId'], 'exist', 'skipOnError' => true, 'targetClass' => AgentByEssay::className(), 'targetAttribute' => ['essayId' => 'essayId', 'agentId' => 'agentId']],
            // [
            //     [
            //         'requestId',
            //         'cropId',
            //         'workFlowId',
            //         'numOrderId'
            //     ],
            //     'exist',
            //     'skipOnError' => true,
            //     'targetClass' => RequestProcess::className(),
            //     'targetAttribute' => [
            //         'requestId' => 'requestId',
            //         'cropId' => 'cropId', 'workFlowId' => 'workFlowId',
            //         'numOrderId' => 'numOrderId'
            //     ]
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'essayId' => 'Essay ID',
            'agentId' => 'Agent ID',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'numOrder' => 'Num Order',
            'intPosition' => 'Int Position',
            'endPosition' => 'End Position',
            'initContent' => 'Init Content',
            'endContent' => 'End Content',
            'activityId' => 'Activity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(AgentByEssay::className(), ['essayId' => 'essayId', 'agentId' => 'agentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(RequestProcess::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId']);
    }
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
