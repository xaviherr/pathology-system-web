<?php

namespace frontend\modules\intelligence\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\intelligence\models\RequestProcessDetail;

/**
 * RequestProcessDetailSearch represents the model behind the search form of `frontend\modules\intelligence\models\RequestProcessDetail`.
 */
class RequestProcessDetailSearch extends RequestProcessDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numOrder', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'processStatusDetailId'], 'integer'],
            [['startDate', 'finishDate', 'checkDate', 'controlArea', 'controllerDirection', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcessDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'numOrder' => $this->numOrder,
            'startDate' => $this->startDate,
            'finishDate' => $this->finishDate,
            'checkDate' => $this->checkDate,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'requestId' => $this->requestId,
            'cropId' => $this->cropId,
            'workFlowId' => $this->workFlowId,
            'numOrderId' => $this->numOrderId,
            'activityId' => $this->activityId,
            'essayId' => $this->essayId,
            'processStatusDetailId' => $this->processStatusDetailId,
        ]);

        $query->andFilterWhere(['like', 'controlArea', $this->controlArea])
            ->andFilterWhere(['like', 'controllerDirection', $this->controllerDirection])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
