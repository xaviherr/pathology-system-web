<?php

namespace frontend\modules\intelligence\controllers;

use Yii;
use frontend\modules\intelligence\models\ReadingData;
use frontend\modules\intelligence\models\ReadingDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReadingDataController implements the CRUD actions for ReadingData model.
 */
class ReadingDataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReadingData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReadingDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReadingData model.
     * @param integer $sampleId
     * @param integer $cellPosition
     * @param integer $supportId
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @param integer $agentId
     * @param integer $graftingNumberId
     * @param integer $symptomId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId)
    {
        return $this->render('view', [
            'model' => $this->findModel($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId),
        ]);
    }

    /**
     * Creates a new ReadingData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReadingData();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'sampleId' => $model->sampleId, 'cellPosition' => $model->cellPosition, 'supportId' => $model->supportId, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId, 'agentId' => $model->agentId, 'graftingNumberId' => $model->graftingNumberId, 'symptomId' => $model->symptomId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ReadingData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $sampleId
     * @param integer $cellPosition
     * @param integer $supportId
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @param integer $agentId
     * @param integer $graftingNumberId
     * @param integer $symptomId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId)
    {
        $model = $this->findModel($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'sampleId' => $model->sampleId, 'cellPosition' => $model->cellPosition, 'supportId' => $model->supportId, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId, 'agentId' => $model->agentId, 'graftingNumberId' => $model->graftingNumberId, 'symptomId' => $model->symptomId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ReadingData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $sampleId
     * @param integer $cellPosition
     * @param integer $supportId
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @param integer $agentId
     * @param integer $graftingNumberId
     * @param integer $symptomId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId)
    {
        $this->findModel($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReadingData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $sampleId
     * @param integer $cellPosition
     * @param integer $supportId
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @param integer $agentId
     * @param integer $graftingNumberId
     * @param integer $symptomId
     * @return ReadingData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($sampleId, $cellPosition, $supportId, $requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId, $agentId, $graftingNumberId, $symptomId)
    {
        if (($model = ReadingData::findOne(['sampleId' => $sampleId, 'cellPosition' => $cellPosition, 'supportId' => $supportId, 'requestId' => $requestId, 'cropId' => $cropId, 'workFlowId' => $workFlowId, 'numOrderId' => $numOrderId, 'activityId' => $activityId, 'essayId' => $essayId, 'agentId' => $agentId, 'graftingNumberId' => $graftingNumberId, 'symptomId' => $symptomId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
