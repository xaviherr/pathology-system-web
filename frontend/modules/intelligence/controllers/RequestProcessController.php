<?php

namespace frontend\modules\intelligence\controllers;

use Yii;
use frontend\modules\intelligence\models\RequestProcess;
use frontend\modules\intelligence\models\RequestProcessSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestProcessController implements the CRUD actions for RequestProcess model.
 */
class RequestProcessController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestProcess models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestProcessSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RequestProcess model.
     * @param integer $requestId
     * @param integer $numOrderId
     * @param integer $cropId
     * @param integer $workFlowId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($requestId, $numOrderId, $cropId, $workFlowId)
    {
        return $this->render('view', [
            'model' => $this->findModel($requestId, $numOrderId, $cropId, $workFlowId),
        ]);
    }

    /**
     * Creates a new RequestProcess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequestProcess();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'requestId' => $model->requestId, 'numOrderId' => $model->numOrderId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RequestProcess model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $requestId
     * @param integer $numOrderId
     * @param integer $cropId
     * @param integer $workFlowId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($requestId, $numOrderId, $cropId, $workFlowId)
    {
        $model = $this->findModel($requestId, $numOrderId, $cropId, $workFlowId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'requestId' => $model->requestId, 'numOrderId' => $model->numOrderId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RequestProcess model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $requestId
     * @param integer $numOrderId
     * @param integer $cropId
     * @param integer $workFlowId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($requestId, $numOrderId, $cropId, $workFlowId)
    {
        $this->findModel($requestId, $numOrderId, $cropId, $workFlowId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestProcess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $requestId
     * @param integer $numOrderId
     * @param integer $cropId
     * @param integer $workFlowId
     * @return RequestProcess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($requestId, $numOrderId, $cropId, $workFlowId)
    {
        if (($model = RequestProcess::findOne(['requestId' => $requestId, 'numOrderId' => $numOrderId, 'cropId' => $cropId, 'workFlowId' => $workFlowId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
