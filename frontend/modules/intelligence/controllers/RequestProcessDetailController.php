<?php

namespace frontend\modules\intelligence\controllers;

use Yii;
use frontend\modules\intelligence\models\RequestProcessDetail;
use frontend\modules\intelligence\models\RequestProcessDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestProcessDetailController implements the CRUD actions for RequestProcessDetail model.
 */
class RequestProcessDetailController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RequestProcessDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestProcessDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RequestProcessDetail model.
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        return $this->render('view', [
            'model' => $this->findModel($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId),
        ]);
    }

    /**
     * Creates a new RequestProcessDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RequestProcessDetail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RequestProcessDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        $model = $this->findModel($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RequestProcessDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        $this->findModel($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestProcessDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @param integer $activityId
     * @param integer $essayId
     * @return RequestProcessDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($requestId, $cropId, $workFlowId, $numOrderId, $activityId, $essayId)
    {
        if (($model = RequestProcessDetail::findOne(['requestId' => $requestId, 'cropId' => $cropId, 'workFlowId' => $workFlowId, 'numOrderId' => $numOrderId, 'activityId' => $activityId, 'essayId' => $essayId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
