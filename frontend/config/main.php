<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);


return [
    // 'id' => 'CIPPATHOLOGYDEV',
    //'id' => 'CIPPATHOLOGYTEST',
    'id' => 'CIPPATHOLOGY',

    // 'name' => 'CIP-Pathology (Developer)',
    //'name' => 'CIP-Pathology (Test)',
    'name' => 'CIP-Pathology',

    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'modules' => [
        'business' => [
            'class' => 'frontend\modules\business\Business',
        ],
        'control' => [
            'class' => 'frontend\modules\control\Control',
        ],
        'financial' => [
            'class' => 'frontend\modules\financial\Financial',
        ],
        'intelligence' => [
            'class' => 'frontend\modules\intelligence\Intelligence',
        ],
        'security' => [
            'class' => 'frontend\modules\security\Security',
        ],
        'configuration' => [
            'class' => 'frontend\modules\configuration\Configuration',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
        ],
    ],

    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'advanced-frontend-dev',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'dateFormat' => 'Y-M-d',
            'datetimeFormat' => 'Y-M-d H:i:s',
            'timeFormat' => 'H:i:s',
        ],
        'urlManager' => [
            'hostInfo' => 'https://research.cip.cgiar.org',
        ],
        // 'urlManager' => [
        //     // 'class' => 'research.cip.cgiar.org',
        //     // 'hostName' => 'research.cip.cgiar.org',
        //     'baseUrl' => 'https://research.cip.cgiar.org',
        //     //'baseUrl' => '/research.cip.cgiar.org',
        //     // 'enablePrettyUrl' => true,
        //     // 'showScriptName' => false,
        //     'rules' => []
        // ],
    ],

    'params' => $params,
];
