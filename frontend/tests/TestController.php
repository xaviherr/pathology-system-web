<?php

class TestController extends Controller
{


    public function actionResultByAgent($requestId, $numOrderId, $essayId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            // Sample
            $arraySample = Sample::find()
                ->where([
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'status' => 'active',
                ])
                // ->andWhere(
                //     [
                //         '>=',
                //         'graftingCount',
                //         1
                //     ]
                // )
                ->asArray()->all();

            // AgentByRequestProcessDetail
            $arrayAgentByEssay = AgentByRequestProcessDetail::find()->where([
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'status' => 'active',
            ])->asArray()->all();

            // ActivityByEssay
            $arrayActivityByEssay = ActivityByEssay::find()->where([
                'ActivityByEssay.essayId' => $essayId,
                'status' => 'active',
            ])->orderBy('activityOrder')->asArray()->all();

            // ReadingData
            $arrayReadingData = ReadingData::find()->where([
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'status' => 'active',
            ])->asArray()->all();

            $essayType = is_null($essayId) ? null : Essay::findOne($essayId)->essayType->code;

            //creation of simple data columns
            // ************************************************************************************************** //
            $return['dataProvider'] = [];
            $return['columns'] = [];
            $arrayColumns = array(
                'Nbr. Order' => null,
                'Accession number' => null,
                'Lab code' => null,
                'User code' => null,
                // 'User code 02'
                // 'User code 03'
                'Collecting number' => null,
                'Accession name' => null,
                'Female parent' => null,
                'Male parent' => null,
                'Is bulk' => null,
                'Crop' => null,
                'Sample type' => null,
                'Position' => null,
            );

            foreach ($arrayColumns as $key => $value) {
                $arrayColumnsResults[] = [
                    'agentId' => null,
                    'activityId' => null,
                    'format' => false,
                    'headerValue' => $key,
                ];
            }

            //creation of reading columns header
            // ************************************************************************************************** //
            foreach ($arrayAgentByEssay as $arrayAgentByEssayKey => $arrayAgentByEssayValue) {
                foreach ($arrayActivityByEssay as $arrayActivityByEssayKey => $arrayActivityByEssayValue) {
                    $modelActivity = Activity::findOne($arrayActivityByEssayValue['activityId']);
                    if (Parameter::findOne($modelActivity->activityPropertyId)->code == 'ACTPA') { // Activity per sample at the beginning
                        //by agent
                        $headerValue = str_replace("agent", Agent::findOne($arrayAgentByEssayValue['agentId'])->shortName, strtolower($modelActivity->shortName));
                        $arrayColumns[$headerValue] = null;
                        $arrayColumnsResults[] = [
                            'agentId' => $arrayAgentByEssayValue['agentId'],
                            'activityId' => $modelActivity->id,
                            'resultType' => Parameter::findOne($modelActivity->activityTypeId)->shortName,
                            'format' => true,
                            'headerValue' =>   $headerValue,
                        ];
                    } else {
                        if (Parameter::findOne($modelActivity->activityPropertyId)->code == 'ASINI' and $arrayAgentByEssayKey  == 0) { // Activity per sample at the end
                            //by sample
                            $headerValue = $modelActivity->shortName;
                            $arrayColumns[$headerValue] = null;
                            $arrayColumnsResults[] = [
                                'activityId' => $modelActivity->id,
                                'format' => false,
                                'headerValue' =>   $headerValue,
                            ];
                        }
                        if (Parameter::findOne($modelActivity->activityPropertyId)->code == 'ASEND' and $arrayAgentByEssayKey  == count($arrayAgentByEssay) - 1) { // Activity by agent
                            //by sample
                            $headerValue = $modelActivity->shortName;
                            $arrayColumns[$headerValue] = null;
                            $arrayColumnsResults[] = [
                                'activityId' => $modelActivity->id,
                                'format' => false,
                                'headerValue' =>   $headerValue,
                            ];
                        }
                    }
                }
            }

            //simple data log and reading data log
            // ************************************************************************************************** //
            foreach ($arraySample as $key => $value) {
                $sampleId = $value['id'];

                $arrayReadingDataBySample = array_filter($arrayReadingData, function ($element) use ($sampleId) {
                    return ($element['sampleId'] == $sampleId);
                });

                $arrayColumns['Nbr. Order'] = $value['numOrder'];
                $arrayColumns['Accession number'] = $value['accessionCode'];
                $arrayColumns['Lab code'] = $value['labCode'];
                $arrayColumns['User code'] = $value['firstUserCode'];
                // $arrayColumns['User code'] = $value['secondUserCode'];
                // $arrayColumns['User code'] = $value['thirdUserCode'];
                $arrayColumns['Collecting number'] = $value['collectingCode'];
                $arrayColumns['Accession name'] = $value['accessionName'];
                $arrayColumns['Female parent'] = $value['female'];
                $arrayColumns['Male parent'] = $value['male'];
                $arrayColumns['Is bulk'] = $value['isBulk'];
                $arrayColumns['Crop'] =   Crop::findOne($value['cropId'])->longName;
                $arrayColumns['Sample type'] = Parameter::findOne($value['sampleTypeId'])->shortName;


                $arrayColumns['Position'] = null;

                foreach ($arrayColumnsResults as $key_1 => $value_1) {
                    if ($value_1['format']) {
                        $agentId = $value_1['agentId'];
                        $activityId = $value_1['activityId'];
                        $arrayReadingDataBySampleAgentActivity = array_filter($arrayReadingDataBySample, function ($element) use ($agentId, $activityId) {
                            return ($element['agentId'] == $agentId and $element['activityId'] == $activityId);
                        });
                        $result = null;
                        $title = null;
                        //-----------------------------------------------------//
                        if (count($arrayReadingDataBySampleAgentActivity) >= 1) {


                            if ($value_1['resultType'] == 'quantitative') {
                                $arrayResult = array_unique(array_column($arrayReadingDataBySampleAgentActivity, 'qResult'));
                            } else if ($value_1['resultType'] == 'qualitative') {
                                $arrayResult = array_unique(array_column($arrayReadingDataBySampleAgentActivity, 'cResult'));
                            }

                            if (count($arrayReadingDataBySampleAgentActivity) == 1) {
                                $result = implode(", ", $arrayResult);

                                $pos =  array_unique(array_column($arrayReadingDataBySampleAgentActivity, 'cellPosition'));
                                $arrayColumns['Position'] = $pos[0];
                            } else {
                                //-----------//
                                $result = implode(", ", $arrayResult);
                                $arrayColumns['Position'] = null;
                                //-----------//
                            }

                            array_walk($arrayResult, function (&$value) use ($activityId, $agentId,  $essayType) {
                                if ($essayType == 'RSGL') {
                                    return $value = "The result in the reading " .
                                        Activity::findOne($activityId)->shortName . " from " .
                                        Agent::findOne($agentId)->shortName . " is " . $value;
                                } else if ($essayType == 'RMUL') {
                                    try {
                                        return $value = $value . "= " . $longName =  Symptom::find()->where([
                                            'shortName' => $value,
                                            'status' => 'active',
                                        ])->one()->longName;
                                    } catch (\Throwable $th) {
                                        return $value = $value . "= Undetermined";
                                    }
                                } else {
                                    return $value = "The result in reading the " .
                                        Activity::findOne($activityId)->shortName . " and the pathogen " .
                                        Agent::findOne($agentId)->shortName . " is " . $value;
                                }
                            });
                            //-----------//
                            $title = implode("\n", $arrayResult);
                        }
                        //-----------------------------------------------------//

                        $arrayColumns[$value_1['headerValue']] = '<span title= "' . $title . '" class="label label-' . $result . ' ">' .  $result . '</span>';
                    }
                }
                $return['dataProvider'][] =  $arrayColumns;
            }

            foreach ($arrayColumnsResults as $key => $value) {
                $return['columns'][] =
                    [
                        'attribute'     =>  $value['headerValue'],
                        'enableSorting' => false,
                        'format' => 'raw',
                    ];
            }

            return $return;
        } else {
            /***********************************************/
            return false;
            /***********************************************/
        }
    }

    public function actionResultByActivity($requestId, $numOrderId, $essayId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $arraySample = Sample::find()->where([
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'status' => 'active',
                'graftingCount' => 1
            ])->asArray()->all();

            $arrayAgentByEssay = AgentByRequestProcessDetail::find()->where([
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'status' => 'active',
            ])->asArray()->all();

            $arrayActivityByEssay = ActivityByEssay::find()->where([
                'ActivityByEssay.essayId' => $essayId,
                'status' => 'active',
            ])->orderBy('activityOrder')->asArray()->all();

            $arrayReadingData = ReadingData::find()->where([
                'requestId' => $requestId,
                'numOrderId' => $numOrderId,
                'essayId' => $essayId,
                'status' => 'active',
            ])->asArray()->all();
            $essayType = is_null($essayId) ? null : Essay::findOne($essayId)->essayType->code;

            //creation of simple data columns
            // ************************************************************************************************** //
            $return['dataProvider'] = [];
            $return['columns'] = [];
            $arrayColumns = array(
                'Nbr. Order' => null,
                'Accession number' => null,
                'Lab code' => null,
                'User code' => null,
                // 'User code 02'
                // 'User code 03'
                'Collecting number' => null,
                'Accession name' => null,
                'Female parent' => null,
                'Male parent' => null,
                'Is bulk' => null,
                'Crop' => null,
                'Sample type' => null,
            );
            foreach ($arrayColumns as $key => $value) {
                $arrayColumnsResults[] = [
                    'agentId' => null,
                    'activityId' => null,
                    'format' => false,
                    'headerValue' => $key,
                ];
            }

            //creation of reading columns header
            // ************************************************************************************************** //
            foreach ($arrayAgentByEssay as $arrayAgentByEssayKey => $arrayAgentByEssayValue) {
                foreach ($arrayActivityByEssay as $arrayActivityByEssayKey => $arrayActivityByEssayValue) {
                    $modelActivity = Activity::findOne($arrayActivityByEssayValue['activityId']);
                    if (Parameter::findOne($modelActivity->activityPropertyId)->code == 'ACTPA') { // Activity per sample at the beginning
                        //by agent
                        $headerValue = str_replace("agent", Agent::findOne($arrayAgentByEssayValue['agentId'])->shortName, strtolower($modelActivity->shortName));
                        $arrayColumns[$headerValue] = null;
                        $arrayColumnsResults[] = [
                            'agentId' => $arrayAgentByEssayValue['agentId'],
                            'activityId' => $modelActivity->id,
                            'resultType' => Parameter::findOne($modelActivity->activityTypeId)->shortName,
                            'format' => true,
                            'headerValue' =>   $headerValue,
                        ];
                    } else {
                        if (Parameter::findOne($modelActivity->activityPropertyId)->code == 'ASINI' and $arrayAgentByEssayKey  == 0) { // Activity per sample at the end
                            //by sample
                            $headerValue = $modelActivity->shortName;
                            $arrayColumns[$headerValue] = null;
                            $arrayColumnsResults[] = [
                                'activityId' => $modelActivity->id,
                                'format' => false,
                                'headerValue' =>   $headerValue,
                            ];
                        }
                        if (Parameter::findOne($modelActivity->activityPropertyId)->code == 'ASEND' and $arrayAgentByEssayKey  == count($arrayAgentByEssay) - 1) { // Activity by agent
                            //by sample
                            $headerValue = $modelActivity->shortName;
                            $arrayColumns[$headerValue] = null;
                            $arrayColumnsResults[] = [
                                'activityId' => $modelActivity->id,
                                'format' => false,
                                'headerValue' =>   $headerValue,
                            ];
                        }
                    }
                }
            }

            //simple data log and reading data log
            // ************************************************************************************************** //
            foreach ($arraySample as $key => $value) {
                $sampleId = $value['id'];
                $arrayReadingDataBySample = array_filter($arrayReadingData, function ($element) use ($sampleId) {
                    return ($element['sampleId'] == $sampleId);
                });
                $arrayColumns['Nbr. Order'] = $value['numOrder'];
                $arrayColumns['Accession number'] = $value['accessionCode'];
                $arrayColumns['Lab code'] = $value['labCode'];
                $arrayColumns['User code'] = $value['firstUserCode'];
                // $arrayColumns['User code'] = $value['secondUserCode'];
                // $arrayColumns['User code'] = $value['thirdUserCode'];
                $arrayColumns['Collecting number'] = $value['collectingCode'];
                $arrayColumns['Accession name'] = $value['accessionName'];
                $arrayColumns['Female parent'] = $value['female'];
                $arrayColumns['Male parent'] = $value['male'];
                $arrayColumns['Is bulk'] = $value['isBulk'];
                $arrayColumns['Crop'] =   Crop::findOne($value['cropId'])->longName;
                $arrayColumns['Sample type'] = Parameter::findOne($value['sampleTypeId'])->shortName;
                foreach ($arrayColumnsResults as $key_1 => $value_1) {
                    if ($value_1['format']) {
                        $agentId = $value_1['agentId'];
                        $activityId = $value_1['activityId'];
                        $arrayReadingDataBySampleAgentActivity = array_filter($arrayReadingDataBySample, function ($element) use ($agentId, $activityId) {
                            return ($element['agentId'] == $agentId and $element['activityId'] == $activityId);
                        });
                        //-----------------------------------------------------//
                        $result = null;
                        $title = null;
                        //-----------------------------------------------------//
                        if (count($arrayReadingDataBySampleAgentActivity) >= 1) {
                            // quantitative
                            // qualitative
                            if ($value_1['resultType'] == 'quantitative') {
                                $arrayResult = array_unique(array_column($arrayReadingDataBySampleAgentActivity, 'qResult'));
                            } else if ($value_1['resultType'] == 'qualitative') {
                                $arrayResult = array_unique(array_column($arrayReadingDataBySampleAgentActivity, 'cResult'));
                            }
                            //-----------//
                            $result = implode(", ", $arrayResult);
                            //-----------//
                            array_walk($arrayResult, function (&$value) use ($activityId, $agentId,  $essayType) {
                                if ($essayType == 'RSGL') {
                                    return $value = "The result in the reading " .
                                        Activity::findOne($activityId)->shortName . " from " .
                                        Agent::findOne($agentId)->shortName . " is " . $value;
                                } else if ($essayType == 'RMUL') {
                                    try {
                                        return $value = $value . "= " . $longName =  Symptom::find()->where([
                                            'shortName' => $value,
                                            'status' => 'active',
                                        ])->one()->longName;
                                    } catch (\Throwable $th) {
                                        return $value = $value . "= Undetermined";
                                    }
                                } else {
                                    return $value = "The result in reading the " .
                                        Activity::findOne($activityId)->shortName . " and the pathogen " .
                                        Agent::findOne($agentId)->shortName . " is " . $value;
                                }
                            });
                            //-----------//
                            $title = implode("\n", $arrayResult);
                        }
                        //-----------------------------------------------------//
                        $arrayColumns[$value_1['headerValue']] = '<span title= "' . $title . '" class="label label-' . $result . ' ">' .  $result . '</span>';
                    }
                }
                $return['dataProvider'][] =  $arrayColumns;
            }

            foreach ($arrayColumnsResults as $key => $value) {
                $return['columns'][] =
                    [
                        'attribute'     =>  $value['headerValue'],
                        'enableSorting' => false,
                        'format' => 'raw',
                    ];
            }

            return $return;
        } else {
            /***********************************************/
            return false;
            /***********************************************/
        }
    }

    public function actionResultByAgentResume($requestId, $numOrderId, $essayId = null)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            // Sample
            $arraySample = Sample::find()
                ->where([
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'status' => 'active',
                ])
                ->andWhere(
                    [
                        '>=',
                        'graftingCount',
                        1
                    ]
                )
                ->asArray()->all();
            //creation of simple data columns
            // ************************************************************************************************** //
            $return['dataProvider'] = [];
            $return['columns'] = [];
            $arrayColumns = array(
                'Nbr. Order' => null,
                'Accession number' => null,
                'Lab code' => null,
                'User code' => null,
                'Collecting number' => null,
                'Accession name' => null,
                'Female parent' => null,
                'Male parent' => null,
                'Is bulk' => null,
                'Crop' => null,
                'Sample type' => null,
                //'Reaction in PVY' => null,
            );

            foreach ($arrayColumns as $key => $value) {
                $arrayColumnsResults[] = [
                    // 'agentId' => null,
                    // 'activityId' => null,
                    'format' => false,
                    'headerValue' => $key,
                ];
            }
            //simple data log and reading data log
            // ************************************************************************************************** //
            foreach ($arraySample as $key => $value) {
                $arrayColumns['Nbr. Order'] = $value['numOrder'];
                $arrayColumns['Accession number'] = $value['accessionCode'];
                $arrayColumns['Lab code'] = $value['labCode'];
                $arrayColumns['User code'] = $value['firstUserCode'];
                $arrayColumns['Collecting number'] = $value['collectingCode'];
                $arrayColumns['Accession name'] = $value['accessionName'];
                $arrayColumns['Female parent'] = $value['female'];
                $arrayColumns['Male parent'] = $value['male'];
                $arrayColumns['Is bulk'] = $value['isBulk'];
                $arrayColumns['Crop'] =   Crop::findOne($value['cropId'])->longName;
                $arrayColumns['Sample type'] = Parameter::findOne($value['sampleTypeId'])->shortName;
                //$arrayColumns['Reaction in PVY'] = "<span class='label label-negative'>negative</span>";
                $return['dataProvider'][] =  $arrayColumns;
            }

            foreach ($arrayColumnsResults as $key => $value) {
                $return['columns'][] =
                    [
                        'attribute'     =>  $value['headerValue'],
                        'enableSorting' => false,
                        'format' => 'raw',
                    ];
            }
            return $return;
        } else {
            /***********************************************/
            return false;
            /***********************************************/
        }
    }

    public function actionResultByActivityResume($requestId, $numOrderId, $essayId = null)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            // Sample
            $arraySample = Sample::find()
                ->where([
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'status' => 'active',
                ])
                ->andWhere(
                    [
                        '>=',
                        'graftingCount',
                        1
                    ]
                )
                ->asArray()->all();
            //creation of simple data columns
            // ************************************************************************************************** //
            $return['dataProvider'] = [];
            $return['columns'] = [];
            $arrayColumns = array(
                'Nbr. Order' => null,
                'Accession number' => null,
                'Lab code' => null,
                'User code' => null,
                'Collecting number' => null,
                'Accession name' => null,
                'Female parent' => null,
                'Male parent' => null,
                'Is bulk' => null,
                'Crop' => null,
                'Sample type' => null,
                //'Reaction in PVY' => null,
            );

            foreach ($arrayColumns as $key => $value) {
                $arrayColumnsResults[] = [
                    // 'agentId' => null,
                    // 'activityId' => null,
                    'format' => false,
                    'headerValue' => $key,
                ];
            }
            //simple data log and reading data log
            // ************************************************************************************************** //
            foreach ($arraySample as $key => $value) {
                $arrayColumns['Nbr. Order'] = $value['numOrder'];
                $arrayColumns['Accession number'] = $value['accessionCode'];
                $arrayColumns['Lab code'] = $value['labCode'];
                $arrayColumns['User code'] = $value['firstUserCode'];
                $arrayColumns['Collecting number'] = $value['collectingCode'];
                $arrayColumns['Accession name'] = $value['accessionName'];
                $arrayColumns['Female parent'] = $value['female'];
                $arrayColumns['Male parent'] = $value['male'];
                $arrayColumns['Is bulk'] = $value['isBulk'];
                $arrayColumns['Crop'] =   Crop::findOne($value['cropId'])->longName;
                $arrayColumns['Sample type'] = Parameter::findOne($value['sampleTypeId'])->shortName;
                //$arrayColumns['Reaction in PVY'] = "<span class='label label-negative'>negative</span>";
                $return['dataProvider'][] =  $arrayColumns;
            }

            foreach ($arrayColumnsResults as $key => $value) {
                $return['columns'][] =
                    [
                        'attribute'     =>  $value['headerValue'],
                        'enableSorting' => false,
                        'format' => 'raw',
                    ];
            }
            return $return;
        } else {
            /***********************************************/
            return false;
            /***********************************************/
        }
    }

    public function actionResultBySupport($requestId, $numOrderId, $essayId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $activities = [];
            $readingDataByActivities = [];
            $arrayRequestProcess = RequestProcess::find()->where(
                [
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'status' => 'active',
                ]
            )->one();
            $arrayActivityByEssay = ActivityByEssay::find()->where(
                [
                    'ActivityByEssay.essayId' => $essayId,
                    'status' => 'active',
                ]
            )->orderBy('activityOrder')->asArray()->all();
            $arraySample = Sample::find()->where(
                [
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                    'status' => 'active',
                    //'graftingCount' => 1
                ]
            )->orderBy('numOrder')->asArray()->all();
            $arraySampleById = ArrayHelper::index($arraySample, 'id'); //with control and test samples
            $arraySampleByOrder = ArrayHelper::index($arraySample, 'numOrder');
            $totalSamples = count($arraySample);
            $totalSamplesByOrder = count($arraySampleByOrder);
            //------------------------------------------------------------------------//
            $controlCellsByDefaultSupport = 6;
            $rowQty =  8;
            $columnQty =  12;
            $cellsPerSupportQty = $columnQty *  $rowQty;
            //------------------------------------------------------------------------//
            $freeCellsPerDefaultSupport = $cellsPerSupportQty - $controlCellsByDefaultSupport;
            if ($totalSamples <= $freeCellsPerDefaultSupport) {
                if ($totalSamples <= $freeCellsPerDefaultSupport / 2) {
                    $defaultLastSupportControlCells =  $controlCellsByDefaultSupport / 2;
                } else if ($totalSamples < $freeCellsPerDefaultSupport) {
                    if ($totalSamples <= $cellsPerSupportQty - $rowQty) {
                        $defaultLastSupportControlCells = $rowQty;
                    } else {
                        $defaultLastSupportControlCells = $controlCellsByDefaultSupport;
                    }
                } else if ($totalSamples == $freeCellsPerDefaultSupport) {
                    $defaultLastSupportControlCells =  $controlCellsByDefaultSupport;
                }
                $defaultTotalSupports = 1;
            } else {
                $samplesRemainingByDefault = $totalSamples % $freeCellsPerDefaultSupport;
                if ($samplesRemainingByDefault < $freeCellsPerDefaultSupport) {
                    if ($samplesRemainingByDefault <= $cellsPerSupportQty - $rowQty) {
                        $defaultLastSupportControlCells = $rowQty;
                    } else {
                        $defaultLastSupportControlCells = $controlCellsByDefaultSupport;
                    }
                    $defaultTotalSupports = intdiv($totalSamples, $freeCellsPerDefaultSupport) + 1;
                } else if ($samplesRemainingByDefault == 0) {
                    $defaultLastSupportControlCells =  $controlCellsByDefaultSupport;
                    $defaultTotalSupports =  $totalSamples / $freeCellsPerDefaultSupport;
                }
            }
            //------------------------------------------------------------------------//
            $defaultTotalCells = $defaultTotalSupports * $cellsPerSupportQty;
            //------------------------------------------------------------------------//
            if (!empty($arrayActivityByEssay)) {
                foreach ($arrayActivityByEssay as $key_1 => $value_1) { // BY ACTIVITY
                    $modelActivity = Activity::findONe($value_1['activityId']);
                    $dataType =  Parameter::findOne($modelActivity->activityTypeId)->shortName;
                    if ($dataType == "quantitative") {
                        $tResult = 'qResult';
                        $graftingNumberId = 1;
                    } else if ($dataType == "qualitative") {
                        $tResult = 'cResult';
                        $graftingNumberId = 0;
                    }
                    $cropId = $arrayRequestProcess->cropId;
                    $workFlowId = $arrayRequestProcess->workFlowId;
                    $supportOrder = 0;
                    $activities[$modelActivity->id] =  $modelActivity->shortName;
                    /******************************************************************************************/
                    $arrayAgentByRequestProcessDetail = AgentByRequestProcessDetail::find()->where([
                        'requestId' => $requestId,
                        'numOrderId' => $numOrderId,
                        'essayId' => $essayId,
                        'status' => 'active',
                    ])->asArray()->all();
                    foreach ($arrayAgentByRequestProcessDetail as $key_2 => $value_2) { // BY AGENT
                        $cellPosition = 0;
                        $sampleOrder = 0;
                        $contOrder = 0;
                        $typeCell = "";
                        $result = "";
                        $supportRows = $rowQty;
                        $supportColumns = $columnQty;
                        $controlCellsPerSupport = $controlCellsByDefaultSupport;
                        $shortNameAgent = Agent::findOne($value_2['agentId'])->shortName;
                        $longNameAgent = Agent::findOne($value_2['agentId'])->longName;
                        $arrayReadingData = ReadingData::find()
                            ->where(
                                [
                                    //'sampleId' => null,
                                    //'cellPosition' => null,
                                    //'supportId' => null,
                                    'status' => 'active',
                                    'requestId' => $requestId,
                                    'cropId' => $cropId,
                                    'workFlowId' => $workFlowId,
                                    'numOrderId' => $numOrderId,
                                    'activityId' => $modelActivity->id,
                                    'essayId' => $essayId,
                                    'agentId' => $value_2['agentId'],
                                    'graftingNumberId' => $graftingNumberId,
                                    // 'symptomId' => null,
                                    // 'readingDataTypeId' => null,
                                ]
                            )->asArray()->all();
                        if (count($arrayReadingData) > 0) {
                            for ($i = 0; $i < $defaultTotalCells; $i++) {
                                try {
                                    //Execute and condition only once
                                    if ($i == 0 or $i %  $cellsPerSupportQty  == 0) {
                                        $supportOrder =  $supportOrder + 1;
                                        $cellPosition = 0;
                                    }
                                    //Define information by sample
                                    $cellPosition = $cellPosition + 1;
                                    $arrayReadingDataOne = array_values(
                                        array_filter($arrayReadingData, function ($element) use ($cellPosition, $supportOrder) {
                                            return ($element['cellPosition'] == $cellPosition and $element['supportId'] == $supportOrder);
                                        })
                                    );
                                    if (count($arrayReadingDataOne) == 1) {
                                        $contOrder = $contOrder + 1;
                                        $sampleOrder  = $contOrder;
                                        $accessionNumber =  $arraySampleById[$arrayReadingDataOne[0]['sampleId']]['accessionCode'];
                                        $labCode = $arraySampleById[$arrayReadingDataOne[0]['sampleId']]['labCode'];
                                        $firstUserCode = $arraySampleById[$arrayReadingDataOne[0]['sampleId']]['firstUserCode'];
                                        $secondUserCode = $arraySampleById[$arrayReadingDataOne[0]['sampleId']]['secondUserCode'];
                                        $thirdUserCode = $arraySampleById[$arrayReadingDataOne[0]['sampleId']]['thirdUserCode'];
                                        $sampleCode = $arraySampleById[$arrayReadingDataOne[0]['sampleId']]['numOrder'];
                                        $result = $arrayReadingDataOne[0][$tResult];
                                        $typeCell = "result_cells";
                                        $class =  "result_cell_class full " . $result;
                                    } else {
                                        $sampleOrder = "-";
                                        $accessionNumber = "-";
                                        $labCode = "-";
                                        $firstUserCode = "-";
                                        $secondUserCode = "-";
                                        $thirdUserCode = "-";
                                        $sampleCode = "-";
                                        $result = "-";
                                        $typeCell = "empty_cells";
                                        $class =  "empty_cell_class empty";
                                    }
                                    //Set reading data in array
                                    $readingDataByActivities[$modelActivity->id][$value_2['agentId']][$supportOrder][$cellPosition] = [
                                        'requestId' =>  $requestId,
                                        'cropId' =>  $cropId,
                                        'workFlowId' =>  $workFlowId,
                                        'numOrderId' =>  $numOrderId,
                                        'activityId' =>  $modelActivity->id,
                                        'essayId' =>  $essayId,
                                        'activityShortName' =>  $modelActivity->shortName,
                                        'supportOrder' =>  $supportOrder,
                                        'supportRows' =>  $supportRows,
                                        'supportColumns' =>  $supportColumns,
                                        'cellPosition' =>  $cellPosition,
                                        'agentId' =>   $value_2['agentId'],
                                        'agentShortName' =>    $shortNameAgent,
                                        'agentLongName' =>  $longNameAgent,
                                        'typeCell' =>  $typeCell,
                                        'dataType' => $dataType,
                                        'result' =>  $result,
                                        'sampleOrder' => $sampleOrder,
                                        'accessionNumber' =>  $accessionNumber,
                                        'labCode' =>  $labCode,
                                        'firstUserCode' => $firstUserCode,
                                        'secondUserCode' => $secondUserCode,
                                        'thirdUserCode' => $thirdUserCode,
                                        'sampleCode' => $sampleCode,
                                        'class' => $class,
                                        'info' => "Order in sample list: " . $sampleOrder . "\n"
                                            . "Result: " . $result . "\n"
                                            . "Accession code: " . $accessionNumber . "\n"
                                            . "User code: " . $firstUserCode . "\n"
                                            . "Data type: " . $dataType . "\n"
                                            . "Type cell: " . $typeCell . "\n"
                                            . "Lab code: " . $labCode,

                                    ];
                                } catch (\Throwable $th) { }
                            }
                        } else {
                            for ($i = 0; $i < $defaultTotalCells; $i++) {
                                //Execute and condition only once
                                if ($i == 0 or $i %  $cellsPerSupportQty  == 0) {
                                    // result cells 
                                    $typeCell = "result_cells";
                                    if ($dataType == 'quantitative') {
                                        $result = 0.000;
                                        $class = "cell_class quantitative ";
                                    } else if ($dataType == 'qualitative') {
                                        $result = "-";
                                        $class = "cell_class qualitative ";
                                    }
                                    $supportOrder =  $supportOrder + 1;
                                    $cellPosition = 0;
                                } else if (
                                    $i >= $cellsPerSupportQty * $supportOrder -  $controlCellsPerSupport
                                    and
                                    $i <  $cellsPerSupportQty * $supportOrder
                                ) {
                                    // control cells
                                    $typeCell = "control_cells";
                                    $result = "control";
                                    $class = "cell_class";
                                } else if ($contOrder == $totalSamplesByOrder) {
                                    // empty cells   
                                    $typeCell = "empty_cells";
                                    $result = "-";
                                    $controlCellsPerSupport = $defaultLastSupportControlCells;
                                    $class = "cell_class";
                                }
                                //Define information by sample
                                $cellPosition = $cellPosition + 1;
                                if ($typeCell == "result_cells") {
                                    $contOrder = $contOrder + 1;
                                    $sampleOrder  = $contOrder;
                                    $accessionNumber = $arraySampleByOrder[$sampleOrder]['accessionCode'];
                                    $labCode = $arraySampleByOrder[$sampleOrder]['labCode'];
                                    $firstUserCode = $arraySampleByOrder[$sampleOrder]['firstUserCode'];
                                    $secondUserCode = $arraySampleByOrder[$sampleOrder]['secondUserCode'];
                                    $thirdUserCode = $arraySampleByOrder[$sampleOrder]['thirdUserCode'];
                                    $sampleCode = $arraySampleByOrder[$sampleOrder]['numOrder'];
                                } else if ($typeCell == "control_cells") {
                                    $sampleOrder = "-";
                                    $accessionNumber = "-";
                                    $labCode = "-";
                                    $firstUserCode = "-";
                                    $secondUserCode = "-";
                                    $thirdUserCode = "-";
                                    $sampleCode = "-";
                                } else if ($typeCell == "empty_cells") {
                                    $sampleOrder = "-";
                                    $accessionNumber = "-";
                                    $labCode = "-";
                                    $firstUserCode = "-";
                                    $secondUserCode = "-";
                                    $thirdUserCode = "-";
                                    $sampleCode = "-";
                                }
                                //Set reading data in array
                                $readingDataByActivities[$modelActivity->id][$value_2['agentId']][$supportOrder][$cellPosition] = [
                                    'requestId' =>  $requestId,
                                    'cropId' =>  $cropId,
                                    'workFlowId' =>  $workFlowId,
                                    'numOrderId' =>  $numOrderId,
                                    'activityId' =>  $modelActivity->id,
                                    'essayId' =>  $essayId,
                                    'activityShortName' =>  $modelActivity->shortName,
                                    'supportOrder' =>  $supportOrder,
                                    'supportRows' =>  $supportRows,
                                    'supportColumns' =>  $supportColumns,
                                    'cellPosition' =>  $cellPosition,
                                    'agentId' =>   $value_2['agentId'],
                                    'agentShortName' =>   $shortNameAgent,
                                    'agentLongName' => $longNameAgent,
                                    'typeCell' =>  $typeCell,
                                    'dataType' => $dataType,
                                    'result' =>  $result,
                                    'sampleOrder' => $sampleOrder,
                                    'accessionNumber' =>  $accessionNumber,
                                    'labCode' =>  $labCode,
                                    'firstUserCode' => $firstUserCode,
                                    'secondUserCode' => $secondUserCode,
                                    'thirdUserCode' => $thirdUserCode,
                                    'sampleCode' => $sampleCode,
                                    'class' => $class,
                                    'info' => "Type cell: " . $typeCell . "\n"
                                        . "Data type: " . $dataType . "\n"
                                        . "Result: " . $result . "\n"
                                        . "Order: " . $sampleOrder . "\n"
                                        . "Accession code: " . $accessionNumber . "\n"
                                        . "Lab code: " . $labCode . "\n"
                                        . "User code: " . $firstUserCode,
                                ];
                            }
                        }
                    }
                    /******************************************************************************************/
                }
            }
            $return['readingDataByActivities'] =  $readingDataByActivities;
            $return['activities'] =  $activities;
            return $return;
        } else {
            /***********************************************/
            return false;
            /***********************************************/
        }
    }
}
