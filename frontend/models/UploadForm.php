<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
  public $imageFile;

  public function rules()
  {
    return [
      [['imageFile'], 'file', 'extensions' => 'png, jpg'],
    ];
  }

  public function upload($username)
  {
    if ($this->validate()) {
      $saveIn = 'photos/' . $username . '.' . $this->imageFile->extension;
      $this->imageFile->saveAs($saveIn);
      return $saveIn;
    } else {
      return false;
    }
  }
}
