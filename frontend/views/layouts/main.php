<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\DashboardAsset;
use common\widgets\Alert;
use yii\helpers\Url;
//use Yii;

DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="/cippathology/frontend/web/icons/23-512.png">
    <?= Html::csrfMetaTags() ?>
    <title>
        <?= Yii::$app->name ?>
    </title>
    <?php $this->head() ?>
</head>

<body onload="$('#loader').hide();" class="sidebar-mini skin-cip sidebar-collapse">
    <div id="loader">
        <div class="loader-message">
            <i>Loading...<i class='fa fa-refresh fa-spin'></i></i>
        </div>
    </div>
    <div class="box-body no-padding">
        <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i> Login</h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-login-content">loading ...<i class='fa fa-refresh fa-spin'></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i> Sign up</h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-signup-content">loading ...<i class='fa fa-refresh fa-spin'></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">

            <!-- <header class="main-header main-header-dev"> -->
            <a href="index.php" class="logo">
                <span class="logo-mini"><b>CIP</b></span>
                <span class="logo-lg"><b>CIP</b>PATHOLOGY</span> -->
            </a>
            <nav class="navbar navbar navbar-static-top">
                <!-- <nav class="navbar navbar-dev navbar-static-top"> -->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav" data-widget="tree">
                        <li class="treeview user user-menu">
                            <a href="#">
                                <img src=<?= Yii::$app->user->isGuest ? 'dist/img/avatar0.jpg' : Html::encode(Yii::$app->user->identity->photo) ?> class="user-image" alt="User Image">
                                <span class="hidden-xs"> <?= Yii::$app->user->isGuest ? 'Guest' : Html::encode(Yii::$app->user->identity->email) ?></span>
                            </a>
                            <ul class="treeview-menu dropdown-menu">
                                <li class="user-header">
                                    <img src=<?= Yii::$app->user->isGuest ? 'dist/img/avatar0.jpg' : Html::encode(Yii::$app->user->identity->photo) ?> class="img-circle" alt="User Image">
                                    <p>
                                        <?= Yii::$app->user->isGuest ? 'Guest' : Html::encode(Yii::$app->user->identity->email) ?>
                                        <?= Yii::$app->user->isGuest ? '<small>Offline <i class="fa fa-circle text-default"></i></small>' : '<small>Online <i class="fa fa-circle text-success"></i></small>' ?>
                                    </p>
                                </li>
                                <?php if (Yii::$app->user->isGuest) { ?>
                                    <li class="user-footer">
                                        <?= Html::button(
                                                '<i class="fa fa-sign-in"></i> Login ',
                                                [
                                                    'value' => Url::to(
                                                        [
                                                            'login'
                                                        ]
                                                    ),
                                                    'class' => 'btn btn-default modal-login-action pull-right',
                                                ]
                                            ) ?>
                                    </li>
                                <?php } else { ?>
                                    <li class="user-footer">
                                        <div class="pull-left"><a href="index.php?r=site/signup-update" class="btn btn-default btn-flat"><i class="fa fa-user Profile"></i> Profile</a></div>
                                        <div class="pull-right"><a href="index.php?r=site/logout" class="btn btn-default btn-flat" data-method="post"><i class="fa fa-sign-out"></i> Logout</a></div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <?php if (Yii::$app->user->isGuest) { ?>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li>
                            <a href="index.php"><i class="fa fa-home"></i><span>Home</span>
                            </a>
                        </li>
                    </ul>
                <?php } else { ?>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li>
                            <a href="index.php"><i class="fa fa-home"></i><span>Home</span>
                            </a>
                        </li>
                        <?php try { ?>
                            <?php if (array_key_exists("client", Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)))    ?>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-paper-plane"></i> <span>Request</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="index.php?r=business/request/create"><i class="fa fa-file-o"></i> New request</a></li>
                                    <li><a href="index.php?r=business/request"><i class="fa fa-file-text-o"></i> All my requests</a></li>
                                </ul>
                            </li>
                            <?php if (array_key_exists("business_admin", Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) { ?>
                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-cogs"></i><span>Administration</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <!-- Settings -->
                                        <li class="treeview">
                                            <a href="#">
                                                <i class="fa fa-cog"></i> Settings
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li><a href="index.php?r=configuration/parameter"><i class="fa fa-angle-right"></i> Parameter</a></li>
                                            </ul>
                                        </li>
                                        <!-- Setup -->
                                        <li class="treeview">
                                            <a href="#">
                                                <i class="fa fa-cog"></i> Setup
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li><a href="index.php?r=configuration/agent"><i class="fa fa-angle-right"></i> Agent</a></li>
                                                <li><a href="index.php?r=configuration/crop"><i class="fa fa-angle-right"></i> Crop</a></li>
                                                <li><a href="index.php?r=configuration/work-flow"><i class="fa fa-angle-right"></i> Workflow</a></li>
                                                <li><a href="index.php?r=configuration/essay"><i class="fa fa-angle-right"></i> Laboratory & greenhouse test</a></li>
                                                <li><a href="index.php?r=configuration/activity"><i class="fa fa-angle-right"></i> Activity</a></li>
                                                <li><a href="index.php?r=configuration/location"><i class="fa fa-angle-right"></i> Workplace</a></li>
                                                <li><a href="index.php?r=configuration/reagent"><i class="fa fa-angle-right"></i> Reagent</a></li>
                                                <li><a href="index.php?r=configuration/symptom"><i class="fa fa-angle-right"></i> Symptom</a></li>
                                            </ul>
                                        </li>
                                        <!-- Set Dependencies -->
                                        <li class="treeview">
                                            <a href="#">
                                                <i class="fa fa-cog"></i> Set Dependencies
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li><a href="index.php?r=control/agent-by-essay"><i class="fa fa-angle-right"></i> Agent by test</a></li>
                                                <li><a href="index.php?r=control/activity-by-essay"><i class="fa fa-angle-right"></i> Activity by test</a></li>
                                                <li><a href="index.php?r=control/essay-by-work-flow"><i class="fa fa-angle-right"></i> Test by workflow</a></li>
                                                <li><a href="index.php?r=control/work-flow-by-crop"><i class="fa fa-angle-right"></i> Workflow by crop</a></li>
                                            </ul>
                                        </li>
                                        <?php if (array_key_exists("system_admin", Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) { ?>
                                            <!-- Security -->
                                            <li class="treeview">
                                                <a href="#">
                                                    <i class="fa fa-cog"></i> Security
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="index.php?r=security/authentication-assignment"><i class="fa fa-angle-right"></i>Authentication & Assignment</a></li>
                                                    <li><a href="index.php?r=security/item-user"><i class="fa fa-angle-right"></i>User</a></li>
                                                    <li><a href="index.php?r=security/item-role"><i class="fa fa-angle-right"></i>Role</a></li>
                                                    <li><a href="index.php?r=security/item-permission"><i class="fa fa-angle-right"></i>Permission</a></li>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                        <!-- Financial -->
                                        <li class="treeview">
                                            <a href="#">
                                                <i class="fa fa-cog"></i> Financial
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li><a href="index.php?r=configuration/cost"><i class="fa fa-angle-right"></i>Fee</a></li>
                                                <li><a href="index.php?r=financial/financial-account"><i class="fa fa-angle-right"></i>Financial account</a></li>
                                                <li><a href="index.php?r=financial/financial-concept-one"><i class="fa fa-angle-right"></i>Financial transactions</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                        <?php } catch (\Throwable $th) { ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content-header">
                <?= Alert::widget() ?>
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
            </section>
            <section class="content">
                <?= $content ?>
            </section>
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.1.3
            </div>
            <img src="images/88x31_v2.png">
            Copyright <?= date('Y') ?> © International Potato Center.
        </footer>
    </div>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
