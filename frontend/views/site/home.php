<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
// $this->title = 'Seed Health Laboratory';

// Url::to('@web') . '/business/request'

$this->title = 'CYP Pathology';
?>
<div class="site-home">

  <div class="row">

    <div class="col-lg-3 col-xs-6">
      <a class="bg-teal-gradient small-box bg-yellow" href="<?= Url::home() ?>?r=business/request/create">
        <div class="inner">
          <p>NEW REQUEST</p>
          <h3> <?= $countRequestMonth ?></h3>
          <small>New requests so far this month.</small>
        </div>
        <div class="icon">
          <i class="ion ion-paper-airplane"></i>
        </div>
        <span class="small-box-footer">Create a new request <i class="fa fa-arrow-circle-right"></i></span>
      </a>
    </div>

    <div class="col-lg-3 col-xs-6">
      <a class="bg-purple-gradient small-box bg-yellow" href="<?= Url::home() ?>?r=business/request">
        <div class="inner">
          <p>ALL MY REQUESTS</p>
          <h3> <?= $countRequest ?> </h3>
          <small>All requests created by me.</small>
        </div>
        <div class="icon">
          <i class="ion ion-navicon-round"></i>
        </div>
        <span class="small-box-footer">Manage all my requests <i class="fa fa-arrow-circle-right"></i></span>
      </a>
    </div>

    <!-- 
    <div class="col-lg-3 col-xs-6">
      <a href="#" class="bg-blue-gradient small-box bg-green">
        <div class="inner">
          <h3>53<sup style="font-size: 20px">%</sup></h3>
          <p>TRACKING</p>
        </div>
        <div class="icon">
          <i class="ion ion-location"></i>
        </div>
        <span class="small-box-footer">Tracking my request <i class="fa fa-arrow-circle-right"></i></span>
      </a>
    </div>

    <div class="col-lg-3 col-xs-6">
      <a class="bg-light-blue-gradient small-box bg-red" href="#">
        <div class="inner">
          <h3>65</h3>
          <p>STATISTICS</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <span class="small-box-footer">Review statistical values <i class="fa fa-arrow-circle-right"></i></span>
      </a>
    </div>
  -->

  </div>

  <br />
  <br />
  <br />
  <br />
  <div class="row">
    <div class="col-md-12">
    </div>
  </div>
</div>