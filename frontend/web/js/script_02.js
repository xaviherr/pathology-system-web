// ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ //

// CREATE
$(function() {
	$(".modal-create-action").click(function() {
		$("#modal-create")
			.modal("show")
			.find(".modal-create-content")
			.load($(this).attr("value"));
		return false;
	});
});

// VIEW
$("#w1").on("draw.dt", function() {
	$(".modal-view-action").click(function() {
		$("#modal-view")
			.modal("show")
			.find(".modal-view-content")
			.load($(this).attr("value"));
		return false;
	});
});

$(function() {
	$(".modal-view-action").click(function() {
		$("#modal-view")
			.modal("show")
			.find(".modal-view-content")
			.load($(this).attr("value"));
		return false;
	});
});

// UPDATE
$("#w1").on("draw.dt", function() {
	$(".modal-update-action").click(function() {
		$("#modal-update")
			.modal("show")
			.find(".modal-update-content")
			.load($(this).attr("value"));
		return false;
	});
});

$(function() {
	$(".modal-update-action").click(function() {
		$("#modal-update")
			.modal("show")
			.find(".modal-update-content")
			.load($(this).attr("value"));
		return false;
	});
});

// ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ CREATE//

// ADD - DataTable
// $("#w0").on("draw.dt", function() {
// 	$(".modal-add-action").click(function() {
// 		$("#modal-add")
// 			.modal("show")
// 			.find(".modal-add-content")
// 			.load($(this).attr("value"));
// 		return false;
// 	});
// });

// REVIEW - DataTable
// $("#w0").on("draw.dt", function() {
// 	$(".modal-review-action").click(function() {
// 		$("#modal-review")
// 			.modal("show")
// 			.find(".modal-review-content")
// 			.load($(this).attr("value"));
// 		return false;
// 	});
// });

// ADD
$(".modal-add-action").click(function() {
	$("#modal-add")
		.modal("show")
		.find(".modal-add-content")
		.load($(this).attr("value"));
	return false;
});

// CONFIG
$(".modal-config-action").click(function() {
	$("#modal-config")
		.modal("show")
		.find(".modal-config-content")
		.load($(this).attr("value"));
	return false;
});

// PAYMENT
$(".modal-payment-action").click(function() {
	$("#modal-payment")
		.modal("show")
		.find(".modal-payment-content")
		.load($(this).attr("value"));
	return false;
});

// SAVE
$(".modal-save-action").click(function() {
	$("#modal-save")
		.modal("show")
		.find(".modal-save-content")
		.load($(this).attr("value"));
	return false;
});

// ------------------------------------------------------------------------ //
// REPORT VIEW 01
$("#w0").on("draw.dt", function() {
	$(".modal-report01-action").click(function() {
		$("#modal-report01")
			.modal("show")
			.find(".modal-report01-content")
			.load($(this).attr("value"));
		return false;
	});
});
// REPORT VIEW 02
$("#w0").on("draw.dt", function() {
	$(".modal-report02-action").click(function() {
		$("#modal-report02")
			.modal("show")
			.find(".modal-report02-content")
			.load($(this).attr("value"));
		return false;
	});
});

// REPORT VIEW 01
$("#w1").on("draw.dt", function() {
	$(".modal-report01-action").click(function() {
		$("#modal-report01")
			.modal("show")
			.find(".modal-report01-content")
			.load($(this).attr("value"));
		return false;
	});
});
// REPORT VIEW 02
$("#w1").on("draw.dt", function() {
	$(".modal-report02-action").click(function() {
		$("#modal-report02")
			.modal("show")
			.find(".modal-report02-content")
			.load($(this).attr("value"));
		return false;
	});
});

// ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ //

/* REQUEST PROCESS */
// VIEW
$("#w5").on("draw.dt", function() {
	$(".modal-view-action").click(function() {
		$("#modal-view")
			.modal("show")
			.find(".modal-view-content")
			.load($(this).attr("value"));
		return false;
	});
});

// UPDATE
$("#w5").on("draw.dt", function() {
	$(".modal-update-action").click(function() {
		$("#modal-update")
			.modal("show")
			.find(".modal-update-content")
			.load($(this).attr("value"));
		return false;
	});
});

// ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ //

// MODAL UPLOAD
$(function() {
	$(".modal-upload-action").click(function() {
		$("#modal-upload")
			.modal("show")
			.find(".modal-upload-content")
			.load($(this).attr("value"));
		return false;
	});
});

// ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ //

// MODAL LOGIN / SIGNUP

$(function() {
	$(".modal-login-action").click(function() {
		$("#modal-login")
			.modal("show")
			.find(".modal-login-content")
			.load($(this).attr("value"));
		return false;
	});
});

$(function() {
	$(".modal-signup-action").click(function() {
		$("#modal-signup")
			.modal("show")
			.find(".modal-signup-content")
			.load($(this).attr("value"));
		return false;
	});
});
